
var target = UIATarget.localTarget();

target.frontMostApp().mainWindow().buttons()[1].tap();
target.frontMostApp().mainWindow().buttons()["Collect Data"].tap();
target.frontMostApp().mainWindow().scrollViews()[0].buttons()["images"].tap();
target.frontMostApp().mainWindow().buttons()["camera"].tap();
target.frontMostApp().mainWindow().buttons()["PhotoCapture"].tap();
target.frontMostApp().mainWindow().buttons()["Use Photo"].tap();
// Alert detected. Expressions for handling alerts should be moved into the UIATarget.onAlert function definition.
target.frontMostApp().alert().defaultButton().tap();

target.frontMostApp().alert().buttons()["No Thanks"].tap();
target.frontMostApp().mainWindow().buttons()["Delete"].tap();
// Alert detected. Expressions for handling alerts should be moved into the UIATarget.onAlert function definition.
target.frontMostApp().alert().defaultButton().tap();

target.frontMostApp().alert().buttons()["Yes"].tap();
target.setDeviceOrientation(UIA_DEVICE_ORIENTATION_PORTRAIT);
target.frontMostApp().mainWindow().images()["background.png"].tapWithOptions({tapOffset:{x:0.04, y:0.04}});
target.frontMostApp().statusBar().tapWithOptions({tapOffset:{x:0.07, y:0.88}});
target.frontMostApp().mainWindow().images()["background.png"].tapWithOptions({tapOffset:{x:0.06, y:0.03}});
