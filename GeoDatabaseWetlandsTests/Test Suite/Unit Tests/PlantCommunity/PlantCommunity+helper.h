//
//  PlantCommunity+helper.h
//  GeoDatabaseWetlands
//
//  Created by Jonathan Mason on 11/27/15.
//  Copyright © 2015 Jonathan Mason. All rights reserved.
//

#import "PlantCommunity.h"

@interface PlantCommunity (helper)

- (void)checkUserInfoSettings;
- (void)getFormID;
- (BOOL)testWifiConnection;

@end
