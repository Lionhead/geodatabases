//
//  PlantCommunityTests.m
//  GeoDatabaseWetlands
//
//  Created by Jonathan Mason on 11/27/15.
//  Copyright © 2015 Jonathan Mason. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "PlantCommunity+helper.h"
#import "UserInfo.h"

@interface PlantCommunityTests : XCTestCase {
    PlantCommunity *plant;
    UserInfo *userInfo;
}

@end

@implementation PlantCommunityTests

- (void)setUp {
    [super setUp];
    userInfo = [UserInfo sharedUserInfo];
    plant = [PlantCommunity new];
    
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

//
- (void)testCheckUserInfoSettings {
    XCTAssertFalse(plant.userInfo.plantWorkingFilename);
    XCTAssertFalse(plant.displayedFilename.text);
    plant.userInfo.plantWorkingFilename = @"test";
    
    [plant checkUserInfoSettings];
    XCTAssertFalse([plant.displayedFilename.text isEqualToString:@""]);
    XCTAssertFalse([plant.userInfo.plantFormID isEqualToString:@""]);
}



- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

@end
