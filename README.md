

* Quick summary:
iOS app used by the Office of Research to collect data from the wetlands. The app is specifically made for the ipad 2. No iphone support.

* Version: 2.1.1


* Summary of set up:
Runs on iOS 9+, Xcode 7+, AFNetworking 2+, and Realm 0.96.3. Majority of code is written in objective-c, with some c code added. A list of mockups and images used in the project are located in Images.xcassets. The folder myIcons has all the working images used in the app. They are dragged from here into the Xcode IDE's folder to install them into the app bundle. AppIcons and LaunchImages are also located in the Images.xcassets folder. The working folder contains artwork used in the app or for mockups. InDesign files can be opened with InDesign CS6 or newer.

* Dependencies:
AudioToolbox, Realm, MessageUI, libc++, QuartzCore, SystemConfiguration, CoreLocation, Foundation, CoreGraphics, UIKit

* Database configuration:
See Realm files located in the UserProvenance/Database folder. For information on how to use Realm visit https://realm.io/docs/objc/latest/

* How to run tests:
Create XCTest unit test cases in Xcode by going to File/New/File.. and select Unit Test Case Class.

* Deployment instructions:
Any ipad you want to install the app on must not have supervisor mode deactivated. Only then can you load the app onto the ipad through Xcode.