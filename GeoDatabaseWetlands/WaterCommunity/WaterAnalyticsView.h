//
//  WaterAnalyticsView.h
//  GeoDatabaseWetlands
//
//  Created by Jonathan Mason on 8/24/15.
//  Copyright (c) 2015 Jonathan Mason. All rights reserved.
//

#import "WeatherBug.h"
#import <UIKit/UIKit.h>
#import <Realm/Realm.h>

@interface WaterAnalyticsView : UIView

@property (strong, nonatomic) UIScrollView *scrollView;
@property (strong, nonatomic) WeatherBug *weatherBug;

- (void)addHeaderInfo:(RLMResults*)array;
- (id)initWithXAmount:(int)amount;
- (void)labelTapped:(UILabel*)label;

@end
