//
//  WaterCommunityMenu.m
//  GeoDatabaseWetlands
//
//  Created by Jonathan Mason on 7/8/15.
//  Copyright (c) 2015 Jonathan Mason. All rights reserved.
//

#import "WaterCommunityMenu.h"
#import "WaterFormManager.h"
#import "WaterFilesTableView.h"
#import "WaterFileViewer.h"
#import "SystemSounds.h"

#define buttonWidth 210
#define buttonHeight 40

@implementation WaterCommunityMenu

UIViewController *vcPtr;
UIColor *shadowColor;
NSArray *buttons;
WaterCommunity *waterPtr;
WaterFormManager *waterFormManager;

// set the properties on initialization
- (id)initWithWaterCommunity:(WaterCommunity*)waterCommPtr {
    self = [super init];
    if (self) {
        UIViewControllerTracker *tracker = [UIViewControllerTracker sharedVC];
        vcPtr = tracker.currentViewController;
        waterFormManager = [WaterFormManager sharedFormManager];
        waterPtr = waterCommPtr;
        [self generateMenuView];
        [self generateButtons];
        [self addToView];
    }
    return self;
}

- (void)generateMenuView {
    self.viewBox = [[UIView alloc]initWithFrame:CGRectMake(529, 110, buttonWidth, 405)];
}

- (void)generateButtons {
    shadowColor = [UIColor colorWithRed:150.0/255.0 green:150.0/255.0 blue:150.0/255.0 alpha:1.0];
    
    UIButton *startOverButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [startOverButton setFrame:CGRectMake(0, 0, buttonWidth, buttonHeight)];
    [startOverButton setTitle:@"Start Over" forState:UIControlStateNormal];
    [startOverButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    startOverButton.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:22];
    [startOverButton.titleLabel setTextAlignment:NSTextAlignmentCenter];
    [startOverButton setBackgroundColor:[UIColor clearColor]];
    [startOverButton setBackgroundImage:[UIImage imageNamed:@"waterCommMenuButtonBackground"]
                             forState:UIControlStateNormal];
    [startOverButton reversesTitleShadowWhenHighlighted];
    [startOverButton addTarget:self action:@selector(hitStartOverButton:)
            forControlEvents:UIControlEventTouchUpInside];
    startOverButton.layer.shadowColor = shadowColor.CGColor;
    startOverButton.layer.shadowOffset = CGSizeMake(2,5);
    startOverButton.layer.shadowOpacity = .75f;
    [self.viewBox addSubview:startOverButton];
    
    UIButton *saveAsButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [saveAsButton setFrame:CGRectMake(0, 45, buttonWidth, buttonHeight)];
    [saveAsButton setTitle:@"Save as" forState:UIControlStateNormal];
    [saveAsButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    saveAsButton.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:22];
    [saveAsButton.titleLabel setTextAlignment:NSTextAlignmentCenter];
    [saveAsButton setBackgroundColor:[UIColor clearColor]];
    [saveAsButton setBackgroundImage:[UIImage imageNamed:@"waterCommMenuButtonBackground"]
                              forState:UIControlStateNormal];
    [saveAsButton addTarget:self action:@selector(hitSaveAsButton:)
             forControlEvents:UIControlEventTouchUpInside];
    saveAsButton.layer.shadowColor = shadowColor.CGColor;
    saveAsButton.layer.shadowOffset = CGSizeMake(2,5);
    saveAsButton.layer.shadowOpacity = .75f;
    [self.viewBox addSubview:saveAsButton];
    
    UIButton *loadDataButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [loadDataButton setFrame:CGRectMake(0, 90, buttonWidth, buttonHeight)];
    [loadDataButton setTitle:@"Load Data" forState:UIControlStateNormal];
    [loadDataButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    loadDataButton.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:22];
    [loadDataButton.titleLabel setTextAlignment:NSTextAlignmentCenter];
    [loadDataButton setBackgroundColor:[UIColor clearColor]];
    [loadDataButton setBackgroundImage:[UIImage imageNamed:@"waterCommMenuButtonBackground"]
                              forState:UIControlStateNormal];
    [loadDataButton addTarget:self action:@selector(hitLoadDataButton:)
             forControlEvents:UIControlEventTouchUpInside];
    loadDataButton.layer.shadowColor = shadowColor.CGColor;
    loadDataButton.layer.shadowOffset = CGSizeMake(2,5);
    loadDataButton.layer.shadowOpacity = .75f;
    [self.viewBox addSubview:loadDataButton];
    
    UIButton *uploadButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [uploadButton setFrame:CGRectMake(0, 135, buttonWidth, buttonHeight)];
    [uploadButton setTitle:@"Upload Data" forState:UIControlStateNormal];
    [uploadButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    uploadButton.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:22];
    [uploadButton.titleLabel setTextAlignment:NSTextAlignmentCenter];
    [uploadButton setBackgroundColor:[UIColor clearColor]];
    [uploadButton setBackgroundImage:[UIImage imageNamed:@"waterCommMenuButtonBackground"]
                              forState:UIControlStateNormal];
    [uploadButton addTarget:self action:@selector(hitUploadDataButton:)
             forControlEvents:UIControlEventTouchUpInside];
    uploadButton.layer.shadowColor = shadowColor.CGColor;
    uploadButton.layer.shadowOffset = CGSizeMake(2,5);
    uploadButton.layer.shadowOpacity = .75f;
    [self.viewBox addSubview:uploadButton];
    
    UIButton *downloadButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [downloadButton setFrame:CGRectMake(0, 180, buttonWidth, buttonHeight)];
    [downloadButton setTitle:@"Download Data" forState:UIControlStateNormal];
    [downloadButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    downloadButton.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:22];
    [downloadButton.titleLabel setTextAlignment:NSTextAlignmentCenter];
    [downloadButton setBackgroundColor:[UIColor clearColor]];
    [downloadButton setBackgroundImage:[UIImage imageNamed:@"waterCommMenuButtonBackground"]
                              forState:UIControlStateNormal];
    [downloadButton addTarget:self action:@selector(hitDownloadButton:)
             forControlEvents:UIControlEventTouchUpInside];
    downloadButton.layer.shadowColor = shadowColor.CGColor;
    downloadButton.layer.shadowOffset = CGSizeMake(2,5);
    downloadButton.layer.shadowOpacity = .75f;
    [self.viewBox addSubview:downloadButton];
    
    UIButton *compareDataButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [compareDataButton setFrame:CGRectMake(0, 225, buttonWidth, buttonHeight)];
    [compareDataButton setTitle:@"Compare Data" forState:UIControlStateNormal];
    [compareDataButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    compareDataButton.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:22];
    [compareDataButton.titleLabel setTextAlignment:NSTextAlignmentCenter];
    [compareDataButton setBackgroundColor:[UIColor clearColor]];
    [compareDataButton setBackgroundImage:[UIImage imageNamed:@"waterCommMenuButtonBackground"]
                                 forState:UIControlStateNormal];
    [compareDataButton addTarget:self action:@selector(hitCompareButton:)
                forControlEvents:UIControlEventTouchUpInside];
    compareDataButton.layer.shadowColor = shadowColor.CGColor;
    compareDataButton.layer.shadowOffset = CGSizeMake(2,5);
    compareDataButton.layer.shadowOpacity = .75f;
    [self.viewBox addSubview:compareDataButton];
    
    UIButton *deleteButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [deleteButton setFrame:CGRectMake(0, 270, buttonWidth, buttonHeight)];
    [deleteButton setTitle:@"Delete Files" forState:UIControlStateNormal];
    [deleteButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    deleteButton.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:22];
    [deleteButton.titleLabel setTextAlignment:NSTextAlignmentCenter];
    [deleteButton setBackgroundColor:[UIColor clearColor]];
    [deleteButton setBackgroundImage:[UIImage imageNamed:@"waterCommMenuButtonBackground"]
                               forState:UIControlStateNormal];
    [deleteButton addTarget:self action:@selector(hitDeleteButton:)
           forControlEvents:UIControlEventTouchUpInside];
    deleteButton.layer.shadowColor = shadowColor.CGColor;
    deleteButton.layer.shadowOffset = CGSizeMake(2,5);
    deleteButton.layer.shadowOpacity = .75f;
    [self.viewBox addSubview:deleteButton];
    
    UIButton *createSpreadsheetButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [createSpreadsheetButton setFrame:CGRectMake(0, 315, buttonWidth, buttonHeight)];
    [createSpreadsheetButton setTitle:@"Create Spreadsheet" forState:UIControlStateNormal];
    [createSpreadsheetButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    createSpreadsheetButton.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:22];
    [createSpreadsheetButton.titleLabel setTextAlignment:NSTextAlignmentCenter];
    [createSpreadsheetButton setBackgroundColor:[UIColor clearColor]];
    [createSpreadsheetButton setBackgroundImage:[UIImage imageNamed:@"waterCommMenuButtonBackground"]
                                forState:UIControlStateNormal];
    [createSpreadsheetButton addTarget:self action:@selector(hitCreateSpreadsheetButton:)
               forControlEvents:UIControlEventTouchUpInside];
    createSpreadsheetButton.layer.shadowColor = shadowColor.CGColor;
    createSpreadsheetButton.layer.shadowOffset = CGSizeMake(2,5);
    createSpreadsheetButton.layer.shadowOpacity = .75f;
    [self.viewBox addSubview:createSpreadsheetButton];
   
    UIButton *viewPhotosButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [viewPhotosButton setFrame:CGRectMake(0, 360, buttonWidth, buttonHeight)];
    [viewPhotosButton setTitle:@"View Photos" forState:UIControlStateNormal];
    [viewPhotosButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    viewPhotosButton.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:22];
    [viewPhotosButton.titleLabel setTextAlignment:NSTextAlignmentCenter];
    [viewPhotosButton setBackgroundColor:[UIColor clearColor]];
    [viewPhotosButton setBackgroundImage:[UIImage imageNamed:@"waterCommMenuButtonBackground"]
                               forState:UIControlStateNormal];
    [viewPhotosButton addTarget:self action:@selector(hitViewPhotosButton:)
               forControlEvents:UIControlEventTouchUpInside];
    viewPhotosButton.layer.shadowColor = shadowColor.CGColor;
    viewPhotosButton.layer.shadowOffset = CGSizeMake(2,5);
    viewPhotosButton.layer.shadowOpacity = .75f;
    [self.viewBox addSubview:viewPhotosButton];
    
    buttons = [NSArray arrayWithObjects:startOverButton,saveAsButton,loadDataButton,uploadButton,
               downloadButton,compareDataButton,deleteButton,createSpreadsheetButton,viewPhotosButton,nil];
}

- (CALayer*)addMaskToButton:(UIButton*)button {
    UIBezierPath *maskPath= [UIBezierPath bezierPathWithRoundedRect:button.bounds
                                                       cornerRadius:8];
    CAShapeLayer *maskLayer = [CAShapeLayer layer];
    maskLayer.frame = button.bounds;
    maskLayer.path = maskPath.CGPath;
    return maskLayer;
}

- (void)addToView{
    [vcPtr.view addSubview:self.viewBox];
}

- (void)hitStartOverButton:(id)sender {
    [SystemSounds playTockSound];
    if (waterFormManager.arrayOfForms.count > 0) {
        [waterPtr popupWarningToDeleteAllForms];
    }
}

- (void)hitSaveAsButton:(id)sender {
    [SystemSounds playTockSound];
    [self animateButtons:0 withSelected:sender];
    if ([waterPtr checkSiteSelections] && [waterPtr checkValidValues]) {
        [waterPtr popupToGetFilenameForSavingForm];
    }
    else {
        [waterPtr popupToVerifyNumeric];
    }
}

- (void)hitLoadDataButton:(id)sender {
    [SystemSounds playTockSound];
    [self animateButtons:0 withSelected:sender];
    UserInfo *userInfo = [UserInfo sharedUserInfo];
    [userInfo setClickedLoadData:YES];
    [self showFileViewer];
}

- (void)hitUploadDataButton:(id)sender {
    [SystemSounds playTockSound];
    [self animateButtons:0 withSelected:sender];
    [waterPtr prepareToUploadData];
}

- (void)hitDownloadButton:(id)sender {
    [SystemSounds playTockSound];
    //[self animateButtons:0 withSelected:sender];
    [self showInProgressAlert];
}

- (void)hitCompareButton:(id)sender {
    [SystemSounds playTockSound];
    [self animateButtons:0 withSelected:sender];
    WaterFilesTableView *fileTable = [WaterFilesTableView new];
    fileTable.userInfo = waterPtr.userInfo;
    [waterPtr presentViewController:fileTable animated:YES completion:nil];

    //[self showInProgressAlert];
}

- (void)hitDeleteButton:(id)sender {
    [SystemSounds playTockSound];
    [self animateButtons:0 withSelected:sender];
    waterPtr.userInfo.clickedDeleteFile = YES;
    [self showFileViewer];
}

- (void)hitCreateSpreadsheetButton:(id)sender {
    [SystemSounds playTockSound];
    [self animateButtons:0 withSelected:sender];
    [waterPtr createSpreadsheet];
}

- (void)hitViewPhotosButton:(id)sender {
    [SystemSounds playTockSound];
    [self animateButtons:0 withSelected:sender];
    double delayInSeconds = 0.3;
    dispatch_time_t endTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(endTime, dispatch_get_main_queue(), ^{
        [waterPtr performSegueWithIdentifier:@"waterToPhotoViewerSegue" sender:waterPtr];
    });
}

- (void)showInProgressAlert {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Build in Progress"
                                        message:nil preferredStyle:UIAlertControllerStyleAlert];
    [vcPtr presentViewController:alert animated:YES completion:nil];
    
    double delayInSeconds = 1.0;
    dispatch_time_t endTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(endTime, dispatch_get_main_queue(), ^{
        [alert dismissViewControllerAnimated:YES completion:nil];
    });
}

// animates all the buttons disappearance with the sender disappearing last
- (void)animateButtons:(int)index withSelected:(id)sender {
    if (!buttons || buttons.count == 0) {
        NSLog(@"Error WaterCommunityMenu [animateButtons:(int)index withSelected:(id)sender]");
        return;
    }
    UIButton *button;
    
    if (index == buttons.count) {
        button = sender;
        [UIView animateWithDuration:0.3 animations:^{
            [button setAlpha:0.00];
        } completion:^(BOOL finished){
            [self.viewBox setHidden:YES];
            [self resetButtonsAlpha];
        }];
    }
    else{
        button = buttons[index];
        index++;
        if (![button isEqual:sender]) {
            [UIView animateWithDuration:0.02 animations:^{
                [button setAlpha:0.0];
            } completion:^(BOOL finished) {
                [self animateButtons:index withSelected:sender];
            }];
        }
        else
            [self animateButtons:index withSelected:sender];
    }
}

- (void)resetButtonsAlpha {
    UIButton *button;
    for (id object in buttons) {
        button = object;
        [button setAlpha:1.0];
    }
}

- (void)showFileViewer {
    CGRect rect = CGRectMake(([UIScreen mainScreen].bounds.size.width *.2)/2,
                             ([UIScreen mainScreen].bounds.size.height *.2)/2,
                             [UIScreen mainScreen].bounds.size.width - ([UIScreen mainScreen].bounds.size.width *.2),
                             [UIScreen mainScreen].bounds.size.height - ([UIScreen mainScreen].bounds.size.height *.2));

    WaterFileViewer *fileViewer = [[WaterFileViewer alloc]initWithFrame:rect andWaterComm:waterPtr];
    [waterPtr.view addSubview:fileViewer];
}

@end
