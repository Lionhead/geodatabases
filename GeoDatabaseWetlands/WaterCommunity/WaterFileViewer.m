//
//  WaterFileViewer.m
//  GeoDatabaseWetlands
//
//  Created by Jonathan Mason on 10/20/15.
//  Copyright © 2015 Jonathan Mason. All rights reserved.
//

#import "WaterFileViewer.h"
#import "UserInfo.h"
#import "FileTableCell.h"
#import "WaterFileViewTableCell.h"
#import "WaterCommunityProvenance.h"
#import "SystemSounds.h"


#define closeButtonWidth 100
#define closeButtonHeight 25

@interface WaterFileViewer() <UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) UITableView *theTableView;
//@property (strong, nonatomic) RLMResults *selectedData;
@property RLMArray<WaterCommunityProvenance *><WaterCommunityProvenance> *selectedData;

@end

@implementation WaterFileViewer

RLMResults *data;
UserInfo *userInfo;
WaterCommunity *waterCommPtr;

- (id)initWithFrame:(CGRect)frame andWaterComm:(WaterCommunity*)water {
    if (self = [super initWithFrame:frame]) {
        self.frame = frame;
        _selectedData = [[RLMArray alloc]initWithObjectClassName:@"WaterCommunityProvenance"];
        waterCommPtr = water;
        userInfo = [UserInfo sharedUserInfo];
        [self queryData];
        [self configureTableView];
    }
    return self;
}

- (void)queryData {
    data = [[WaterCommunityProvenance objectsWhere:[NSString stringWithFormat:@"username = '%@'",userInfo.username]]
            sortedResultsUsingProperty:@"dateStarted" ascending:NO];
}

- (void)configureTableView {
    CGRect tableRect = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height-(closeButtonHeight+10));
    
    self.theTableView = [[UITableView alloc]initWithFrame:tableRect];
    self.theTableView.dataSource = self;
    self.theTableView.delegate = self;
    self.theTableView.layer.opacity = 0.95f;
    [self.theTableView setContentSize:CGSizeMake(self.theTableView.bounds.size.width, 1500)];
    [self.theTableView setAllowsMultipleSelection:YES];
    [self.theTableView setBackgroundColor:[UIColor whiteColor]];
    
    UIView *myMask = [[UIView alloc]initWithFrame:
                      CGRectMake(0, 0, self.theTableView.bounds.size.width, self.theTableView.bounds.size.height)];
    [myMask setClipsToBounds:YES];
    UIBezierPath *tableViewMaskPath = [UIBezierPath bezierPathWithRoundedRect:self.theTableView.bounds
                                                                 cornerRadius:10];
    CAShapeLayer *tableViewMaskLayer = [CAShapeLayer layer];
    tableViewMaskLayer.frame = myMask.bounds;
    tableViewMaskLayer.path = tableViewMaskPath.CGPath;
    [myMask.layer setMask:tableViewMaskLayer];
    [myMask addSubview:self.theTableView];
    [self addSubview:myMask];
    
    UIButton *closeButton = [[UIButton alloc]initWithFrame:
                             CGRectMake((self.frame.size.width/2)-(closeButtonWidth/2),
                                        self.frame.size.height-closeButtonHeight,
                                        closeButtonWidth,
                                        closeButtonHeight)];
    [closeButton setTitle:@"Done" forState:UIControlStateNormal];
    [closeButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [closeButton.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue" size:22]];
    [closeButton setShowsTouchWhenHighlighted:YES];
    [closeButton addTarget:self
                    action:@selector(hitClose:)
          forControlEvents:UIControlEventTouchUpInside];
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:closeButton.bounds
                                                        cornerRadius:8];
    CAShapeLayer *maskLayer = [CAShapeLayer layer];
    maskLayer.frame = closeButton.bounds;
    maskLayer.path = maskPath.CGPath;
    [closeButton.layer setMask:maskLayer];
    [closeButton setBackgroundColor:[UIColor whiteColor]];
    [self addSubview:closeButton];
    
}

- (void)hitClose:(id)sender {
    [SystemSounds playTockSound];
    [self setHidden:YES];
    if (userInfo.clickedDeleteFile) { // delete data
        [userInfo setClickedDeleteFile:NO];
        for (id object in self.selectedData) {
            WaterCommunityProvenance *wcp = object;
            [self deleteRecordFromDatabase:wcp];
        }
    }
    else{ // load data
        if (self.selectedData.count > 0) {
            [waterCommPtr loadDataFromDatabase:self.selectedData];
        }
    }
}

- (void)deleteRecordFromDatabase:(WaterCommunityProvenance*)wcp {
    RLMRealm *realm = [RLMRealm defaultRealm];
    [realm beginWriteTransaction];
    [realm deleteObject:wcp];
    [realm commitWriteTransaction];
}

- (NSIndexPath *)tableView:(UITableView *)tableView
  willSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    return indexPath;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (data[indexPath.row]) {
        [self.selectedData addObject:data[indexPath.row]];
    }
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    WaterCommunityProvenance *wcpToRemove = data[indexPath.row];
    for (int i = 0; i < self.selectedData.count; i++) {
        WaterCommunityProvenance *wcp = self.selectedData[i];
        if ([wcp.filename isEqualToString:wcpToRemove.filename] && [wcp.siteID isEqualToString:wcpToRemove.siteID]) {
            [self.selectedData removeObjectAtIndex:i];
        }
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [data count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    WaterFileViewTableCell *cell = (WaterFileViewTableCell*)[tableView
                                                             dequeueReusableCellWithIdentifier:@"WaterFileViewTableCell"];
    
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"WaterFileViewTableCell"
                                                     owner:self
                                                   options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    // Configure the cell...
    int idx = 0;
    for (id object in data) {
        if (indexPath.row == idx) {
            if ([object isKindOfClass:[WaterCommunityProvenance class]]) {
                WaterCommunityProvenance *wcp = object;
                cell.filename.text = [NSString stringWithFormat:@"%@: %@",wcp.filename,wcp.siteID];
                cell.date.text = [NSString stringWithFormat:@"%@ at %@",
                                  wcp.dateStarted,wcp.timeStarted];
            }
        }
        idx++;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell
forRowAtIndexPath:(NSIndexPath *)indexPath {
}

- (RLMResults*)returnData:(RLMResults*)data{
    return data;
}

- (void)confirmDeleteRecord:(WaterCommunityProvenance*)record withName:(NSString*)filename {
    UIViewControllerTracker *vcTracker = [UIViewControllerTracker sharedVC];
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:
                                [NSString stringWithFormat:@"Delete %@?",filename]
                                                                   message:nil
                                                            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel"
                                                           style:UIAlertActionStyleCancel
                                                         handler:nil];
    [alert addAction:cancelAction];
    UIAlertAction *yesAction = [UIAlertAction actionWithTitle:@"Yes"
                                                        style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction *action)
                                {
                                    RLMRealm *realm = [RLMRealm defaultRealm];
                                    [realm beginWriteTransaction];
                                    [realm deleteObject:record];
                                    [realm commitWriteTransaction];
                                    [self.theTableView reloadData];
                                }];
    [alert addAction:yesAction];
    [vcTracker.currentViewController presentViewController:alert animated:YES completion:nil];
}


/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */

@end