//
//  WaterCommunity.h
//  GeoDatabaseWetlands
//
//  Created by Jonathan Mason on 5/6/15.
//  Copyright (c) 2015 Jonathan Mason. All rights reserved.
//
//  This software is intended soley for the University of Washington Bothell.

#import <UIKit/UIKit.h>
#import "CSVformatting.h"
#import "Reachability.h"
#import "UserLogin.h"
#import "UserInfo.h"
#import "UIViewControllerTracker.h"
#import "WaterFormManager.h"
#import "WaterCommunityMenu.h"
#import "WaterCommunityProvenance.h"

// See comment at top of file for a complete description
@interface WaterCommunity : UIViewController

@property BOOL loadDataComparator;
@property (strong, nonatomic) UserLogin *login;
@property (strong, nonatomic) UserInfo *userInfo;
@property (strong, nonatomic) WaterFormManager *waterFormManager;
@property (strong, nonatomic) WaterCommunityMenu *dropDownMenu;
@property (strong, nonatomic) Reachability *internetReachable;

@property (strong, nonatomic) UIView *formContainer;
@property (strong, nonatomic) IBOutlet UIImageView *backgroundView;
@property (strong, nonatomic) IBOutlet UILabel *displayedFilename;
@property (strong, nonatomic) IBOutlet UILabel *loggedInAs;
@property (strong, nonatomic) IBOutlet UIImageView *formContainerBackground;
@property (strong, nonatomic) IBOutlet UIScrollView *mainScrollView;
@property (strong, nonatomic) IBOutlet UIView *formView; // contains the scrollView
@property (strong, nonatomic) IBOutlet UIScrollView *formScrollView; // holds the forms

- (IBAction)hitDropDownMenu:(id)sender;
- (IBAction)swipeToSegueHome:(id)sender;
- (void)adjustFormScrollerSize;
- (void)popupToGetFilenameForSavingForm;
- (void)popupToVerifyNumeric;
- (BOOL)checkSiteSelections;
- (void)loadDataComparatorWithData:(NSMutableArray*)array;
- (void)loadDataFromDatabase:(RLMArray*)data;
- (void)prepareToUploadData;
- (void)createSpreadsheet;
- (void)popupWarningToDeleteAllForms;
- (BOOL)checkValidValues;
- (BOOL)isNumeric:(NSString*)stringValue;

@end
