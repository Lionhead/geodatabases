//
//  WaterCommunityForm.h
//  GeoDatabaseWetlands
//
//  Created by Jonathan Mason on 7/8/15.
//  Copyright (c) 2015 Jonathan Mason. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import "WaterFormManager.h"
#import "WaterFormSiteMenu.h"
@class WaterFormManager;
@class WaterFormSiteMenu;

// See comment at top of file for a complete description
@interface WaterCommunityForm : NSObject <CLLocationManagerDelegate>

@property (strong, nonatomic) WaterFormSiteMenu *formSiteMenu;
@property (strong, nonatomic) WaterFormManager *formManagerPtr;
@property (strong, nonatomic) NSDictionary *sitesForKeys;

@property (strong, nonatomic) UIView *formView;
@property (strong, nonatomic) NSString *uuid;
@property (strong, nonatomic) UILabel *filename;
@property (strong, nonatomic) UIButton *deleteButton;
@property (strong, nonatomic) UILabel *displayedTime;
@property (strong, nonatomic) NSString *timeStarted;
@property (strong, nonatomic) NSString *dateStarted;
@property (strong, nonatomic) NSString *siteID;
@property (strong, nonatomic) UIButton *siteButton;
@property (strong, nonatomic) UIButton *latitude;
@property (strong, nonatomic) UIButton *longitude;
@property (strong, nonatomic) NSString *latitudeDecDegrees;
@property (strong, nonatomic) NSString *latitudeDecDegreesMin;
@property (strong, nonatomic) NSString *latitudeDecDegreesMinSec;
@property (strong, nonatomic) NSString *longitudeDecDegrees;
@property (strong, nonatomic) NSString *longitudeDecDegreesMin;
@property (strong, nonatomic) NSString *longitudeDecDegreesMinSec;
@property (strong, nonatomic) UIButton *unitOfMeasure;
@property (strong, nonatomic) UITextField *temp;
@property (strong, nonatomic) UITextField *suna;
@property (strong, nonatomic) UITextField *depth;
@property (strong, nonatomic) UITextField *conductivity;
@property (strong, nonatomic) UITextField *chlorophyll;
@property (strong, nonatomic) UITextField *pH;
@property (strong, nonatomic) UITextField *waterFlowVelocity;
@property (strong, nonatomic) UITextField *tideStage;
@property (strong, nonatomic) UITextField *salinity;
@property (strong, nonatomic) UITextField *dissolvedOxygenSat;
@property (strong, nonatomic) UITextField *dissolvedOxygenMg;
@property (strong, nonatomic) UITextField *secchiDiskExtDepth;
@property (strong, nonatomic) UITextField *meanTurbidity;
@property (strong, nonatomic) UITextField *blueGreenAlgae;
@property (strong, nonatomic) UITextField *waterBodyType;
@property (strong, nonatomic) UIButton *labAnalysis;
@property (strong, nonatomic) UITextView *notes;
@property (strong, nonatomic) NSString *weatherConditions;
@property (strong, nonatomic) NSArray *indexOfProperties;

- (id)initAtYpos:(CGFloat)y withManager:(WaterFormManager*)manager;
- (void)removeForm;
- (void)updateButtonSiteInfo:(NSString*)site forForm:(WaterCommunityForm*)form;
- (void)updateCustomButtonSiteInfo:(NSString*)customSite forForm:(WaterCommunityForm*)form;

@end
