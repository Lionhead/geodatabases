//
//  WaterFormSiteMenu.h
//  GeoDatabaseWetlands
//
//  Created by Jonathan Mason on 7/14/15.
//  Copyright (c) 2015 Jonathan Mason. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WaterCommunityForm.h"
@class WaterCommunityForm;

// See comment at top of file for a complete description
@interface WaterFormSiteMenu : NSObject

@property (strong, nonatomic) WaterCommunityForm *waterForm;
@property (strong, nonatomic) UIImageView *menuView;

- (id)initWithView:(WaterCommunityForm*)view;
- (void)selectionMade:(id)sender;

@end
