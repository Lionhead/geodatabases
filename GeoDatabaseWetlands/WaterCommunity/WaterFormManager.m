//
//  WaterFormManager.m
//  GeoDatabaseWetlands
//
//  Created by Jonathan Mason on 7/8/15.
//  Copyright (c) 2015 Jonathan Mason. All rights reserved.
//

#import "WaterFormManager.h"
#import "TimeUnits.h"
#import "IDGenerator.h"
#define formWidth 704
#define formHeight 552

@implementation WaterFormManager

+ (id)sharedFormManager {
    static dispatch_once_t predicate = 0;
    __strong static id sharedOject = nil;
    
    dispatch_once(&predicate, ^{
        sharedOject = [[self alloc]init];
    });
    return sharedOject;
}

- (id)init{
    if (self = [super init]) {
        _arrayOfForms = [NSMutableArray new];
    }
    return self;
}

// reads the site of the form deleted and checks to see if it affects how any other present site
// number is altered. e.g. deleting 'RR1' should make 'RR2' = 'RR1'.
- (void)updateFormSite:(NSString*)site {
    NSRange range = NSMakeRange(2, site.length-2);
    int siteNum = [[NSString stringWithFormat:@"%@",[site substringWithRange:range]]intValue];
    if (siteNum == 0 && ![site isEqualToString:@"SEL"]) {
        NSLog(@"Error: [ViewManager updateFormSite]");
    }
    else{
        NSRange nextRange = NSMakeRange(0, 2);
        NSString *nextSite = [NSString stringWithFormat:@"%@%d",[site substringWithRange:nextRange],siteNum+1];
        for (id object in self.arrayOfForms) {
            WaterCommunityForm *form = object;
            if ([form.siteButton.titleLabel.text isEqualToString:nextSite]) {
                [form.siteButton setTitle:site forState:UIControlStateNormal];
                [self updateFormSite:nextSite];
                break;
            }
        }
    }
}

- (void)createNewForm {
    WaterCommunityForm *form = [[WaterCommunityForm alloc]
                initAtYpos:(self.arrayOfForms.count * formHeight) withManager:self];
    form.uuid = [IDGenerator getUUID];
    [self.waterPtr.formScrollView addSubview:form.formView];
    [self animateAdditionOfNewForm:form];
    [self.arrayOfForms addObject:form];
    [self.waterPtr adjustFormScrollerSize];
    // add form to View
}

- (void)createNewFormWithData:(WaterCommunityProvenance*)data {
    WaterCommunityForm *form = [[WaterCommunityForm alloc]
                                initAtYpos:(self.arrayOfForms.count * formHeight) withManager:self];
    [self copyData:data ToForm:form];
    [self.waterPtr.formScrollView addSubview:form.formView];
    [self animateAdditionOfNewForm:form];
    [self.arrayOfForms addObject:form];
    [self.waterPtr adjustFormScrollerSize];
}

// assigns the queried values to the form properties
- (void)copyData:(WaterCommunityProvenance*)data ToForm:(WaterCommunityForm*)form {
    UserInfo *userInfo = [UserInfo sharedUserInfo];
    userInfo.waterWorkingFilename = data.filename;
    form.filename.text = data.filename;
    form.siteID = data.siteID;
    if ([form.sitesForKeys valueForKey:data.siteID]) {
        [form.siteButton setTitle:[form.sitesForKeys valueForKey:data.siteID]
                         forState:UIControlStateNormal];
    }
    else {
        [form updateCustomButtonSiteInfo:data.siteID forForm:form];
    }
    
    form.dateStarted = data.dateStarted;
    form.timeStarted = data.timeStarted;
    form.latitudeDecDegrees = data.latitudeDecDegrees;
    form.latitudeDecDegreesMin = data.latitudeDecDegreesMin;
    form.latitudeDecDegreesMinSec = data.latitudeDecDegreesMinSec;
    form.longitudeDecDegrees = data.longitudeDecDegrees;
    form.longitudeDecDegreesMin = data.longitudeDecDegreesMin;
    form.longitudeDecDegreesMinSec = data.longitudeDecDegreesMinSec;
    form.temp.text = data.waterTemp;
    form.suna.text = data.suna;
    form.depth.text = data.depth;
    form.conductivity.text = data.conductivity;
    form.chlorophyll.text = data.chlorophyll;
    form.pH.text = data.pH;
    form.waterFlowVelocity.text = data.waterFlowVelocity;
    form.tideStage.text = data.tideStage;
    form.salinity.text = data.salinity;
    form.dissolvedOxygenSat.text = data.dissolvedOxygenSat;
    form.dissolvedOxygenMg.text = data.dissolvedOxygenMg;
    form.secchiDiskExtDepth.text = data.secchiDiskExtDepth;
    form.meanTurbidity.text = data.meanTurbidity;
    form.blueGreenAlgae.text = data.blueGreenAlgae;
    form.waterBodyType.text = data.waterBodyType;
    form.notes.text = data.notes;
    [self setSiteID:form];
    [self setGPS:form];
}

- (void)setSiteID:(WaterCommunityForm*)form {
    if ([form.siteButton.titleLabel.text isEqualToString:@""]) {
        form.siteButton.titleLabel.text = @"SEL";
    }
}

- (void)setGPS:(WaterCommunityForm*)form {
    if (![form.latitudeDecDegrees isEqualToString:@""]) {
        [form.latitude setTitle:form.latitudeDecDegrees forState:UIControlStateNormal];
    }
    if (![form.longitudeDecDegrees isEqualToString:@""]) {
        [form.longitude setTitle:form.longitudeDecDegrees forState:UIControlStateNormal];
    }
}

- (void)animateAdditionOfNewForm:(WaterCommunityForm*)form {
    [form.formView setAlpha:0.0];
    [UIView animateWithDuration:0.75 animations:^{
        [form.formView setAlpha:1.0];
    }];
}

// removes form from the view controller, does not delete anything saved to database
- (void)deleteForm:(id)sender {
    UISwipeGestureRecognizer *swipe = sender;
    UIAlertController *deleteAlert = [UIAlertController alertControllerWithTitle:@"Warning"
                                    message:@"Are you sure you want to remove this form?"
                                    preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel"
                                    style:UIAlertActionStyleCancel handler:nil];
    [deleteAlert addAction:cancel];
    UIAlertAction *yesDelete = [UIAlertAction actionWithTitle:@"Yes"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction *action) {
        WaterCommunityForm *form;
        
        for (id object in self.arrayOfForms) {
            form = object;
            if ([form.formView isEqual:swipe.view]) {
                [self.arrayOfForms removeObject:object];
                [self updateFormSite:form.siteButton.titleLabel.text];
                [form removeForm];
                [self.waterPtr adjustFormScrollerSize];
                [self refreshForms];
                break;
            }
        }
    }];
    [deleteAlert addAction:yesDelete];
    [self.waterPtr presentViewController:deleteAlert animated:YES completion:nil];
}

/******************************************************************************
 * refreshForms
 *
 * This refreshes the placement of the forms when going by order of them in the
 * array. This is called after a change is made to the array, such as deletion.
 ******************************************************************************/
- (void)refreshForms {
    // redraw forms from
    WaterCommunity *form;
    for (int i = 0; i < self.arrayOfForms.count; i++){
        form = self.arrayOfForms[i];
        [UIView animateWithDuration:0.5 animations:^{
            [form.formView setFrame:CGRectMake(0, i*formHeight, formWidth, formHeight)];
        }];
    }
}

/******************************************************************************
 * reloadForms
 *
 * This methods reloads all forms onto the screen once the viewLoads.
 ******************************************************************************/
- (void)reloadForms {
    // redraw forms from
    WaterCommunity *form;
    [self.waterPtr adjustFormScrollerSize];
    for (int i = 0; i < self.arrayOfForms.count; i++){
        form = self.arrayOfForms[i];
        [form.formView setFrame:CGRectMake(0, i*formHeight, formWidth, formHeight)];
        [self.waterPtr.formScrollView addSubview:form.formView];
    }
}

// returns 1 counting the selection just made
- (int)returnCountForSite:(NSString*)site forForm:(WaterCommunityForm*)form {
    int count = 1;
    WaterCommunityForm *formPtr;
    for (id object in self.arrayOfForms) {
        formPtr = object;
        if (![formPtr isEqual:form]) {
            NSString *test = [formPtr.siteButton.titleLabel.text substringToIndex:2];
            if ([test isEqualToString:site]) {
                count++;
            }
        }
    }
    return count;
}

// removes all forms from the view and empties the container
- (void)reset {
    WaterCommunityForm *formPtr;
    for (id object in self.arrayOfForms) {
        formPtr = object;
        [formPtr.formView removeFromSuperview];
        formPtr.formSiteMenu = nil;
        formPtr.formManagerPtr = nil;
    }
    [self.arrayOfForms removeAllObjects];
}

@end
