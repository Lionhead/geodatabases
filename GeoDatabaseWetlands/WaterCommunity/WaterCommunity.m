//
//  WaterCommunity.m
//  GeoDatabaseWetlands
//
//  Created by Jonathan Mason on 5/6/15.
//  Copyright (c) 2015 Jonathan Mason. All rights reserved.
//

#import "IDGenerator.h"
#import "WaterCommunity.h"
#import "HomeScreen.h"
#import "WaterCommunityMenu.h"
#import "PhotoViewer.h"
#import "Reachability.h"
#import "CSVformatting.h"
#import "DirectUploadForm.h"
#import "UploadData.h"
#import "TimeUnits.h"
#import "LocalFileManager.h"
#import "TableFileViewer.h"
#import "WaterAnalyticsBuilder.h"
#import "ConnectionWatcher.h"
#import "MailAttachment.h"
#import <Realm/Realm.h>
#import <AudioToolbox/AudioServices.h>
#import "SystemSounds.h"

#define waterFormsKey @"waterForms"
#define csvFolder @"waterCommunityCSV"
#define WaterCommunityFolder @"WaterCommunity"
#define ipadPortraitWidth 768
#define ipadPortraitHeight 1024
#define formContainerWidth 704
#define formContainerHeight 862
#define formWidth 704
#define formHeight 552
#define ipadPortraitKeyboardHeight 303
#define waterType @"water"

@interface WaterCommunity()

@property (strong, nonatomic) UIViewControllerTracker *vcTracker;

@end

@implementation WaterCommunity {
   CSVformatting *csvFileFormatter;
}

- (void)viewWillAppear:(BOOL)animated {
    self.vcTracker = [UIViewControllerTracker sharedVC];
    self.vcTracker.currentViewController = self;
}

- (void)loadDataComparatorWithData:(NSMutableArray*)array {
    WaterAnalyticsBuilder *analyticsBuilder = [[WaterAnalyticsBuilder alloc]initWithData:array];
    if ([analyticsBuilder returnBuildSuccess]) {
        [self.view addSubview:[analyticsBuilder returnAnalyticsView]];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.userInfo = [UserInfo sharedUserInfo];
    self.vcTracker = [UIViewControllerTracker sharedVC];
    self.vcTracker.currentViewController = self;
    self.waterFormManager = [WaterFormManager sharedFormManager];
    self.waterFormManager.waterPtr = self;
    
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        dispatch_async(dispatch_get_main_queue(), ^{
            self.loggedInAs.text = [NSString stringWithFormat:@"Logged in as: %@",self.userInfo.username];
            self.loggedInAs.textColor = [UIColor lightGrayColor];
        });
    });
    
    [self.formScrollView setUserInteractionEnabled:YES];
    [self configureNotifications];
    
    self.dropDownMenu = [[WaterCommunityMenu alloc]initWithWaterCommunity:self];
    [self prepDropDownMenuOnLoad];
    [self prepNavigationControllerOnLoad];
    [self setupFormContainer];
    [self setupBackButton];
    [self configureSaveDataButton];
    [self configureAddFormButton];
    
    if (self.waterFormManager) {
        [self.waterFormManager reloadForms];
    }
    if ([self.userInfo.waterFormID isEqualToString:@""]) {
        self.userInfo.waterFormID = [IDGenerator getFormID];
    }
}

- (void)configureSaveDataButton {
    UIButton *saveDataButton = [UIButton buttonWithType:UIButtonTypeSystem];
    [saveDataButton setFrame:CGRectMake(579, 97, 150, 30)];
    [saveDataButton setBackgroundImage:[UIImage imageNamed:@"waterCommSaveDataButton.png"]
                             forState:UIControlStateNormal];
    [saveDataButton addTarget:self action:@selector(prepareToSaveData:)
            forControlEvents:UIControlEventTouchUpInside];
    [saveDataButton setShowsTouchWhenHighlighted:YES];
    [saveDataButton setReversesTitleShadowWhenHighlighted:YES];
    [self.mainScrollView addSubview:saveDataButton];
}

- (void)configureAddFormButton {
    UIButton *addFormButton = [UIButton buttonWithType:UIButtonTypeSystem];
    [addFormButton setFrame:CGRectMake(40, 97, 150, 30)];
    [addFormButton setBackgroundImage:[UIImage imageNamed:@"waterCommAddFormButton.png"]
                             forState:UIControlStateNormal];
    [addFormButton addTarget:self action:@selector(addForm)
            forControlEvents:UIControlEventTouchUpInside];
    [addFormButton setShowsTouchWhenHighlighted:YES];
    [addFormButton setReversesTitleShadowWhenHighlighted:YES];
    [self.mainScrollView addSubview:addFormButton];
}

/***************************************************************************************************
 * prepNavigationControllerOnLoad
 *
 * This method sets the navigation controller properties for this class.
 **************************************************************************************************/
-(void)prepNavigationControllerOnLoad {
    self.navigationController.navigationBarHidden = YES;
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.view.backgroundColor = [UIColor clearColor];
}

// segue back to Home Page when hitting back arrow button
- (IBAction)hitBackButton:(id)sender {
    [SystemSounds playTockSound];
    HomeScreen *homeController = (HomeScreen*)
        [self.navigationController.viewControllers objectAtIndex:0];
    homeController.waterFormManager = self.waterFormManager;
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)setupFormContainer{
    self.formContainer = [[UIView alloc]initWithFrame:
                      CGRectMake(3, 80, formContainerWidth, formContainerHeight)];
    [self.formContainerBackground addSubview:self.formContainer];
}

/***************************************************************************************************
 * setupBackButton
 *
 * This method creates a button on top of where the navigation bar would be
 * so that the user can cancel and segue back to the Home Page.
 **************************************************************************************************/
-(void)setupBackButton {
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeSystem];
    backButton.titleLabel.text = @"backButton";
    [backButton setFrame:CGRectMake(43, 58, 20, 35)];
    [backButton setBackgroundImage:[UIImage imageNamed:@"leftArrow"] forState:0];
    backButton.titleLabel.font = [UIFont systemFontOfSize:18];
    backButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [backButton setShowsTouchWhenHighlighted:YES];
    [backButton addTarget:self action:@selector(hitBackButton:)
         forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backButton];
}

- (void)addForm {
    [SystemSounds playTockSound];
    [self.waterFormManager createNewForm];
}

- (void)updateFormWeatherConditions {
    WaterCommunityForm *form;
    for (id object in self.waterFormManager.arrayOfForms) {
        form = object;
        if (!self.userInfo.weatherConditions) {
            form.weatherConditions = @"";
        }
        else
            form.weatherConditions = self.userInfo.weatherConditions;
    }
}

- (void)prepareToSaveData:(id)sender {
    /*  SAVING
     Case 1: No filenames
        1) popup to get filename, save to database
     Case 2: One file and has filename
        1) overwrite current database object
     Case 3: Multiple files, but only one filename
        1) overwrite one with filename and add same name to new
     Case 4: Multiple files and filenames
        1) ask to choose filename for all
     */
    [self updateFormWeatherConditions];
    
    if (![self checkValidValues]) {
        [self popupToVerifyNumeric];
        return;
    }
    
    NSString *current; // current filename being checked
    NSMutableArray *existingFilenames = [NSMutableArray new];
    for (int i = 0; i < self.waterFormManager.arrayOfForms.count; i++) {
        WaterCommunityForm *form = self.waterFormManager.arrayOfForms[i];
        current = form.filename.text;
        
        if (current && ![current isEqualToString:@""]) {
            [existingFilenames addObject:current];
        }
    }
    
    
    // Case 1
    if (existingFilenames.count == 0) {
         NSLog(@"Case 1");
        [self popupToGetFilenameForSavingForm];
    }
    else if (self.waterFormManager.arrayOfForms.count == 1 && existingFilenames.count != 0) {
        // Case 2
         NSLog(@"Case 2");
        WaterCommunityForm *form = [self.waterFormManager.arrayOfForms firstObject];
        [self updateDatabaseForFile:[existingFilenames firstObject] withForm:form];
    }
    else if (self.waterFormManager.arrayOfForms.count > 1 && existingFilenames.count != 0) {
         NSLog(@"Case 3");
        if (existingFilenames.count == 1) { // >= existing filenames
            // Case 3
            [self saveAllFormsWithName:[existingFilenames firstObject]];
        }
        else {
            // Case 4
            [self popupAskForFilenameOverwriteAll];
        }
    }
}


- (void)saveAllFormsWithName:(NSString*)name {
    for (id object in self.waterFormManager.arrayOfForms) {
        WaterCommunityForm *form = object;
        // new save
        if (!form.filename.text) {
            form.filename.text = name;
            WaterCommunityProvenance *wcp = [WaterCommunityProvenance new];
            [self configureDatabaseSave:wcp withForm:form];
            [self saveToDatabase:wcp];
        }
        else{
            // update form with filename
            form.filename.text = name;
            [self updateDatabaseForFile:name withForm:form];
        }
    }
}

- (void)updateFormsAndSaveNewWithName:(NSString*)name {
    for (id object in self.waterFormManager.arrayOfForms) {
        WaterCommunityForm *form = object;
        // new save
        if (!form.filename.text) {
            form.filename.text = name;
            WaterCommunityProvenance *wcp = [WaterCommunityProvenance new];
            [self configureDatabaseSave:wcp withForm:form];
            [self saveToDatabase:wcp];
        }
        else{
            // update form
            [self updateDatabaseForFile:form.filename.text withForm:form];
        }
    }
}

- (void)popupAskForFilenameOverwriteAll {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Enter new filename"
                                                                   message:@"You can use a new filename for all, "
                                                                            "or keep existing filenames and use new "
                                                                            "filename only for files not saved yet"
                                                            preferredStyle:UIAlertControllerStyleAlert];
    [alert addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        textField.placeholder = @"Enter file name here";
        textField.keyboardType = UIKeyboardAppearanceDark;
    }];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    [alert addAction:cancel];
    
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Ok, keep filenames as is" style:UIAlertActionStyleDefault
                                               handler:^(UIAlertAction * _Nonnull action)
    {
        // overwrite with existing filenames
        [self updateAllForms];
    }];
    [alert addAction:ok];
    
    UIAlertAction *overwrite = [UIAlertAction actionWithTitle:@"Ok, overwrite with new name" style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction * _Nonnull action)
    {
        UITextField *filenameField = alert.textFields.firstObject;
        if ([filenameField.text isEqualToString:@""]) {
            [self popupAskForFilenameOverwriteAll];
        }
        else
            [self saveAllFormsWithName:filenameField.text];
    }];
    [alert addAction:overwrite];
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)saveToDatabase:(WaterCommunityProvenance*)wcp {
    RLMRealm *realm = [RLMRealm defaultRealm];
    [realm beginWriteTransaction];
    [realm addObject:wcp];
    [realm commitWriteTransaction];
}

- (WaterCommunityProvenance*)configureDatabaseSave:(WaterCommunityProvenance*)wcp
                                          withForm:(WaterCommunityForm*)form {
    wcp.siteID = form.siteID;
    wcp.deviceID = [IDGenerator getDeviceId];
    wcp.username = self.userInfo.username;
    wcp.filename = form.filename.text;
    wcp.researchers = self.userInfo.researchers;
    wcp.temperature = self.userInfo.currentTemp;
    wcp.weather = self.userInfo.weatherConditions;
    wcp.project = self.userInfo.project;
    wcp.classNumber = self.userInfo.classNumber;
    wcp.instructor = self.userInfo.instructor;
    wcp.dateStarted = form.dateStarted;
    wcp.timeStarted = form.timeStarted;
    wcp.latitudeDecDegrees = form.latitudeDecDegrees;
    wcp.latitudeDecDegreesMin = form.latitudeDecDegreesMin;
    wcp.latitudeDecDegreesMinSec = form.latitudeDecDegreesMinSec;
    wcp.longitudeDecDegrees = form.longitudeDecDegrees;
    wcp.longitudeDecDegreesMin = form.longitudeDecDegreesMin;
    wcp.longitudeDecDegreesMinSec = form.longitudeDecDegreesMinSec;
    wcp.waterTemp = form.temp.text;
    wcp.suna = form.suna.text;
    wcp.depth = form.depth.text;
    wcp.conductivity = form.conductivity.text;
    wcp.chlorophyll = form.chlorophyll.text;
    wcp.pH = form.pH.text;
    wcp.waterFlowVelocity = form.waterFlowVelocity.text;
    wcp.tideStage = form.tideStage.text;
    wcp.salinity = form.salinity.text;
    wcp.blueGreenAlgae = form.blueGreenAlgae.text;
    wcp.dissolvedOxygenSat = form.dissolvedOxygenSat.text;
    wcp.dissolvedOxygenMg = form.dissolvedOxygenMg.text;
    wcp.secchiDiskExtDepth = form.secchiDiskExtDepth.text;
    wcp.meanTurbidity = form.meanTurbidity.text;
    wcp.waterBodyType = form.waterBodyType.text;
    wcp.notes = form.notes.text;
    
    return wcp;
}

- (void)updateDatabaseForFile:(NSString*)filename withForm:(WaterCommunityForm*)form {
    WaterCommunityProvenance *wcp;

    if (!form.uuid || [form.uuid isEqualToString:@""]) {
        form.uuid = [IDGenerator getUUID];
        // save new instance of form with uuid
        wcp = [WaterCommunityProvenance new];
        wcp = [self configureDatabaseSave:wcp withForm:form];
        [self saveToDatabase:wcp];
    }
    else {
        RLMResults *results = [WaterCommunityProvenance objectsWhere:[NSString stringWithFormat:
                                                                      @"username = '%@' AND filename = '%@' AND uuid = '%@'",
                                                                      self.userInfo.username,filename,form.uuid]];
        
        if ([results firstObject]) {
            wcp = [results firstObject];
            RLMRealm *realm = [RLMRealm defaultRealm];
            [realm beginWriteTransaction];
            [self configureDatabaseSave:wcp withForm:form];
            [realm commitWriteTransaction];
        }
        else {
            NSLog(@"Error: [WaterCommunity updateDatabaseForFile]: could not find form:%@ to update in database",form.uuid);
        }
    }
}

- (void)updateAllForms {
    for (id object in self.waterFormManager.arrayOfForms) {
        WaterCommunityForm *form = object;
        [self updateDatabaseForFile:form.filename.text withForm:form];
    }
}

- (void)popupToGetFilenameForSavingForm {
    NSLog(@"Save file water");
    UIAlertController *alert = [UIAlertController
                                alertControllerWithTitle:@"Enter Filename for Saving"
                                message:nil
                                preferredStyle:UIAlertControllerStyleAlert];
    
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Enter file name here";
        textField.keyboardType = UIKeyboardAppearanceDark;
    }];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel"
                                                           style:UIAlertActionStyleCancel
                                                         handler:nil];
    [alert addAction:cancelAction];
    
    UIAlertAction *saveLocallyAction = [UIAlertAction actionWithTitle:@"OK"
                                                                style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * action)
        {
            UITextField *filenameField = alert.textFields.firstObject;
            if ([self filenameExists:filenameField.text]) {
                NSLog(@"Filename exists water");
                [self getNewFilenameInsteadOfCurrent:filenameField.text];
            }
            else{
                for (id object in self.waterFormManager.arrayOfForms) {
                    WaterCommunityForm *form = object;
                    form.filename.text = filenameField.text;
                    WaterCommunityProvenance *wcp = [WaterCommunityProvenance new];
                    wcp = [self configureDatabaseSave:wcp withForm:form];
                    [self saveToDatabase:wcp];
                }
                [[NSNotificationCenter defaultCenter] postNotificationName:@"filenameEntered" object:nil];
                [[NSNotificationCenter defaultCenter] removeObserver:self];
            }
        }];
    [alert addAction:saveLocallyAction];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)popupToGetFilenameForSavingSpreadsheet {
    NSLog(@"Save Spread Water"); 
    UIAlertController *alert = [UIAlertController
          alertControllerWithTitle:@"Enter Filename for Saving"
                           message:nil
                    preferredStyle:UIAlertControllerStyleAlert];
    
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Enter file name here";
        textField.keyboardType = UIKeyboardAppearanceDark;
    }];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel"
                                                           style:UIAlertActionStyleCancel
                                                         handler:nil];
    [alert addAction:cancelAction];
    
    UIAlertAction *saveLocallyAction = [UIAlertAction actionWithTitle:@"OK"
                                                                style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * action)
        {
            UITextField *filenameField = alert.textFields.firstObject;
            if ([self filenameExists:filenameField.text]) {
                NSLog(@"File name exists water");
                [self getNewFilenameInsteadOfCurrent:filenameField.text];
            }
            else{
                NSLog(@"File name does not exist water");
                for (id object in self.waterFormManager.arrayOfForms) {
                    
                    WaterCommunityForm *form = object;
                    form.filename.text = filenameField.text;
                    WaterCommunityProvenance *wcp = [WaterCommunityProvenance new];
                    wcp = [self configureDatabaseSave:wcp withForm:form];
                    [self saveToDatabase:wcp];
                }
                [[NSNotificationCenter defaultCenter] postNotificationName:@"filenameEntered" object:nil];
                [[NSNotificationCenter defaultCenter] removeObserver:self];
            }

        }];
    [alert addAction:saveLocallyAction];
    [self presentViewController:alert animated:YES completion:nil];
}

/******************************************************************************
 * getNewFilenameInsteadOfCurrent
 *
 * This method presents the user with a popup asking to enter a new file name.
 ******************************************************************************/
- (void)getNewFilenameInsteadOfCurrent:(NSString*)currentFilename {
    NSLog(@"In getNewFilenameInsteadOfCurrent");
    UIAlertController *alert = [UIAlertController
        alertControllerWithTitle:@"Warning"
                        message:@"That file name already exists. You can enter a new name below, "
        "or select to update the current file."
                preferredStyle:UIAlertControllerStyleAlert];
    
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Enter new file name here";
        textField.keyboardType = UIKeyboardAppearanceDark;
    }];
    
    UIAlertAction *newNameAction = [UIAlertAction actionWithTitle:@"Ok With New Name"
                                                            style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action)
        {
            UITextField *temp = alert.textFields.firstObject;
            if ([self filenameExists:temp.text]) {
                [self getNewFilenameInsteadOfCurrent:temp.text];
            }
            else{
                for (id object in self.waterFormManager.arrayOfForms) {
                    WaterCommunityForm *form = object;
                    form.filename.text = temp.text;
                    WaterCommunityProvenance *wcp = [WaterCommunityProvenance new];
                    wcp = [self configureDatabaseSave:wcp withForm:form];
                    [self saveToDatabase:wcp];
                }
                [[NSNotificationCenter defaultCenter] postNotificationName:@"filenameEntered" object:nil];
                [[NSNotificationCenter defaultCenter] removeObserver:self];
            }
        }];
    [alert addAction:newNameAction];
    /*
    UIAlertAction *overwriteAction = [UIAlertAction actionWithTitle:@"Overwrite"
                                                              style:UIAlertActionStyleDefault
                                                            handler:^(UIAlertAction * action)
      {
          // update database object
          
      }];
    [alert addAction:overwriteAction];
    */
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel"
                                                           style:UIAlertActionStyleDefault
                                                         handler:nil];
    [alert addAction:cancelAction];
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (BOOL)filenameExists:(NSString*)filename {
    RLMResults *results = [WaterCommunityProvenance objectsWhere:
                           [NSString stringWithFormat:@"filename = '%@'",filename]];
    if (results.count > 0) {
        return YES;
    }
    return NO;
}


/***************************************************************************************************
 * testConnectionToWifi
 *
 * checks if ipad is connected to wifi. Returns 1 if YES
 **************************************************************************************************/
- (NSInteger)testConnectionToWifi {
    self.internetReachable = [Reachability reachabilityForLocalWiFi];
    [self.internetReachable startNotifier];
    NSInteger status = self.internetReachable.currentReachabilityStatus;
    //NSLog(@"Internet Status =%d",(int)status);
    return status;
}

- (IBAction)swipeToSegueHome:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)prepDropDownMenuOnLoad {
    [self.dropDownMenu.viewBox setHidden:YES];
    [self.view addSubview:self.dropDownMenu.viewBox];
    [self.view bringSubviewToFront:self.dropDownMenu.viewBox];
}

- (IBAction)hitDropDownMenu:(id)sender {
    [SystemSounds playTockSound];
    if (self.dropDownMenu.viewBox.hidden == YES) {
        self.dropDownMenu.viewBox.hidden = NO;
        [self.view bringSubviewToFront:self.dropDownMenu.viewBox];
    }
    else
        self.dropDownMenu.viewBox.hidden = YES;
}

- (void)adjustFormScrollerSize {
    if (self.waterFormManager.arrayOfForms.count > 1) {
        self.formScrollView.contentSize = CGSizeMake(formContainerWidth,
                                        (formHeight * self.waterFormManager.arrayOfForms.count)+7);
    }
    else
        self.formScrollView.contentSize = CGSizeMake(formContainerWidth, formContainerHeight);
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"waterToPhotoViewerSegue"]) {
        PhotoViewer *controller = (PhotoViewer*)segue.destinationViewController;
        controller.sourceSegue = segue;
        controller.userInfo = self.userInfo;
        controller.community = @"WaterCommunity";
    }
    else if ([segue.identifier isEqualToString:@"waterToFileViewerSegue"]) {
        TableFileViewer *controller = (TableFileViewer*)segue.destinationViewController;
        controller.communitySource = @"WaterCommunity";
        controller.login = self.login;
        controller.userInfo = self.userInfo;
        controller.clickedCompareDataFromWaterCommunity = self.loadDataComparator;
        self.loadDataComparator = NO;
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:YES];
}

- (void)keyboardDidHide:(NSNotification*)notification {
    [self.mainScrollView setContentOffset:CGPointMake(0.0, -0) animated:YES];
}

- (void)UITextFieldDidBeginEditing:(NSNotification*)notification {
    UITextView *textView = [notification object];
    CGPoint point = [self.view convertPoint:textView.frame.origin fromView:textView.superview];
    CGFloat yPosition = point.y + textView.frame.size.height;
    if (yPosition > (ipadPortraitHeight - ipadPortraitKeyboardHeight)) {
        CGFloat offset = yPosition + (textView.frame.size.height) -
        (ipadPortraitHeight - ipadPortraitKeyboardHeight);
        [self.mainScrollView setContentOffset:CGPointMake(0.0, offset) animated:YES];
    }
}

- (void)configureNotifications {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                        selector:@selector(UITextFieldDidBeginEditing:)
                                        name:UITextFieldTextDidBeginEditingNotification
                                        object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                        selector:@selector(keyboardDidHide:)
                                        name:UIKeyboardDidHideNotification
                                        object:nil];
}

- (BOOL)checkSiteSelections {
    WaterCommunityForm *form;
    for (id object in self.waterFormManager.arrayOfForms) {
        form = object;
        if ([form.siteButton.titleLabel.text isEqualToString:@"SEL"]) {
            [self popupSiteSelectionWarning];
            return NO;
        }
    }
    return YES;
}

/*
 * checkValidValues:
 *
 * Checks to make sure all numeric fields are either empty, or have 
 * a numeric value. 
 *
 * Input Parameters:
 *  None (just uses the WaterCommunityForm's)
 *
 * Output Parameters:
 *  No is returned if there are invalid values. Yes otherwise.
 *
 *
 * Written by: Team LionHead
 * CSS 490 Data Provenance Spring 2016
 */
- (BOOL)checkValidValues {
    WaterCommunityForm *form;
    /*
     * Goes through all of the forms that are open, verifying that
     * every numeric field is either empty or has numeric data.
     */
    for (id object in self.waterFormManager.arrayOfForms) {
        form = object;
        
        /*
         * Checks to make sure the temp field is valid
         */
        if (![form.temp.text isEqualToString:@"" ] && ![self isNumeric:form.temp.text]) {
            return NO;
        }
        
        /*
         * Checks to make sure the suna field is valid
         */
        if (![form.suna.text isEqualToString:@""] && ![self isNumeric:form.suna.text]) {
            return NO;
        }
        
        /*
         * Checks to make sure the depth field is valid
         */
        if (![form.depth.text isEqualToString:@""] && ![self isNumeric:form.depth.text]) {
            return NO;
        }
        
        /*
         * Checks to make sure the conductivity field is valid
         */
        if (![form.conductivity.text isEqualToString:@""] && ![self isNumeric:form.conductivity.text]) {
            return NO;
        }
        
        /*
         * Checks to make sure the chlorophyll field is valid
         */
        if (![form.chlorophyll.text isEqualToString:@""] && ![self isNumeric:form.chlorophyll.text]) {
            return NO;
        }
        
        /*
         * Checks to make sure the pH field is valid
         */
        if (![form.pH.text isEqualToString:@""] && ![self isNumeric:form.pH.text]) {
            return NO;
        }
        
        /*
         * Checks to make sure the waterFlowVelocity field is valid
         */
        if (![form.waterFlowVelocity.text isEqualToString:@""] && ![self isNumeric:form.waterFlowVelocity.text]) {
            return NO;
        }
        
        /*
         * Checks to make sure the salinity field is valid
         */
        if (![form.salinity.text isEqualToString:@""] && ![self isNumeric:form.salinity.text]) {
            return NO;
        }
        
        /*
         * Checks to make sure the dissolvedOxygenSat field is valid
         */
        if (![form.dissolvedOxygenSat.text isEqualToString:@""] && ![self isNumeric:form.dissolvedOxygenSat.text]) {
            return NO;
        }
        
        /*
         * Checks to make sure the dissolvedOxygenMg field is valid
         */
        if (![form.dissolvedOxygenMg.text isEqualToString:@""] && ![self isNumeric:form.dissolvedOxygenMg.text]) {
            return NO;
        }
        
        /*
         * Checks to make sure the secchiDiskExtDepth field is valid
         */
        if (![form.secchiDiskExtDepth.text isEqualToString:@""] && ![self isNumeric:form.secchiDiskExtDepth.text]) {
            return NO;
        }
        
        /*
         * Checks to make sure the meanTurbidity field is valid
         */
        if (![form.meanTurbidity.text isEqualToString:@""] && ![self isNumeric:form.meanTurbidity.text]) {
            return NO;
        }
        
        /*
         * Checks to make sure the blueGreenAlgae field is valid
         */
        if (![form.blueGreenAlgae.text isEqualToString:@""] && ![self isNumeric:form.blueGreenAlgae.text]) {
            return NO;
        }
    }
    return YES;
}

/*
 * isNumeric:
 *
 * Checks to see if stringValue (a string) can be converted into a number.
 *
 * Input Parameters:
 *  stringValue: the string value to try and convert to a number
 *
 * Output Parameters:
 *  YES if the string is a number, NO otherwise
 *
 * Written by: Team LionHead
 * CSS 490 Data Provenance Spring 2016
 */
- (BOOL)isNumeric:(NSString*)stringValue {
    // Creates a NumberFormatter
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    // Converts the stringValue to a number
    id value = [formatter numberFromString:stringValue];
    // numberFromString returns nil if it is not a number, so compare to
    // nil to get return value
    return value != nil;
}

- (void)popupSiteSelectionWarning {
    UIAlertController *alert = [UIAlertController
                                alertControllerWithTitle:@"Warning"
                                message:@"You must select a site for each form."
                                preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:nil];
    [alert addAction:ok];
    [self presentViewController:alert animated:YES completion:nil];
}

/*
 * popupToVerifyNumeric:
 *
 * Creates a popup to notify the user that the numeric fields
 * do not have numeric values.
 *
 * Input Parameters:
 *  None
 *
 * Output Parameters:
 *  None
 *
 * Written by: Team LionHead
 * CSS 490 Data Provenance Spring 2016
 */
- (void)popupToVerifyNumeric {
    // Create the alert dialoge
    UIAlertController *alert = [UIAlertController
                                alertControllerWithTitle:@"Warning"
                                message:@"Please make sure all numeric fields have numeric values"
                                preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
    // add the ok button to the dialoge
    [alert addAction:ok];
    // present the dialoge
    [self presentViewController:alert animated:YES completion:nil];
    
}

- (void)loadDataFromDatabase:(RLMArray*)data {
    // go through array to create each form
    NSMutableString *openedForms = [[NSMutableString alloc]init]; // forms that are already open and won't reopen
    
    for (id object in data) {
        BOOL isAlreadyOpen = NO;
        WaterCommunityProvenance *wcp = object;
        for (id object in self.waterFormManager.arrayOfForms) {
            WaterCommunityForm *form = object;
            if ([form.filename.text isEqualToString:wcp.filename] &&
                [form.dateStarted isEqualToString:wcp.dateStarted] &&
                [form.timeStarted isEqualToString:wcp.timeStarted] &&
                [form.siteID isEqualToString:wcp.siteID]) {
                [openedForms appendString:[NSString stringWithFormat:@"(%@:%@) ",
                                           form.filename.text,
                                           form.siteID]];
                isAlreadyOpen = YES;
            }
        }
        if (!isAlreadyOpen) {
            [self.waterFormManager createNewFormWithData:wcp];
            isAlreadyOpen = NO;
        }
    }
    if (![openedForms isEqualToString:@""]) {
        [self popupFormsAlreadyLoaded:openedForms];
    }
    [self.waterFormManager reloadForms];
}

- (void)popupFormsAlreadyLoaded:(NSString*)message {
    NSString *displayedMessage = [NSString stringWithFormat:@"The following are already opened: %@",message];
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Note"
                                                                   message:displayedMessage
                                                            preferredStyle:UIAlertControllerStyleAlert];
    [self presentViewController:alert animated:YES completion:nil];
    
    double delayInSeconds = 3.0;
    dispatch_time_t endTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(endTime, dispatch_get_main_queue(), ^{
        [alert dismissViewControllerAnimated:YES completion:nil];
    });
}

// asks user to confirm if they want to delete all forms
- (void)popupWarningToDeleteAllForms {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Warning"
                                                                   message:@"Are you sure you want to remove all forms?"
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel"
                                                     style:UIAlertActionStyleCancel
                                                   handler:nil];
    [alert addAction:cancel];
    UIAlertAction *delete = [UIAlertAction actionWithTitle:@"Yes"
                                                     style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction *action) {
                                                       [self.waterFormManager reset];
                                                       self.displayedFilename.text = @"";
                                                       self.userInfo.waterFormID = [IDGenerator getFormID];
                                                   }];
    [alert addAction:delete];
    [self presentViewController:alert animated:YES completion:^{
        [self.dropDownMenu.viewBox setHidden:YES];
    }];
}

- (void)prepareToUploadData {
    BOOL uploadReady = YES;
    for (id object in self.waterFormManager.arrayOfForms) {
        WaterCommunityForm *form = object;
        if (!form.siteID || [form.siteID isEqualToString:@""] || [form.siteID isEqualToString:@"SEL"]) {
            uploadReady = NO;
            [self popupSiteSelectionWarning];
            break;
        }
        if (!form.filename.text || [form.filename.text isEqualToString:@""]) {
            uploadReady = NO;
            [self popupMissingFilenameForUpload];
            break;
        }
    }
    
    if (uploadReady) {
        [self uploadData];
    }
}

- (void)popupMissingFilenameForUpload {
    UIAlertController *alert = [UIAlertController
                                alertControllerWithTitle:@"Note"
                                message:@"All forms must be saved first"
                                preferredStyle:UIAlertControllerStyleAlert];
    
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Enter file name here";
        textField.keyboardType = UIKeyboardAppearanceDark;
    }];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel"
                                                           style:UIAlertActionStyleCancel
                                                         handler:nil];
    [alert addAction:cancelAction];
    UIAlertAction *saveLocallyAction = [UIAlertAction actionWithTitle:@"OK"
                                                                style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * action)
    {
        UITextField *filenameField = alert.textFields.firstObject;
        if ([self filenameExists:filenameField.text]) {
            [self getNewFilenameInsteadOfCurrent:filenameField.text];
        }
        else{
            for (id object in self.waterFormManager.arrayOfForms) {
                WaterCommunityForm *form = object;
                form.filename.text = filenameField.text;
                WaterCommunityProvenance *wcp = [WaterCommunityProvenance new];
                wcp = [self configureDatabaseSave:wcp withForm:form];
                [self saveToDatabase:wcp];
            }
            [[NSNotificationCenter defaultCenter] postNotificationName:@"filenameEntered" object:nil];
            [[NSNotificationCenter defaultCenter] removeObserver:self];
        }
    }];
    [alert addAction:saveLocallyAction];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)uploadData {
    NSInteger test = [self testConnectionToWifi];
    // user approved and connection to internet
    if (self.userInfo.approvedUser == YES && test == 1) {
        for (int i = 0; i < self.waterFormManager.arrayOfForms.count; i++) {
            if (i > 0) {
                double delayInSeconds = 1.5;
                dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
                dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                    //code to be executed on the main queue after delay
                    [self uploadForElementAtIndex:i];
                });
            }
            else{
                [self uploadForElementAtIndex:i];
            }
        } // close for loop
    }
    // user is approved but there is no connection to internet
    else if (self.userInfo.approvedUser == YES && test != 1){
        //[ConnectionWatcher startConnectionWatcher]; // will check every 5 mins for connection
        [self popupNoWifiForSaving];
    }
    else{ // guest login, cannot upload
        [self popupLoginWarning];
    }
}

- (void)popupLoginWarning {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Note"
                                                                   message:@"You must be logged in with a valid username or "
                                                                            "password to connect with the campus server."
                                                            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:nil];
    [alert addAction:ok];
    [self presentViewController:alert animated:YES completion:nil];
}

// 
- (void)uploadForElementAtIndex:(int)index {
    DirectUploadForm *uploadForm = [DirectUploadForm sharedUploadFormWithUserInfo:self.userInfo];
    WaterCommunityForm *form = self.waterFormManager.arrayOfForms[index];
    UploadData *uploadData = [[UploadData alloc]initWithParameters:[self getPostParametersFrom:form]]; // crash
    uploadData.dataType = waterType;
    [uploadForm postFormDataToServer:uploadData.data ofType:uploadData.dataType];
}

// assign all properties being used to upload data to campus server
- (NSDictionary*)getPostParametersFrom:(WaterCommunityForm*)form {
    NSDictionary *dict = @{@"id" : [IDGenerator getFormID],
                           @"deviceID" : [IDGenerator getDeviceId],
                           @"username" : self.userInfo.username,
                           @"researchers" : self.userInfo.researchers,
                           @"currentTemp" : self.userInfo.currentTemp,
                           @"weatherConditions" : [self getWeather],
                           @"project" : self.userInfo.project,
                           @"classNumber" : [self getClassNumber],
                           @"instructor" : self.userInfo.instructor,
                           @"startTime" : [self returnStringValueFrom:form.timeStarted],
                           @"startDate" : [self returnStringValueFrom:form.dateStarted],
                           @"siteID" : [self returnStringValueFrom:form.siteID],
                           @"latitude" : [self returnStringValueFrom:form.latitude.titleLabel.text],
                           @"longitude" : [self returnStringValueFrom:form.longitude.titleLabel.text],
                           @"unitOfMeasure" : [self returnStringValueFrom:form.unitOfMeasure.titleLabel.text],
                           @"temp" : [self returnStringValueFrom:form.temp.text],
                           @"suna" : [self returnStringValueFrom:form.suna.text],
                           @"depth" : [self returnStringValueFrom:form.depth.text],
                           @"conductivity" : [self returnStringValueFrom:form.conductivity.text],
                           @"chlorophyll" : [self returnStringValueFrom:form.chlorophyll.text],
                           @"pH" : [self returnStringValueFrom:form.pH.text],
                           @"waterFlowVelocity" : [self returnStringValueFrom:form.waterFlowVelocity.text],
                           @"tideStage" : [self returnStringValueFrom:form.tideStage.text],
                           @"salinity" : [self returnStringValueFrom:form.salinity.text],
                           @"dissolvedOxygenSat" : [self returnStringValueFrom:form.dissolvedOxygenSat.text],
                           @"dissolvedOxygenMg" : [self returnStringValueFrom:form.dissolvedOxygenMg.text],
                           @"secchiDiskExtDepth" : [self returnStringValueFrom:form.secchiDiskExtDepth.text],
                           @"meanTurbidity" : [self returnStringValueFrom:form.meanTurbidity.text],
                           @"blueGreenAlgae" : [self returnStringValueFrom:form.blueGreenAlgae.text],
                           @"waterBodyType" : [self returnStringValueFrom:form.waterBodyType.text],
                           @"labAnalysis" : [self returnStringValueFrom:form.labAnalysis.titleLabel.text],
                           @"notes" : [self returnStringValueFrom:form.notes.text]};
                            
    return dict;
}

- (NSString*)returnStringValueFrom:(id)object {
    if ([object isKindOfClass:[UIButton class]]) {
        UIButton *button = object;
        if (button.titleLabel.text) {
            return button.titleLabel.text;
        }
    }
    else if ([object isKindOfClass:[UITextView class]]) {
        UITextView *textView = object;
        if (textView.text) {
            return textView.text;
        }
    }
    else if ([object isKindOfClass:[UILabel class]]) {
        UILabel *label = object;
        if (label.text) {
            return label.text;
        }
    }
    else if ([object isKindOfClass:[NSString class]]) {
        NSString *str = object;
        if (str) {
            return str;
        }
    }
    return @"";
}

// calls to create csv spreadsheet saved locally
- (void)createSpreadsheet {
    // first check if there is already a filename, if not get one first, then proceed
    NSString *filename = [self getFirstFilename];
    if ([filename isEqualToString:@""]) {
        [self popupToGetFilenameForSavingSpreadsheet];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(createSpreadsheet)
                                                     name:@"filenameEntered"
                                                   object:nil];
    }
    else{
        NSString *spreadsheet = [CSVformatting formatWaterFormsIntoCSV:self.waterFormManager
                                                          withUserInfo:self.userInfo];
        NSString *savePath = [LocalFileManager checkForDirectoryForUser:self.userInfo.username
                                                         inSubdirectory:WaterCommunityFolder];
        if (savePath) {
            NSString * path = [NSString stringWithFormat:@"%@/%@.csv",savePath,filename];
            BOOL save = [LocalFileManager saveAsCSVFile:spreadsheet withPathAndName:path];
            
            //  TODO  email spreadsheet
            if (save) {
                NSData *fileData = [NSData dataWithContentsOfFile:path];
                MailAttachment *mail = [[MailAttachment alloc] initWithViewController:(UIViewController*)self];
                [mail popupAskToEmailSpreadsheetForFile:fileData withName:filename];
            }
             
        }
        else{
            NSString *error = @"Could not save as CSV file. Please notify Office of Research.";
            [self showError:error];
        }
    }
}

- (NSString*)getFirstFilename {
    for (int i = 0; i < self.waterFormManager.arrayOfForms.count; i++) {
        WaterCommunityForm *form = self.waterFormManager.arrayOfForms[i];
        if (form.filename.text && ![form.filename.text isEqualToString:@""]) {
            return form.filename.text;
        }
    }
    return @"";
}

- (NSString*)getClassNumber {
    NSString *classNumber;
    if (![self.userInfo.classNumber isEqualToString:@""]) {
        classNumber = self.userInfo.classNumber;
    }
    else
        classNumber = @"Independent Research";
    
    return classNumber;
}
    
- (NSString*)getWeather {
    NSString *weather;
    if (self.userInfo.weatherConditions) {
        weather = self.userInfo.weatherConditions;
    }
    else
        weather = @"N/A";
    
    return weather;
}

- (void)showError:(NSString*)errorMessage {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error"
                                                                   message:errorMessage
                                                            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Ok"
                                                     style:UIAlertActionStyleCancel
                                                   handler:nil];
    [alert addAction:cancel];
    [self presentViewController:alert animated:YES completion:nil];
}


- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context{
    
    if ([keyPath isEqualToString:@"waterWorkingFilename"]) {
        if (![self.userInfo.waterWorkingFilename isEqualToString:@""]) {
            NSString *spreadsheet = [CSVformatting formatWaterFormsIntoCSV:self.waterFormManager
                                                              withUserInfo:self.userInfo];
            NSString *savePath = [LocalFileManager checkForDirectoryForUser:self.userInfo.username
                                                             inSubdirectory:WaterCommunityFolder];
            if (savePath) {
                BOOL saved = [LocalFileManager saveAsCSVFile:spreadsheet withPathAndName:savePath];
                if (saved) {
                    NSString *title = @"CSV File Created";
                    [self popupWithTitle:title andText:@""];
                }
            }
            else{
                NSString *error= @"Could not save as CSV file. Please notify Office of Research.";
                [self showError:error];
            }
        }
    }
    [self.userInfo removeObserver:self forKeyPath:@"waterWorkingFilename"];
}

- (void)popupNoWifiForSaving {
    UIAlertController *errorAlert = [UIAlertController
                                     alertControllerWithTitle:@"Notice"
                                     message:@"There is no connection to the server."
                                     " Data will be saved locally."
                                     " The app will automatically check later for a connection"
                                     " and prompt you to upload when ready."
                                     preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel"
                                                     style:UIAlertActionStyleCancel
                                                   handler:^(UIAlertAction * _Nonnull action)
                             {
                                 //[ConnectionWatcher stopWatching];
                             }];
    [errorAlert addAction:cancel];
    
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK"
                                                 style:UIAlertActionStyleDefault
                                               handler:^(UIAlertAction *action)
                         {

                         }];
    [errorAlert addAction:ok];
    
    [self presentViewController:errorAlert animated:YES completion:^{
        
    }];
}

- (void)popupWithTitle:(NSString*)title andText:(NSString*)text {
    UIAlertController *errorAlert = [UIAlertController
                                     alertControllerWithTitle:title
                                     message:text
                                     preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK"
                                                 style:UIAlertActionStyleDefault
                                               handler:nil];
    [errorAlert addAction:ok];
    
    [self presentViewController:errorAlert animated:YES completion:nil];
}

@end
