//
//  FileComparatorCell.m
//  GeoDatabaseWetlands
//
//  Created by Jonathan Mason on 8/24/15.
//  Copyright (c) 2015 Jonathan Mason. All rights reserved.
//

#import "FileComparatorCell.h"

@implementation FileComparatorCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
