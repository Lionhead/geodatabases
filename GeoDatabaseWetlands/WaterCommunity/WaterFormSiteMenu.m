//
//  WaterFormSiteMenu.m
//  GeoDatabaseWetlands
//
//  Created by Jonathan Mason on 7/14/15.
//  Copyright (c) 2015 Jonathan Mason. All rights reserved.
//

#import "WaterFormSiteMenu.h"
#import "UIViewControllerTracker.h"

#define menuWidth 230
#define menuHeight 365
#define buttonWidth 220
#define buttonHeight 40

@interface WaterFormSiteMenu ()

@property (strong, nonatomic) UIButton *catchBasin;
@property (strong, nonatomic) UIButton *roofRunoff;
@property (strong, nonatomic) UIButton *runoffPond;
@property (strong, nonatomic) UIButton *runoffSwale;
@property (strong, nonatomic) UIButton *surfaceWater;
@property (strong, nonatomic) UIButton *northCreek;
@property (strong, nonatomic) UIButton *lakeTruly;
@property (strong, nonatomic) UIButton *customSite;
@property (strong, nonatomic) NSDictionary *sites;

@end

@implementation WaterFormSiteMenu

- (id)initWithView:(WaterCommunityForm*)waterForm {
    if (self = [super init]) {
        self.waterForm = waterForm;
        [self storeSiteKeys];
        [self setupBackground];
        [self setupButtons];
        [self.menuView setHidden:YES];
    }
    return self;
}

- (void)storeSiteKeys {
    self.sites = @{@"CB" : @"Catch Basin",
                   @"RR" : @"Roof Runoff",
                   @"RP" : @"Runoff Pond",
                   @"RS" : @"Runoff Swale",
                   @"SW" : @"Surface Water",
                   @"NC" : @"North Creek",
                   @"LT" : @"Lake Truly"};
}

- (void)setupBackground {
    self.menuView = [[UIImageView alloc]initWithFrame:CGRectMake(64, 52, menuWidth, menuHeight)];
    [self.menuView setUserInteractionEnabled:YES];
    [self.menuView setImage:[UIImage imageNamed:@"waterSiteMenu"]];
    [self.waterForm.formView addSubview:self.menuView];
}

- (void)setupButtons {
    self.catchBasin = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, buttonWidth, buttonHeight)];
    [self.catchBasin setShowsTouchWhenHighlighted:YES];
    [self.catchBasin addTarget:self action:@selector(selectionMade:) forControlEvents:UIControlEventTouchUpInside];
    [self.menuView addSubview:self.catchBasin];
    
    self.roofRunoff = [[UIButton alloc]initWithFrame:CGRectMake(0, 45, buttonWidth, buttonHeight)];
    [self.roofRunoff setShowsTouchWhenHighlighted:YES];
    [self.roofRunoff addTarget:self action:@selector(selectionMade:) forControlEvents:UIControlEventTouchUpInside];
    [self.menuView addSubview:self.roofRunoff];
    
    self.runoffPond = [[UIButton alloc]initWithFrame:CGRectMake(0, 90, buttonWidth, buttonHeight)];
    [self.runoffPond setShowsTouchWhenHighlighted:YES];
    [self.runoffPond addTarget:self action:@selector(selectionMade:) forControlEvents:UIControlEventTouchUpInside];
    [self.menuView addSubview:self.runoffPond];
    
    self.runoffSwale = [[UIButton alloc]initWithFrame:CGRectMake(0, 135, buttonWidth, buttonHeight)];
    [self.runoffSwale setShowsTouchWhenHighlighted:YES];
    [self.runoffSwale addTarget:self action:@selector(selectionMade:) forControlEvents:UIControlEventTouchUpInside];
    [self.menuView addSubview:self.runoffSwale];
    
    self.surfaceWater = [[UIButton alloc]initWithFrame:CGRectMake(0, 180, buttonWidth, buttonHeight)];
    [self.surfaceWater setShowsTouchWhenHighlighted:YES];
    [self.surfaceWater addTarget:self action:@selector(selectionMade:) forControlEvents:UIControlEventTouchUpInside];
    [self.menuView addSubview:self.surfaceWater];
    
    self.northCreek = [[UIButton alloc]initWithFrame:CGRectMake(0, 225, buttonWidth, buttonHeight)];
    [self.northCreek setShowsTouchWhenHighlighted:YES];
    [self.northCreek addTarget:self action:@selector(selectionMade:) forControlEvents:UIControlEventTouchUpInside];
    [self.menuView addSubview:self.northCreek];
    
    self.lakeTruly = [[UIButton alloc]initWithFrame:CGRectMake(0, 270, buttonWidth, buttonHeight)];
    [self.lakeTruly setShowsTouchWhenHighlighted:YES];
    [self.lakeTruly addTarget:self action:@selector(selectionMade:) forControlEvents:UIControlEventTouchUpInside];
    [self.menuView addSubview:self.lakeTruly];
    
    self.customSite = [[UIButton alloc]initWithFrame:CGRectMake(0, 315, buttonWidth, buttonHeight)];
    [self.customSite setShowsTouchWhenHighlighted:YES];
    [self.customSite addTarget:self action:@selector(makeCustomSelection:) forControlEvents:UIControlEventTouchUpInside];
    [self.menuView addSubview:self.customSite];
}

// this method uses a dictionary to count how many times a site has been selected
- (void)selectionMade:(id)sender {
    NSString *result;
    
    if ([sender isEqual:self.catchBasin]) {
        result = @"CB";
    }
    else if ([sender isEqual:self.roofRunoff]) {
        result = @"RR";
    }
    else if ([sender isEqual:self.runoffPond]) {
        result = @"RP";
    }
    else if ([sender isEqual:self.runoffSwale]) {
        result = @"RS";
    }
    else if ([sender isEqual:self.surfaceWater]) {
        result = @"SW";
    }
    else if ([sender isEqual:self.northCreek]) {
        result = @"NC";
    }
    else if ([sender isEqual:self.lakeTruly]) {
        result = @"LT";
    }
    
    self.waterForm.siteID = [self.sites valueForKey:result];
    [self.waterForm updateButtonSiteInfo:result forForm:self.waterForm];
    [self.menuView setHidden:YES];
}

- (void)makeCustomSelection:(id)sender {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Enter Custom Site"
                                                                   message:nil
                                                            preferredStyle:UIAlertControllerStyleAlert];
    [alert addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        
    }];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    [alert addAction:cancel];
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        UITextField *textField = [alert.textFields firstObject];
        if ([textField.text isEqualToString:@""]) {
            [self makeCustomSelection:nil];
        }
        else {
            self.waterForm.siteID = textField.text;
            [self.waterForm updateCustomButtonSiteInfo:textField.text forForm:self.waterForm];
        }
    }];
    [alert addAction:ok];
    UIViewControllerTracker *vcTracker = [UIViewControllerTracker sharedVC];
    [vcTracker.currentViewController presentViewController:alert animated:YES completion:^{
        [self.menuView setHidden:YES];
    }];
}

@end
