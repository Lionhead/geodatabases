//
//  WaterCommunityMenu.h
//  GeoDatabaseWetlands
//
//  Created by Jonathan Mason on 7/8/15.
//  Copyright (c) 2015 Jonathan Mason. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UIViewControllerTracker.h"
@class WaterCommunity;
@class WaterFormManager;

// See comment at top of file for a complete description
@interface WaterCommunityMenu : NSObject

@property (strong, nonatomic) UIView *viewBox;

- (id)initWithWaterCommunity:(WaterCommunity*)waterCommPtr;

@end
