//
//  WaterAnalyticsBuilder.m
//  GeoDatabaseWetlands
//
//  Created by Jonathan Mason on 8/24/15.
//  Copyright (c) 2015 Jonathan Mason. All rights reserved.
//

#import "WaterCommunity.h"
#import "WaterAnalyticsBuilder.h"
#import "WaterCommunityForm.h"
#import "DataNode.h"
#import "UIViewControllerTracker.h"
#import "WeatherBug.h"
#import "DataCell.h"
#import <Realm/Realm.h>

#define xPos1 43
#define xPos2 386
#define yPos1 20
#define yPos2 320
#define yPos3 620
#define yPos4 920
#define yPos5 1220
#define yPos6 1520

@implementation WaterAnalyticsBuilder

BOOL successfulBuild;
UIViewControllerTracker *vcTracker;

- (id)initWithData:(RLMResults*)data {
    if (self = [super init]) {
        vcTracker = [UIViewControllerTracker sharedVC];
        
        if (!data) {
            successfulBuild = NO;
            NSLog(@"Error: [WaterAnalyticsBuilder getDataFromArray]");
            [self popupErrorGettingData];
        }
        else{
            successfulBuild = YES;
            self.analyticsView = [[WaterAnalyticsView alloc]initWithXAmount:(int)data.count];
            [self.analyticsView setAutoresizingMask:UIViewAutoresizingFlexibleHeight |
             UIViewAutoresizingFlexibleWidth];
            [self.analyticsView addHeaderInfo:data];
            WeatherBug *weatherBug = [[WeatherBug alloc] initWithSuperView:self.analyticsView andData:data];
            [self.analyticsView addSubview:weatherBug];
            self.analyticsView.weatherBug = weatherBug;
            self.analyticsView.weatherBug.delegate = self.analyticsView;
            [self configureCells:data];
        }
        
    }
    return self;
}

- (void)configureCells:(RLMResults*)data {
    // verify data is correct class
    if (![[data firstObject] isKindOfClass:[WaterCommunityProvenance class]]) {
        NSLog(@"Error: [WaterAnalyticsBuilder configureCells]: invalid object passed in");
        return;
    }

    // setup grid positions
    int xGridPositions[] = {xPos1,xPos2,xPos1,xPos2,xPos1,xPos2,xPos1,xPos2,xPos1,xPos2,xPos1,xPos2};
    int yGridPositions[] = {yPos1,yPos1,yPos2,yPos2,yPos3,yPos3,yPos4,yPos4,yPos5,yPos5,yPos6,yPos6};
    WaterCommunityForm *genForm = [WaterCommunityForm new];
    
    // create data cells with grid positions
    for (int i = 0; i < genForm.indexOfProperties.count; i++){
        DataCell *cell = [[DataCell alloc]initWithData:data
                                               forUnit:genForm.indexOfProperties[i]
                                                   atX:xGridPositions[i]
                                                  andY:yGridPositions[i]];

        [self.analyticsView.scrollView addSubview:cell];
    }
}


- (BOOL)returnBuildSuccess {
    return successfulBuild;
}

- (void)popupErrorGettingData {
    UIAlertController *alert = [UIAlertController
                    alertControllerWithTitle:@"Error"
                                message:@"Could not read in data to create analytics."
                                " Please contact tech support."
                                preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Ok"
                                                 style:UIAlertActionStyleDefault
                                               handler:nil];
    [alert addAction:ok];
    WaterCommunity *waterPtr = (WaterCommunity*)vcTracker.currentViewController;
    [waterPtr presentViewController:alert animated:YES completion:nil];
}

- (WaterAnalyticsView*)returnAnalyticsView {
    return self.analyticsView;
}

- (BOOL)getDataFromArray:(NSMutableArray*)array {
    
    return YES;
}

@end
