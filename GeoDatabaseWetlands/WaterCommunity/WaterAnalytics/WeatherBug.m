//
//  WeatherBug.m
//  GeoDatabaseWetlands
//
//  Created by Jonathan Mason on 8/25/15.
//  Copyright (c) 2015 Jonathan Mason. All rights reserved.
//

#import "WeatherBug.h"
#import "DataNode.h"
#import "WaterCommunityProvenance.h"

#define viewHeight 150
#define xOffset 10
#define YOffset 40
#define labelX 80

@interface WeatherBug()

@end

UIView *theSuperView;

@implementation WeatherBug

- (id)initWithSuperView:(UIView*)view andData:(RLMResults*)data {
    if (self = [super init]) {
        theSuperView = view;
        [self configureView];
        [self setHeader];
        [self buildViewWithData:data];
    }
    return self;
}

- (void)configureView {
    self.frame = CGRectMake((theSuperView.frame.size.width /2)+xOffset, YOffset, theSuperView.frame.size.width/2 - xOffset, viewHeight);
    [self setContentMode:UIViewContentModeScaleAspectFit];
    [self setImage:[UIImage imageNamed:@"waterBugBackground"]];
    [self setUserInteractionEnabled:YES];
}

- (void)addTouchRecognizer:(UILabel*)label {
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]
                                          initWithTarget:self action:@selector(userTappedLabel:)];
    [label addGestureRecognizer:tapGesture];
}

- (void)setHeader{
    UILabel *header = [[UILabel alloc]initWithFrame:CGRectMake(50, 7, 210, 20)];
    [header setText:@"Weather Conditions"];
    [header setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:18]];
    [self addSubview:header];
}

- (void)buildViewWithData:(RLMResults*)data {
    WaterCommunityProvenance *wcp;
    int step = 0;
    int LabelYOffset = 35;
    for (int i = 0; i < data.count; i++) {
        wcp = data[i];
        
        UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(labelX,
                                                                  LabelYOffset+step,
                                                                  self.frame.size.width - (labelX+5),
                                                                  20)];
        [label setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:16]];
        [label setUserInteractionEnabled:YES];

        if ([wcp.weather isEqualToString:@""]) {
            [label setText:@"N/A"];
        }
        else
             [label setText:[NSString stringWithFormat:@"%@",wcp.weather]];
        
        // if the text won't fit into this size label, then add gesture recognizer to show
        // a preview of label at full size and all text on touch
        if ([self getCorrectLabelWidth:label] > label.frame.size.width) {
            [self addTouchRecognizer:label];
        }
        NSLog(@"the width of label is %f", label.frame.size.width);
        NSLog(@"the width of label should be %f", [self getCorrectLabelWidth:label]);
             
        [label setTextColor:[self getLabelColor:i]];
        if (![label.text isEqualToString:@""]) {
            [self addSubview:label];
        }

        step +=22;
    }
}

- (int)yPosStart:(int)count {
    switch (count) {
        case 3:
            return 175;
        case 4:
            return 200;
        case 5:
            return 225;
    }
    return 150;
}

- (UIColor*)getLabelColor:(int)number {
    UIColor *color;
    
    switch (number) {
        case 0:
            color = [UIColor colorWithRed:30.0/255.0
                                    green:180.0/255.0
                                     blue:240.0/255.0
                                    alpha:1.0];
            break;
        case 1:
            color = [UIColor colorWithRed:15.0/255.0
                                    green:215.0/255.0
                                     blue:100.0/255.0
                                    alpha:1.0];
            break;
        case 2:
            color = [UIColor colorWithRed:150.0/255.0
                                    green:90.0/255.0
                                     blue:220.0/255.0
                                    alpha:1.0];
            break;
        case 3:
            color = [UIColor colorWithRed:30.0/255.0
                                    green:90.0/255.0
                                     blue:230.0/255.0
                                    alpha:1.0];
            break;
        case 4:
            color = [UIColor colorWithRed:15.0/255.0
                                    green:150.0/255.0
                                     blue:60.0/255.0
                                    alpha:1.0];
            break;
            
        default:
            break;
    }
    
    return color;
}

// when user taps label whose string is too long to fit, it will show a preview of the whole label on screen
- (void)userTappedLabel:(UITapGestureRecognizer*)recognizer {
    UILabel *label = (UILabel*)recognizer.view;
    [label setAlpha:0.0];
    [self fitLabelToContainingText:label];
}

- (void)fitLabelToContainingText:(UILabel*)label {
    CGFloat newWidth = [self getCorrectLabelWidth:label];
    
    CGRect previewFrame = CGRectMake([self superview].frame.size.width - newWidth-5,
                                     self.frame.origin.y + label.frame.origin.y,
                                     newWidth,
                                     label.frame.size.height);
    
    UILabel *previewLabel = [[UILabel alloc] initWithFrame:previewFrame];
    [previewLabel setText:label.text];
    [previewLabel setFont:label.font];
    [previewLabel setTextColor:label.textColor];
    [previewLabel setTextAlignment:NSTextAlignmentRight];
    [previewLabel setBackgroundColor:[UIColor whiteColor]];
    [self roundLayerCornerWithRadius:6.0 forView:previewLabel];
    [[self superview] addSubview:previewLabel];
    
    [UIView animateWithDuration:1.0 delay:4.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
        [previewLabel setAlpha:0.0];
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.5 animations:^{
            [label setAlpha:1.0];
            [previewLabel setHidden:YES];
        }];
    }];
}

- (void)roundLayerCornerWithRadius:(CGFloat)radius forView:(UIView*)view{
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:view.bounds
                                                        cornerRadius:radius];
    CAShapeLayer *maskLayer = [CAShapeLayer layer];
    maskLayer.frame = view.bounds;
    maskLayer.path = maskPath.CGPath;
    [view.layer setMask:maskLayer];
}

- (CGFloat)getCorrectLabelWidth:(UILabel*)label {
    CGFloat width = [label.text boundingRectWithSize:label.frame.size
                                             options:NSStringDrawingUsesDeviceMetrics
                                          attributes:@{ NSFontAttributeName:label.font }
                                             context:nil].size.width+10;
    return width;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
