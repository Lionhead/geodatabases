//
//  DataCell.m
//  GeoDatabaseWetlands
//
//  Created by Jonathan Mason on 8/26/15.
//  Copyright (c) 2015 Jonathan Mason. All rights reserved.
//

#import "DataCell.h"
#import "WaterCommunityProvenance.h"
#import <Realm/Realm.h>

#define tempKey @"temperature"
#define waterTempKey @"waterTemp"
#define sunaKey @"suna"
#define depthKey @"depth"
#define conductivityKey @"conductivity"
#define chlorophyllKey @"chlorophyll"
#define pHKey @"pH"
#define waterFlowVelocityKey @"waterFlowVelocity"
#define salinityKey @"salinity"
#define dissolvedOxygenSatKey @"dissolvedOxygenSat"
#define dissolvedOxygenMgKey @"dissolvedOxygenMg"
#define secchiDiskExtDepthKey @"secchiDiskExtDepth"
#define meanTurbidityKey @"meanTurbidity"

#define viewWidth 300
#define viewHeight 250
#define graphSpace 250
#define barGutter 20

@interface DataCell ()

@property (strong, nonatomic) NSMutableArray *values;
@property CGFloat highestValue;
@property CGFloat lowestValue;
@property CGFloat catalyst;

@end

int unitCount;
int barWidth;
int barOffset;

@implementation DataCell

- (id)initWithData:(RLMResults*)data forUnit:(NSString*)unit atX:(int)x andY:(int)y {
    if (self = [super init]) {
        _values = [NSMutableArray new];
        _highestValue = 0;
        _catalyst = 0;
        self.frame = CGRectMake(x, y, viewWidth, viewHeight);
        [self setOpaque:NO];
        
        barWidth = [self returnBarWidth:(int)data.count];
        [self getHighestValue:data fromUnit:unit];
        _catalyst = [self getCatalystFromHigh:_highestValue];
        [self setHeader:unit];
        [self drawBars:data forUnit:unit];
    }
    return self;
}

- (void)drawRect:(CGRect)rect {
    
    [[UIColor blackColor] set];
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetLineWidth(context, 2.0f);
    CGContextSetLineCap(context, kCGLineCapSquare);
    
    // vertical line
    drawPoint(context,
              CGPointMake(50.0f, 50.0f),
              CGPointMake(50.0f, self.bounds.size.height));
    // horizontal line
    drawPoint(context,
              CGPointMake(50.0f, self.bounds.size.height-1),
              CGPointMake(self.bounds.size.width, self.bounds.size.height-1));
    // top marker
    drawPoint(context,
              CGPointMake(45.0f, 50.0f),
              CGPointMake(50.0f, 50.0f));
    // 3/4 marker
    drawPoint(context,
              CGPointMake(45.0f, 100.0f),
              CGPointMake(50.0f, 100.0f));
    // mid marker
    drawPoint(context,
              CGPointMake(45.0f, 150.0f),
              CGPointMake(50.0f, 150.0f));
    // 1/4 marker
    drawPoint(context,
              CGPointMake(45.0f, 200.0f),
              CGPointMake(50.0f, 200.0f));
}

void drawPoint(CGContextRef context, CGPoint startPoint, CGPoint endPoint) {
    CGContextMoveToPoint(context, startPoint.x, startPoint.y);
    CGContextAddLineToPoint(context, endPoint.x, endPoint.y);
    CGContextStrokePath(context);
}

- (NSString*)returnLabelValue:(CGFloat)input {
    if (input == 0) {
        return @"0.0";
    }
    if (input < 0.001) {
        return [NSString stringWithFormat:@"%.5f",input];
    }
    else if (input < 0.01) {
        return [NSString stringWithFormat:@"%.4f",input];
    }
    else if (input < 0.1) {
        return [NSString stringWithFormat:@"%.3f",input];
    }
    else if (input < 10) {
        return [NSString stringWithFormat:@"%.2f",input];
    }
    
    return [NSString stringWithFormat:@"%.1f",input];
}

- (void)drawBars:(RLMResults*)data forUnit:(NSString*)unit {
    UILabel *maxLabel = [[UILabel alloc]initWithFrame:CGRectMake(-10, 40, 50, 20)];
    [maxLabel setText:[self returnLabelValue:self.highestValue]];
    [maxLabel setTextAlignment:NSTextAlignmentRight];
    [maxLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:13]];
    [self addSubview:maxLabel];
    
    UILabel *midLabel = [[UILabel alloc]initWithFrame:CGRectMake(-10, 140, 50, 20)];
    [midLabel setText:[self returnLabelValue:self.highestValue/2]];
    [midLabel setTextAlignment:NSTextAlignmentRight];
    [midLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:13]];
    [self addSubview:midLabel];
    
    WaterCommunityProvenance *wcp;
    int start = 50;
    for (int i = 0; i < data.count; i++) {
        wcp = data[i];
        
        UIView *view = [[UIView alloc]initWithFrame:CGRectMake
                        (start+barGutter+(barGutter*i)+([self returnBarWidth:(int)data.count]*i),
                         self.bounds.size.height,
                         [self returnBarWidth:(int)data.count],
                         0)];
        [view setBackgroundColor:[self getColor:i]];
        [self addSubview:view];
        
        [UIView animateWithDuration:1.0 animations:^{
            view.frame = CGRectMake(start+barGutter+(barGutter*i)+([self returnBarWidth:(int)data.count]*i),
                                    [self getBarYPositionWithUnitValue:[[wcp valueForKey:unit]floatValue] andCatalyst:self.catalyst],
                                    [self returnBarWidth:(int)data.count],
                                    [[wcp valueForKey:unit]floatValue] * self.catalyst);
        }];
    }
}

- (CGFloat)getBarYPositionWithUnitValue:(CGFloat)value andCatalyst:(CGFloat)catalyst {
    return self.bounds.size.height-(value*catalyst);
}

- (int)returnBarOffset:(int)count {
    return 20;
}

- (int)returnBarWidth:(int)count {
    int width = 0;
    
    switch (count) {
        case 2:
            width = graphSpace/2 - barGutter;
            break;
        case 3:
            width = graphSpace/3 - barGutter;
            break;
        case 4:
            width = graphSpace/4 - barGutter;
            break;
        case 5:
            width = graphSpace/5 - barGutter;
            break;
        default:
            break;
    }
    return width;
}

// puts a header over the cell to identify the data
- (void)setHeader:(NSString*)title {
    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(50, 0, 250, 40)];
    [titleLabel setText:[self returnTitleForKey:title]];
    [titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:18]];
    [self addSubview:titleLabel];
}

// returns the max value of all being compared
- (void)getHighestValue:(RLMResults*)data fromUnit:(NSString*)unit {
    
    WaterCommunityProvenance *wcp;
    for (int i = 0; i < data.count; i++) {
        wcp = data[i];
        CGFloat value = [[wcp valueForKey:unit] floatValue];
        NSNumber *num = [NSNumber numberWithFloat:value];
        [self.values addObject:num];
        
        if (value > self.highestValue) {
            self.highestValue = value;
        }
    }
}

// returns value used to determine
- (CGFloat)getCatalystFromHigh:(CGFloat)high {
    if (high == 0) {
        return 1;
    }
    return 200.0/high;
}


- (void)addBarsWithData:(NSMutableArray*)data {
    int x = 0;
    for (int i = 0; i < data.count; i++) {
        x += barOffset;
    }
}

- (NSString*)returnTitleForKey:(NSString*)key {
    
    if ([key isEqualToString:waterTempKey]) {
        return @"temperature (c)";
    }
    else if ([key isEqualToString:sunaKey]) {
        return @"suna nitrate (mg/l)";
    }
    else if ([key isEqualToString:depthKey]) {
        return @"depth (ft)";
    }
    else if ([key isEqualToString:conductivityKey]) {
        return @"conductivity (uS)";
    }
    else if ([key isEqualToString:chlorophyllKey]) {
        return @"chlorophyll (ug/l)";
    }
    else if ([key isEqualToString:pHKey]) {
        return @"pH";
    }
    else if ([key isEqualToString:waterFlowVelocityKey]) {
        return @"water flow velocity (cm/s)";
    }
    else if ([key isEqualToString:salinityKey]) {
        return @"salinity (ppt)";
    }
    else if ([key isEqualToString:dissolvedOxygenSatKey]) {
        return @"dissolved oxygen (%sat)";
    }
    else if ([key isEqualToString:dissolvedOxygenMgKey]) {
        return @"dissolved oxygen (mg/l)";
    }
    else if ([key isEqualToString:secchiDiskExtDepthKey]) {
        return @"secchi disk ext. depth (m)";
    }
    else if ([key isEqualToString:meanTurbidityKey]) {
        return @"mean turbidity (NTU)";
    }
    
    return @"";
}

- (UIColor*)getColor:(int)number {
    UIColor *color;
    
    switch (number) {
        case 0:
            color = [UIColor colorWithRed:30.0/255.0
                                    green:180.0/255.0
                                     blue:240.0/255.0
                                    alpha:1.0];
            break;
        case 1:
            color = [UIColor colorWithRed:15.0/255.0
                                    green:215.0/255.0
                                     blue:100.0/255.0
                                    alpha:1.0];
            break;
        case 2:
            color = [UIColor colorWithRed:150.0/255.0
                                    green:90.0/255.0
                                     blue:220.0/255.0
                                    alpha:1.0];
            break;
        case 3:
            color = [UIColor colorWithRed:30.0/255.0
                                    green:90.0/255.0
                                     blue:230.0/255.0
                                    alpha:1.0];
            break;
        case 4:
            color = [UIColor colorWithRed:15.0/255.0
                                    green:150.0/255.0
                                     blue:60.0/255.0
                                    alpha:1.0];
            break;
            
        default:
            break;
    }
    
    return color;
}

@end