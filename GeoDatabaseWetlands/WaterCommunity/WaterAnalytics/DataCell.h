//
//  DataCell.h
//  GeoDatabaseWetlands
//
//  Created by Jonathan Mason on 8/26/15.
//  Copyright (c) 2015 Jonathan Mason. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Realm/Realm.h>

@interface DataCell : UIView

- (id)initWithData:(RLMResults*)data forUnit:(NSString*)unit atX:(int)x andY:(int)y;

@end
