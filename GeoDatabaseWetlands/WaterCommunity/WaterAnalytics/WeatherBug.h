//
//  WeatherBug.h
//  GeoDatabaseWetlands
//
//  Created by Jonathan Mason on 8/25/15.
//  Copyright (c) 2015 Jonathan Mason. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Realm/Realm.h>

@protocol WeatherLabelDelegate <NSObject>

@required
- (void)labelTapped:(UILabel*)label;

@end

@interface WeatherBug : UIImageView
{
    id<WeatherLabelDelegate> labelDelegate;
}

@property (strong, nonatomic) id delegate;

- (id)initWithSuperView:(UIView*)view andData:(RLMResults*)data;

@end
