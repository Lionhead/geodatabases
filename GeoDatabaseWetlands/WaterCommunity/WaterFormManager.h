//
//  WaterFormManager.h
//  GeoDatabaseWetlands
//
//  Created by Jonathan Mason on 7/8/15.
//  Copyright (c) 2015 Jonathan Mason. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WaterCommunityForm.h"
#import "WaterCommunityMenu.h"
#import "WaterCommunity.h"
#import "WaterCommunityProvenance.h"
@class WaterCommunityForm;

// See comment at top of file for a complete description
@interface WaterFormManager : NSObject

@property (strong, nonatomic) WaterCommunity *waterPtr;

//BOOL samplesForLabAnalysis;
@property (strong, nonatomic) NSDictionary *waterQualityDict;
@property (strong, nonatomic) NSMutableArray *arrayOfForms;

- (int)returnCountForSite:(NSString*)site forForm:(WaterCommunityForm*)form;
+ (WaterFormManager*)sharedFormManager;
- (void)createNewForm;
- (void)createNewFormWithData:(WaterCommunityProvenance*)data;
- (void)deleteForm:(id)sender;
- (void)reset;
- (void)reloadForms;


@end
