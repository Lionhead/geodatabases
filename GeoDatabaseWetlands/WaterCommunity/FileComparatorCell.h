//
//  FileComparatorCell.h
//  GeoDatabaseWetlands
//
//  Created by Jonathan Mason on 8/24/15.
//  Copyright (c) 2015 Jonathan Mason. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FileComparatorCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *filename;
@property (weak, nonatomic) IBOutlet UILabel *dateSaved;

@end
