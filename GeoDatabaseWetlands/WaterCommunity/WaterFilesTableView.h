//
//  WaterFilesTableView.h
//  GeoDatabaseWetlands
//
//  Created by Jonathan Mason on 8/24/15.
//  Copyright (c) 2015 Jonathan Mason. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserInfo.h"

@interface WaterFilesTableView : UIViewController <UITableViewDelegate , UITableViewDataSource>

@property (weak,nonatomic) UserInfo *userInfo;

@end
