//
//  WaterFileViewTableCell.m
//  GeoDatabaseWetlands
//
//  Created by Jonathan Mason on 10/20/15.
//  Copyright © 2015 Jonathan Mason. All rights reserved.
//

#import "WaterFileViewTableCell.h"

@implementation WaterFileViewTableCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {

    }
    return self;
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
