//
//  WaterAnalyticsBuilder.h
//  GeoDatabaseWetlands
//
//  Created by Jonathan Mason on 8/24/15.
//  Copyright (c) 2015 Jonathan Mason. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WaterAnalyticsView.h"

@interface WaterAnalyticsBuilder : NSObject

@property (strong, nonatomic) WaterAnalyticsView *analyticsView;

- (id)initWithData:(NSMutableArray*)dataArray;

- (WaterAnalyticsView*)returnAnalyticsView;
- (BOOL)returnBuildSuccess;

@end
