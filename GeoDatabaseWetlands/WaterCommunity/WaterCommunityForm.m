//
//  WaterCommunityForm.m
//  GeoDatabaseWetlands
//
//  Created by Jonathan Mason on 7/8/15.
//  Copyright (c) 2015 Jonathan Mason. All rights reserved.
//

#import "WaterCommunityForm.h"
#import "TimeUnits.h"

#define formWidth 704
#define formHeight 552

#define formViewKey @"formView"
#define siteIDKey @"siteID"
#define latitudeKey @"latitude"
#define longitudeKey @"longitude"
#define latitudeDecDegreesKey @"latitudeDecDegrees"
#define latitudeDecDegreesMinKey @"latitudeDecDegreesMin"
#define latitudeDecDegreesMinSecKey @"latitudeDecDegreesMinSec"
#define longitudeDecDegreesKey @"longitudeDecDegrees"
#define longitudeDecDegreesMinKey @"longitudeDecDegreesMin"
#define longitudeDecDegreesMinSecKey @"longitudeDecDegreesMinSec"
#define unitOfMeasureKey @"UnitOfMeasure"
#define displayedTimeKey @"displayedTime"
#define tempKey @"temperature"
#define waterTempKey @"waterTemp"
#define sunaKey @"suna"
#define depthKey @"depth"
#define conductivityKey @"conductivity"
#define chlorophyllKey @"chlorophyll"
#define pHKey @"pH"
#define waterFlowVelocityKey @"waterFlowVelocity"
#define tideStageKey @"tideStage"
#define salinityKey @"salinity"
#define dissolvedOxygenSatKey @"dissolvedOxygenSat"
#define dissolvedOxygenMgKey @"dissolvedOxygenMg"
#define secchiDiskExtDepthKey @"secchiDiskExtDepth"
#define meanTurbidityKey @"meanTurbidity"
#define waterBodyTypeKey @"waterBodyType"
#define labAnalysisKey @"labAnalysis"
#define notesKey @"notes"
#define weatherConditionsKey @"weatherConditions"
#define indexOfPropertiesKey @"indexOfProperties"

@interface WaterCommunityForm()

@property (strong, nonatomic) UIImageView *formBackground;

@end

@implementation WaterCommunityForm {
    CLLocationManager *locationManager;
    CLGeocoder *geocoder;
    CLPlacemark *placemark;
    NSArray *decodedObjects;
}

// converts decimal degrees to decimal mins/secs
NSString* (^convertDecToDecMin)(NSString* decimal);
NSString* (^convertDecToDecMinSec)(NSString* decimal);
NSString* (^convertDecMinSecToDec)(NSString* decimalMinSec);

- (id)init{
    if (self = [super init]) {
        [self buildIndexOfProperties];
    }
    return self;
}

- (id)initAtYpos:(CGFloat)y withManager:(WaterFormManager*)manager {
    if (self = [super init]) {
        _formManagerPtr = manager;
        [self setupFormAtYPos:y];
        [self buildViewBackground];
        [self createHeader];
        [self setDataFields];
        [self createUnitConvertingBlocks];
        [self showUnitConvertingButton];
        [self addSwipeGestures];
        _formSiteMenu = [[WaterFormSiteMenu alloc]initWithView:self];
        [self buildIndexOfProperties];
        _weatherConditions = self.formManagerPtr.waterPtr.userInfo.weatherConditions;
        [self setDefaultPropertyValues];
        [self storeSiteKeys];
    }
    return self;
}

- (void)storeSiteKeys {
    self.sitesForKeys = @{@"Catch Basin" : @"CB",
                          @"Roof Runoff" : @"RR",
                          @"Runoff Pond" : @"RP",
                          @"Runoff Swale" : @"RS",
                          @"Surface Water" : @"SW",
                          @"North Creek" : @"NC",
                          @"Lake Truly" : @"LT",
                          @"Custom Site" : @"SEL"};
}

 - (void)setDefaultPropertyValues {
     self.latitudeDecDegrees = @"";
     self.latitudeDecDegreesMin = @"";
     self.latitudeDecDegreesMinSec = @"";
     self.longitudeDecDegrees = @"";
     self.longitudeDecDegreesMin = @"";
     self.longitudeDecDegreesMinSec = @"";
 }

- (void)placeAlldecodedObjects:(NSArray*)array {
    for (id object in array){
        [self.formView addSubview:object];
    }
}

// converts decimal degrees to decimal mins/secs and back
- (void)createUnitConvertingBlocks {
    convertDecToDecMin = ^NSString*(NSString* decimal){
        double deg = (int)[decimal doubleValue];
        double min = (int)(([decimal doubleValue] - deg) * 60);
        NSString *result = [NSString stringWithFormat:@"%.0f %.0f'",deg,min];
        return result;
    };
    convertDecToDecMinSec = ^NSString*(NSString* decimal){
        double deg = (int)[decimal doubleValue];
        double min = (int)(([decimal doubleValue] - deg) * 60);
        double sec = ([decimal doubleValue] - deg - min/60)*3600;
        NSString *result = [NSString stringWithFormat:@"%.0f %.0f' %.6f\"",deg,min,sec];
        return result;
    };
    convertDecMinSecToDec = ^NSString*(NSString* decimalMinSec){
        NSArray *set = [decimalMinSec componentsSeparatedByString:@" "];
        double dec = [set[0] doubleValue];
        double min = [set[1] doubleValue];
        double sec = [set[2] doubleValue];
        double decDegrees = (dec + min/60 + sec/3600);
        NSString *result = [NSString stringWithFormat:@"%.6f",decDegrees];
        return result;
    };
}

- (void)setupFormAtYPos:(CGFloat)y {
    self.formView = [[UIView alloc]initWithFrame:CGRectMake(0, y, formWidth, formHeight)];
}

- (void)buildViewBackground {
    self.formBackground = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, formWidth, formHeight)];
    [self.formBackground setImage:[UIImage imageNamed:@"waterFormBackground"]];
    [self.formView addSubview:self.formBackground];
}

- (void)createHeader {

    self.filename = [[UILabel alloc]initWithFrame:CGRectMake(15, 15, 125, 22)];
    [self.filename setFont:[UIFont fontWithName:@"HelveticaNeue-Italic" size:16]];
    [self.filename setTextColor:[UIColor whiteColor]];
    [self.formView addSubview:self.filename];
    
    self.siteButton = [[UIButton alloc]initWithFrame:CGRectMake(16, 42, 45, 24)];
    self.siteButton.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:16];
    [self.siteButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.siteButton setTitle:@"SEL" forState:UIControlStateNormal];
    [self.siteButton setShowsTouchWhenHighlighted:YES];
    [self.siteButton setBackgroundColor:[UIColor whiteColor]];
    self.siteButton.layer.mask = [self addMaskToButton:self.siteButton];
    [self.siteButton addTarget:self action:@selector(hitsiteIDButton:)
          forControlEvents:UIControlEventTouchUpInside];
    [self.formView addSubview:self.siteButton];
    
    self.latitude = [[UIButton alloc]initWithFrame:CGRectMake(159, 42, 160, 24)];
    self.latitude.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:18];
    [self.latitude setTitle:@"" forState:UIControlStateNormal];
    self.latitude.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    self.latitude.contentEdgeInsets = UIEdgeInsetsMake(0, 5, 0, 0);
    [self.latitude setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    self.latitude.layer.backgroundColor = [UIColor whiteColor].CGColor;
    self.latitude.layer.mask = [self addMaskToButton:self.latitude];
    [self.latitude addTarget:self action:@selector(hitLatitudeButton:)
            forControlEvents:UIControlEventTouchUpInside];
    [self.formView addSubview:self.latitude];
    
    self.longitude = [[UIButton alloc]initWithFrame:CGRectMake(429, 42, 160, 24)];
    self.longitude.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:18];
    [self.longitude setTitle:@"" forState:UIControlStateNormal];
    self.longitude.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    self.longitude.contentEdgeInsets = UIEdgeInsetsMake(0, 5, 0, 0);
    [self.longitude setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    self.longitude.layer.backgroundColor = [UIColor whiteColor].CGColor;
    self.longitude.layer.mask = [self addMaskToButton:self.longitude];
    [self.longitude addTarget:self action:@selector(hitLongitudeButton:)
             forControlEvents:UIControlEventTouchUpInside];
    [self.formView addSubview:self.longitude];
    
    self.unitOfMeasure = [UIButton new];
    [self.unitOfMeasure setFrame:CGRectMake(612, 42, 75, 24)];
    self.unitOfMeasure.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:16];
    [self.unitOfMeasure setTitle:@"Dec" forState:UIControlStateNormal];
    [self.unitOfMeasure setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.unitOfMeasure setBackgroundColor:[UIColor whiteColor]];
    self.unitOfMeasure.layer.mask = [self addMaskToButton:self.unitOfMeasure];
    [self.unitOfMeasure addTarget:self action:@selector(unitConvert:)
                 forControlEvents:UIControlEventTouchUpInside];
    [self.formView addSubview:self.unitOfMeasure];
    
    self.dateStarted = [TimeUnits presentDate];
    self.timeStarted = [TimeUnits presentTime];
    
    self.displayedTime = [[UILabel alloc]initWithFrame:CGRectMake(152, 13, 400, 24)];
    [self.displayedTime setText:[NSString stringWithFormat:@"Started on: %@  at: %@",
                               self.dateStarted,self.timeStarted]];
    [self.displayedTime setFont:[UIFont fontWithName:@"HelveticaNeue" size:18]];
    [self.displayedTime setTextColor:[UIColor whiteColor]];
    self.displayedTime.textAlignment = NSTextAlignmentCenter;
    [self.formView addSubview:self.displayedTime];
}

- (void)setDataFields {
    self.temp = [[UITextField alloc]initWithFrame:CGRectMake(17, 113, 73, 32)];
    [self.temp setFont:[UIFont fontWithName:@"HelveticaNeue" size:24]];
    self.temp.textAlignment = NSTextAlignmentCenter;
    self.temp.keyboardType = UIKeyboardTypeDecimalPad;
    [self.formView addSubview:self.temp];
    
    self.suna = [[UITextField alloc]initWithFrame:CGRectMake(107, 113, 146, 32)];
    [self.suna setFont:[UIFont fontWithName:@"HelveticaNeue" size:24]];
    self.suna.textAlignment = NSTextAlignmentCenter;
    self.suna.keyboardType = UIKeyboardTypeDecimalPad;
    [self.formView addSubview:self.suna];
    
    self.depth = [[UITextField alloc]initWithFrame:CGRectMake(272, 113, 73, 32)];
    [self.depth setFont:[UIFont fontWithName:@"HelveticaNeue" size:24]];
    self.depth.textAlignment = NSTextAlignmentCenter;
    self.depth.keyboardType = UIKeyboardTypeDecimalPad;
    [self.formView addSubview:self.depth];
    
    self.conductivity = [[UITextField alloc]initWithFrame:CGRectMake(356, 113, 160, 32)];
    [self.conductivity setFont:[UIFont fontWithName:@"HelveticaNeue" size:24]];
    self.conductivity.textAlignment = NSTextAlignmentCenter;
    self.conductivity.keyboardType = UIKeyboardTypeDecimalPad;
    [self.formView addSubview:self.conductivity];
    
    self.chlorophyll = [[UITextField alloc]initWithFrame:CGRectMake(526, 113, 160, 32)];
    [self.chlorophyll setFont:[UIFont fontWithName:@"HelveticaNeue" size:24]];
    self.chlorophyll.textAlignment = NSTextAlignmentCenter;
    self.chlorophyll.keyboardType = UIKeyboardTypeDecimalPad;
    [self.formView addSubview:self.chlorophyll];
    
    self.pH = [[UITextField alloc]initWithFrame:CGRectMake(17, 198, 73, 32)];
    [self.pH setFont:[UIFont fontWithName:@"HelveticaNeue" size:24]];
    self.pH.textAlignment = NSTextAlignmentCenter;
    self.pH.keyboardType = UIKeyboardTypeDecimalPad;
    [self.formView addSubview:self.pH];
    
    self.waterFlowVelocity = [[UITextField alloc]initWithFrame:CGRectMake(101, 198, 245, 32)];
    [self.waterFlowVelocity setFont:[UIFont fontWithName:@"HelveticaNeue" size:24]];
    self.waterFlowVelocity.textAlignment = NSTextAlignmentCenter;
    self.waterFlowVelocity.keyboardType = UIKeyboardTypeDecimalPad;
    [self.formView addSubview:self.waterFlowVelocity];
    
    self.tideStage = [[UITextField alloc]initWithFrame:CGRectMake(356, 198, 160, 32)];
    [self.tideStage setFont:[UIFont fontWithName:@"HelveticaNeue" size:24]];
    self.tideStage.textAlignment = NSTextAlignmentCenter;
    [self.formView addSubview:self.tideStage];
    
    self.salinity = [[UITextField alloc]initWithFrame:CGRectMake(526, 198, 160, 32)];
    [self.salinity setFont:[UIFont fontWithName:@"HelveticaNeue" size:24]];
    self.salinity.textAlignment = NSTextAlignmentCenter;
    self.salinity.keyboardType = UIKeyboardTypeDecimalPad;
    [self.formView addSubview:self.salinity];
    
    self.dissolvedOxygenSat = [[UITextField alloc]initWithFrame:CGRectMake(16, 308, 160, 32)];
    [self.dissolvedOxygenSat setFont:[UIFont fontWithName:@"HelveticaNeue" size:24]];
    self.dissolvedOxygenSat.textAlignment = NSTextAlignmentCenter;
    self.dissolvedOxygenSat.keyboardType = UIKeyboardTypeDecimalPad;
    [self.formView addSubview:self.dissolvedOxygenSat];
    
    self.dissolvedOxygenMg = [[UITextField alloc]initWithFrame:CGRectMake(186, 308, 160, 32)];
    [self.dissolvedOxygenMg setFont:[UIFont fontWithName:@"HelveticaNeue" size:24]];
    self.dissolvedOxygenMg.textAlignment = NSTextAlignmentCenter;
    self.dissolvedOxygenMg.keyboardType = UIKeyboardTypeDecimalPad;
    [self.formView addSubview:self.dissolvedOxygenMg];
    
    self.secchiDiskExtDepth = [[UITextField alloc]initWithFrame:CGRectMake(356, 308, 160, 32)];
    [self.secchiDiskExtDepth setFont:[UIFont fontWithName:@"HelveticaNeue" size:24]];
    self.secchiDiskExtDepth.textAlignment = NSTextAlignmentCenter;
    self.secchiDiskExtDepth.keyboardType = UIKeyboardTypeDecimalPad;
    [self.formView addSubview:self.secchiDiskExtDepth];
    
    self.meanTurbidity = [[UITextField alloc]initWithFrame:CGRectMake(526, 308, 160, 32)];
    [self.meanTurbidity setFont:[UIFont fontWithName:@"HelveticaNeue" size:24]];
    self.meanTurbidity.textAlignment = NSTextAlignmentCenter;
    self.meanTurbidity.keyboardType = UIKeyboardTypeDecimalPad;
    [self.formView addSubview:self.meanTurbidity];
    
    self.blueGreenAlgae = [[UITextField alloc]initWithFrame:CGRectMake(16, 393, 245, 32)];
    [self.blueGreenAlgae setFont:[UIFont fontWithName:@"HelveticaNeue" size:24]];
    [self.blueGreenAlgae setTextAlignment:NSTextAlignmentCenter];
    [self.blueGreenAlgae setKeyboardType:UIKeyboardTypeDecimalPad];
    [self.formView addSubview:self.blueGreenAlgae];
    
    self.waterBodyType = [[UITextField alloc]initWithFrame:CGRectMake(526, 393, 160, 32)];
    [self.waterBodyType setFont:[UIFont fontWithName:@"HelveticaNeue" size:24]];
    [self.waterBodyType setBackgroundColor:[UIColor clearColor]];
    self.waterBodyType.textAlignment = NSTextAlignmentCenter;
    [self.formView addSubview:self.waterBodyType];
    
    self.labAnalysis = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.labAnalysis setFrame:CGRectMake(369, 390, 50, 35)];
    [self.labAnalysis.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue" size:23]];
    [self.labAnalysis setTitle:@"NO" forState:UIControlStateNormal];
    [self.labAnalysis setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.labAnalysis setShowsTouchWhenHighlighted:YES];
    [self.labAnalysis addTarget:self
                         action:@selector(hitAnalysisButton:)
               forControlEvents:UIControlEventTouchUpInside];
    [self.formView addSubview:self.labAnalysis];
    
    self.notes = [[UITextView alloc]initWithFrame:CGRectMake(16, 470, 670, 64)];
    [self.notes setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:20]];
    [self.formView addSubview:self.notes];
}

// swipe left or right to delete
- (void)addSwipeGestures {
    UISwipeGestureRecognizer *swipeLeft = [[UISwipeGestureRecognizer alloc]
                                           initWithTarget:self
                                           action:@selector(deleteForm:)];
    swipeLeft.direction = UISwipeGestureRecognizerDirectionLeft;
    [self.formView addGestureRecognizer:swipeLeft];
    
    UISwipeGestureRecognizer *swipeRight = [[UISwipeGestureRecognizer alloc]
                                            initWithTarget:self
                                            action:@selector(deleteForm:)];
    swipeRight.direction = UISwipeGestureRecognizerDirectionRight;
    [self.formView addGestureRecognizer:swipeRight];
}

- (void)deleteForm:(id)sender {
    [self.formManagerPtr deleteForm:sender];
}

- (void)hitsiteIDButton:(id)sender {
    if (self.formSiteMenu) {
        if (self.formSiteMenu.menuView.hidden == NO) {
            [self.formSiteMenu.menuView setHidden:YES];
        }
        else{
            [self.formSiteMenu.menuView setHidden:NO];
        }
    }
    else{
        self.formSiteMenu = [[WaterFormSiteMenu alloc]initWithView:self];
    }
}

- (void)hitGPSButton:(id)sender {
    if (![self.latitude.titleLabel.text isEqualToString:@""] ||
        ![self.longitude.titleLabel.text isEqualToString:@""]) {
        UIAlertController *alert = [UIAlertController
                alertControllerWithTitle:@"Warning"
                    message:@"This will erase the current latitude and longitude information."
                        preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel"
                                    style:UIAlertActionStyleCancel handler:nil];
        [alert addAction:cancel];
        UIAlertAction *proceed = [UIAlertAction actionWithTitle:@"Proceed"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction *action) {
            [self startGPSLocationServices];
        }];
        [alert addAction:proceed];
        [self.formManagerPtr.waterPtr presentViewController:alert animated:YES completion:nil];
    }
    else
        [self startGPSLocationServices];
}

- (void)showUnitConvertingButton {
    if (([self.latitude.titleLabel.text isEqualToString:@""] ||
         self.latitude.titleLabel.text == nil) &&
        ([self.longitude.titleLabel.text isEqualToString:@""] ||
         self.longitude.titleLabel.text == nil)) {
        [self.unitOfMeasure setHidden:YES];
    }
    else
        [self.unitOfMeasure setHidden:NO];
}

// gives user alertController popup to manually enter latitude
- (void)hitLatitudeButton:(id)sender {
    UIAlertController *getLatitude = [UIAlertController alertControllerWithTitle:@"Enter Latitude"
                                            message:nil
                                            preferredStyle:UIAlertControllerStyleAlert];
    [getLatitude addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Enter Degrees";
        textField.keyboardType = UIKeyboardTypeDecimalPad;
    }];
    [getLatitude addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Enter Minutes";
        textField.keyboardType = UIKeyboardTypeDecimalPad;
    }];
    [getLatitude addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Enter Seconds";
        textField.keyboardType = UIKeyboardTypeDecimalPad;
    }];
    UIAlertAction *switchToDec = [UIAlertAction actionWithTitle:@"Switch to Decimal Degree"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction *action)
                                  {[self popupToGetDecimalDegree:sender];}];
    [getLatitude addAction:switchToDec];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel"
                                                     style:UIAlertActionStyleCancel
                                                   handler:nil];
    [getLatitude addAction:cancel];
    UIAlertAction *done = [UIAlertAction actionWithTitle:@"Done"
                                                   style:UIAlertActionStyleDefault
                                                 handler:^(UIAlertAction *action) {
        UITextField *textField0 = getLatitude.textFields[0];
        UITextField *textField1 = getLatitude.textFields[1];
        UITextField *textField2 = getLatitude.textFields[2];
        NSString *deg = textField0.text;
        NSString *min = textField1.text;
        NSString *sec = textField2.text;
        NSString *strToConvert = [NSString stringWithFormat:@"%@ %@ %@",deg,min,sec];
        self.latitudeDecDegrees = convertDecMinSecToDec(strToConvert);
        self.latitudeDecDegreesMin = convertDecToDecMin(self.latitudeDecDegrees);
        self.latitudeDecDegreesMinSec = [NSString stringWithFormat:@"%@ %@' %@\"",deg,min,sec];
        [self.latitude setTitle:self.latitudeDecDegreesMinSec forState:UIControlStateNormal];
        [self.unitOfMeasure setTitle:@"Dec/M/S" forState:UIControlStateNormal];
        if (self.longitude.titleLabel.text &&
            ![self.longitude.titleLabel.text isEqualToString:@""]) {
         [self.longitude setTitle:[NSString stringWithFormat:@"%@",self.longitudeDecDegreesMinSec]
                         forState:UIControlStateNormal];
        }
        [self showUnitConvertingButton];
    }];
    [getLatitude addAction:done];
    [self.formManagerPtr.waterPtr presentViewController:getLatitude animated:YES completion:nil];
}

// gives user alertController popup to manually enter longitude
- (void)hitLongitudeButton:(id)sender {
    UIAlertController *getLongitude =
            [UIAlertController alertControllerWithTitle:@"Enter Longitude"
                                                message:nil
                                         preferredStyle:UIAlertControllerStyleAlert];
    [getLongitude addTextFieldWithConfigurationHandler:^(UITextField *textField) {
            textField.placeholder = @"Enter Degrees";
        textField.keyboardType = UIKeyboardTypeDecimalPad;
    }];
    [getLongitude addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Enter Minutes";
        textField.keyboardType = UIKeyboardTypeDecimalPad;
    }];
    [getLongitude addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Enter Seconds";
        textField.keyboardType = UIKeyboardTypeDecimalPad;
    }];
    UIAlertAction *switchToDec = [UIAlertAction actionWithTitle:@"Switch to Decimal Degree"
                                                          style:UIAlertActionStyleDefault
                                                        handler:^(UIAlertAction *action){
                                                        [self popupToGetDecimalDegree:sender];}];
    [getLongitude addAction:switchToDec];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel"
                                                     style:UIAlertActionStyleCancel handler:nil];
    [getLongitude addAction:cancel];
    UIAlertAction *done = [UIAlertAction actionWithTitle:@"Done"
                                                   style:UIAlertActionStyleDefault
                                                 handler:^(UIAlertAction *action) {
        UITextField *textField0 = getLongitude.textFields[0];
        UITextField *textField1 = getLongitude.textFields[1];
        UITextField *textField2 = getLongitude.textFields[2];
        NSString *deg = textField0.text;
        NSString *min = textField1.text;
        NSString *sec = textField2.text;
        NSString *strToConvert = [NSString stringWithFormat:@"%@ %@ %@",deg,min,sec];
        self.longitudeDecDegrees = convertDecMinSecToDec(strToConvert);
        self.longitudeDecDegreesMin = convertDecToDecMin(self.longitudeDecDegrees);
        self.longitudeDecDegreesMinSec = [NSString stringWithFormat:@"%@ %@' %@\"",deg,min,sec];
        [self.longitude setTitle:[NSString stringWithFormat:@"%@ %@' %@\"",deg,min,sec]
                        forState:UIControlStateNormal];
        [self.unitOfMeasure setTitle:@"Dec/M/S" forState:UIControlStateNormal];
        if (self.latitude.titleLabel.text &&
            ![self.latitude.titleLabel.text isEqualToString:@""]) {
            [self.latitude setTitle:[NSString stringWithFormat:@"%@",self.latitudeDecDegreesMinSec]
                            forState:UIControlStateNormal];
        }
        [self showUnitConvertingButton];
    }];
    [getLongitude addAction:done];
    [self.formManagerPtr.waterPtr presentViewController:getLongitude animated:YES completion:nil];
}

- (void)setLatitudeButton:(NSString*)latitude {
    //
    if ([latitude containsString:@"min"] && [latitude containsString:@"sec"]) {
        self.latitudeDecDegrees = convertDecMinSecToDec(latitude);
        self.latitudeDecDegreesMin = convertDecToDecMin(self.latitudeDecDegrees);
        self.latitudeDecDegreesMinSec = latitude;
    }
    else if ([latitude containsString:@"min"] && ![latitude containsString:@"sec"]) {
        self.latitudeDecDegreesMin = latitude;
    }
    else{
        self.latitudeDecDegrees = latitude;
    }
}
- (void)setLongitudeButton:(NSString*)longitude {
    if ([longitude containsString:@"min"] && [longitude containsString:@"sec"]) {
        self.longitudeDecDegrees = convertDecMinSecToDec(longitude);
        self.longitudeDecDegreesMin = convertDecToDecMin(self.longitudeDecDegrees);
        self.longitudeDecDegreesMinSec = longitude;
    }
    else if ([longitude containsString:@"min"] && ![longitude containsString:@"sec"]) {
        
    }
}

- (void)hitAnalysisButton:(id)sender {
    UIButton *button = sender;
    if ([button.titleLabel.text isEqualToString:@"NO"]) {
        [button setTitle:@"YES" forState:UIControlStateNormal];
    }
    else
        [button setTitle:@"NO" forState:UIControlStateNormal];
}

// this method gives user a popup to enter decimal degrees
//
- (void)popupToGetDecimalDegree:(id)sender {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil
                                            message:@"Enter in decimal degrees."
                                            preferredStyle:UIAlertControllerStyleAlert];
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Decimal numbers only.";
        textField.keyboardType = UIKeyboardTypeDecimalPad;
    }];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel"
                                                     style:UIAlertActionStyleCancel
                                                   handler:nil];
    [alert addAction:cancel];
    UIAlertAction *done = [UIAlertAction actionWithTitle:@"Done"
                                                   style:UIAlertActionStyleDefault
                                                 handler:^(UIAlertAction *action) {
        UITextField *textField = alert.textFields.firstObject;
        if ([sender isEqual:self.latitude]) {
            self.latitudeDecDegrees = textField.text;
            self.latitudeDecDegreesMin = convertDecToDecMin(self.latitudeDecDegrees);
            self.latitudeDecDegreesMinSec = convertDecToDecMinSec(self.latitudeDecDegrees);
            [self.latitude setTitle:self.latitudeDecDegrees forState:UIControlStateNormal];
            if (self.longitude.titleLabel.text &&
                ![self.longitude.titleLabel.text isEqualToString:@""]) {
                [self.longitude setTitle:[NSString stringWithFormat:@"%@",self.longitudeDecDegrees]
                                forState:UIControlStateNormal];
            }
        }
        else{
            self.longitudeDecDegrees = textField.text;
            self.longitudeDecDegreesMin = convertDecToDecMin(self.longitudeDecDegrees);
            self.longitudeDecDegreesMinSec = convertDecToDecMinSec(self.longitudeDecDegrees);
            [self.longitude setTitle:self.longitudeDecDegrees forState:UIControlStateNormal];
            if (self.latitude.titleLabel.text &&
                ![self.latitude.titleLabel.text isEqualToString:@""]) {
                [self.latitude setTitle:[NSString stringWithFormat:@"%@",self.latitudeDecDegrees]
                                forState:UIControlStateNormal];
            }
        }
        [self.unitOfMeasure setTitle:@"Dec" forState:UIControlStateNormal];
        [self showUnitConvertingButton];
    }];
    [alert addAction:done];
    [self.formManagerPtr.waterPtr presentViewController:alert animated:YES completion:nil];
}

// removes self from view
- (void)removeForm {
    self.formManagerPtr = nil;
    [self.formView setHidden:YES];
}

// rounds the corners of a UIButton
- (CALayer*)addMaskToButton:(UIButton*)button {
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:button.bounds
                                                        cornerRadius:6];
    CAShapeLayer *maskLayer = [CAShapeLayer layer];
    maskLayer.frame = button.bounds;
    maskLayer.path = maskPath.CGPath;
    return maskLayer;
}

- (void)updateCustomButtonSiteInfo:(NSString*)customSite forForm:(WaterCommunityForm*)form {
    NSString *result;
    if (customSite.length < 3) {
        result = [customSite uppercaseString];
    }
    else
        result = [[customSite substringToIndex:3]uppercaseString];
    
    [self.siteButton setTitle:result forState:UIControlStateNormal];
}

// this method uses a dictionary to count how many times a site has been selected
- (void)updateButtonSiteInfo:(NSString*)site forForm:(WaterCommunityForm *)form {
    /*
    int siteCount = [self.formManagerPtr returnCountForSite:site forForm:form];
    NSString *result = [NSString stringWithFormat:@"%@%d",site,siteCount];
    [self updateSiteSelection:result];
     */
    [self.siteButton setTitle:site forState:UIControlStateNormal];
}

- (void)unitConvert:(id)sender {
    UIButton *button = sender;
    if ([button.titleLabel.text isEqualToString:@"Dec"]) {
        [button setTitle:@"Dec/Mn" forState:UIControlStateNormal];
        if (![self.latitude.titleLabel.text isEqualToString:@""]) {
            [self.latitude setTitle:self.latitudeDecDegreesMin forState:UIControlStateNormal];
        }
        if (![self.latitude.titleLabel.text isEqualToString:@""]) {
            [self.longitude setTitle:self.longitudeDecDegreesMin forState:UIControlStateNormal];
        }
    }
    else if ([button.titleLabel.text isEqualToString:@"Dec/Mn"]) {
        [button setTitle:@"Dec/M/S" forState:UIControlStateNormal];
        if (![self.latitude.titleLabel.text isEqualToString:@""]) {
            [self.latitude setTitle:self.latitudeDecDegreesMinSec forState:UIControlStateNormal];
        }
        if (![self.latitude.titleLabel.text isEqualToString:@""]) {
            [self.longitude setTitle:self.longitudeDecDegreesMinSec forState:UIControlStateNormal];
        }
    }
    else if ([button.titleLabel.text isEqualToString:@"Dec/M/S"]) {
        [button setTitle:@"Dec" forState:UIControlStateNormal];
        if (![self.latitude.titleLabel.text isEqualToString:@""]) {
            [self.latitude setTitle:self.latitudeDecDegrees forState:UIControlStateNormal];
        }
        if (![self.latitude.titleLabel.text isEqualToString:@""]) {
            [self.longitude setTitle:self.longitudeDecDegrees forState:UIControlStateNormal];
        }
    }
}

/******************************************************************************
 * startGPSLocationServices (saving a photo taken)
 *
 * Instantiate locationManager if needed, set properties, and start updating
 * location. Also instantiate geocoder if needed.
 ******************************************************************************/
- (void)startGPSLocationServices {
    if (locationManager == nil) {
        locationManager = [[CLLocationManager alloc] init];
        locationManager.delegate = self;
        locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        locationManager.distanceFilter = kCLDistanceFilterNone;
        if ([locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
            [locationManager requestWhenInUseAuthorization];
        }
        [locationManager startUpdatingLocation];
    }
    if (geocoder == nil) {
        geocoder = [[CLGeocoder alloc] init];
    }
    [locationManager startMonitoringSignificantLocationChanges];
}

-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error{
    NSLog(@"Error [locationManager]: %@",error);
    NSLog(@"Failed to get GPS location.");
    
    UIAlertController *locationError =
            [UIAlertController alertControllerWithTitle:@"Error"
                                                message:@"Failed to Get Your Location"
                                         preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Ok"
                                                 style:UIAlertActionStyleCancel
                                               handler:nil];
    [locationError addAction:ok];
    [self.formManagerPtr.waterPtr presentViewController:locationError animated:YES completion:nil];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    CLLocation *currentLocation = [locations lastObject];
    
    if (currentLocation != nil) {
        self.latitudeDecDegrees = [NSString stringWithFormat:@"%.8f",
                              currentLocation.coordinate.latitude];
        self.longitudeDecDegrees = [NSString stringWithFormat:@"%.8f",
                               currentLocation.coordinate.longitude];
        
        self.latitudeDecDegreesMin = convertDecToDecMin(self.latitudeDecDegrees);
        self.longitudeDecDegreesMin = convertDecToDecMin(self.longitudeDecDegrees);
        
        self.latitudeDecDegreesMinSec = convertDecToDecMinSec(self.latitudeDecDegrees);
        self.longitudeDecDegreesMinSec = convertDecToDecMinSec(self.longitudeDecDegrees);
        
        self.latitude.titleLabel.text = [NSString stringWithFormat:@"%.8f",
                                         currentLocation.coordinate.latitude];
        self.longitude.titleLabel.text = [NSString stringWithFormat:@"%.8f",
                                          currentLocation.coordinate.longitude];
        [locationManager stopUpdatingLocation];
        locationManager = nil;
    }
}

- (void)buildIndexOfProperties {
    self.indexOfProperties = [[NSArray alloc]initWithObjects:waterTempKey,sunaKey,depthKey,conductivityKey,chlorophyllKey,pHKey,salinityKey,dissolvedOxygenMgKey,dissolvedOxygenSatKey,waterFlowVelocityKey,secchiDiskExtDepthKey,meanTurbidityKey, nil];
}


@end
