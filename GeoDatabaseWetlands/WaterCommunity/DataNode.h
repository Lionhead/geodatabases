//
//  DataNode.h
//  GeoDatabaseWetlands
//
//  Created by Jonathan Mason on 8/24/15.
//  Copyright (c) 2015 Jonathan Mason. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DataNode : NSObject

@property (strong, nonatomic) NSString *filename;
@property (strong, nonatomic) NSString *siteName;
@property (strong, nonatomic) NSString *dateSaved;
@property (strong, nonatomic) NSString *filePath;

@end
