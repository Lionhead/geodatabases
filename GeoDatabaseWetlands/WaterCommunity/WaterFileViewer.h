//
//  WaterFileViewer.h
//  GeoDatabaseWetlands
//
//  Created by Jonathan Mason on 10/20/15.
//  Copyright © 2015 Jonathan Mason. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WaterCommunity.h"
#import <Realm/Realm.h>

@interface WaterFileViewer : UIView

- (RLMResults*)returnData:(RLMResults*)data;
- (id)initWithFrame:(CGRect)frame andWaterComm:(WaterCommunity*)water;

@end
