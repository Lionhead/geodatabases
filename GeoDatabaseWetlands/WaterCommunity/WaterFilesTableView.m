//
//  WaterFilesTableView.m
//  GeoDatabaseWetlands
//
//  Created by Jonathan Mason on 8/24/15.
//  Copyright (c) 2015 Jonathan Mason. All rights reserved.
//

#import "WaterFilesTableView.h"
#import "WaterCommunity.h"
#import "FileComparatorCell.h"
#import "TimeUnits.h"
#import "DataNode.h"
#import "UIViewControllerTracker.h"

#define WaterCommunityFolder @"WaterCommunity"
#define messageViewFont @"Avenir-Roman"
#define navBarHeight 55

@interface WaterFilesTableView ()

@property (strong, nonatomic) UINavigationBar *navBar;
@property (strong, nonatomic) UITableView *customTableView;

@end

int fileCount;
NSMutableArray *dataToCompare;
UIViewControllerTracker *vcTracker;
WaterCommunity *waterPtr;
RLMResults *data;

@implementation WaterFilesTableView

- (void)viewDidLoad {
    [super viewDidLoad];

    [self setupNavBar];
    [self configureTableView];
    
    vcTracker = [UIViewControllerTracker sharedVC];
    waterPtr = (WaterCommunity*)vcTracker.currentViewController;
    fileCount = 0;
    dataToCompare = [NSMutableArray new];

    [self queryData];
    //[self buildListOfFiles:[NSString stringWithFormat:@"%@/%@",WaterCommunityFolder,self.userInfo.username]];
}

- (void)queryData {
    UserInfo *userInfo = [UserInfo sharedUserInfo];
    data = [[WaterCommunityProvenance objectsWhere:[NSString stringWithFormat:@"username = '%@'",userInfo.username]]
            sortedResultsUsingProperty:@"dateStarted" ascending:NO];
}

- (void)configureTableView {
    self.customTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, navBarHeight, 768, 1024 - navBarHeight)];
    [self.customTableView setAllowsMultipleSelection:YES];
    [self.customTableView setDelegate:self];
    [self.customTableView setDataSource:self];
    [self.view addSubview:self.customTableView];
}

- (void)setupNavBar {
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    
    self.navBar = [[UINavigationBar alloc]initWithFrame:
               CGRectMake(0, 0, self.view.frame.size.width, navBarHeight)];
    [self.navBar setBarStyle:UIBarStyleDefault];
    self.navBar.translucent = NO;
    [self.navBar setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
    UINavigationItem *titleItem = [[UINavigationItem alloc] initWithTitle:@"Select 2 to 5"];
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc]initWithTitle:@"Cancel"
                                                                    style:UIBarButtonItemStylePlain
                                                                   target:self
                                                                   action:@selector(cancel)];
    titleItem.leftBarButtonItem = cancelButton;
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc]initWithTitle:@"Done"
                                                                    style:UIBarButtonItemStylePlain
                                                                   target:self
                                                                   action:@selector(dismissSelf)];
    titleItem.rightBarButtonItem = doneButton;
    
    self.navBar.items = [NSArray arrayWithObject:titleItem];

    [self.view addSubview:self.navBar];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [data count];
}

- (void)cancel {
    [waterPtr dismissViewControllerAnimated:YES completion:nil];
}

- (void)dismissSelf {
    if (dataToCompare.count >= 2 && dataToCompare.count <= 5) {
        waterPtr.loadDataComparator = YES;
        [waterPtr dismissViewControllerAnimated:YES completion:^{
            [waterPtr loadDataComparatorWithData:dataToCompare];
        }];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    FileComparatorCell *cell = (FileComparatorCell*)[tableView dequeueReusableCellWithIdentifier:@"FileComparatorCell"];
    
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"FileComparatorCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    // Configure the cell...
    int idx = 0;
    for (id object in data) {
        if (indexPath.row == idx) {
            if ([object isKindOfClass:[WaterCommunityProvenance class]]) {
                WaterCommunityProvenance *wcp = object;
                cell.filename.text = [NSString stringWithFormat:@"%@: %@",wcp.filename,wcp.siteID];
                cell.dateSaved.text = [NSString stringWithFormat:@"%@ at %@",
                                  wcp.dateStarted,wcp.timeStarted];
            }
        }
        idx++;
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    WaterCommunityProvenance *wcp = data[indexPath.row];
    [dataToCompare addObject:wcp];
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    WaterCommunityProvenance *selected = data[indexPath.row];
    for (int i = 0; i < dataToCompare.count; i++) {
        WaterCommunityProvenance *check = dataToCompare[i];
        if ([check.filename isEqualToString:selected.filename] && [check.siteID isEqualToString:selected.siteID]) {
            [dataToCompare removeObjectAtIndex:i];
        }
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 18)];
    /* Create custom view to display section header... */
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, tableView.frame.size.width, 18)];
    [label setFont:[UIFont boldSystemFontOfSize:12]];
    NSString *string = @"test";
    /* Section header is in 0th index... */
    [label setText:string];
    [view addSubview:label];
    [view setBackgroundColor:[UIColor colorWithRed:166/255.0 green:177/255.0 blue:186/255.0 alpha:1.0]]; //your background color...
    return view;
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
