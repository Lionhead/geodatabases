//
//  WaterAnalyticsView.m
//  GeoDatabaseWetlands
//
//  Created by Jonathan Mason on 8/24/15.
//  Copyright (c) 2015 Jonathan Mason. All rights reserved.
//

#import "WaterAnalyticsView.h"
#import "WaterCommunityProvenance.h"
#import "DataNode.h"
#import "DataCell.h"

#define closeButtonWidth 80
#define closeButtonHeight 35
#define viewWidth 728
#define viewHeight 974
#define XOffset 30
#define YOffset 35
#define scrollViewWidth 728
#define scrollViewContentHeight 1800
#define borderSize 8

@interface WaterAnalyticsView() <WeatherLabelDelegate>

@end

@implementation WaterAnalyticsView


- (id)initWithXAmount:(int)amount {
    if (self = [super init]) {
        [self configureView:amount];
    }
    return self;
}

- (void)drawRect:(CGRect)rect {
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.bounds
                                                        cornerRadius:8];
    CAShapeLayer *maskLayer = [CAShapeLayer layer];
    maskLayer.frame = self.bounds;
    maskLayer.path = maskPath.CGPath;
    [self.layer setMask:maskLayer];
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGRect myRect = self.bounds;
    
    UIColor *topColor = [UIColor colorWithRed:240.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:1.0];
    UIColor *bottomColor = [UIColor colorWithRed:200.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:1.0];
    
    addGradientBackground(context, myRect, topColor.CGColor, bottomColor.CGColor);
}

void addGradientBackground(CGContextRef context, CGRect rect, CGColorRef startColor, CGColorRef endColor) {
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGFloat locations[] = { 0.0, 1.0 };
    
    NSArray *colors = @[(__bridge id) startColor, (__bridge id) endColor];
    
    CGGradientRef gradient = CGGradientCreateWithColors(colorSpace, (__bridge CFArrayRef)colors, locations);
    
    CGPoint startPoint = CGPointMake(CGRectGetMidX(rect), CGRectGetMinY(rect));
    CGPoint endPoint = CGPointMake(CGRectGetMidX(rect), CGRectGetMaxY(rect));
    
    CGContextSaveGState(context);
    CGContextAddRect(context, rect);
    CGContextClip(context);
    CGContextDrawLinearGradient(context, gradient, startPoint, endPoint, 0);
    CGContextRestoreGState(context);
    
    CGGradientRelease(gradient);
    CGColorSpaceRelease(colorSpace);
}

- (void)configureView:(int)amount {
    self.frame = CGRectMake(20, 30, viewWidth, viewHeight);
    [self.layer setBorderWidth:5];
    [self.layer setBorderColor:[UIColor whiteColor].CGColor];

    self.scrollView = [[UIScrollView alloc]initWithFrame:
                CGRectMake(0, YOffset+[self getHeaderBuffer:amount],
                           scrollViewWidth, viewHeight-(YOffset+borderSize+[self getHeaderBuffer:amount]))];
    [self.scrollView setContentSize:CGSizeMake(scrollViewWidth, scrollViewContentHeight)];
    [self.scrollView setClipsToBounds:YES];
    [self addSubview:self.scrollView];
    
    UIButton *closeButton = [[UIButton alloc]initWithFrame:
                             CGRectMake((self.frame.size.width/2)-(closeButtonWidth/2),
                                        0,
                                        closeButtonWidth,
                                        closeButtonHeight)];
    [closeButton addTarget:self action:@selector(hitCloseButton:)
          forControlEvents:UIControlEventTouchUpInside];
    [closeButton setTitle:@"close" forState:UIControlStateNormal];
    [closeButton setTitleColor:[self getLabelColor:0] forState:UIControlStateNormal];
    closeButton.titleLabel.font = [UIFont fontWithName:@"Avenir-Medium" size:18];
    [closeButton.titleLabel setTextAlignment:NSTextAlignmentCenter];
    [closeButton setShowsTouchWhenHighlighted:YES];
    
    [self addSubview:closeButton];
}

- (void)hitCloseButton:(id)sender {
    [self removeFromSuperview];
}

- (void)addHeaderInfo:(RLMResults*)data {
    NSString *header;
    WaterCommunityProvenance *wcp;
    int step = 22;
    
    for (int i = 0; i < data.count; i++) {
        wcp = data[i];
        header = [NSString stringWithFormat:@"%@  site:%@  %@\n",
                   wcp.filename,
                   wcp.siteID,
                   wcp.dateStarted];
        
        UILabel *headerLabel = [[UILabel alloc]initWithFrame:CGRectMake(XOffset, YOffset+step, (scrollViewWidth/2)-38, 20)];
        [headerLabel setText:header];
        [headerLabel setTextColor:[self getLabelColor:i]];
        [headerLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Medium" size:17]];
        [headerLabel setUserInteractionEnabled:YES];
        
        if ([self getCorrectLabelWidth:headerLabel] > headerLabel.frame.size.width) {
            [self addTouchRecognizer:headerLabel];
        }
        
        [self addSubview:headerLabel];

        step += 25;
    }
}

- (void)addTouchRecognizer:(UILabel*)label {
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]
                                          initWithTarget:self action:@selector(userTappedLabel:)];
    [label addGestureRecognizer:tapGesture];
}

// when user taps label whose string is too long to fit, it will show a preview of the whole label on screen
- (void)userTappedLabel:(UITapGestureRecognizer*)recognizer {
    UILabel *label = (UILabel*)recognizer.view;
    [label setAlpha:0.0];
    [self fitLabelToContainingText:label];
}

- (void)fitLabelToContainingText:(UILabel*)label {
    CGFloat newWidth = [self getCorrectLabelWidth:label];
    
    CGRect previewFrame = CGRectMake(label.frame.origin.x,
                                     self.frame.origin.y + label.frame.origin.y,
                                     newWidth,
                                     label.frame.size.height);
    
    UILabel *previewLabel = [[UILabel alloc] initWithFrame:previewFrame];
    [previewLabel setText:label.text];
    [previewLabel setFont:label.font];
    [previewLabel setTextColor:label.textColor];
    [previewLabel setBackgroundColor:[UIColor whiteColor]];
    [self roundLayerCornerWithRadius:6.0 forView:previewLabel];
    [[self superview] addSubview:previewLabel];
    
    [UIView animateWithDuration:1.0 delay:4.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
        [previewLabel setAlpha:0.0];
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.5 animations:^{
            [label setAlpha:1.0];
            [previewLabel setHidden:YES];
        }];
    }];
}

- (void)roundLayerCornerWithRadius:(CGFloat)radius forView:(UIView*)view{
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:view.bounds
                                                        cornerRadius:radius];
    CAShapeLayer *maskLayer = [CAShapeLayer layer];
    maskLayer.frame = view.bounds;
    maskLayer.path = maskPath.CGPath;
    [view.layer setMask:maskLayer];
}

- (CGFloat)getCorrectLabelWidth:(UILabel*)label {
    CGFloat width = [label.text boundingRectWithSize:label.frame.size
                                             options:NSStringDrawingUsesDeviceMetrics
                                          attributes:@{ NSFontAttributeName:label.font }
                                             context:nil].size.width+10;
    return width;
}

- (void)labelTapped:(UILabel*)label {

    CGRect shownFrame = CGRectMake(0,
                                   label.frame.origin.y,
                                   self.frame.size.width,
                                   label.frame.size.height);
    
    UILabel *tempLabel = [[UILabel alloc]initWithFrame:shownFrame];
    [tempLabel setText:label.text];
    [tempLabel setFont:label.font];
    [tempLabel setTextColor:label.textColor];
    [tempLabel setTextAlignment:NSTextAlignmentCenter];
    [self.weatherBug addSubview:tempLabel];
    
    [UIView animateWithDuration:1.0 delay:4.0 options:UIViewAnimationOptionCurveLinear animations:^{
        [tempLabel setAlpha:0.0f];
    } completion:^(BOOL finished) {
        [tempLabel setHidden:YES];
    }];
}

- (UIColor*)getLabelColor:(int)number {
    UIColor *color;
    
    switch (number) {
        case 0:
            color = [UIColor colorWithRed:30.0/255.0
                                    green:180.0/255.0
                                     blue:240.0/255.0
                                    alpha:1.0];
            break;
        case 1:
            color = [UIColor colorWithRed:15.0/255.0
                                    green:215.0/255.0
                                     blue:100.0/255.0
                                    alpha:1.0];
            break;
        case 2:
            color = [UIColor colorWithRed:150.0/255.0
                                    green:90.0/255.0
                                     blue:220.0/255.0
                                    alpha:1.0];
            break;
        case 3:
            color = [UIColor colorWithRed:30.0/255.0
                                    green:90.0/255.0
                                     blue:230.0/255.0
                                    alpha:1.0];
            break;
        case 4:
            color = [UIColor colorWithRed:15.0/255.0
                                    green:150.0/255.0
                                     blue:60.0/255.0
                                    alpha:1.0];
            break;
            
        default:
            break;
    }
    
    return color;
}

- (int)getHeaderBuffer:(int)count {
    switch (count) {
        case 3:
            return 150;
        case 4:
            return 175;
        case 5:
            return 200;
    }
    return 125;
}


@end
