//
//  WaterFileViewTableCell.h
//  GeoDatabaseWetlands
//
//  Created by Jonathan Mason on 10/20/15.
//  Copyright © 2015 Jonathan Mason. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WaterFileViewTableCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *filename;
@property (strong, nonatomic) IBOutlet UILabel *date;

@end
