//
//  PlantCommunityForm.m
//  GeoDatabaseWetlands
//
//  Created by Jonathan Mason on 5/13/15.
//  Copyright (c) 2015 Jonathan Mason. All rights reserved.
//

#import "PlantCommunityForm.h"

#define formWidth 704
#define formHeight 175

#define plantFormManagerPtrKey @"plantFormManagerPtr"
#define formIDKey @"formID"
#define formViewKey @"formView"
#define formBackgroundKey @"formBackground"
#define deleteButtonKey @"deleteButton"
#define takePhotoButtonKey @"takePhotoButton"
#define clearTextButtonKey @"clearTextButton"
#define communityLabelKey @"communityLabel"
#define communityTextKey @"communityText"

@implementation PlantCommunityForm{
    
}

- (id)initWithCommunity:(NSString*)community atYpos:(CGFloat)y {
    if (self = [super init]) {
        [self setUpFormForCommunity:community atYpos:y];
        _formID = community;
        [self addGestures];
    }
    return self;
}

- (id)initWithCommunity:(NSString*)community andText:(NSString*)text {
    if (self = [super init]) {
        [self setUpFormForCommunity:community atYpos:0];
        _formID = community;
        _communityText.text = text;
        [self addGestures];
    }
    return self;
}

- (id)initWithPickerInView:(UIView*)view{
    if (self = [super init]) {
        
    }
    return self;
}

- (void)setUpFormForCommunity:(NSString*)communitySelected atYpos:(CGFloat)y {
    
    self.formView = [[UIView alloc]initWithFrame:CGRectMake(0, y, formWidth, formHeight)];
    
    self.formBackground = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, formWidth, formHeight)];
    [self.formBackground setImage:[UIImage imageNamed:@"communityFormBackground"]];
    [self.formView addSubview:self.formBackground];
    
    self.communityLabel = [[UILabel alloc]initWithFrame:CGRectMake(15, 7, 250, 27)];
    self.communityLabel.text = communitySelected;
    [self.communityLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Bold" size:22]];
    [self.communityLabel setTextColor:[UIColor whiteColor]];
    self.communityLabel.numberOfLines = 0;
    self.communityLabel.lineBreakMode = 0;
    [self.formView addSubview:self.communityLabel];
    
    self.communityText = [[UITextView alloc]initWithFrame:CGRectMake(15, 40, 674, 130)];
    [self.communityText setFont:[UIFont fontWithName:@"HelveticaNeue" size:22]];
    self.communityText.textColor = [UIColor whiteColor];
    [self.communityText setBackgroundColor:[UIColor colorWithWhite:0 alpha:0]]; // set to transparent
    [self.formView addSubview:self.communityText];

    self.clearTextButton = [[UIButton alloc]initWithFrame:CGRectMake(610, 5, 90, 35)];
    [self.clearTextButton setTitle:@"Clear Text" forState:UIControlStateNormal];
    [self.clearTextButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.clearTextButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.clearTextButton.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-CondensedBold" size:20];
    self.clearTextButton.titleLabel.numberOfLines = 1;
    [self.clearTextButton setShowsTouchWhenHighlighted:YES];
    [self.clearTextButton addTarget:self action:@selector(hitClearTextButton:)
               forControlEvents:UIControlEventTouchUpInside];
    [self.formView addSubview:self.clearTextButton];
}

- (void)addGestures {
    UISwipeGestureRecognizer *swipeLeft = [[UISwipeGestureRecognizer alloc]
                                           initWithTarget:self
                                           action:@selector(deleteForm:)];
    swipeLeft.direction = UISwipeGestureRecognizerDirectionLeft;
    [self.formBackground addGestureRecognizer:swipeLeft];
    [self.communityText addGestureRecognizer:swipeLeft];
    UISwipeGestureRecognizer *swipeRight = [[UISwipeGestureRecognizer alloc]
                                            initWithTarget:self
                                            action:@selector(deleteForm:)];
    swipeRight.direction = UISwipeGestureRecognizerDirectionRight;
    [self.formView addGestureRecognizer:swipeRight];
    [self.communityText addGestureRecognizer:swipeRight];
    
    UIPanGestureRecognizer *panGesture = [[UIPanGestureRecognizer alloc]
                                          initWithTarget:self
                                          action:@selector(moveForm:)];
    [panGesture setMinimumNumberOfTouches:2];
    [panGesture setMaximumNumberOfTouches:2];
    [self.formView addGestureRecognizer:panGesture];
}

- (void)addBackgroundImage {
    [self.formBackground setImage:[UIImage imageNamed:@"communityFormBackground"]];
}

/******************************************************************************
 * deleteForm
 *
 * This method creates a UIAlert to ask the user to confirm if they want to
 * delete the form that they are working on.
 ******************************************************************************/
- (void)deleteForm:(id)sender{
    if (self.communityText.text.length > 0) {
        UIAlertController *deleteAlert = [UIAlertController alertControllerWithTitle:@"Delete"
                                                                             message:@"Are you sure you want to delete this form?"
                                                                      preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel"
                                                         style:UIAlertActionStyleCancel
                                                       handler:nil];
        [deleteAlert addAction:cancel];
        
        UIAlertAction *delete = [UIAlertAction actionWithTitle:@"Yes"
                                                         style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction *action) {
                                                           [self deleteForm];
                                                       }];
        [deleteAlert addAction:delete];
        [self.formManagerPtr.plantCommunityPtr presentViewController:deleteAlert animated:YES completion:nil];
    }
    else
        [self deleteForm];
}

- (void)moveForm:(UIPanGestureRecognizer*)pan {
    if (pan.state == UIGestureRecognizerStateChanged) {
        CGPoint center = pan.view.center;
        CGPoint translation = [pan translationInView:pan.view];
        center = CGPointMake(center.x + translation.x, center.y + translation.y);
        pan.view.center = center;
        [pan setTranslation:CGPointZero inView:pan.view];
    }
    else if (pan.state == UIGestureRecognizerStateEnded){
        // adjust positions by animation
        [self.formManagerPtr rearrangeForms];
        
    }
}

/******************************************************************************
 * hitClearTextButton
 *
 * This method creates a UIAlert to ask the user to confirm if they want to
 * delete the form that they are working on.
 ******************************************************************************/
- (void)hitClearTextButton:(id)sender {
    // if text is very short in length then just clear
    if (self.communityText.text.length < 10) {
        self.communityText.text = @"";
    }
    else{ // else give a popup to confirm
        UIAlertController *clearAlert = [UIAlertController alertControllerWithTitle:@"Clear"
                                message:@"Are you sure you want to clear all text?"
                                preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel"
                                style:UIAlertActionStyleCancel handler:nil];
        [clearAlert addAction:cancel];
        
        UIAlertAction *clear = [UIAlertAction actionWithTitle:@"Yes"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction *action) {
            self.communityText.text = @"";
        }];
        [clearAlert addAction:clear];
        
        [self.formManagerPtr.plantCommunityPtr presentViewController:clearAlert animated:YES completion:nil];
    }
}

/******************************************************************************
 * deleteForm
 *
 * This method creates a UIAlert to ask the user to confirm if they want to
 * delete the form that they are working on.
 ******************************************************************************/
- (void)deleteForm {
    //remove from PlantFormManager
    [self.formView removeFromSuperview];
    PlantFormManager *plantFormManager = [PlantFormManager sharedFormManager];
    [plantFormManager removeFormFromArray:self.formID];
    self.formManagerPtr = nil;
    self.formView = nil;
    self.formBackground = nil;
    self.deleteButton = nil;
    self.communityLabel = nil;
    self.communityText = nil;
    self.formID = nil;
}

/******************************************************************************
 * launchCameraControllerFromViewController
 *
 * This method launches the camera by using UIImagePickerController class,
 * and setting sourceType to default camera w/ no user editing, and self delegate.
 ******************************************************************************/
-(BOOL) launchCameraControllerFromViewController:(UIViewController*) controller usingDelegate:
(id <UIImagePickerControllerDelegate, UINavigationControllerDelegate>) delegate {
    
    //variable to check whether there is a camera available
    BOOL truefalse = [UIImagePickerController isSourceTypeAvailable:
                      UIImagePickerControllerSourceTypeCamera];
    
    //if there is a camera, the delegate passed exists, and the controller
    //passed exists, proceed on, otherwise don't go any further
    if (!truefalse || (delegate == nil) || (controller == nil)) {
        NSLog(@"no can do, delegate/camera/view controller doesn't exist!");
        return NO;
    }
    
    // set cameraController properties
    UIImagePickerController *cameraController = [[UIImagePickerController alloc] init];
    cameraController.sourceType = UIImagePickerControllerSourceTypeCamera;
    cameraController.allowsEditing = NO;
    cameraController.delegate = delegate;
    
    [controller presentViewController:cameraController animated:YES completion:nil];
    
    return false;
}

// Handles when user hits the cancel button
- (void) imagePickerControllerDidCancel:(UIImagePickerController *) picker {
    [picker dismissViewControllerAnimated:YES completion:nil]; //dismisses the camera controller
    NSLog(@"camera picker dismissed,");
    
}

@end
