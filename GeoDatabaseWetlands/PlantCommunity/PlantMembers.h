//
//  PlantMembers.h
//  GeoDatabaseWetlands
//
//  Created by Jonathan Mason on 2/25/15.
//  Copyright (c) 2015 Jonathan Mason. All rights reserved.
//
//  This software is intended soley for the University of Washington Bothell.

#import <Foundation/Foundation.h>

// See comment at top of file for a complete description
@interface PlantMembers : NSObject <UIPickerViewDelegate, UIPickerViewAccessibilityDelegate, UIPickerViewDataSource>

@property (weak, nonatomic) id objectChosen;
@property (strong, nonatomic) NSDictionary *communities;
@property (strong, nonatomic) NSDictionary *memberNotes;
@property (strong, nonatomic) UIPickerView *communityPicker;
@property (strong, nonatomic) NSMutableArray *memberArray;

+(PlantMembers*)sharedPlantMembers;
- (id)returnSelection;

- (void)resetPicker;
- (void)addCommunityToPicker:(NSString*)community;
- (void)removeCommunityFromPicker:(NSString*)community;
- (void)updatePickerByRemovingLoadedEntries:(NSMutableArray*)array;

@end
