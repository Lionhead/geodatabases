//
//  PlantCommunity.m
//  GeoDatabaseWetlands
//
//  Created by Jonathan Mason on 5/12/14.
//  Copyright (c) 2014 Jonathan Mason. All rights reserved.
//
/******************************************************************************
 * PlantCommunity class inherits from UIViewController, and contains the
 * methods and variables used to track data from the wetlands. This controls
 * the main interface of the app and is the most used. 
 *****************************************************************************/

#import "IDGenerator.h"
#import "PlantCommunity.h"
#import "TableFileViewer.h"
#import "PhotoViewer.h"
#import "HomeScreen.h"
#import "FilesToServer.h"
#import "PlantCommunityForm.h"
#import "ConnectionWatcher.h"
#import "WeatherOptions.h"
#import "TimeUnits.h"
#import "UploadData.h"
#import "HomeDataForm.h"
#import "UserProvenance.h"
#import "SystemSounds.h"
#import "MailAttachment.h" //CSS490: Added MailAttachment.h

#define formKey @"keyForForms"
#define csvFolder @"plantCommunityCSV"
#define PlantCommunityFolder @"PlantCommunity"
#define CommunityScrollViewWidth 704
#define CommunityScrollViewHeight 709
#define CommunityFormHeight 175
#define CommunityFormWidth 704
#define plantType @"plant"

@interface PlantCommunity () <UITextFieldDelegate, UITextViewDelegate, UIScrollViewDelegate>{
    UIButton *okButtonForPicker;
    UIButton *cancelButtonForPicker;
    UIImageView *pickerBackground;
    CSVformatting *csvFileFormatter;
}

@property (strong, nonatomic) IBOutlet UILabel *displayedTimeLabel;

@end

@implementation PlantCommunity

- (void)viewWillAppear:(BOOL)animated{

    if (self.vcTracker.currentViewController != self) {
        self.vcTracker.currentViewController = self;
    }
    
    
    if (self.plantFormManager != nil) {
        [self refreshScreen]; // adds the forms back to the view
    }
    self.loggedInAs.text = [NSString stringWithFormat:@"Logged in as: %@",self.userInfo.username];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.vcTracker = [UIViewControllerTracker sharedVC];
    self.vcTracker.currentViewController = self;
    self.userInfo = [UserInfo sharedUserInfo];

    self.dropDownMenu = [[PlantCommMenu alloc]initWithPlant:self];
    [self prepDropDownMenuOnLoad];
    
    self.loggedInAs.text = [NSString stringWithFormat:@"Logged in as: %@",self.userInfo.username];
    self.loggedInAs.textColor = [UIColor lightGrayColor];
    [self checkUserInfoSettings];
    
    [self setupBackButton];
    [self prepNavigationControllerOnLoad];
    self.device = [[UIDevice alloc] init]; // alloc mem for UIDevice to get device name when called
    
    [self addObserversOnLoad];
    [self prepScrollViewOnLoad];
    
    [self setDateOnStartOrReset:NO]; // displays the date and time on the GUI if not present already
    // display working filename if created or loaded
}

- (void)checkUserInfoSettings{
    if ([self.userInfo.plantFormID isEqualToString:@""]) {
        self.userInfo.plantFormID = [IDGenerator getFormID];
    }
    if (self.userInfo.plantWorkingFilename && ![self.userInfo.plantWorkingFilename isEqualToString:@""]) {
            self.displayedFilename.text = self.userInfo.plantWorkingFilename;
    }
}

/***************************************************************************************************
 * prepNavigationControllerOnLoad
 *
 * This method sets the navigation controller properties for this class.
 **************************************************************************************************/
-(void)prepNavigationControllerOnLoad {
    self.navigationController.navigationBarHidden = YES;
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.view.backgroundColor = [UIColor clearColor];
}

// scrolls the view up if the keyboard blocks the textView
- (void)textViewDidBeginEditing:(NSNotification*)textView{
    // if textView being edited is the 5th in container
    if (self.plantFormManager.arrayOfPlantForms.count >= 4) {
        PlantCommunityForm *tempForm;
        UITextView *tempText;
        NSUInteger index = 0;
        for (int i = 3; i < self.plantFormManager.arrayOfPlantForms.count; i++) {
            tempForm = self.plantFormManager.arrayOfPlantForms[i];
            tempText = tempForm.communityText;
            if ([textView.object isEqual:tempText]) {
                index = i;
            }
        }
        if (index == 3) {
            [self.scroller setContentOffset:CGPointMake(0.0, 165) animated:YES];
        }
        else if (index >= 4){
            [self.scroller setContentOffset:CGPointMake(0.0, 290) animated:YES];
        }
    }
}

- (void)textViewDidEndEditing:(NSNotification*)textView{
    if (self.plantFormManager.arrayOfPlantForms.count > 3) {
        [self.scroller setContentOffset:CGPointMake(0.0, -0) animated:YES];
    }
}

- (IBAction)retractKeyboard:(id)sender {
    //self.scroller.scrollEnabled = NO;
    [self resignFirstResponder];
}

// Checks if we have an internet connection or not
- (BOOL)testWifiConnection{
    self.internetReachable = [Reachability reachabilityForLocalWiFi];
    [self.internetReachable startNotifier];
    return self.internetReachable.currentReachabilityStatus;
}

/******************************************************************************
 * This method is reserved for observers.
 *
 * The observed properties: authenticationCheckComplete, changedPassword
 * When either property is edited the appropriate change will be made, which
 * can include approval of user login, and changing the user's password.
 ******************************************************************************/
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object
                        change:(NSDictionary *)change context:(void *)context
{
    if ([keyPath isEqualToString:@"uploadSuccess"])
    {
        if(self.uploadForm.uploadSuccess == YES){
            UIAlertController *uploadSuccess = [UIAlertController alertControllerWithTitle:@"Success"
                            message:[NSString stringWithFormat:@"Your data has been uploaded to the server under the name: %@",
                                                                                            self.login.username]
                            preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:nil];
            [uploadSuccess addAction:ok];
            [self presentViewController:uploadSuccess animated:YES completion:nil];
        }
        else{
            NSInteger connection = [self testWifiConnection];
            if (connection == 0) {
                UIAlertController *uploadError = [UIAlertController alertControllerWithTitle:@"Error"
                            message:@"You are currently not connected to wifi or the signal is too weak."
                            preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:nil];
                [uploadError addAction:ok];
                [self presentViewController:uploadError animated:YES completion:nil];
            }
            else{
                UIAlertController *uploadError = [UIAlertController alertControllerWithTitle:@"Error"
                             message:@"You are unable to connect to the server at this moment."
                            preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:nil];
                [uploadError addAction:ok];
                [self presentViewController:uploadError animated:YES completion:nil];
            }
        }
    }
    if ([keyPath isEqualToString:@"downloadSuccess"])
    {
        if(self.uploadForm.downloadSuccess == YES){
            UIAlertController *downloadSuccess = [UIAlertController alertControllerWithTitle:@"Success"
                            message:@"Your data has been downloaded from the server."
                            preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:nil];
            [downloadSuccess addAction:ok];
            [self presentViewController:downloadSuccess animated:YES completion:nil];
        }
        else{
            UIAlertController *downloadError = [UIAlertController alertControllerWithTitle:@"Error"
                            message:@"Your data could not be downloaded. Please check your connection."
                            preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:nil];
            [downloadError addAction:ok];
            [self presentViewController:downloadError animated:YES completion:nil];
        }
    }
}

- (void)saveToDatabase:(PlantCommunityProvenance*)plantProv{
    // persist data
    RLMRealm *realm = [RLMRealm defaultRealm];
    [realm beginWriteTransaction];
    [realm addObject:plantProv];
    [realm commitWriteTransaction];
}

- (PlantCommunityProvenance*)configureDatabaseSave:(PlantCommunityProvenance*)plantProv{
    plantProv.formId = self.userInfo.plantFormID;
    plantProv.deviceId = [IDGenerator getDeviceId];
    plantProv.username = self.userInfo.username;
    plantProv.filename = self.userInfo.plantWorkingFilename;
    plantProv.researchers = self.userInfo.researchers;
    plantProv.temperature = self.userInfo.currentTemp;
    plantProv.weather = [self getWeather];
    plantProv.project = self.userInfo.project;
    plantProv.classNumber = [self getClassNumber];
    plantProv.instructor = self.userInfo.instructor;
    plantProv.dateStarted = self.userInfo.plantDateStarted;
    plantProv.timeStarted = self.userInfo.plantTimeStarted;
    plantProv.alderHairGrass = [self plantNotesFor:@"Alder-Hairgrass"];
    plantProv.alderShrub = [self plantNotesFor:@"Alder-Shrub"];
    plantProv.alderSloughSedge = [self plantNotesFor:@"Alder-Slough Sedge"];
    plantProv.conifer = [self plantNotesFor:@"Conifer"];
    plantProv.creekBuffer = [self plantNotesFor:@"Creek Buffer"];
    plantProv.edgeGrassBuffer = [self plantNotesFor:@"Edge Grass Buffer"];
    plantProv.edgeTreeBuffer = [self plantNotesFor:@"Edge Tree Buffer"];
    plantProv.logJam = [self plantNotesFor:@"Log Jam"];
    plantProv.meadow = [self plantNotesFor:@"Meadow"];
    plantProv.pointBar = [self plantNotesFor:@"Point Bar"];
    plantProv.shadyDepression = [self plantNotesFor:@"Shady Depression"];
    plantProv.shrubThicket = [self plantNotesFor:@"Shrub Thicket"];
    plantProv.sunnyDepression = [self plantNotesFor:@"Sunny Depression"];
    
    return plantProv;
}


/*
 * popupToGetFilenameForSavingSpreadsheet:
 *
 * Pops up alert to save spreadsheet locally.
 *
 * Written by: Team LionHead
 * CSS 490 Data Provenance Spring 2016
 */
- (void)popupToGetFilenameForSavingSpreadsheet {
    NSLog(@"Save Spread Plant");
    UIAlertController *alert = [UIAlertController
                                alertControllerWithTitle:@"Enter Filename for Saving"
                                message:nil
                                preferredStyle:UIAlertControllerStyleAlert];
    
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Enter file name here";
        textField.keyboardType = UIKeyboardAppearanceDark;
    }];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel"
                                                           style:UIAlertActionStyleCancel
                                                         handler:nil];
    [alert addAction:cancelAction];
    
    UIAlertAction *saveLocallyAction = [UIAlertAction actionWithTitle:@"OK"
                                                                style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * action)
                                        {
                                            UITextField *filenameField = alert.textFields.firstObject;
                                            if ([self filenameExists:filenameField.text]) {
                                                NSLog(@"File name exists");
                                                [self getNewFilenameInsteadOfCurrent:filenameField.text];
                                            }
                                            else{
                                                NSLog(@"File name does not exist"); 
                                                for (id object in self.plantFormManager.arrayOfPlantForms) {
                                                    PlantCommunityForm *form = object;
                                                    form.filename.text = filenameField.text;
                                                    PlantCommunityProvenance *pcp = [PlantCommunityProvenance new];
                                                    pcp = [self configureDatabaseSave:pcp];
                                                    [self saveToDatabase:pcp];
                                                }
                                                [[NSNotificationCenter defaultCenter] postNotificationName:@"filenameEntered" object:nil];
                                                [[NSNotificationCenter defaultCenter] removeObserver:self];
                                            }
                                            
                                        }];
    [alert addAction:saveLocallyAction];
    [self presentViewController:alert animated:YES completion:nil];
}

/*
 * filenameExists:
 *
 * Pops up alert to save spreadsheet locally.
 *
 * Input Parameters:
 *  stringValue: the string value for validating if the filename exists.
 *
 * Output Parameters:
 *  YES if the file exists, NO otherwise
 *
 * Written by: Team LionHead
 * CSS 490 Data Provenance Spring 2016
 */

- (BOOL)filenameExists:(NSString*)filename {
    RLMResults *results = [PlantCommunityProvenance objectsWhere:
                           [NSString stringWithFormat:@"filename = '%@'",filename]];
    if (results.count > 0) {
        return YES;
    }
    return NO;
}

/*
 * ceateSpreadsheet:
 *
 * Creates the spreadsheet and call popupAskToEmailSpreadsheetForFile to allow user to email their CSV file.
 *
 * Input Parameters:
 *  stringValue: the string value for validating if the filename exists.
 *
 * Output Parameters:
 *  YES if the file exists, NO otherwise
 *
 * Written by: Team LionHead
 * CSS 490 Data Provenance Spring 2016
 */

- (void)createSpreadsheet {
    // first check if there is already a filename, if not get one first, then proceed
    
    NSString *filename = [self getFirstFilename];
//    if ([filename isEqualToString:@""]) {
//         NSLog(@"Is empty");
//        [self popupToGetFilenameForSavingSpreadsheet];
//        [[NSNotificationCenter defaultCenter] addObserver:self
//                                                 selector:@selector(createSpreadsheet)
//                                                     name:@"filenameEntered"
//                                                   object:nil];
//    }
//    else{
//Commented out because of problem with arrayOfPlantForms not populating.
        NSLog(@"Is not empty");
        NSString *spreadsheet = [CSVformatting formatPlantFormsIntoCSV:self.plantFormManager withUserInfo:self.userInfo andPlantCommunity:self];
        NSString *savePath = [LocalFileManager checkForDirectoryForUser:self.userInfo.username
                                                         inSubdirectory:PlantCommunityFolder];
        if (savePath) {
            NSString * path = [NSString stringWithFormat:@"%@/%@.csv",savePath,filename];
            BOOL save = [LocalFileManager saveAsCSVFile:spreadsheet withPathAndName:path];
            
            //  TODO  email spreadsheet
            if (save) {
                
                NSData *fileData = [NSData dataWithContentsOfFile:path];
                MailAttachment *mail = [[MailAttachment alloc] initWithViewController:(UIViewController*)self];
                [mail popupAskToEmailSpreadsheetForFile:fileData withName:filename];
            }
            
        }
        else{
            NSString *error = @"Could not save as CSV file. Please notify Office of Research.";
            [self showError:error];
        }
  //  }

}

/*
 * getFirstFilename:
 *
 * Retrieves the index 0 of arrayOfPlantForms
 *
 * Input Parameters:
 *  stringValue: the string value for validating if the filename exists.
 *
 * Output Parameters:
 *  YES if the file exists, NO otherwise
 *
 * Written by: Team LionHead
 * CSS 490 Data Provenance Spring 2016
 */
- (NSString*)getFirstFilename {
    for (int i = 0; i < self.plantFormManager.arrayOfPlantForms.count; i++) {
        PlantCommunityForm *form = self.plantFormManager.arrayOfPlantForms[i];
        if (form.filename.text && ![form.filename.text isEqualToString:@""]) {
            return form.filename.text;
        }
    }
    return @"";
}

/*
 * showError:
 *
 * Gets called for unsuccessful operation.
 *
 * Input Parameters:
 *  stringValue: This is the string that the user would like to show for the error message.
 *
 *
 * Written by: Team LionHead
 * CSS 490 Data Provenance Spring 2016
 */
- (void)showError:(NSString*)errorMessage {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error"
                                                                   message:errorMessage
                                                            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Ok"
                                                     style:UIAlertActionStyleCancel
                                                   handler:nil];
    [alert addAction:cancel];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)uploadData{
    
    NSInteger test = [self testConnectionToWifi];
    // user is approved to save to server and connection exists, also create spreadsheet
    if (self.userInfo.approvedUser == YES && test == 1) { // use guest only for testing  self.login.approvedUser == YES &&
        self.uploadForm = [DirectUploadForm sharedUploadFormWithUserInfo:self.userInfo];
        UploadData *uploadData = [[UploadData alloc]initWithParameters:[self getPostParameters]];
        uploadData.dataType = plantType;
        [self.uploadForm postFormDataToServer:uploadData.data ofType:uploadData.dataType];

    }
    // user is approved but there is no connection to server
    // check for connection later while saving spreadsheet
    else if (self.userInfo.approvedUser == YES && test != 1){
        if (!csvFileFormatter) {
            csvFileFormatter = [[CSVformatting alloc]init];
        }
        [ConnectionWatcher startConnectionWatcher]; // will check every 5 mins for connection
        [self popupNoWifiForSaving];
    }
    else{ // guest login, save spreadsheet only
        
        /*
        if (!csvFileFormatter) {
            csvFileFormatter = [[CSVformatting alloc]init];
        }
         */
    }
    
}

- (void)popupNoWifiForSaving{
    UIAlertController *errorAlert = [UIAlertController
                                     alertControllerWithTitle:@"Notice"
                                                    message:@"There is no connection to the server."
                                     " Data will be saved locally."
                                     " The app will automatically check later for a connection"
                                     " and prompt you to upload when ready."
                                                preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel"
                                                     style:UIAlertActionStyleCancel
                                                   handler:^(UIAlertAction * _Nonnull action)
    {
        [ConnectionWatcher stopWatching];
    }];
    [errorAlert addAction:cancel];
    
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK"
                                                 style:UIAlertActionStyleDefault
                                               handler:^(UIAlertAction *action)
     {
         [self saveButton:nil];
     }];
    [errorAlert addAction:ok];
    
    [self presentViewController:errorAlert animated:YES completion:^{
        
    }];
}

- (void)saveAs{
    [self popupToGetFilenameForSaving];
}

/******************************************************************************
 * popupToGetFilenameForSaving
 *
 * This method gives the user a popup for retrieving a file name for saving.
 ******************************************************************************/
- (void)popupToGetFilenameForSaving{
    UITextField *temp;
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Enter File Name"
                    message:nil
                    preferredStyle:UIAlertControllerStyleAlert];
    
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Enter file name here";
        textField.keyboardType = UIKeyboardAppearanceDark;
        temp.text = textField.text;
    }];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel"
            style:UIAlertActionStyleCancel handler:^(UIAlertAction * action)
    {
        
    }];
    [alert addAction:cancelAction];
    
    UIAlertAction *saveLocallyAction = [UIAlertAction actionWithTitle:@"OK"
            style:UIAlertActionStyleDefault
        handler:^(UIAlertAction * action)
        {
            UITextField *filenameField = alert.textFields.firstObject;
            // check database if there is already a matching filename
            RLMResults *results = [PlantCommunityProvenance objectsWhere:[NSString stringWithFormat:
                        @"username = '%@' AND filename = '%@'",self.userInfo.username,filenameField.text]];
            // filename already exists
            if ([results count] > 0) {
                [self getNewFilenameInsteadOfCurrent:filenameField.text];
            }
            else{
                self.userInfo.plantWorkingFilename = filenameField.text;
                self.displayedFilename.text = filenameField.text;
                
                PlantCommunityProvenance *plantProv = [PlantCommunityProvenance new];
                plantProv = [self configureDatabaseSave:plantProv];
                [self saveToDatabase:plantProv];

            }
        }];
    [alert addAction:saveLocallyAction];
    [self presentViewController:alert animated:YES completion:^{
        
    }];
}

// adds the current parameters to be uploaded into a queue, which will run as soon
// as there is a connection to the server again
- (void)queueFormUpload{
    self.uploadForm = [DirectUploadForm sharedUploadFormWithUserInfo:self.userInfo];
    UploadData *uploadData = [[UploadData alloc]initWithParameters:[self getPostParameters]];
    uploadData.dataID = self.userInfo.plantWorkingFilename;
    uploadData.dataType = plantType;
    self.userInfo.uploadQueue = [UploadQueue sharedUploadQueue]; // create queue for uploads
    [self.userInfo.uploadQueue queueObject:uploadData]; // queue parameters
}

- (void)loadDataFromDatabase:(PlantCommunityProvenance*)data{
    if (self.plantFormManager.arrayOfPlantForms.count > 0) {
        [self.plantFormManager deleteAllForms];
    }
    [self.plantMembers resetPicker];
    // ask to update userInfo
    [self popupReplaceUserInfo:data];
}

/***************************************************************************************************
 * popupReplaceUserInfo
 *
 * This method gives the user a popup asking if they want to also replace all
 * the current user information entered on the home screen with the user
 * information that was saved when the file being opened was saved.
 **************************************************************************************************/
- (void)popupReplaceUserInfo:(PlantCommunityProvenance*)data{
    UserInfo *userInfo = [UserInfo sharedUserInfo];
    
    UIAlertController *warning = [UIAlertController
                                  alertControllerWithTitle:@"Replace User Information on Home Screen?"
                                  message:@"Select Yes, if you also want to replace all the user information"
                                  " on the Home Screen with what was entered when this file was saved."
                                  preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *no = [UIAlertAction actionWithTitle:@"No"
                                                 style:UIAlertActionStyleDefault
                                               handler:^(UIAlertAction *action)
    {
        self.displayedFilename.text = data.filename;
        userInfo.plantWorkingFilename = data.filename;
        [self updateTimeLabelWithDate:self.userInfo.plantDateStarted andTime:self.userInfo.plantTimeStarted];
        [self loadPlantCommunitiesFromDatabase:data];
   }];
    [warning addAction:no];
    UIAlertAction *yes = [UIAlertAction actionWithTitle:@"Yes"
                                                  style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction *action)
    {
        self.displayedFilename.text = data.filename;
        userInfo.plantWorkingFilename = data.filename;
        [self updateTimeLabelWithDate:data.dateStarted andTime:data.timeStarted];
        userInfo.researchers = data.researchers;
        userInfo.project = data.project;
        userInfo.classNumber = data.classNumber;
        userInfo.instructor = data.instructor;
        userInfo.currentTemp = data.temperature;
        userInfo.weatherConditions = data.weather;
        [userInfo setUpdateHomeScreen:YES];
        [self loadPlantCommunitiesFromDatabase:data];
    }];
    [warning addAction:yes];
    [self presentViewController:warning animated:YES completion:nil];
}



// update all plant communities being edited
- (void)loadPlantCommunitiesFromDatabase:(PlantCommunityProvenance*)data{
    self.plantFormManager = [self returnFormManager];
    if (![data.alderHairGrass isEqualToString:@""]) {
        [self createForm:@"Alder-Hairgrass" withText:data.alderHairGrass];
    }
    if (![data.alderShrub isEqualToString:@""]) {
        [self createForm:@"Alder-Shrub" withText:data.alderShrub];
    }
    if (![data.alderSloughSedge isEqualToString:@""]) {
        [self createForm:@"Alder-Slough Sedge" withText:data.alderSloughSedge];
    }
    if (![data.conifer isEqualToString:@""]) {
        [self createForm:@"Conifer" withText:data.conifer];
    }
    if (![data.creekBuffer isEqualToString:@""]) {
        [self createForm:@"Creek Buffer" withText:data.creekBuffer];
    }
    if (![data.edgeGrassBuffer isEqualToString:@""]) {
        [self createForm:@"Edge Grass Buffer" withText:data.edgeGrassBuffer];
    }
    if (![data.edgeTreeBuffer isEqualToString:@""]) {
        [self createForm:@"Edge Tree Buffer" withText:data.edgeTreeBuffer];
    }
    if (![data.logJam isEqualToString:@""]) {
        [self createForm:@"Log Jam" withText:data.logJam];
    }
    if (![data.meadow isEqualToString:@""]) {
        [self createForm:@"Meadow" withText:data.meadow];
    }
    if (![data.pointBar isEqualToString:@""]) {
        [self createForm:@"Point Bar" withText:data.pointBar];
    }
    if (![data.shadyDepression isEqualToString:@""]) {
        [self createForm:@"Shady Depression" withText:data.shadyDepression];
    }
    if (![data.shrubThicket isEqualToString:@""]) {
        [self createForm:@"Shrub Thicket" withText:data.shrubThicket];
    }
    if (![data.sunnyDepression isEqualToString:@""]) {
        [self createForm:@"Sunny Depression" withText:data.sunnyDepression];
    }
    
    [self refreshScreen];
}

/******************************************************************************
 * loadDataFromPath
 *
 * This method loads in the locally saved data
 ******************************************************************************/
- (void)loadDataFromPath:(NSString*)path{
    NSString *filePath;
    NSError *error;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask,YES);
    filePath = [[paths objectAtIndex:0] stringByAppendingPathComponent:
            [NSString stringWithFormat:@"/%@/%@/%@",
             PlantCommunityFolder,self.userInfo.username,path]];
    
    NSArray *contents = [[NSFileManager defaultManager]
                         contentsOfDirectoryAtPath: filePath error:&error];
    
    if (error) {
        NSLog(@"Error [loadDataFromPath] Desc:%@  Domain:%@",error.description,error.domain);
        return;
    }
    if (contents.count > 0) { // check if any data was saved (something wrong if not)
        NSError *readingError;
        if (self.plantFormManager.arrayOfPlantForms.count > 0) {
            [self.plantFormManager deleteAllForms];
        }
        PlantCommunityForm *communityForm;
        id object;
        for (object in contents) {
            NSData *data = [NSData dataWithContentsOfFile:
                            [NSString stringWithFormat:@"%@/%@",filePath,object]
                            options:NSDataReadingUncached error:&readingError];
            
            if ([object isEqualToString:@"userInfo"]) {
                if (self.userInfo.clickedLoadDataWithUserInfo == YES) {
                    UserInfo *info = [NSKeyedUnarchiver unarchiveObjectWithData:data];
                    [self.userInfo updateUserInfoWithFileLoaded:info];
                    info = nil;
                }
            }
            else if ([object isEqualToString:@"weather"]) {
                if (self.userInfo.clickedLoadDataWithUserInfo == YES) {
                    WeatherOptions *weather = [NSKeyedUnarchiver unarchiveObjectWithData:data];
                    [self.userInfo.weatherOptionsPtr copyLoadedWeatherOptions:weather];
                    [self.userInfo.weatherOptionsPtr refreshWeatherSelection];
                    weather = nil;
                }
            }
            else{ // object is a community form
                communityForm = [NSKeyedUnarchiver unarchiveObjectWithData:data];
                // create a new form object and copy this forms text over to it
                PlantCommunityForm *newForm = [[PlantCommunityForm alloc]
                                               initWithCommunity:communityForm.formID
                                                andText:communityForm.communityText.text];
                newForm.formManagerPtr = self.plantFormManager;
                [self.plantFormManager.arrayOfPlantForms addObject:newForm];
                [self.plantFormManager.plantMembersPtr
                 removeCommunityFromPicker:communityForm.formID];
            }
        }
        communityForm = nil;
    }
    self.userInfo.clickedLoadDataWithUserInfo = NO;
}

/******************************************************************************
 * addCommunityForm
 *
 * This method dynamically adds new communities to the scrolling box where the
 * user selects a community to add notes to.
 ******************************************************************************/
- (void)addCommunityForm{
    self.plantMembers = [PlantMembers sharedPlantMembers];
    if ([self pickerOkToAppear]) {
        self.plantMembers.objectChosen = self.plantMembers.memberArray[0];
        pickerBackground = [[UIImageView alloc]initWithFrame:CGRectMake(214, 140, 280, 180)];
        [pickerBackground setImage:[UIImage imageNamed:@"plantCommUIPickerBackground"]];
        [pickerBackground setUserInteractionEnabled:YES];
        UITapGestureRecognizer *tapGesture = [UITapGestureRecognizer new];
        tapGesture.numberOfTapsRequired = 0;
        [pickerBackground addGestureRecognizer:tapGesture];
        [pickerBackground addSubview:self.plantMembers.communityPicker];
        [self.communityFormView addSubview:pickerBackground];
        
        okButtonForPicker = [[UIButton alloc]initWithFrame:CGRectMake(368, 330, 95, 40)];
        [okButtonForPicker setImage:[UIImage imageNamed:@"plantCommUIPickerOKButton"]
                           forState:UIControlStateNormal];
        [okButtonForPicker addTarget:self action:@selector(hitPickerOkButton:)
                forControlEvents:UIControlEventTouchUpInside];
        [self.communityFormView addSubview:okButtonForPicker];
        
        cancelButtonForPicker = [[UIButton alloc]initWithFrame:CGRectMake(243, 330, 95, 40)];
        [cancelButtonForPicker setImage:[UIImage imageNamed:@"plantCommUIPickerCancelButton"]
                               forState:UIControlStateNormal];
        [cancelButtonForPicker addTarget:self action:@selector(hitPickerCancelButton:)
                forControlEvents:UIControlEventTouchUpInside];
        [self.communityFormView addSubview:cancelButtonForPicker];
    }
}

/******************************************************************************
 * pickerOkToAppear
 *
 * This method checks to see if the community picker should appear.
 ******************************************************************************/
- (BOOL)pickerOkToAppear{
    if (self.plantMembers.memberArray.count > 0){
        if  (pickerBackground == nil) {
            return YES;
        }
        else if (pickerBackground != nil && pickerBackground.hidden == YES) {
            return YES;
        }
        else
            return NO;
    }
    else{
        UIAlertController *warning = [UIAlertController alertControllerWithTitle:@"Note"
                                     message:@"All communities have been added already"
                                     preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *dismiss = [UIAlertAction actionWithTitle:@"OK"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction *action){}];
        [warning addAction:dismiss];
        [self presentViewController:warning animated:YES completion:nil];
    }
    return NO;
}

/******************************************************************************
 * returnPlantFormManager
 *
 * This method returns the singleton plantFormManager with pointers to
 * PlantCommunity and PlantMembers objects set.
 ******************************************************************************/
- (PlantFormManager*)returnFormManager{
    self.plantFormManager = [PlantFormManager sharedFormManager];
    // the manager must later reference the members
    self.plantFormManager.plantMembersPtr = self.plantMembers;
    self.plantFormManager.plantCommunityPtr = self;
    return self.plantFormManager;
}

/******************************************************************************
 * hitPickerOkButton
 *
 * This method
 ******************************************************************************/
- (void)hitPickerOkButton:(id)sender{
    [SystemSounds playTockSound];
    // 1. Close Picker Window
    [pickerBackground setHidden:YES];
    pickerBackground = nil;
    [sender setHidden:YES];
    sender = nil;
    cancelButtonForPicker.hidden = YES;
    cancelButtonForPicker = nil;
    
    [self createForm:[self.plantMembers returnSelection] withText:@""];
}

- (void)createForm:(NSString*)form withText:(NSString*)text{
    // 2. Create the form and add to PlantFormManager array
    self.plantFormManager = [self returnFormManager];
    [self.plantFormManager createPlantCommunityForm:form withText:text];
    
    // 3. Now see where it fits and if we need to increase size of communityScrollView3
    [self setCommunityScrollViewSizeAndScrollEnable];
    
    // 4. Add to communityFormsArray and subview
    [self.communityScrollView addSubview:self.plantFormManager.returnCurrentForm.formView];
}

// Close Picker Window and Ok Button
- (void)hitPickerCancelButton:(id)sender{
    [SystemSounds playTockSound];
    [pickerBackground setHidden:YES];
    pickerBackground = nil;
    [sender setHidden:YES];
    sender = nil;
    [okButtonForPicker setHidden:YES];
    okButtonForPicker = nil;
}

// adjusts the size of the scroller once it fills up to accomodate more forms
- (void)setCommunityScrollViewSizeAndScrollEnable{
    if (self.plantFormManager.arrayOfPlantForms.count > 4) {
        self.communityScrollView.contentSize =
                CGSizeMake(CommunityScrollViewWidth,
                ((self.plantFormManager.arrayOfPlantForms.count)* CommunityFormHeight));
    }
    else
        self.communityScrollView.contentSize =
                CGSizeMake(CommunityScrollViewWidth, CommunityScrollViewHeight);
}

/******************************************************************************
 * startOver
 *
 * This method removes resets the time and deletes all the forms that were
 * added. The user will be prompted with a warning if they have more than one
 * form added.
 ******************************************************************************/
- (IBAction)startOver:(id)sender {
    // if there is more than one form created
    if (self.plantFormManager.arrayOfPlantForms.count > 0) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Warning"
                                message:@"Are you sure you want to erase all data and start over? "
                                    "Note: Information on the home page will remain."
                                preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel"
                                        style:UIAlertActionStyleCancel
                                        handler:^(UIAlertAction *action) {}];
        [alert addAction:cancel];
        UIAlertAction *yesAction = [UIAlertAction actionWithTitle:@"Yes"
                                        style:UIAlertActionStyleDefault
                                        handler:^(UIAlertAction *action)
        {
            [self.plantFormManager deleteAllForms];
            [self setDateOnStartOrReset:YES];
            [self setCommunityScrollViewSizeAndScrollEnable];
            self.userInfo.plantWorkingFilename = @"";
            self.userInfo.plantFormID = [IDGenerator getFormID];
            self.displayedFilename.text = @"";          
        }];
        [alert addAction:yesAction];
        [self presentViewController:alert animated:YES completion:nil];
    }
    else if (self.plantFormManager.arrayOfPlantForms.count == 1){
        // if only one then just delete
        [self.plantFormManager deleteAllForms];
        [self setDateOnStartOrReset:YES];
        [self setCommunityScrollViewSizeAndScrollEnable];
        self.userInfo.plantWorkingFilename = @"";
        self.displayedFilename.text = @"";
    }
}

/******************************************************************************
 * getNewFilenameInsteadOfCurrent
 *
 * This method presents the user with a popup asking to enter a new file name.
 ******************************************************************************/
- (void)getNewFilenameInsteadOfCurrent:(NSString*)currentFilename{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Warning"
                    message:@"That file name already exists. You can enter a new name below, "
                                "or select to overwrite the current file."
                    preferredStyle:UIAlertControllerStyleAlert];
    
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Enter new file name here";
        textField.keyboardType = UIKeyboardAppearanceDark;
    }];
    
    UIAlertAction *newNameAction = [UIAlertAction actionWithTitle:@"Ok With New Name"
                    style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
    {
        UITextField *temp = alert.textFields.firstObject;
        // if a file by that name already exists, ask for new name
        // check database if there is already a matching filename
        RLMResults *results = [PlantCommunityProvenance objectsWhere:[NSString stringWithFormat:
                          @"username = '%@' AND filename = '%@'",self.userInfo.username,temp.text]];
        // filename already exists
        if ([results count] > 0 || [temp.text isEqualToString:currentFilename]) {
            [self getNewFilenameInsteadOfCurrent:temp.text];
        }
        else{
            self.userInfo.plantWorkingFilename = temp.text;
            self.displayedFilename.text = temp.text;
            PlantCommunityProvenance *plantProv = [PlantCommunityProvenance new];
            plantProv = [self configureDatabaseSave:plantProv];
            [self saveToDatabase:plantProv];
        }
    }];
    [alert addAction:newNameAction];
    
    UIAlertAction *overwriteAction = [UIAlertAction actionWithTitle:@"Overwrite"
                                        style:UIAlertActionStyleDefault
                                        handler:^(UIAlertAction * action)
    {
        // update Realm object
        RLMResults *dataToUpdate = [PlantCommunityProvenance objectsWhere:[NSString stringWithFormat:
                    @"username = '%@' AND filename = '%@'",self.userInfo.username,currentFilename]];
        
        PlantCommunityProvenance *plantProv = [dataToUpdate firstObject];
        // perform update
        RLMRealm *realm = [RLMRealm defaultRealm];
        [realm beginWriteTransaction];
        plantProv = [self configureDatabaseSave:plantProv];
        [realm commitWriteTransaction];
        self.userInfo.plantWorkingFilename = currentFilename;
        self.displayedFilename.text = currentFilename;
    }];
    [alert addAction:overwriteAction];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel"
                                        style:UIAlertActionStyleDefault
                                        handler:nil];
    [alert addAction:cancelAction];
    
    [self presentViewController:alert animated:YES completion:nil];
}

/******************************************************************************
 * clickedAddCommunityButton
 *
 * This method handles the action when the user clicks the Add Community button
 * by calling the addCommunityForm method.
 ******************************************************************************/
- (IBAction)clickedAddCommunityButton:(id)sender {
    [SystemSounds playTockSound];
    [self addCommunityForm];
}

/******************************************************************************
 * getPresentTime
 *
 * Return's the present time in format:HH:mm:ss
 ******************************************************************************/
- (NSString*)getPresentTime{
    NSDate *date = [NSDate date];
    NSDateFormatter *timeFormatter = [[NSDateFormatter alloc] init];
    [timeFormatter setDateFormat:@"HH:mm:ss"];
    NSString *theTime = [timeFormatter stringFromDate:date];
    return theTime;
}

/******************************************************************************
 * getPresentDate
 *
 * Return's the present date in format:yyyy-MM-dd
 ******************************************************************************/
- (NSString*)getPresentDate{
    NSDate *date = [NSDate date];
    NSDateFormatter *timeFormatter = [[NSDateFormatter alloc] init];
    [timeFormatter setDateFormat:@"yyyy-MM-dd"];
    NSString *theDate = [timeFormatter stringFromDate:date];
    return theDate;
}

/******************************************************************************
 * setDateOnStartOrReset
 *
 * Gets the current date and time to show on the GUI when the user started
 * working on this form, or when the user resets all data on the form.
 ******************************************************************************/
- (void)setDateOnStartOrReset:(BOOL)reset{
    if (self.userInfo.plantTimeStarted == nil || reset == YES
        || [self.userInfo.plantDateStarted isEqualToString:@""]) {
        self.userInfo.plantTimeStarted = [self getPresentTime];
        self.userInfo.plantDateStarted = [self getPresentDate];
        self.displayedTimeLabel.text = [NSString stringWithFormat:@"Started on: %@  at: %@",
                             self.userInfo.plantDateStarted,
                             self.userInfo.plantTimeStarted];
    }
    else{
        self.displayedTimeLabel.text = [NSString stringWithFormat:@"Started on: %@  at: %@",
                             self.userInfo.plantDateStarted,
                             self.userInfo.plantTimeStarted];
    }
}

- (void)updateTimeLabelWithDate:(NSString*)date andTime:(NSString*)time{
    self.displayedTimeLabel.text = [NSString stringWithFormat:@"Started on: %@  at: %@",date,time];
}

/******************************************************************************
 * testConnectionToWifi
 *
 * checks if ipad is connected to wifi. Returns 1 if YES
 ******************************************************************************/
- (NSInteger)testConnectionToWifi{
    self.internetReachable = [Reachability reachabilityForLocalWiFi];
    [self.internetReachable startNotifier];
    NSInteger status = self.internetReachable.currentReachabilityStatus;
    //NSLog(@"Internet Status =%d",(int)status);
    return status;
}

/******************************************************************************
 * resetInstance
 *
 * This method resets everything temporarily saved into NSUserDefaults.
 ******************************************************************************/
- (void)resetInstance{
    [NSUserDefaults resetStandardUserDefaults];
}

/******************************************************************************
 * refreshScreen
 *
 * This method goes through all the forms saved in the PlantFormManager and displays
 * them on screen in the same order as PlantFormManager's array.
 ******************************************************************************/
- (void)refreshScreen{
    [self setCommunityScrollViewSizeAndScrollEnable];
    PlantCommunityForm *form;
    int offset = 0;
    for (id object in self.plantFormManager.arrayOfPlantForms){
        form = object;
        form.formView.frame = CGRectMake(0, CommunityFormHeight * offset,
                                         CommunityFormWidth, CommunityFormHeight);
        [self.communityScrollView addSubview:form.formView];
        offset++;
    }
}

/******************************************************************************
 * switchDropDownButtonHidden
 *
 * This method swaps the drop down menu between visible or not based on the
 * current status of the menu. It flops whatever the current status is.
 ******************************************************************************/
-(void)switchDropDownButtonHidden{
    if (self.dropDownMenu.viewBox.hidden == NO) {
        self.dropDownMenu.viewBox.hidden = YES;
    }
}

/******************************************************************************
 * dropDownMenuButton
 *
 * This method swaps the drop down menu between visible or not when the user
 * hits the button for it.
 ******************************************************************************/
- (IBAction)dropDownMenuButton:(id)sender {
    [SystemSounds playTockSound];
    if (self.dropDownMenu.viewBox.hidden == YES) {
        self.dropDownMenu.viewBox.hidden = NO;
    }
    else
        self.dropDownMenu.viewBox.hidden = YES;
}

- (IBAction)newDocumentButton:(id)sender {
    [self startOver:sender];
}


/*
 * CSS490: Code added to ensure that data gets saved.
 */
- (IBAction)saveButton:(id)sender {
    [SystemSounds playTockSound];
    // new save
    NSString *current; // current filename being checked
    NSMutableArray *existingFilenames = [NSMutableArray new];
    
    for(int i = 0; i<self.plantFormManager.arrayOfPlantForms.count;i++){
        PlantCommunityForm *form = self.plantFormManager.arrayOfPlantForms[i];
        current = form.filename.text;
        
        if (current && ![current isEqualToString:@""]) {
            [existingFilenames addObject:current];
        }
    }
    
    // Case 1
    if (existingFilenames.count == 0) {
        NSLog(@"Case 1");
        [self popupToGetFilenameForSaving];
    }
    else if (self.plantFormManager.arrayOfPlantForms.count == 1 && existingFilenames.count != 0) {
         NSLog(@"Case 2");
        // Case 2
       // PlantCommunityForm *form = [self.plantFormManager.arrayOfPlantForms firstObject];
        [self updateDatabase];
    }
    else if (self.plantFormManager.arrayOfPlantForms.count > 1 && existingFilenames.count != 0) {
        NSLog(@"Case 3");
        if (existingFilenames.count == 1) { // >= existing filenames
            // Case 3
            [self saveAllFormsWithName:[existingFilenames firstObject]];
        }
        else {
            // Case 4
            [self popupToGetFilenameForSaving];
        }
    }

}

/*
 * saveAllFormsWithName:
 *
 * Saves the forms with specified name.
 *
 * Input Parameters:
 *  stringValue: the string value for the name of the forms.
 *
 * Written by: Team LionHead
 * CSS 490 Data Provenance Spring 2016
 */
- (void)saveAllFormsWithName:(NSString*)name {
    for (id object in self.plantFormManager.arrayOfPlantForms) {
        PlantCommunityForm *form = object;
        // new save
        if (!form.filename.text) {
            form.filename.text = name;
            PlantCommunityProvenance *pcp = [PlantCommunityProvenance new];
            [self configureDatabaseSave:pcp ];
            [self saveToDatabase:pcp];
        }
        else{
            // update form with filename
            form.filename.text = name;
            [self updateDatabase];
        }
    }
}
- (void)updateDatabase {
    // update database
    RLMResults *dataToUpdate = [PlantCommunityProvenance objectsWhere:[NSString stringWithFormat:
                                                                       @"username = '%@' AND filename = '%@'",self.userInfo.username,self.userInfo.plantWorkingFilename]];
    PlantCommunityProvenance *plantProv = [dataToUpdate firstObject];
    // perform update
    RLMRealm *realm = [RLMRealm defaultRealm];
    [realm beginWriteTransaction];
    plantProv = [self configureDatabaseSave:plantProv];
    [realm commitWriteTransaction];
}

- (IBAction)saveNewButton:(id)sender {
    // [self SaveNew:sender]; // deprecated (01/31/15)
}

- (IBAction)downloadDataFromServer:(id)sender {
    [self.uploadForm getDataFromServer:self.login];
}

/******************************************************************************
 * prepDropDownMenuOnLoad
 *
 * This method sets the attributes of the drop down menu used to present the
 * user with the different options available in this class.
 ******************************************************************************/
- (void)prepDropDownMenuOnLoad {
    self.dropDownMenu.viewBox.hidden = YES;
    [self.scroller addSubview:self.dropDownMenu.viewBox];
    [self.scroller bringSubviewToFront:self.dropDownMenu.viewBox];
}

// segue back to Home Page when hitting back arrow button
- (IBAction)hitBackButton:(id)sender {
    [SystemSounds playTockSound];
    HomeScreen *homeController = (HomeScreen*)
        [self.navigationController.viewControllers objectAtIndex:0];
    homeController.plantFormManager = self.plantFormManager;
    [self.navigationController popToRootViewControllerAnimated:YES];
}

/******************************************************************************
 * setupBackButton
 *
 * This method creates a button on top of where the navigation bar would be
 * so that the user can cancel and segue back to the Home Page.
 ******************************************************************************/
-(void)setupBackButton {
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeSystem];
    backButton.titleLabel.text = @"BackButton";
    [backButton setFrame:CGRectMake(43, 60, 20, 35)];
    [backButton setBackgroundImage:[UIImage imageNamed:@"leftArrow"] forState:0];
    backButton.titleLabel.font = [UIFont systemFontOfSize:18];
    backButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [backButton setShowsTouchWhenHighlighted:YES];
    [backButton addTarget:self action:@selector(hitBackButton:)
         forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backButton];
}

-(NSString *)getWaypointsFromWaypointArray:(NSMutableArray*)waypointArray {
    NSString *result = @"";
    UITextField *waypointText;
    for (int i = 0; i < [waypointArray count]; i++) {
        waypointText = waypointArray[i];
        result = [result stringByAppendingString:[NSString stringWithFormat:@"%@\n",
                                                  waypointText.text]];
    }
    return result;
}

- (NSString*)getClassNumber {
    NSString *classNumber;
    if (![self.userInfo.classNumber isEqualToString:@""]) {
        classNumber = self.userInfo.classNumber;
    }
    else
        classNumber = @"Independent Research";
    
    return classNumber;
}

- (NSString*)getWeather {
    NSString *weather;
    if (self.userInfo.weatherConditions) {
        weather = self.userInfo.weatherConditions;
    }
    else
        weather = @"N/A";
    
    return weather;
}

// gathers all parameter data into dictionary for uploading
- (NSDictionary*)getPostParameters {
    NSDictionary *dict = @{@"id" : self.userInfo.plantFormID,
          @"deviceId" : [IDGenerator getDeviceId],
          @"username" : self.userInfo.username,
          @"researchers" : self.userInfo.researchers,
          @"temperature" : self.userInfo.currentTemp,
          @"weather" : [self getWeather],
          @"project" : self.userInfo.project,
          @"classNumber" : [self getClassNumber],
          @"instructor" : self.userInfo.instructor,
          @"dateStarted" : self.userInfo.plantDateStarted,
          @"timeStarted" : self.userInfo.plantTimeStarted,
          @"alderHairGrass" : [self plantNotesFor:@"Alder-Hairgrass"],
          @"alderShrub" : [self plantNotesFor:@"Alder-Shrub"],
          @"alderSloughSedge" : [self plantNotesFor:@"Alder-Slough Sedge"],
          @"conifer" : [self plantNotesFor:@"Conifer"],
          @"creekBuffer" : [self plantNotesFor:@"Creek Buffer"],
          @"edgeGrassBuffer" : [self plantNotesFor:@"Edge Grass Buffer"],
          @"edgeTreeBuffer" : [self plantNotesFor:@"Edge Tree Buffer"],
          @"logJam" : [self plantNotesFor:@"Log Jam"],
          @"meadow" : [self plantNotesFor:@"Meadow"],
          @"pointBar" : [self plantNotesFor:@"Point Bar"],
          @"shadyDepression" : [self plantNotesFor:@"Shady Depression"],
          @"shrubThicket" : [self plantNotesFor:@"Shrub Thicket"],
          @"sunnyDepression" : [self plantNotesFor:@"Sunny Depression"]};
    return dict;
}

// returns the notes from the community forms contained in the form manager array
// by using the formID to match the community parameter
- (NSString*)plantNotesFor:(NSString*)community {
    PlantCommunityForm *form; //= [self.plantFormManager.plantForms valueForKey:community];
    for (id object in self.plantFormManager.arrayOfPlantForms){
        form = object;
        if ([form.formID isEqualToString:community]) {
            return form.communityText.text;
        }
    }
    return @"";
}

/******************************************************************************
 * prepScrollViewOnLoad
 *
 * This method sets the delegate, contentSize, and disables scrolling of the
 * main view scroller which can be scrolled up when keyboard is used.
 ******************************************************************************/
-(void)prepScrollViewOnLoad {
    self.scroller.delegate = self;
    self.scroller.contentSize = CGSizeMake(320.0, 600.0);
    self.scroller.scrollEnabled = NO;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:YES];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

 #pragma mark - Navigation

/******************************************************************************
 * shouldPerformSegueWithIdentifier
 *
 * This method checks conditions to make sure there is a name and project name
 * filled out, so that it can be used in the file naming of a photo taken.
 * If those fields are blank then the segue will not perform.
 ******************************************************************************/
- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    if ([identifier isEqualToString:@"SegueToUploadFile"] ||
        [identifier isEqualToString:@"SegueToUploadFile2"] ||
        [identifier isEqualToString:@"SequeToDownloadFile"] ||
        [identifier isEqualToString:@"SequeToDownloadFile2"]) {
        if ([self.login getNetworkApproval] == NO) { // YES = approved to connect to server
            UIAlertController *errorAlert = [UIAlertController alertControllerWithTitle:@"ERROR"
                            message:@"You must be logged in with a valid username and password"
                            preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *dismiss = [UIAlertAction actionWithTitle:@"OK"
                                                              style:UIAlertActionStyleDefault
                        handler:^(UIAlertAction *action){}];
            [errorAlert addAction:dismiss];
            [self presentViewController:errorAlert animated:YES completion:nil];

            return NO;
        }
    }
    if ([identifier isEqualToString:@"homeAndPlant"]) {
        //NSLog(@"Passed Segue Test");
        return YES;
    }
    return YES;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"plantToPhotoViewerSegue"]) {
        [self switchDropDownButtonHidden];
        PhotoViewer *controller = (PhotoViewer *)segue.destinationViewController;
        controller.userInfo = self.userInfo;
        controller.community = @"PlantCommunity";
        
        
    }
    if ([segue.identifier isEqualToString:@"SegueToDeleteFile"]) {
        [self switchDropDownButtonHidden];
    }
    if ([segue.identifier isEqualToString:@"SegueToUploadFile"] ||
        [segue.identifier isEqualToString:@"SegueToUploadFile2"]) {
        if (self.login.guestLogin == NO) {
            
        }
        [self switchDropDownButtonHidden];
        FilesToServer *controller = (FilesToServer *)segue.destinationViewController;
        controller.username = self.login.username;
        controller.clickedUploadFile = YES;
    }
    if ([segue.identifier isEqualToString:@"SequeToDownloadFile"] ||
        [segue.identifier isEqualToString:@"SequeToDownloadFile2"]) {
        [self switchDropDownButtonHidden];
        FilesToServer *controller = (FilesToServer *)segue.destinationViewController;
        controller.username = self.login.username;
        controller.projectName = self.userInfo.project;
        controller.clickedGetFile = YES;
        NSLog(@"SegueToDownloadFile");
    }
    if ([segue.identifier isEqualToString:@"plantToFileViewerSegue"]) {
        [self switchDropDownButtonHidden];
        TableFileViewer *controller = (TableFileViewer *)segue.destinationViewController;
        controller.communitySource = @"PlantCommunity";
        controller.clickedLoadDataFromPlantCommunity = self.userInfo.clickedLoadData;
        controller.clickedDeleteDataFromPlantCommunity = self.clickedDeleteData;
        controller.login = self.login;
        controller.userInfo = self.userInfo;
        controller.workingFilename = self.userInfo.plantWorkingFilename;
    }
}

- (void)addObserversOnLoad {
    [self.uploadForm addObserver:self
                      forKeyPath:@"uploadSuccess"
                         options:NSKeyValueObservingOptionNew context:NULL];
    [self.uploadForm addObserver:self
                      forKeyPath:@"downloadSuccess"
                         options:NSKeyValueObservingOptionNew context:NULL];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(textViewDidBeginEditing:)
                                             name:UITextViewTextDidBeginEditingNotification
                                             object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(textViewDidEndEditing:)
                                             name:UITextViewTextDidEndEditingNotification
                                             object:nil];
}


@end
