//
//  PlantFileViewer.h
//  GeoDatabaseWetlands
//
//  Created by Jonathan Mason on 10/1/15.
//  Copyright © 2015 Jonathan Mason. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PlantCommunity.h"
#import <Realm/Realm.h>

@interface PlantFileViewer : UIView

@property (weak, nonatomic) id <PlantFileViewerDelegate> delegate;

- (RLMResults*)returnData:(RLMResults*)data;
- (id)initWithFrame:(CGRect)frame andPlantComm:(PlantCommunity*)plant;

@end
