//
//  PlantCommMenu.h
//  GeoDatabaseWetlands
//
//  Created by Jonathan Mason on 3/3/15.
//  Copyright (c) 2015 Jonathan Mason. All rights reserved.
//
//  This software is intended soley for the University of Washington Bothell.

#import <UIKit/UIKit.h>
@class PlantCommunity;

// See comment at top of file for a complete description
@interface PlantCommMenu : NSObject

@property (strong, nonatomic)UIView *viewBox;

-(id)initWithPlant:(PlantCommunity*)plant;
- (void)hitstartOverButtonButton:(id)sender;

@end
