//
//  PlantCommMenu.m
//  GeoDatabaseWetlands
//
//  Created by Jonathan Mason on 11/21/14.
//  Copyright (c) 2014 Jonathan Mason. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PlantCommMenu.h"
#import "PlantCommunity.h"
#import "PlantFileViewer.h"
#import "UserInfo.h"
#import "SystemSounds.h"
#define buttonHeight 40
#define buttonWidth 210

@implementation PlantCommMenu
PlantCommunity *plantObject;
NSArray *buttons;
UIColor *shadowColor;

// set the properties on initialization
-(id)initWithPlant:(PlantCommunity*)plant {
    self = [super init];
    if (self) {
        [self generateMenuView];
        [self generateButtons];
        plantObject = plant;
    }
    
    return self;
}

- (void)generateMenuView {
    // 60
    self.viewBox = [[UIView alloc]initWithFrame:CGRectMake(529, 10, buttonWidth, 405)];
}

// create all UIButtons used in the menu
- (void)generateButtons {
    shadowColor = [UIColor colorWithRed:50.0/255.0 green:50.0/255.0 blue:50.0/255.0 alpha:1.0];
    //Start Over
    UIButton *startOverButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [startOverButton setFrame:CGRectMake(0, 0, buttonWidth, buttonHeight)];
    [startOverButton setTitle:@"Start Over" forState:UIControlStateNormal];
    [startOverButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    startOverButton.titleLabel.font = [UIFont systemFontOfSize:20];
    [startOverButton setBackgroundImage:[UIImage imageNamed:@"plantCommMenuButtonBackground"]
                               forState:UIControlStateNormal];
    [startOverButton addTarget:self
                    action:@selector(hitstartOverButtonButton:)
                    forControlEvents:UIControlEventTouchUpInside];
    startOverButton.layer.shadowColor = shadowColor.CGColor;
    startOverButton.layer.shadowOffset = CGSizeMake(2,5);
    startOverButton.layer.shadowOpacity = .75f;
    [self.viewBox addSubview:startOverButton];
    
    //Save
    UIButton *saveLocallyButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [saveLocallyButton setFrame:CGRectMake(0, 45, buttonWidth, buttonHeight)];
    [saveLocallyButton setTitle:@"Save As" forState:UIControlStateNormal];
    [saveLocallyButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    saveLocallyButton.titleLabel.font = [UIFont systemFontOfSize:20];
    [saveLocallyButton setBackgroundImage:[UIImage imageNamed:@"plantCommMenuButtonBackground"]
                               forState:UIControlStateNormal];
    [saveLocallyButton addTarget:self
                          action:@selector(hitSaveButton:)
                forControlEvents:UIControlEventTouchUpInside];
    saveLocallyButton.layer.shadowColor = shadowColor.CGColor;
    saveLocallyButton.layer.shadowOffset = CGSizeMake(2,5);
    saveLocallyButton.layer.shadowOpacity = .75f;
    [self.viewBox addSubview:saveLocallyButton];
    
    //Load Data
    UIButton *openLocalFileButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [openLocalFileButton setFrame:CGRectMake(0, 90, buttonWidth, buttonHeight)];
    [openLocalFileButton setTitle:@"Load Data" forState:UIControlStateNormal];
    [openLocalFileButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    openLocalFileButton.titleLabel.font = [UIFont systemFontOfSize:20];
    [openLocalFileButton setBackgroundImage:[UIImage imageNamed:@"plantCommMenuButtonBackground"]
                               forState:UIControlStateNormal];
    [openLocalFileButton addTarget:self
                            action:@selector(hitLoadDataButton:)
                  forControlEvents:UIControlEventTouchUpInside];
    openLocalFileButton.layer.shadowColor = shadowColor.CGColor;
    openLocalFileButton.layer.shadowOffset = CGSizeMake(2,5);
    openLocalFileButton.layer.shadowOpacity = .75f;
    [self.viewBox addSubview:openLocalFileButton];
    
    //Upload Data
    UIButton *uploadDataButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [uploadDataButton setFrame:CGRectMake(0, 135, buttonWidth, buttonHeight)];
    [uploadDataButton setTitle:@"Upload Data" forState:UIControlStateNormal];
    [uploadDataButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    uploadDataButton.titleLabel.font = [UIFont systemFontOfSize:20];
    [uploadDataButton setBackgroundImage:[UIImage imageNamed:@"plantCommMenuButtonBackground"]
                                   forState:UIControlStateNormal];
    [uploadDataButton addTarget:self
                            action:@selector(hitUploadDataButton:)
                  forControlEvents:UIControlEventTouchUpInside];
    uploadDataButton.layer.shadowColor = shadowColor.CGColor;
    uploadDataButton.layer.shadowOffset = CGSizeMake(2,5);
    uploadDataButton.layer.shadowOpacity = .75f;
    [self.viewBox addSubview:uploadDataButton];
    
    //Download Files
    UIButton *downloadDataButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [downloadDataButton setFrame:CGRectMake(0, 180, buttonWidth, buttonHeight)];
    [downloadDataButton setTitle:@"Download Data" forState:UIControlStateNormal];
    [downloadDataButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    downloadDataButton.titleLabel.font = [UIFont systemFontOfSize:20];
    [downloadDataButton setBackgroundImage:[UIImage imageNamed:@"plantCommMenuButtonBackground"]
                               forState:UIControlStateNormal];
    [downloadDataButton addTarget:self
                           action:@selector(hitDownloadDataButton:)
                 forControlEvents:UIControlEventTouchUpInside];
    downloadDataButton.layer.shadowColor = shadowColor.CGColor;
    downloadDataButton.layer.shadowOffset = CGSizeMake(2,5);
    downloadDataButton.layer.shadowOpacity = .75f;
    [self.viewBox addSubview:downloadDataButton];
    
    //Delete Files
    UIButton *deleteLocalFilesButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [deleteLocalFilesButton setFrame:CGRectMake(0, 225, buttonWidth, buttonHeight)];
    [deleteLocalFilesButton setTitle:@"Delete Files" forState:UIControlStateNormal];
    [deleteLocalFilesButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    deleteLocalFilesButton.titleLabel.font = [UIFont systemFontOfSize:20];
    [deleteLocalFilesButton setBackgroundImage:[UIImage imageNamed:@"plantCommMenuButtonBackground"]
                               forState:UIControlStateNormal];
    [deleteLocalFilesButton addTarget:self
                               action:@selector(hitDeleteLocalFilesButton:)
                     forControlEvents:UIControlEventTouchUpInside];
    deleteLocalFilesButton.layer.shadowColor = shadowColor.CGColor;
    deleteLocalFilesButton.layer.shadowOffset = CGSizeMake(2,5);
    deleteLocalFilesButton.layer.shadowOpacity = .75f;
    [self.viewBox addSubview:deleteLocalFilesButton];
    
    
    UIButton *createSpreadsheetButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [createSpreadsheetButton setFrame:CGRectMake(0, 315, buttonWidth, buttonHeight)];
    [createSpreadsheetButton setTitle:@"Create Spreadsheet" forState:UIControlStateNormal];
    [createSpreadsheetButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    createSpreadsheetButton.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:22];
    [createSpreadsheetButton.titleLabel setTextAlignment:NSTextAlignmentCenter];
    [createSpreadsheetButton setBackgroundColor:[UIColor clearColor]];
    [createSpreadsheetButton setBackgroundImage:[UIImage imageNamed:@"plantCommMenuButtonBackground"]
                                       forState:UIControlStateNormal];
    [createSpreadsheetButton addTarget:self action:@selector(hitCreateSpreadsheetButton:)
                      forControlEvents:UIControlEventTouchUpInside];
    createSpreadsheetButton.layer.shadowColor = shadowColor.CGColor;
    createSpreadsheetButton.layer.shadowOffset = CGSizeMake(2,5);
    createSpreadsheetButton.layer.shadowOpacity = .75f;
    [self.viewBox addSubview:createSpreadsheetButton];
    
    
    
    //Photos
    UIButton *goToPhotosButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [goToPhotosButton setFrame:CGRectMake(0, 270, buttonWidth, buttonHeight)];
    [goToPhotosButton setTitle:@"View Photos" forState:UIControlStateNormal];
    [goToPhotosButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    goToPhotosButton.titleLabel.font = [UIFont systemFontOfSize:20];
    [goToPhotosButton setBackgroundImage:[UIImage imageNamed:@"plantCommMenuButtonBackground"]
                                      forState:UIControlStateNormal];
    [goToPhotosButton addTarget:self
                         action:@selector(hitGoToPhotosButton:)
               forControlEvents:UIControlEventTouchUpInside];
    goToPhotosButton.layer.shadowColor = shadowColor.CGColor;
    goToPhotosButton.layer.shadowOffset = CGSizeMake(2,5);
    goToPhotosButton.layer.shadowOpacity = .75f;
    [self.viewBox addSubview:goToPhotosButton];
    
}

/******************************************************************************
 * hitstartOverButtonButton
 *
 * This method
 ******************************************************************************/
- (void)hitstartOverButtonButton:(id)sender {
    [SystemSounds playTockSound];
    [self.viewBox setHidden:YES];
    [plantObject startOver:self];
}

- (void)hitLoadDataButton:(id)sender {
    [SystemSounds playTockSound];
    [self.viewBox setHidden:YES];
    UserInfo *userInfo = [UserInfo sharedUserInfo];
    [userInfo setClickedLoadData:YES];
    [self showFileViewer];
}

- (void)hitUploadDataButton:(id)sender {
    [SystemSounds playTockSound];
    UserInfo *userInfo = [UserInfo sharedUserInfo];
    if (userInfo.approvedUser == NO) {
        [self popupNoGuestWarning];
    }
    else{
        if (plantObject.plantFormManager.arrayOfPlantForms.count > 1) {
            [self.viewBox setHidden:YES];
            [plantObject uploadData];
        }
        else
            [self popupNoFormsToUpload];
    }
}

- (void)hitSaveButton:(id)sender {
    [SystemSounds playTockSound];
    [self.viewBox setHidden:YES];
    [plantObject saveAs];
}

- (void)hitDeleteLocalFilesButton:(id)sender {
    [SystemSounds playTockSound];
    [self.viewBox setHidden:YES];
    UserInfo *userInfo = [UserInfo sharedUserInfo];
    [userInfo setClickedDeleteFile:YES];
    [self showFileViewer];
    //[plantObject.userInfo setClickedDeleteFile:YES];
    //[plantObject performSegueWithIdentifier:@"plantToFileViewerSegue" sender:sender];
}

- (void)hitCreateSpreadsheetButton:(id)sender {
    [SystemSounds playTockSound];
    [self animateButtons:0 withSelected:sender];
    [plantObject createSpreadsheet];
}

- (void)hitDownloadDataButton:(id)sender {
    [SystemSounds playTockSound];
    [self showInProgressAlert];
}

/******************************************************************************
 * hitGoToPhotosButton
 *
 * This method calls the PlantCommunity's Segue to push over to the PhotoViewer
 ******************************************************************************/
- (void)hitGoToPhotosButton:(id)sender {
    [SystemSounds playTockSound];
    self.viewBox.hidden = YES;
    [plantObject performSegueWithIdentifier:@"plantToPhotoViewerSegue" sender:sender];
}

- (void)resetButtonsAlpha {
    UIButton *button;
    for (id object in buttons) {
        button = object;
        [button setAlpha:1.0];
    }
}

- (void)showFileViewer {
    CGRect rect = CGRectMake(([UIScreen mainScreen].bounds.size.width *.2)/2,
                             ([UIScreen mainScreen].bounds.size.height *.2)/2,
                             [UIScreen mainScreen].bounds.size.width - ([UIScreen mainScreen].bounds.size.width *.2),
                             [UIScreen mainScreen].bounds.size.height - ([UIScreen mainScreen].bounds.size.height *.2));
    
    PlantFileViewer *fileViewer = [[PlantFileViewer alloc]initWithFrame:rect andPlantComm:(PlantCommunity*)plantObject];
    [plantObject.view addSubview:fileViewer];
}

- (void)showInProgressAlert {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Build in Progress"
                                                                   message:nil
                                                            preferredStyle:UIAlertControllerStyleAlert];
    [plantObject presentViewController:alert animated:YES completion:nil];
    
    double delayInSeconds = 1.0;
    dispatch_time_t endTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(endTime, dispatch_get_main_queue(), ^{
        [alert dismissViewControllerAnimated:YES completion:nil];
    });
}

// animates all the buttons disappearance with the sender disappearing last
- (void)animateButtons:(int)index withSelected:(id)sender {
    if (!buttons || buttons.count == 0) {
        NSLog(@"Error PlantCommMenu [animateButtons:(int)index withSelected:(id)sender]");
        return;
    }
    UIButton *button;
    
    if (index == buttons.count) {
        button = sender;
        [UIView animateWithDuration:0.3 animations:^{
            [button setAlpha:0.00];
        } completion:^(BOOL finished){
            [self.viewBox setHidden:YES];
            [self resetButtonsAlpha];
        }];
    }
    else{
        button = buttons[index];
        index++;
        if (![button isEqual:sender]) {
            [UIView animateWithDuration:0.02 animations:^{
                [button setAlpha:0.0];
            } completion:^(BOOL finished) {
                [self animateButtons:index withSelected:sender];
            }];
        }
        else
            [self animateButtons:index withSelected:sender];
    }
}

- (void)popupNoGuestWarning {
    UIViewControllerTracker *vcTracker = [UIViewControllerTracker sharedVC];
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Notice"
                                    message:@"Guest login cannot upload data to the campus server."
                                " You must login with a username and password."
                                    preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
    [alert addAction:ok];
    [vcTracker.currentViewController presentViewController:alert animated:YES completion:nil];
}

- (void)popupNoFormsToUpload {
    UIViewControllerTracker *vcTracker = [UIViewControllerTracker sharedVC];
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Notice"
                                                                   message:@"There are no forms to upload."
                                                            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
    [alert addAction:ok];
    [vcTracker.currentViewController presentViewController:alert animated:YES completion:nil];
}



@end  // end of PlantCommMenu


