//
//  PlantCommunityForm.h
//  GeoDatabaseWetlands
//
//  Created by Jonathan Mason on 5/13/15.
//  Copyright (c) 2015 Jonathan Mason. All rights reserved.
//
//  This software is intended soley for the University of Washington Bothell.

#import <Foundation/Foundation.h>
#import "PlantMembers.h"
#import "PlantFormManager.h"
#import "PhotoViewer.h"
#import "UIViewControllerTracker.h"
@class PlantFormManager;

// See comment at top of file for a complete description
@interface PlantCommunityForm : NSObject <UIImagePickerControllerDelegate,
                                          UINavigationControllerDelegate>

@property (strong,nonatomic) PlantFormManager *formManagerPtr;
@property (strong, nonatomic) NSString *uuid;
@property (strong, nonatomic) UILabel *filename;
@property (strong,nonatomic) NSString *formID;
@property (strong,nonatomic) NSString *formKey;
@property (strong,nonatomic) UIView *formView;
@property (strong,nonatomic) UIImageView *formBackground;
@property (strong,nonatomic) UIButton *deleteButton;
@property (strong,nonatomic) UIButton *takePhotoButton;
@property (strong,nonatomic) UIButton *clearTextButton;
@property (strong,nonatomic) UILabel *communityLabel;
@property (strong,nonatomic) UITextView *communityText;

- (id)initWithCommunity:(NSString*)community atYpos:(CGFloat)y;
- (id)initWithCommunity:(NSString*)community andText:(NSString*)text;
- (id)initWithPickerInView:(UIView*)view;

- (void)hitClearTextButton:(id)sender;
- (void)deleteForm;
- (void)addBackgroundImage;

@end
