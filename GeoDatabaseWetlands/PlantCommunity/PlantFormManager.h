//
//  PlantFormManager.h
//  GeoDatabaseWetlands
//
//  Created by Jonathan Mason on 5/18/15.
//  Copyright (c) 2015 Jonathan Mason. All rights reserved.
//
//  This software is intended soley for the University of Washington Bothell.

/***********************************************************************
 * PlantFormManager tracks all the forms that have been created and holds
 * a reference to all of them in a mutable array. The manager is also
 * responsible for determining the position of where each shall display.
 ***********************************************************************/

#import <Foundation/Foundation.h>
#import "PlantCommMenu.h"
#import "PlantCommunityForm.h"
#import "PlantMembers.h"
@class PlantCommunity;
@class PlantCommunityForm;

// See comment at top of file for a complete description
@interface PlantFormManager : NSObject

@property (strong, nonatomic) PlantCommunity* plantCommunityPtr;
@property (strong, nonatomic) PlantMembers *plantMembersPtr;
@property (strong, nonatomic) PlantCommunityForm *currentFormPtr;
@property (strong, nonatomic) NSMutableArray *arrayOfPlantForms; // points to all data forms created
@property (strong, nonatomic) NSMutableDictionary *plantForms;

+ (PlantFormManager*)sharedFormManager;
+ (PlantFormManager*)sharedFormManagerWithObject:(PlantCommunityForm*)form;
- (id)initWithForm:(PlantCommunityForm*)form;

- (void)addFormToArray:(id)form;
- (void)removeFormFromArray:(NSString*)formID;
- (void)createPlantCommunityForm:(NSString*)community;
- (void)createPlantCommunityForm:(NSString*)community withText:(NSString*)text;
- (void)deleteAllForms;
- (void)rearrangeForms;

- (PlantCommunityForm*)returnCurrentForm;

@end
