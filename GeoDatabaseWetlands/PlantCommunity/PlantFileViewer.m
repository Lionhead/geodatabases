//
//  PlantFileViewer.m
//  GeoDatabaseWetlands
//
//  Created by Jonathan Mason on 10/1/15.
//  Copyright © 2015 Jonathan Mason. All rights reserved.
//

#import "PlantFileViewer.h"
#import "UserInfo.h"
#import "FileTableCell.h"
#import "PlantFileViewTableCell.h"
#import "PlantCommunityProvenance.h"
#import "SystemSounds.h"


#define closeButtonWidth 100
#define closeButtonHeight 25

@interface PlantFileViewer() <UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) UITableView *theTableView;

@end

@implementation PlantFileViewer

RLMResults *data;
UserInfo *userInfo;
PlantCommunity *plantCommPtr;

- (id)initWithFrame:(CGRect)frame andPlantComm:(PlantCommunity*)plant {
    if (self = [super initWithFrame:frame]) {
        self.frame = frame;
        plantCommPtr = plant;
        userInfo = [UserInfo sharedUserInfo];
        [self queryData];
        [self configureTableView];
    }
    return self;
}

- (void)queryData {
    data = [[PlantCommunityProvenance objectsWhere:[NSString stringWithFormat:@"username = '%@'",userInfo.username]]
            sortedResultsUsingProperty:@"dateStarted" ascending:NO];
}

- (void)configureTableView {
    CGRect tableRect = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height-(closeButtonHeight+10));
    
    self.theTableView = [[UITableView alloc]initWithFrame:tableRect];
    self.theTableView.dataSource = self;
    self.theTableView.delegate = self;
    self.theTableView.layer.opacity = 0.9f;
    [self.theTableView setBackgroundView:nil];
    [self.theTableView setBackgroundColor:[UIColor colorWithRed:30.0/255.0
                                                          green:150.0/255.0
                                                           blue:50.0/255.0
                                                          alpha:1.0]];
    
    self.theTableView = [[UITableView alloc]initWithFrame:tableRect];
    self.theTableView.dataSource = self;
    self.theTableView.delegate = self;
    self.theTableView.layer.opacity = 0.95f;
    [self.theTableView setContentSize:CGSizeMake(self.theTableView.bounds.size.width, 1500)];
    [self.theTableView setAllowsMultipleSelection:YES];
    [self.theTableView setBackgroundColor:[UIColor whiteColor]];
    
    UIView *myMask = [[UIView alloc]initWithFrame:
                      CGRectMake(0, 0, self.theTableView.bounds.size.width, self.theTableView.bounds.size.height)];
    [myMask setClipsToBounds:YES];
    UIBezierPath *tableViewMaskPath = [UIBezierPath bezierPathWithRoundedRect:self.theTableView.bounds
                                                                 cornerRadius:10];
    CAShapeLayer *tableViewMaskLayer = [CAShapeLayer layer];
    tableViewMaskLayer.frame = myMask.bounds;
    tableViewMaskLayer.path = tableViewMaskPath.CGPath;
    [myMask.layer setMask:tableViewMaskLayer];
    [myMask addSubview:self.theTableView];
    [self addSubview:myMask];
    [myMask addSubview:self.theTableView];
    
    UIButton *closeButton = [[UIButton alloc]initWithFrame:
                              CGRectMake((self.frame.size.width/2)-(closeButtonWidth/2),
                                         self.frame.size.height-closeButtonHeight,
                                         closeButtonWidth,
                                         closeButtonHeight)];
    [closeButton setTitle:@"close" forState:UIControlStateNormal];
    [closeButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [closeButton.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue" size:22]];
    [closeButton setShowsTouchWhenHighlighted:YES];
    [closeButton addTarget:self
                     action:@selector(hitClose:)
           forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:closeButton];
    
}

- (void)hitClose:(id)sender {
    [SystemSounds playTockSound];
    [self setHidden:YES];
}

- (NSIndexPath *)tableView:(UITableView *)tableView
  willSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    return indexPath;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    RLMResults *selectedData = data[indexPath.row];
    NSLog(@"Data loaded: %@",selectedData);
    if (userInfo.clickedLoadData == YES) {
        [userInfo setClickedLoadData:NO];
        if ([selectedData isKindOfClass:[PlantCommunityProvenance class]]) {
            PlantCommunityProvenance *plantProv = (PlantCommunityProvenance *)selectedData;
            [plantCommPtr loadDataFromDatabase:plantProv];
        }
        else{
            // Log Error
            NSLog(@"Error [PlantFileViewer didSelectRowAtIndexPath]: Incorrect Data Format");
        }
        
        
        /*
        if ([self.delegate respondsToSelector:@selector(returnData:)]) { // not responding
            [self returnData:selectedData];
        }
         */
        [self setHidden:YES];
    }
    else if(userInfo.clickedDeleteFile == YES){
        if ([selectedData isKindOfClass:[PlantCommunityProvenance class]]) {
            PlantCommunityProvenance *plantProv = (PlantCommunityProvenance *)selectedData;
            [self confirmDeleteRecord:plantProv withName:plantProv.filename];
        }
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [data count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PlantFileViewTableCell *cell = (PlantFileViewTableCell*)[tableView
                                        dequeueReusableCellWithIdentifier:@"PlantFileViewTableCell"];
    
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"PlantFileViewTableCell"
                                                     owner:self
                                                   options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    // Configure the cell...
    int idx = 0;
    for (id object in data) {
        if (indexPath.row == idx) {
            if ([object isKindOfClass:[PlantCommunityProvenance class]]) {
                PlantCommunityProvenance *plantProv = object;
                cell.filename.text = plantProv.filename;
                cell.date.text = [NSString stringWithFormat:@"%@ at %@",
                                       plantProv.dateStarted,plantProv.timeStarted];
            }
        }
        idx++;
    }

    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell
                                    forRowAtIndexPath:(NSIndexPath *)indexPath {
}

- (RLMResults*)returnData:(RLMResults*)data{
    return data;
}


- (void)confirmDeleteRecord:(PlantCommunityProvenance*)record withName:(NSString*)filename {
    UIViewControllerTracker *vcTracker = [UIViewControllerTracker sharedVC];
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:
                                [NSString stringWithFormat:@"Delete %@?",filename]
                                                                   message:nil
                                                            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel"
                                                           style:UIAlertActionStyleCancel
                                                         handler:nil];
    [alert addAction:cancelAction];
    UIAlertAction *yesAction = [UIAlertAction actionWithTitle:@"Yes"
                                                        style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction *action)
        {
            RLMRealm *realm = [RLMRealm defaultRealm];
            [realm beginWriteTransaction];
            [realm deleteObject:record];
            [realm commitWriteTransaction];
            [self.theTableView reloadData];
        }];
    [alert addAction:yesAction];
    [vcTracker.currentViewController presentViewController:alert animated:YES completion:nil];
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
