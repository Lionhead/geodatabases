//
//  PlantFormManager.m
//  GeoDatabaseWetlands
//
//  Created by Jonathan Mason on 5/18/15.
//  Copyright (c) 2015 Jonathan Mason. All rights reserved.
//

#import "PlantFormManager.h"
#import "PlantCommunity.h"

#define formWidth 704
#define formHeight 175

@implementation PlantFormManager

+ (id)sharedFormManager{
    static dispatch_once_t predicate = 0;
    __strong static id sharedOject = nil;
    
    dispatch_once(&predicate, ^{
        sharedOject = [[self alloc]init];
    });
    return sharedOject;
}

+ (id)sharedFormManagerWithObject:(PlantCommunityForm*)form {
    static dispatch_once_t predicate = 0;
    __strong static id sharedOject = nil;
    
    dispatch_once(&predicate, ^{
        sharedOject = [[self alloc]initWithForm:form];
    });
    return sharedOject;
}

- (id)init {
    self = [super init];
    if (self) {
        _arrayOfPlantForms = [NSMutableArray new];
    }
    return self;
}

- (id)initWithForm:(PlantCommunityForm*)form {
    self = [super init];
    if (self) {
        _arrayOfPlantForms = [[NSMutableArray alloc]initWithObjects:form, nil];
    }
    return self;
}

/******************************************************************************
 * createPlantCommunityForm
 *
 * This method instantiates a PlantCommunityForm at specific coordinates based on
 * where it can fit in the scroll view. It then calls to remove the community
 * that was selected from the picker.
 ******************************************************************************/
- (void)createPlantCommunityForm:(NSString*)community {
    self.plantMembersPtr = [PlantMembers sharedPlantMembers];
    PlantCommunityForm *form = [[PlantCommunityForm alloc]initWithCommunity:
                community atYpos:self.arrayOfPlantForms.count*formHeight];
    self.currentFormPtr = form;
    form.formManagerPtr = self;
    [self animateAdditionOfNewForm:form];
    [self addFormToArray:form]; // add to array after created
    [self.plantMembersPtr removeCommunityFromPicker:community];
}

- (void)createPlantCommunityForm:(NSString*)community withText:(NSString*)text {
    self.plantMembersPtr = [PlantMembers sharedPlantMembers];
    PlantCommunityForm *form = [[PlantCommunityForm alloc]initWithCommunity:
                                community atYpos:self.arrayOfPlantForms.count*formHeight];
    form.communityText.text = text;
    self.currentFormPtr = form;
    form.formManagerPtr = self;
    [self animateAdditionOfNewForm:form];
    [self addFormToArray:form]; // add to array after created
    [self.plantMembersPtr removeCommunityFromPicker:community];
}

- (void)animateAdditionOfNewForm:(PlantCommunityForm*)form {
    [form.formView setAlpha:0.0];
    [UIView animateWithDuration:0.75 animations:^{
        [form.formView setAlpha:1.0];
    }];
}

/******************************************************************************
 * addFormToArray
 *
 * This method adds the new PlantCommunityForm created to the container.
 ******************************************************************************/
- (void)addFormToArray:(id)form {
    [self.arrayOfPlantForms addObject:form];
}

/******************************************************************************
 * removeFormFromArray
 *
 * This method removes a PlantCommunityForm from the container going by the FormID.
 * It will search the container until the FormID matches, and then removes it.
 ******************************************************************************/
- (void)removeFormFromArray:(NSString*)formID {
    self.plantMembersPtr = [PlantMembers sharedPlantMembers];
    BOOL found = NO;
    PlantCommunityForm *form;
    int index = 0;
    
    for(form in self.arrayOfPlantForms){
        if ([form.communityLabel.text isEqualToString:formID]) {
            found = YES;
            // add that community back to Picker
            [self.plantMembersPtr addCommunityToPicker:formID];
            [self.arrayOfPlantForms removeObjectAtIndex:index];
            // reposition if needed
            if (index < self.arrayOfPlantForms.count) {
                [self repositionFormsFromIndex:index];
            }
            // set the size of communityScroller
            [self.plantCommunityPtr setCommunityScrollViewSizeAndScrollEnable];
            break;
        }
        index++;
    }
    if (found == NO) {
        NSLog(@"ERROR [PlantFormManager(removeFormFromArray)] "
              "Description: Could not find form in array for deletion");
    }
}

/******************************************************************************
 * deleteAllForms
 *
 * This method iterates through the container with all community forms and then
 * deletes the data from it.
 ******************************************************************************/
- (void)deleteAllForms {
    self.plantMembersPtr = [PlantMembers sharedPlantMembers];
    PlantCommunityForm *form;
    for (int i = (int)self.arrayOfPlantForms.count-1; i >= 0; i--) {
        form = self.arrayOfPlantForms[i];
        [form deleteForm];
    }
    [self.plantMembersPtr resetPicker];
    [self.arrayOfPlantForms removeAllObjects];
}

/******************************************************************************
 * repositionFormsFromIndex
 *
 * When a plant community form is removed from the scroll view, if there are
 * forms below it they need to be repositioned. This method calculates where
 * the forms from the given index in the container need to reposition in the
 * scroll view, and do so while being animated.
 ******************************************************************************/
- (void)repositionFormsFromIndex:(int)index {
    // redraw forms from
    PlantCommunityForm *form;
    for (;index < self.arrayOfPlantForms.count; index++){
        form = self.arrayOfPlantForms[index];
        [UIView animateWithDuration:0.5 animations:^{
            // [testView setFrame:CGRectMake(0.0f, 100.0f, 300.0f, 200.0f)];
            [form.formView setFrame:CGRectMake(0, index*formHeight, formWidth, formHeight)];
        }];
    }
}

/***************************************************************************************************
 * returnCurrentForm
 *
 * This method returns a pointer to the current PlantCommunityForm that PlantFormManager
 * is pointing to.
 **************************************************************************************************/
- (PlantCommunityForm*)returnCurrentForm {
    return self.currentFormPtr;
}

/***************************************************************************************************
 * rearrangeForms
 *
 * This method rearranges all the forms in the array based on the vertical origin. This happens
 * after the user uses a pan gesture to move the order of the forms.
 **************************************************************************************************/
- (void)rearrangeForms {
    [self.arrayOfPlantForms sortUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
         PlantCommunityForm *form1 = obj1;
         PlantCommunityForm *form2 = obj2;
        
        if (form1.formView.frame.origin.y < form2.formView.frame.origin.y) {
            return (NSComparisonResult)NSOrderedAscending;
        }
        else if (form1.formView.frame.origin.y > form2.formView.frame.origin.y) {
            return (NSComparisonResult)NSOrderedDescending;
        }
        else
            return (NSComparisonResult)NSOrderedSame;
    }];
    [self repositionFormsFromIndex:0];
}

@end
