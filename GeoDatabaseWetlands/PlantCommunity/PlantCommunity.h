//
//  PlantCommunity.h
//  GeoDatabaseWetlands
//
//  Created by Jonathan Mason on 5/12/14.
//  Copyright (c) 2014 Jonathan Mason. All rights reserved.
//
//  This software is intended soley for the University of Washington Bothell.

#import <UIKit/UIKit.h>
#import "UserLogin.h"
#import "UserInfo.h"
#import "DirectUploadForm.h"
#import "CSVformatting.h"
#import "Reachability.h"
#import "PlantCommMenu.h"
#import "PlantMembers.h"
#import "LocalFileManager.h"
#import "UIViewControllerTracker.h"
#import "PlantCommunityProvenance.h"
#import <Realm/Realm.h>
@class PlantFormManager;
@class PlantFileViewer;

@protocol PlantFileViewerDelegate <NSObject>

@required
- (RLMResults*)returnData:(RLMResults*)data;

@end

// See comment at top of file for a complete description
@interface PlantCommunity : UIViewController {
    id <PlantFileViewerDelegate> _delegate;
}

@property (strong, nonatomic) id delegate;

@property (strong, nonatomic) UserLogin *login;
@property (strong, nonatomic) UserInfo *userInfo;
@property (strong, atomic) DirectUploadForm *uploadForm;
@property (strong, nonatomic) Reachability *internetReachable;
@property (strong, nonatomic) PlantCommMenu *dropDownMenu;
@property (weak, nonatomic) PlantMembers *plantMembers;
@property (strong, nonatomic) PlantFormManager *plantFormManager;
@property (strong, nonatomic) UIViewControllerTracker *vcTracker;

@property (strong) NSString* deviceID;
@property (strong) NSString* dateSaved;
@property int saveNumber;

@property BOOL clickedDeleteData;
@property BOOL clickedUpload;
@property BOOL clickedDownload;
@property BOOL hasClassNumber;
@property UIDevice *device;
@property NSString *openedCSVFile;
@property (weak, nonatomic) UIImageView *waypointSuperviewBackground;
@property (strong, nonatomic) IBOutlet UILabel *loggedInAs;
@property (strong, nonatomic) IBOutlet UILabel *displayedFilename;
@property (retain, nonatomic) IBOutlet UIScrollView *scroller;
@property (strong, nonatomic) IBOutlet UIView *communityFormView;
@property (strong, nonatomic) IBOutlet UIScrollView *communityScrollView;

- (IBAction)startOver:(id)sender; //resets all fields
- (void)saveAs;
- (IBAction)clickedAddCommunityButton:(id)sender;
- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender;
- (void)setCommunityScrollViewSizeAndScrollEnable;

- (IBAction)dropDownMenuButton:(id)sender;
- (IBAction)newDocumentButton:(id)sender;
- (IBAction)saveButton:(id)sender;
- (IBAction)saveNewButton:(id)sender;
- (IBAction)downloadDataFromServer:(id)sender; // connects to GeoDatabase Server
- (void)prepDropDownMenuOnLoad;
- (void)uploadData;
- (void)createSpreadsheet;
- (void)loadDataFromPath:(NSString*)path;
- (void)loadDataFromDatabase:(PlantCommunityProvenance*)data;

@end
