//
//  PlantMembers.m
//  GeoDatabaseWetlands
//
//  Created by Jonathan Mason on 2/25/15.
//  Copyright (c) 2015 Jonathan Mason. All rights reserved.
//
#import "PlantCommunityForm.h"
#import "PlantMembers.h"

@interface PlantMembers(){

}

@end

@implementation PlantMembers

// ensures only one instance is made
+(id)sharedPlantMembers{
    static dispatch_once_t predicate = 0;
    __strong static id sharedObject = nil;
    
    dispatch_once(&predicate, ^{
        sharedObject = [[self alloc]init];
    });
    
    return sharedObject;
}

- (id)init {
    self = [super init];
    if (self) {
        [self createPicker];
    }
    return self;
}

- (void)createPicker {
    [self createArrayOfMembers];
    self.communityPicker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 0, 280, 180)];
    self.communityPicker.delegate = self;
    self.communityPicker.dataSource = self;
    self.communityPicker.showsSelectionIndicator = YES;
    self.communityPicker.layer.opacity = .7;
    self.communityPicker.layer.shadowColor = [UIColor whiteColor].CGColor;
    self.communityPicker.layer.shadowOffset = CGSizeMake(2, 5);
    self.communityPicker.layer.shadowOpacity = 0.5;
    [self.communityPicker selectRow:0 inComponent:0 animated:YES]; // selects first by default
    self.objectChosen = self.memberArray[0]; // assign the first value to selection when initialized
}

- (void)createArrayOfMembers {
    self.memberArray = [[NSMutableArray alloc]initWithObjects:@"Alder-Hairgrass",
                             @"Alder-Shrub",@"Alder-Slough Sedge",
                             @"Conifer",@"Creek Buffer",@"Edge Grass Buffer",
                             @"Edge Tree Buffer",@"Log Jam",@"Meadow",
                             @"Point Bar",@"Shady Depression",@"Shrub Thicket",
                             @"Sunny Depression",nil];
}

- (void)resetPicker {
    [self.memberArray removeAllObjects];
    self.memberArray = nil;
    [self createArrayOfMembers];
}

- (NSArray*)pickerData {
    if (!self.memberArray) {
        [self createArrayOfMembers];
    }
    return self.memberArray;
}

- (int)numberOfCommunites {
    return (int)[self.communities count];
}

// *************************
- (id)returnSelection {
    return self.objectChosen;
}

/******************************************************************************
 * addCommunityToPicker
 *
 * This method adds a community back to the selection list.
 ******************************************************************************/
- (void)addCommunityToPicker:(NSString*)community {
    [self.memberArray addObject:community];
    // sort array by name
    [self.memberArray sortUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    [self.communityPicker reloadAllComponents];
    [self.communityPicker selectRow:0 inComponent:0 animated:YES]; // selects first by default
}

/******************************************************************************
 * removeCommunityFromPicker
 *
 * This method removes the user chosen community from the selection list.
 ******************************************************************************/
- (void)removeCommunityFromPicker:(NSString*)community {
    [self.memberArray removeObject:community];
    [self.communityPicker reloadAllComponents];
    [self.communityPicker selectRow:0 inComponent:0 animated:YES]; // selects first by default
}

/******************************************************************************
 * updatePicker
 *
 * This method updates the communities available for selection after loading
 * any possible communites from a locally saved file.
 ******************************************************************************/
- (void)updatePickerByRemovingLoadedEntries:(NSMutableArray*)array {
    PlantCommunityForm *form;
    for (id object in array) {
        form = object;
        if ([self.memberArray containsObject:form.formID]) {
            [self.memberArray removeObject:form.formID];
        }
    }
    [self.communityPicker reloadAllComponents];
    [self.communityPicker selectRow:0 inComponent:0 animated:YES]; // selects first by default
}

// ***********  PickerView Protocols  ****************** //
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return [self.pickerData count];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row
            forComponent:(NSInteger)component {
    return [self.pickerData objectAtIndex:row];
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component {
    return 35;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row
       inComponent:(NSInteger)component {
    int chosen = (int)[pickerView selectedRowInComponent:component];
    self.objectChosen = [self.pickerData objectAtIndex:chosen];
}
// ***********  End of PickerView Protocols  ****************** //

@end
