//
//  main.m
//  GeoDatabaseWetlands
//
//  Created by Jonathan Mason on 5/11/14.
//  Copyright (c) 2014 Jonathan Mason. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
