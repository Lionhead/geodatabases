//
//  HomeDataForm.h
//  GeoDatabaseWetlands
//
//  Created by Jonathan Mason on 9/23/15.
//  Copyright © 2015 Jonathan Mason. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HomeScreen.h"

@interface HomeDataForm : NSObject 

@property (strong, nonatomic) CommunityButtons *communityButtons;

@property (strong, nonatomic) UIView *viewBox; // view holding all elements
@property (strong, nonatomic) UILabel *namesLabel;
@property (strong, nonatomic) UIButton *addNameButton; // adds researcher names
@property (strong, nonatomic) UIButton *subtractNameButton; // button to remove names
@property (strong, nonatomic) UIButton *weatherButton; // button opens the list of different weather conditions
@property (strong, nonatomic) UITextField *projectNameTF; // holds name of project researcher is working on
@property (strong, nonatomic) UITextField *instructorMentorTF; // holds name of instructor/mentor for project
@property (strong, nonatomic) UITextField *currentTempTF; // holds name of instructor/mentor for project
@property (strong, nonatomic) UITextField *classNumberTF; // holds the class number if any
@property (strong, nonatomic) UILabel *independentResearchLabel; // label to display that this is independent research/No class#
@property (strong, nonatomic) UISwitch *classSwitch; // switches between class number or independent research

+ (id)sharedHomeDataForm;
- (void)reset;
- (void)addNameToArray:(NSString*)name;
- (void)getNewName:(id)sender;
- (void)subtractAllNames;
- (void)subtractName:(NSUInteger)index;
- (void)createCommunityButtons;
- (void)classSwitchEvent:(id)sender;
- (void)createWithViewController:(UIViewController*)vc;
- (void)updateWeatherConditions;
- (void)setLoadedWeatherConditions;
- (void)showOrHideButtons;
- (void)setClassSwitch;

@end
