//
//  HomeScreen.h
//  GeoDatabaseWetlands
//
//  Created by Jonathan Mason on 5/11/14.
//  Copyright (c) 2014 Jonathan Mason. All rights reserved.
//
//  This software is intended soley for the University of Washington Bothell.
//
/*******************************************************************************************
 *******************************************************************************************
 *
 * This class is used to display the home screen and give the user the elements used to
 * login and logout. The HomeScreen can call the UserInfo class to display more information
 * to fill out, and also segue to communities for data collection.
 *
 *******************************************************************************************
 *******************************************************************************************/

#import <UIKit/UIKit.h>
#import "UserLogin.h"
#import "Documentation.h"
#import "Reachability.h"
#import "UserInfo.h"
#import "CommunityButtons.h"
#import "PlantFormManager.h"
#import "SystemSounds.h"
#import "UIViewControllerTracker.h"
@class WaterFormManager;

// See comment at top of file for a complete description
@interface HomeScreen : UIViewController
@property BOOL Match;
@property (strong, nonatomic) PlantFormManager *plantFormManager;
@property (strong, nonatomic) WaterFormManager *waterFormManager;
@property (strong, nonatomic) UserInfo *userInfo;
@property (strong, nonatomic) UserLogin *login;

- (void)setLoginFieldsAfterLogin;

@end
