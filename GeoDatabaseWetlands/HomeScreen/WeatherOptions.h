//
//  WeatherOptions.h
//  GeoDatabaseWetlands
//
//  Created by Jonathan Mason on 4/30/15.
//  Copyright (c) 2015 Jonathan Mason. All rights reserved.
//
//  This software is intended soley for the University of Washington Bothell.
/*******************************************************************************************
 *******************************************************************************************
 *
 * This class is used to display a table of selectable weather conditions. The conditions
 * are displayed on the home screen in front of the user information.
 *
 *******************************************************************************************
 *******************************************************************************************/

#import <Foundation/Foundation.h>

// See comment at top of file for a complete description
@interface WeatherOptions : NSObject

@property (strong, nonatomic) UILabel *sunny;
@property (strong, nonatomic) UILabel *partlyCloudy;
@property (strong, nonatomic) UILabel *cloudy;
@property (strong, nonatomic) UILabel *overcast;
@property (strong, nonatomic) UILabel *fogMist;
@property (strong, nonatomic) UILabel *lightRain;
@property (strong, nonatomic) UILabel *rain;
@property (strong, nonatomic) UILabel *thunderstorm;
@property (strong, nonatomic) UILabel *lightWind;
@property (strong, nonatomic) UILabel *windy;
@property (strong, nonatomic) UILabel *heavyWinds;
@property (strong, nonatomic) UILabel *rainSnowMix;
@property (strong, nonatomic) UILabel *lightSnow;
@property (strong, nonatomic) UILabel *snow;
@property (strong, nonatomic) UILabel *heavySnow;
@property (strong, nonatomic) UILabel *hazeSmoke;
@property (strong, nonatomic) UILabel *hail;
@property (strong, nonatomic) UILabel *humid;

@property (strong, nonatomic) UILabel *instructions;
@property (strong, nonatomic) UIImageView *backgroundView;
@property (strong, nonatomic) NSMutableDictionary *selectedWeatherConditions;

+ (id)sharedWeatherOptions;
- (void)refreshWeatherSelection;
- (void)copyLoadedWeatherOptions:(WeatherOptions*)copy;
- (void)toggleVisibility;
- (void)reset;
- (void)copyWeatherOptionsFromDatabase:(NSArray*)conditions;

@end
