//
//  CommunityButtons.m
//  GeoDatabaseWetlands
//
//  Created by Jonathan Mason on 4/30/15.
//  Copyright (c) 2015 Jonathan Mason. All rights reserved.
//
#import "HomeScreen.h"
#import "CommunityButtons.h"
#import "UIViewControllerTracker.h"
#define ShowingYPosition 792
#define HiddenYPosition 685
#define ViewXPosition 154
#define ViewWidth 460
#define ViewHeight 103

@implementation CommunityButtons {
    UIButton *plantCommunityButton;
    UIButton *waterCommunityButton;
    UIButton *animalCommunityButton;
}

- (id)init {
    if (self = [super init]) {
        _buttonsInView = NO;
        [self setupBackground];
        [self setupButtons];
    }
    return self;
}

- (void)setupBackground {
    self.buttonsView = [[UIView alloc]initWithFrame:
                CGRectMake(ViewXPosition, HiddenYPosition, ViewWidth, ViewHeight)];
    
    UIGraphicsBeginImageContext(self.buttonsView.frame.size);
    [[UIImage imageNamed:@"communityButtonsBackground"]drawInRect:self.buttonsView.bounds];
    UIImage *backgroundImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    self.buttonsView.backgroundColor = [UIColor colorWithPatternImage:backgroundImage];
    [self.buttonsView setHidden:YES];
}

- (void)setupButtons {
    // initialize view and buttons
    
    plantCommunityButton = [[UIButton alloc]initWithFrame:CGRectMake(30, 53, 100, 35)];
    waterCommunityButton = [[UIButton alloc]initWithFrame:CGRectMake(180, 53, 100, 35)];
    animalCommunityButton = [[UIButton alloc]initWithFrame:CGRectMake(330, 53, 100, 35)];
    
    // set button properties
    plantCommunityButton.titleLabel.textAlignment = 1;
    plantCommunityButton.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:30];
    [plantCommunityButton setTitleColor:[UIColor colorWithRed:(15.0/255)
            green:(215.0/255) blue:(100.0/255) alpha:1.0] forState:UIControlStateNormal];
    [plantCommunityButton setTitle:@"Plant" forState:UIControlStateNormal];
    plantCommunityButton.showsTouchWhenHighlighted = YES;
    [plantCommunityButton addTarget:self action:@selector(clickedPlantButton)
                   forControlEvents:UIControlEventTouchUpInside];
    
    waterCommunityButton.titleLabel.textAlignment = 1;
    waterCommunityButton.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:30];
    [waterCommunityButton setTitleColor:[UIColor colorWithRed:(50.0/255)
            green:(230.0/255) blue:(255.0/255) alpha:1.0] forState:UIControlStateNormal];
    [waterCommunityButton setTitle:@"Water" forState:UIControlStateNormal];
    waterCommunityButton.showsTouchWhenHighlighted = YES;
    [waterCommunityButton addTarget:self action:@selector(clickedWaterButton)
                   forControlEvents:UIControlEventTouchUpInside];
    
    animalCommunityButton.titleLabel.textAlignment = 1;
    animalCommunityButton.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:30];
    [animalCommunityButton setTitleColor:[UIColor colorWithRed:(255.0/255)
            green:(150.0/255) blue:(100.0/255) alpha:1.0] forState:UIControlStateNormal];
    [animalCommunityButton setTitle:@"Bird" forState:UIControlStateNormal];
    animalCommunityButton.showsTouchWhenHighlighted = YES;
    [animalCommunityButton addTarget:self action:@selector(clickedAnimalButton)
                   forControlEvents:UIControlEventTouchUpInside];
    
    // add buttons to view
    [self.buttonsView addSubview:plantCommunityButton];
    [self.buttonsView addSubview:waterCommunityButton];
    [self.buttonsView addSubview:animalCommunityButton];
}

// switches between hidden or not
- (void)reverseVisible {
    if (self.buttonsView.hidden == NO) {
        [self hideButtons];
    }
    else
        [self.buttonsView setHidden:NO];
}

// sets the uiview to hidden
- (void)hideButtons {
    [self.buttonsView setHidden:YES];
}

// makes the uiview show and animate moving down into view
- (void)moveDownIntoView {
    [self.buttonsView setHidden:NO];
    [UIView animateWithDuration:0.875 animations:^{
        self.buttonsView.frame = CGRectMake(ViewXPosition, ShowingYPosition, ViewWidth, ViewHeight);
    }];
    self.buttonsInView = YES;
}

// makes the uiview hidden and animate moving up out of view
- (void)moveUpOutOfView{
    [UIView animateWithDuration:0.875 animations:^{
        self.buttonsView.frame = CGRectMake(ViewXPosition, HiddenYPosition, ViewWidth, ViewHeight);
    } completion:^(BOOL finished) {
        [self.buttonsView setHidden:YES];
    }];
    self.buttonsInView = NO;
}

// segues to PlantCommunity
- (void)clickedPlantButton {
    [SystemSounds playTockSound];
    UIViewControllerTracker *vcTracker = [UIViewControllerTracker sharedVC];
    [vcTracker.currentViewController performSegueWithIdentifier:@"homeToPlantSeg" sender:vcTracker.currentViewController];
}

// segues to WaterCommunity (future work)
- (void)clickedWaterButton {
    [SystemSounds playTockSound];
    UIViewControllerTracker *vcTracker = [UIViewControllerTracker sharedVC];
    [vcTracker.currentViewController performSegueWithIdentifier:@"homeToWaterSegue" sender:vcTracker.currentViewController];
}

// segues to AnimalCommunity (future work)
- (void)clickedAnimalButton {
    [SystemSounds playTockSound];
    UIViewControllerTracker *vcTracker = [UIViewControllerTracker sharedVC];
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Build In Progress"
                            message:nil preferredStyle:UIAlertControllerStyleAlert];
    
    [vcTracker.currentViewController presentViewController:alert animated:YES completion:nil];
    
    double delayInSeconds = 1.0;
    dispatch_time_t endTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(endTime, dispatch_get_main_queue(), ^{
        [alert dismissViewControllerAnimated:YES completion:nil];
    });
     
    //[viewController performSegueWithIdentifier:@"homeToAnimalSegue" sender:viewController];
}

@end


