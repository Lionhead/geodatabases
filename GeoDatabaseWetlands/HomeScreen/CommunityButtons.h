//
//  CommunityButtons.h
//  GeoDatabaseWetlands
//
//  Created by Jonathan Mason on 4/30/15.
//  Copyright (c) 2015 Jonathan Mason. All rights reserved.
//
//  This software is intended soley for the University of Washington Bothell.

#import <Foundation/Foundation.h>
@class UserInfo;

// Creates a drop down menu giving user ability to choose a class
@interface CommunityButtons : NSObject

@property (strong, nonatomic)UIView *buttonsView;
@property BOOL buttonsInView; // YES if buttons are moved down

- (void)setupButtons;
- (void)reverseVisible;
- (void)moveDownIntoView;
- (void)moveUpOutOfView;
- (void)hideButtons;

@end
