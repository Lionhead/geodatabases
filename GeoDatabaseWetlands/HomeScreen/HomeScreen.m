//
//  HomeScreen.m
//  GeoDatabaseWetlands
//
//  Created by Jonathan Mason on 5/11/14.
//  Copyright (c) 2014 Jonathan Mason. All rights reserved.
//

#import "HomeScreen.h"
#import "HomeDataForm.h"
#import "PlantCommunity.h"
#import "WaterCommunity.h"
#import "PlantFormManager.h"
#import <AVFoundation/AVFoundation.h>
#import <AudioToolbox/AudioServices.h>
#import <Realm/Realm.h>
#import "UserProvenance.h"
#import "SystemSounds.h"

@interface HomeScreen () <UITextFieldDelegate>
    
@property (strong, nonatomic) UIViewControllerTracker *vcTracker;
@property (strong) CommunityButtons *communityButtons;
@property (strong) Reachability *internetReachable;
@property (weak, nonatomic) IBOutlet UIImageView *homeImage; // image used in background
@property (strong, nonatomic) IBOutlet UITextField *loginField;
@property (strong, nonatomic) IBOutlet UITextField *passwordField;
@property (strong, nonatomic) IBOutlet UILabel *loggedInAs;
@property (strong, nonatomic) IBOutlet UIButton *loginButton;
@property (strong, nonatomic) IBOutlet UIButton *yellowLoginButton;
@property (strong, nonatomic) HomeDataForm *homeDataForm;
@property (strong, nonatomic) IBOutlet UIImageView *background;

@end

@implementation HomeScreen{
    @private
    CLLocationManager *locationManager;
    CLGeocoder *geocoder;
    CLPlacemark *placemark;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.vcTracker = [UIViewControllerTracker sharedVC];
    self.vcTracker.currentViewController = self;
    
    self.login = [UserLogin sharedUserLogin];
    [self.login addObserver:self forKeyPath:@"authenticationCheckComplete"
                    options:NSKeyValueObservingOptionNew context:NULL];
    [self.login addObserver:self forKeyPath:@"passwordChangeResult"
                    options:NSKeyValueObservingOptionNew context:NULL];
    
    [self prepNavigationControllerOnLoad];
    self.loggedInAs.textColor = [UIColor lightGrayColor];
    
    self.homeDataForm = [HomeDataForm sharedHomeDataForm];
    [self.homeDataForm createWithViewController:self];
    [self.view addSubview:self.homeDataForm.viewBox];

    //[self displayAllFontsAvailable];
    self.passwordField.delegate = self;
    self.loginField.delegate = self;
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    if (self.userInfo) {
        if (self.userInfo.updateHomeScreen) {
            [self.userInfo setUpdateHomeScreen:NO];
            [self checkForUpdatedInformation];
        }
    }
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:YES];
    if (self.vcTracker.currentViewController != self) {
        self.vcTracker.currentViewController = self;
    }
}

- (void)checkForUpdatedInformation{
    if (![self.homeDataForm.namesLabel.text isEqualToString:self.userInfo.researchers]) {
        self.homeDataForm.namesLabel.text = self.userInfo.researchers;
    }
    if (![self.homeDataForm.projectNameTF.text isEqualToString:self.userInfo.project]) {
        self.homeDataForm.projectNameTF.text = self.userInfo.project;
    }
    if (![self.homeDataForm.instructorMentorTF.text isEqualToString:self.userInfo.instructor]) {
        self.homeDataForm.instructorMentorTF.text = self.userInfo.instructor;
    }
    if (![self.homeDataForm.currentTempTF.text isEqualToString:self.userInfo.currentTemp]) {
        self.homeDataForm.currentTempTF.text = self.userInfo.currentTemp;
    }
    if (self.userInfo.classNumber && ![self.userInfo.classNumber isEqualToString:@""]
        && ![self.userInfo.classNumber isEqualToString:@"Independent Research"]) {
        [self.homeDataForm.classSwitch setOn:YES];
        [self.homeDataForm classSwitchEvent:nil];
        self.homeDataForm.classNumberTF.text = self.userInfo.classNumber;
    }
    [self.homeDataForm setLoadedWeatherConditions];
}

/******************************************************************************
 * prepNavigationControllerOnLoad
 *
 * This method sets the navigation controller properties for this class.
 ******************************************************************************/
-(void)prepNavigationControllerOnLoad{
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.view.backgroundColor = [UIColor clearColor];
}

/******************************************************************************
 * This method is reserved for observers.
 *
 * The observed properties: authenticationCheckComplete, changedPassword
 * When either property is edited the appropriate change will be made, which
 * can include approval of user login, and changing the user's password.
 ******************************************************************************/
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if ([keyPath isEqualToString:@"authenticationCheckComplete"])
    {
        [self checkIfUserApproved];
    }
    else if ([keyPath isEqualToString:@"passwordChangeResult"])
    {
        // Success, now update the password
        if ([self.login.passwordChangeResult isEqualToString:@"OK"]) {
            self.login.password = self.login.changedPassword; //update to the new password
            UIAlertController *success = [UIAlertController alertControllerWithTitle:@"Success"
                                message:[NSString stringWithFormat:@"Your password has been changed to: %@",self.login.password]
                                preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:nil];
            [success addAction:ok];
            [self presentViewController:success animated:YES completion:nil];
        }
        else if ([self.login.passwordChangeResult isEqualToString:@"Wrong username or password."]){
            UIAlertController *loginFail = [UIAlertController alertControllerWithTitle:@"Error"
                                message:@"Login failure."
                                preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Ok"
                                                         style:UIAlertActionStyleCancel
                                                       handler:nil];
            [loginFail addAction:ok];
            [self presentViewController:loginFail animated:YES completion:nil];
        }
        else if ([self.login.passwordChangeResult isEqualToString:@"No new password."]){
            UIAlertController *loginFail = [UIAlertController alertControllerWithTitle:@"Error"
                                message:@"Your password could not be changed."
                                            " Please make sure you are connected to the server."
                                preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Ok"
                                                         style:UIAlertActionStyleCancel
                                                       handler:nil];
            [loginFail addAction:ok];
            [self presentViewController:loginFail animated:YES completion:nil];
        }
    }
}

/******************************************************************************
 * checkIfUserApproved
 *
 * This method checks to see it the user has been authenticated and has
 * permission to access the server.
 ******************************************************************************/
- (void)checkIfUserApproved{
     // could not login with entered username and password
    if (self.login.authenticationCheckComplete == YES
        && [self.login.serverAuthenticationResponseString isEqualToString:@"Wrong username or password."]) {
        self.userInfo.approvedUser = NO;
        [self invalidLoginAlert];
    }
    else if (self.login.authenticationCheckComplete == YES
             && [self.login.serverAuthenticationResponseString isEqualToString:@"OK"]){ // login successful
        
        self.userInfo.approvedUser = YES;
        if (self.userInfo.approvedUser) {
            [self.userInfo resetFormIDs];
        }
        self.loggedInAs.text = [NSString stringWithFormat:@"Logged in as: %@",self.login.username];
        [self setLoginFieldsAfterLogin];
        [self.view endEditing:YES]; // hide keyboard
        [self checkForPreviousLogin];
        self.homeDataForm = [HomeDataForm sharedHomeDataForm];
        [self.homeDataForm showOrHideButtons];
        [self.homeDataForm setClassSwitch];
    }
}

// uses database to check if user has a previous login
- (void)checkForPreviousLogin{
    RLMResults *results = [UserProvenance objectsWhere:
                           [NSString stringWithFormat:@"username = '%@'",self.userInfo.username]];
    UserProvenance *userProv;
    if (results.count == 1) {
        userProv = [results firstObject];
        [self automateUserCredentialsOnLogin:userProv];
    }
    else{ // new user
        userProv = [[UserProvenance alloc]init];
        userProv.username = self.userInfo.username;
    }
    RLMRealm *realm = [RLMRealm defaultRealm];
    [realm beginWriteTransaction];
    [realm addObject:userProv];
    [realm commitWriteTransaction];
}

// autofill fields previous data persists and fields are empty
- (void)automateUserCredentialsOnLogin:(UserProvenance*)userProv{
    if ([self.homeDataForm.namesLabel.text isEqualToString:@""]) {
        self.homeDataForm.namesLabel.text = self.userInfo.researchers = userProv.researchers;
        [self.homeDataForm addNameToArray:userProv.researchers];
    }
    if ([self.homeDataForm.projectNameTF.text isEqualToString:@""]) {
        self.homeDataForm.projectNameTF.text = self.userInfo.project = userProv.project;
    }
    if ([self.homeDataForm.instructorMentorTF.text isEqualToString:@""]) {
        self.homeDataForm.instructorMentorTF.text = self.userInfo.instructor = userProv.instructor;
    }
    if ([self.homeDataForm.classNumberTF.text isEqualToString:@""]) {
        self.homeDataForm.classNumberTF.text = self.userInfo.classNumber = userProv.classNumber;
    }
}

/******************************************************************************
 * loginAndOut
 *
 * This method checks to see if the user is logging in, or loggin out, and then
 * takes the appropriate step after deciding which.
 ******************************************************************************/
- (IBAction)loginAndOut:(id)sender {
    // LOGIN
    [SystemSounds playTockSound];
    if([self.loginButton.currentTitle isEqualToString:@"Login"]) {
        if ([self testConnectionToServer] == 0) {
            [self popupNoWifiWarning];
        }
        //check if user left fields blank, if yes error, no continue
        else if (![self checkForEmptyLoginFields]) {
            // error was signaled in method called if a field was blank
        }
        else{
            //check loginName & password
            self.login.username = self.loginField.text;
            self.login.password = self.passwordField.text;
            [self.login authenticateUser]; // connect to server for authentication
            
        }
    }
    // LOGOUT
    else{
        //prompt user to save information
        [self popupLogoutMessage];
    }
}

- (void)popupLogoutMessage{
    UIAlertController *alert = [UIAlertController
                                alertControllerWithTitle:@"Notice"
                                message:@"This will reset all data entered."
                                preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel"
                                                     style:UIAlertActionStyleCancel
                                                   handler:nil];
    [alert addAction:cancel];
    UIAlertAction *proceed = [UIAlertAction actionWithTitle:@"Ok"
                                                      style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction *action)
    {
        [self setLoginFieldsAfterLogout]; // reset the login fields and properties after logging out
        [self.plantFormManager deleteAllForms];
        self.plantFormManager.plantCommunityPtr.plantMembers = nil;
        self.plantFormManager = nil; // removes instance of data created in Plant Community Forms
        [self.waterFormManager reset];
        self.waterFormManager = nil;
        self.userInfo.approvedUser = NO;
        WeatherOptions *weather = [WeatherOptions sharedWeatherOptions];
        [weather reset];
    }];
    [alert addAction:proceed];
    [self presentViewController:alert animated:YES completion:nil];
}

/******************************************************************************
 * invalidLoginAlert
 *
 * This method displays an error message when the user cannot login.
 ******************************************************************************/
-(void)invalidLoginAlert{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error"
                        message:@"Username and Password Incorrect"
                        preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *dismiss = [UIAlertAction actionWithTitle:@"Ok"
                    style:UIAlertActionStyleDefault
                    handler:nil];
    [alert addAction:dismiss];
    [self presentViewController:alert animated:YES completion:nil];
}

/******************************************************************************
 * checkForEmptyLoginFields
 *
 * This method checks to see if any of the fields to login were left empty
 * while attempting to login.
 ******************************************************************************/
- (BOOL)checkForEmptyLoginFields{
    if ([self.loginField.text isEqualToString:@""]) {
        [self popupNoNameEntered];
        return NO;
    }
    else if ([self.passwordField.text isEqualToString:@""]){
        [self popupNoPasswordEntered];
        return NO;
    }
    return YES; // passed tests
}

/******************************************************************************
 * loginAsGuest (also change password button after logged in)
 *
 * This method handles all actions when a user logs into a guest account, or
 * if the buttons state is set to changePassword, then it will prompt to ask
 * user for new password information.
 ******************************************************************************/
- (IBAction)hityellowLoginButton:(id)sender {
    [SystemSounds playTockSound];
    if ([self.yellowLoginButton.currentTitle isEqualToString:@"changePassword"]) {
        [self popupForNewPassword];
    }
    else if ([self.yellowLoginButton.currentTitle isEqualToString:@"loginAsUser"]){
        [self popupLoginAsUser];
    }
    else{
        [self.login setGuestLogin:YES];
        self.login.username = @"Guest";
        self.loggedInAs.text = [NSString stringWithFormat:@"Logged in as: %@",self.login.username];
        [self setLoginFieldsAfterLogin];
    }
}

// this gives the user a chance to login while still under guest login
- (void)popupLoginAsUser{
    UIAlertController *login = [UIAlertController
                                alertControllerWithTitle:nil
                                message:@"Enter username and password"
                                preferredStyle:UIAlertControllerStyleAlert];
    [login addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"username";
    }];
    [login addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"password";
    }];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel"
                                                     style:UIAlertActionStyleCancel
                                                   handler:nil];
    [login addAction:cancel];
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Ok"
                                                 style:UIAlertActionStyleDefault
                                               handler:^(UIAlertAction *action)
    {
        UITextField *user = login.textFields[0];
        UITextField *password = login.textFields[1];
        
        self.login.username = user.text;
        self.login.password = password.text;
        [self.login authenticateUser]; // connect to server for authentication
    }];
    [login addAction:ok];
    [self presentViewController:login animated:YES completion:nil];
}

/******************************************************************************
 * setLoginFieldsAfterLogin
 *
 * This method hides the login fields once the user logs in and then displays
 * the label of the username used to login.
 ******************************************************************************/
- (void)setLoginFieldsAfterLogin{
    [self.loggedInAs setHidden:NO];
    [self.loginField setHidden:YES];
    [self.passwordField setHidden:YES];
    
    [self.loginButton setTitle:@"Logout" forState:0];
    
    if (self.login.guestLogin == YES) {
        [self.loginButton setImage:[UIImage imageNamed:@"logoutButton.png"] forState:UIControlStateNormal];
        [self.yellowLoginButton setImage:[UIImage imageNamed:@"loginAsUserButton"] forState:UIControlStateNormal];
        [self.yellowLoginButton setTitle:@"loginAsUser" forState:UIControlStateNormal];
    }
    else{
        [self.loginButton setImage:[UIImage imageNamed:@"logoutButton.png"] forState:UIControlStateNormal];
        [self.yellowLoginButton setImage:[UIImage imageNamed:@"changePasswordButton"] forState:UIControlStateNormal];
        [self.yellowLoginButton setTitle:@"changePassword" forState:UIControlStateNormal];
    }
    if (!self.userInfo) {
        self.userInfo = [UserInfo sharedUserInfo];
        [self.view addSubview:self.homeDataForm.viewBox];
    }
    [self.homeDataForm.viewBox setHidden:NO];
    [self.userInfo resetFormIDs]; // incase user logs in after being logged in as guest
    self.userInfo.username = self.login.username;
    [self.userInfo setApprovedUser:[self.login getNetworkApproval]];
}

/******************************************************************************
 * setLoginFieldsAfterLogout
 *
 * This method resets the login credentials and unhides the login fields so
 * that the user is logged out and can log back in again.
 ******************************************************************************/
- (void)setLoginFieldsAfterLogout{
    if (self.login.guestLogin == YES) {
        [self.login setGuestLogin:NO];
        [self.yellowLoginButton setHidden:NO];
    }

    self.loginField.text = @"";
    self.passwordField.text = @"";
    
    [self.loggedInAs setHidden:YES];
    [self.loginField setHidden:NO];
    [self.passwordField setHidden:NO];

    [self.loginButton setTitle:@"Login" forState:0];
    [self.loginButton setImage:[UIImage imageNamed:@"loginButton"] forState:0];
    [self.yellowLoginButton setTitle:@"guestLogin" forState:0];
    [self.yellowLoginButton setImage:[UIImage imageNamed:@"loginAsGuestButton"] forState:0];
    
    [self.homeDataForm.viewBox setHidden:YES];
    [self.homeDataForm.communityButtons setButtonsInView:NO];
    [self.homeDataForm.communityButtons hideButtons];
    [self.homeDataForm reset];
    
    [self.userInfo setApprovedUser:NO];
    [self.userInfo resetFields];
    
    [self showLogoutMessage];
    [SystemSounds playLogoutSound];
}

/******************************************************************************
 * validatePassword
 *
 * This method will check to see if the old password matches the user entry
 * for it, and if it does return YES, else NO.
 CSS490: Code modified to add confirm password******************************************************************************/
- (BOOL)validatePassword: (NSString*)oldPassword newPassword:(NSString*)newPassword confirmPassword:(NSString*) confirmPassword{
    if ([oldPassword isEqualToString:self.login.password ] && [newPassword isEqualToString:confirmPassword]) {
        return true; // correct old password entered
    }
    return NO;
}

/******************************************************************************
 * testConnectionToServer
 *
 * This method will check to see if the old password matches the user entry
 * for it, and if it does return YES, else NO.
 ******************************************************************************/
- (NSInteger)testConnectionToServer{
    self.internetReachable = [Reachability reachabilityForLocalWiFi];    
    [self.internetReachable startNotifier];
    NSInteger status = self.internetReachable.currentReachabilityStatus;
    if (status == 0) {
        NSLog(@"Not connected to wifi: Internet Status =%d",(int)status);
        [self popupNoWifiWarning];
    }
    return status;
}

/******************************************************************************
 * popupNoWifiWarning
 *
 * This method gives a user a popup warning them there is no wifi connection.
 ******************************************************************************/
- (void)popupNoWifiWarning{
    UIAlertController *noWifiAlert = [UIAlertController alertControllerWithTitle:@"Warning"
                            message:@"No internet connection present. Please check wifi connection."
                            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Ok"
                            style:UIAlertActionStyleCancel
                            handler:nil];
    [noWifiAlert addAction:okAction];
    [self presentViewController:noWifiAlert animated:YES completion:^{}];
}

/******************************************************************************
 * popupNoNameEntered
 *
 * This method gives a user a popup warning when no name was entered for login.
 ******************************************************************************/
- (void)popupNoNameEntered{
    UIAlertController *noNameAlert = [UIAlertController alertControllerWithTitle:@"Error"
                        message:@"You must enter a name."
                        preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Ok"
                        style:UIAlertActionStyleCancel
                        handler:nil];
    [noNameAlert addAction:okAction];
    [self presentViewController:noNameAlert animated:YES completion:^{}];
}

/******************************************************************************
 * popupNoPasswordEntered
 *
 * This method gives a user a popup warning when no password was entered to login.
 ******************************************************************************/
- (void)popupNoPasswordEntered{
    UIAlertController *noPasswordAlert = [UIAlertController alertControllerWithTitle:@"Error"
                        message:@"You must enter a password."
                        preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Ok"
                        style:UIAlertActionStyleCancel
                        handler:nil];
    [noPasswordAlert addAction:okAction];
    [self presentViewController:noPasswordAlert animated:YES completion:nil];
}

/******************************************************************************
 * popupForNewPassword
 *
 * This method gives a user a popup to enter their old password and then enter
 * a new password.
 /*
 * CSS490: Code modified to ensure there's no crash when changing password
******************************************************************************/
- (void)popupForNewPassword{
    UIAlertController *changePassword = [UIAlertController alertControllerWithTitle:@"Change Password"
                                message:@"First enter your OLD password"
                                preferredStyle:UIAlertControllerStyleAlert];
    [changePassword addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Enter Old Password Here";
    }];
    [changePassword addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Enter New Password Here";
    }];
    
    //CSS490: Added reenter
    
    [changePassword addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Re-enter New Password Here";
    }];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel"
                                style:UIAlertActionStyleCancel
                                handler:nil];
    [changePassword addAction:cancel];
    UIAlertAction *done = [UIAlertAction actionWithTitle:@"Done"
                        style:UIAlertActionStyleDefault
                        handler:^(UIAlertAction *action)
   {
       UITextField *oldPass = changePassword.textFields[0];
       UITextField *newPass = changePassword.textFields[1];
       UITextField *confirmPass = changePassword.textFields[2];
       
       newPass.secureTextEntry = true;
       confirmPass.secureTextEntry = true;
       BOOL passwordMatch = [self validatePassword:oldPass.text newPassword:newPass.text confirmPassword:confirmPass.text];
       // password correct, so change password on server
       if (passwordMatch == YES) {
           
           self.login.changedPassword = newPass.text;
           [self.login changePassword];
       }
       else{
           // wrong password was entered, so popup error and call popupForNewPassword again
       NSLog(@"Enter wrong pass");
           
           UIAlertController *wrongPassword = [UIAlertController alertControllerWithTitle:@"Error"
                            message:@"You entered the wrong password."
                            preferredStyle:UIAlertControllerStyleAlert];
           
           UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Ok"
                        style:UIAlertActionStyleCancel
                        handler:^(UIAlertAction *action){[self popupForNewPassword];}];
            [wrongPassword addAction:ok];
           [self presentViewController:wrongPassword animated:YES completion:^{}];
       }
   }];
    [changePassword addAction:done];
    [self presentViewController:changePassword animated:YES completion:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:YES];
}

- (void)popupMissingName{
    UIAlertController *missingNameAlert = [UIAlertController alertControllerWithTitle:@"Missing Information"
                                    message:@"You need to enter researcher name(s) first."
                                    preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel"
                                    style:UIAlertActionStyleCancel handler:nil];
    [missingNameAlert addAction:cancelAction];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Ok"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction *action) {
        [self.homeDataForm getNewName:self.userInfo];
    }];
    [missingNameAlert addAction:okAction];
    [self presentViewController:missingNameAlert animated:YES completion:nil];
}

- (void)displayAllFontsAvailable{
    // Logs what fonts are available for use
     for (NSString* family in [UIFont familyNames])
         {
         NSLog(@"%@", family);
         
         for (NSString* name in [UIFont fontNamesForFamilyName: family])
         {
         NSLog(@"  %@", name);
         }
     }
}

- (void)showLogoutMessage{
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Logout Successful"
                            message:nil
                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK"
                                    style:UIAlertActionStyleDefault
                                    handler:nil];
    
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == self.loginField) {
        [self.passwordField becomeFirstResponder];
    }
    else if (textField == self.passwordField) {
        [self loginAndOut:self.loginButton];
    }

    return YES;
}

- (IBAction)tappedDocumentationButton:(id)sender {
    [SystemSounds playTockSound];
}

// prep all data that needs to be sent over to the PhotoViewer during segue
- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"homeToPlantSeg"]) {
        PlantCommunity *controller = (PlantCommunity *)segue.destinationViewController;
        controller.internetReachable = self.internetReachable;
        if (self.plantFormManager != nil) {
            controller.plantFormManager = self.plantFormManager;
        }
    }
    else if ([segue.identifier isEqualToString:@"homeToWaterSegue"]) {
        WaterCommunity *controller = (WaterCommunity *)segue.destinationViewController;
        controller.internetReachable = self.internetReachable;
    }
    else if ([segue.identifier isEqualToString:@"homeToDocSegue"]) {

    }
    if (self.userInfo.weatherOptionsPtr.backgroundView.hidden == NO) {
        [self.userInfo.weatherOptionsPtr toggleVisibility];
    }
}

@end
