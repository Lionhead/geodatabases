//
//  HomeDataForm.m
//  GeoDatabaseWetlands
//
//  Created by Jonathan Mason on 9/23/15.
//  Copyright © 2015 Jonathan Mason. All rights reserved.
//

#import "UserInfo.h"
#import "HomeDataForm.h"
#import "UIViewControllerTracker.h"
#import "NameDeleteMenu.h"
#import "WeatherOptions.h"
#import "CommunityButtons.h"
#import "UserProvenance.h"
#import "SystemSounds.h"
#import <Realm/Realm.h>

@interface HomeDataForm() <UITextFieldDelegate>

@property (strong, nonatomic) UIViewControllerTracker *vcTracker;
@property (strong, nonatomic) WeatherOptions *weatherOptionsPtr;
@property (strong, nonatomic) NSMutableArray *names;
@end

@implementation HomeDataForm

+ (id)sharedHomeDataForm {
    static dispatch_once_t onceToken;
    __strong static id sharedObject = nil;
    
    dispatch_once(&onceToken, ^{
        sharedObject = [[self alloc]init];
    });
    
    return sharedObject;
}

- (id)init {
    if (self = [super init]) {
        
    }
    return self;
}

- (void)createWithViewController:(UIViewController*)vc {
    self.vcTracker = [UIViewControllerTracker sharedVC];
    self.vcTracker.currentViewController = vc;
    
    [self configureBackground];
    [self configureAreaForNames];
    [self configureTextFieldsAndSwitch];
    [self configureWeatherButton];
    
    self.communityButtons = [[CommunityButtons alloc]init];
    [self.vcTracker.currentViewController.view addSubview:self.communityButtons.buttonsView];
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(doneEditingTextField:)
     name:UITextFieldTextDidEndEditingNotification
     object:nil];
    
    [self initializeProperties];
}


- (void)configureBackground {
    self.viewBox = [[UIView alloc]initWithFrame:CGRectMake(59, 540, 650, 255)];
    [self.viewBox setHidden:YES];
    UIImageView *userInfoBackground = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"homeUserInfoBackground"]];
    [userInfoBackground setFrame:CGRectMake(0, 0, self.viewBox.frame.size.width, self.viewBox.frame.size.height)];
    [userInfoBackground setContentMode:UIViewContentModeScaleAspectFit];
    [self.viewBox addSubview:userInfoBackground];
}

- (void)configureAreaForNames {
    // add image of where names will go
    UIImageView *researchersImage = [[UIImageView alloc]initWithImage:
                                     [UIImage imageNamed:@"researchers540x40"]];
    researchersImage.frame = CGRectMake(56, 17, 540, 40);
    [self.viewBox addSubview:researchersImage];
    
    self.addNameButton = [[UIButton alloc]initWithFrame:CGRectMake(13, 20, 34, 34)];
    self.addNameButton.showsTouchWhenHighlighted = YES;
    [[self.addNameButton imageView]setContentMode:UIViewContentModeScaleAspectFit]; // scale to fit
    [self.addNameButton setImage:[UIImage imageNamed:@"addButton"]
                        forState:UIControlStateNormal];
    [self.addNameButton addTarget:self action:@selector(getNewName:)
             forControlEvents:UIControlEventTouchUpInside];
    [self.viewBox addSubview:self.addNameButton];
    
    self.subtractNameButton = [[UIButton alloc]initWithFrame:CGRectMake(606, 20, 34, 34)];
    self.subtractNameButton.showsTouchWhenHighlighted = YES;
    [[self.subtractNameButton imageView]setContentMode:UIViewContentModeScaleAspectFit];
    [self.subtractNameButton setImage:[UIImage imageNamed:@"subtractButton"]
                             forState:UIControlStateNormal];
    [self.subtractNameButton addTarget:self action:@selector(removeTheName:)
                      forControlEvents:UIControlEventTouchUpInside];
    [self.viewBox addSubview:self.subtractNameButton];
    
    // add label to display names
    self.namesLabel = [[UILabel alloc]initWithFrame:CGRectMake(198, 22, 390, 30)];
    self.namesLabel.textColor = [UIColor whiteColor];
    [self.namesLabel setFont:[UIFont fontWithName:@"HelveticaNeue" size:22]]; // custom font
    self.namesLabel.userInteractionEnabled = YES;
    [self.viewBox addSubview:self.namesLabel];
    UITapGestureRecognizer *namesTapGesture = [[UITapGestureRecognizer alloc]
                                               initWithTarget:self action:@selector(tappedNamesLabel)];
    [self.namesLabel addGestureRecognizer:namesTapGesture];
}

- (void)configureTextFieldsAndSwitch {
    self.projectNameTF = [[UITextField alloc]initWithFrame:CGRectMake(56, 79, 260, 40)];
    self.projectNameTF.borderStyle = UITextBorderStyleRoundedRect;
    self.projectNameTF.font = [UIFont systemFontOfSize:18];
    self.projectNameTF.placeholder = @"Project Name";
    self.projectNameTF.autocorrectionType = UITextAutocorrectionTypeNo;
    self.projectNameTF.keyboardType = UIKeyboardTypeDefault;
    self.projectNameTF.returnKeyType = UIReturnKeyDone;
    self.projectNameTF.clearButtonMode = UITextFieldViewModeWhileEditing;
    self.projectNameTF.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    self.projectNameTF.delegate = self;
    [self.viewBox addSubview:self.projectNameTF];
    
    self.instructorMentorTF = [[UITextField alloc]initWithFrame:CGRectMake(336, 79, 260, 40)];
    self.instructorMentorTF.borderStyle = UITextBorderStyleRoundedRect;
    self.instructorMentorTF.font = [UIFont systemFontOfSize:18];
    self.instructorMentorTF.placeholder = @"Instructor/Mentor";
    self.instructorMentorTF.autocorrectionType = UITextAutocorrectionTypeNo;
    self.instructorMentorTF.keyboardType = UIKeyboardTypeDefault;
    self.instructorMentorTF.returnKeyType = UIReturnKeyDone;
    self.instructorMentorTF.clearButtonMode = UITextFieldViewModeWhileEditing;
    self.instructorMentorTF.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    self.instructorMentorTF.delegate = self;
    [self.viewBox addSubview:self.instructorMentorTF];
    
    self.currentTempTF = [[UITextField alloc]initWithFrame:CGRectMake(56, 141, 260, 40)];
    self.currentTempTF.borderStyle = UITextBorderStyleRoundedRect;
    self.currentTempTF.font = [UIFont systemFontOfSize:18];
    self.currentTempTF.placeholder = @"Current Air Temperature";
    self.currentTempTF.autocorrectionType = UITextAutocorrectionTypeNo;
    self.currentTempTF.keyboardType = UIKeyboardTypeNumberPad;
    self.currentTempTF.returnKeyType = UIReturnKeyDone;
    self.currentTempTF.clearButtonMode = UITextFieldViewModeWhileEditing;
    self.currentTempTF.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    self.currentTempTF.delegate = self;
    [self.viewBox addSubview:self.currentTempTF];
    
    self.classNumberTF = [[UITextField alloc]initWithFrame:CGRectMake(400, 141, 196, 40)];
    self.classNumberTF.borderStyle = UITextBorderStyleRoundedRect;
    self.classNumberTF.font = [UIFont systemFontOfSize:18];
    self.classNumberTF.placeholder = @"Class Number";
    self.classNumberTF.autocorrectionType = UITextAutocorrectionTypeNo;
    self.classNumberTF.keyboardType = UIKeyboardTypeNumberPad;
    self.classNumberTF.returnKeyType = UIReturnKeyDone;
    self.classNumberTF.clearButtonMode = UITextFieldViewModeWhileEditing;
    self.classNumberTF.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    self.classNumberTF.hidden = YES;
    self.classNumberTF.delegate = self;
    [self.viewBox addSubview:self.classNumberTF];
    
    self.independentResearchLabel = [[UILabel alloc]initWithFrame:CGRectMake(400, 141, 196, 40)];
    self.independentResearchLabel.text = @"Independent Research";
    self.independentResearchLabel.textColor = [UIColor whiteColor];
    [self.independentResearchLabel setFont:[UIFont fontWithName:@"HelveticaNeue" size:19]];
    [self.viewBox addSubview:self.independentResearchLabel];
    
    self.classSwitch = [[UISwitch alloc]initWithFrame:CGRectMake(336, 145, 0, 0)];
    [self.classSwitch addTarget:self action:@selector(classSwitchEvent:)
               forControlEvents:UIControlEventValueChanged];
    [self.viewBox addSubview:self.classSwitch];
}

- (void)configureWeatherButton {
    // add weather conditions button
    self.weatherButton = [[UIButton alloc]initWithFrame:CGRectMake(195, 203, 260, 40)];
    [self.weatherButton setBackgroundImage:[UIImage imageNamed:@"weatherConditionsButton"]
                              forState:UIControlStateNormal];
    self.weatherButton.showsTouchWhenHighlighted = YES;
    [self.viewBox addSubview:self.weatherButton];
    UITapGestureRecognizer *weatherTapGesture = [[UITapGestureRecognizer alloc]
                                                 initWithTarget:self action:@selector(tappedWeatherButton)];
    [self.weatherButton addGestureRecognizer:weatherTapGesture];
}

// avoids inserting nil properties into NSDictionary during POST operation
- (void)initializeProperties {
    self.namesLabel.text = @"";
    self.projectNameTF.text = @"";
    self.instructorMentorTF.text = @"";
    self.currentTempTF.text = @"";
    self.classNumberTF.text = @"";
}

- (void)createCommunityButtons {
    if (!self.communityButtons) {
        self.communityButtons = [[CommunityButtons alloc]init];
    }
}

/******************************************************************************
 * tappedWeatherButton
 *
 * This method switches the visibility of the weather options between hidden
 * or not hidden.
 ******************************************************************************/
- (void)tappedWeatherButton {
    [SystemSounds playTockSound];
    // call to open weather menu
    WeatherOptions *weather = [WeatherOptions sharedWeatherOptions];
    //self.weatherOptionsPtr = weather; //this will allow the userInfo class to reference weather data
    [self.vcTracker.currentViewController.view addSubview:weather.backgroundView];
    [self.vcTracker.currentViewController.view addSubview:weather.instructions];
    [weather toggleVisibility];
    
    if (weather.backgroundView.isHidden) {
        [self updateWeatherConditions];
    }
}

// updates any selected changes to userInfo after closing weather window
- (void)updateWeatherConditions {
    WeatherOptions *weather = [WeatherOptions sharedWeatherOptions];
    UserInfo *userInfo = [UserInfo sharedUserInfo];
    userInfo.weatherConditions = @"";
    for (id key in weather.selectedWeatherConditions) {
        UILabel *label = weather.selectedWeatherConditions[key];
        if (label.textColor == [UIColor whiteColor]) {
            label = [weather.selectedWeatherConditions valueForKey:key];
            userInfo.weatherConditions = [userInfo.weatherConditions stringByAppendingString:
                                      [NSString stringWithFormat:@"%@, ",label.text]];
        }
    }
    if (userInfo.weatherConditions) {
        if (userInfo.weatherConditions.length > 2) {
            userInfo.weatherConditions = [userInfo.weatherConditions
                                          substringToIndex:userInfo.weatherConditions.length-2];
        }
    }
}

// takes weather conditions loaded in from local database and passes to WeatherOptions to update
- (void)setLoadedWeatherConditions {
    UserInfo *userInfo = [UserInfo sharedUserInfo];
    WeatherOptions *weather = [WeatherOptions sharedWeatherOptions];
    NSString *str = [userInfo.weatherConditions stringByReplacingOccurrencesOfString:@", " withString:@","];
    NSArray *conditions = [str componentsSeparatedByString:@","];
    [weather copyWeatherOptionsFromDatabase:conditions];
}

/******************************************************************************
 * removeTheName
 
 * This method creates a NameDeleteMenu object which inherits from UITableView
 * to display a list of options to delete a name.
 *****************************************************************************/
- (void)removeTheName:(id)sender {
    // only call if there is at least one name present
    if (![self.namesLabel.text isEqualToString:@""]) {
        [SystemSounds playTockSound];
        NameDeleteMenu *deleteMenu = [[NameDeleteMenu alloc]initWithNames:self.names viewController:self.vcTracker.currentViewController];
        [deleteMenu loadView:self.names]; // loads the UITableView
    }
    
}

/******************************************************************************
 * classSwitchEvent
 *
 * This method switches between showing class number or independent research.
 ******************************************************************************/
- (void)classSwitchEvent:(id)sender {
    if (self.classSwitch.on == YES) {
        self.classNumberTF.hidden = NO;
        self.independentResearchLabel.hidden = YES;
    }
    else{
        self.classNumberTF.hidden = YES;
        self.independentResearchLabel.hidden = NO;
    }
}

/******************************************************************************
 * tappedNamesLabel
 *
 * Shows the user what names have been entered. This is useful if the length
 * of the screen becomes truncated, not allowing all names to appear.
 ******************************************************************************/
- (void)tappedNamesLabel {
    if (self.names.count > 0) {
        [SystemSounds playTockSound];
        UIAlertController *editName = [UIAlertController
                                       alertControllerWithTitle:@"Names Entered"
                                       message:@"Select a name to edit."
                                       preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel"
                                                         style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction *action) {
                                                           NSLog(@"cancel");
                                                       }];
        [editName addAction:cancel];
        
        for (int i = 0; i < self.names.count; i++) {
            UIAlertAction *addName = [UIAlertAction
                                      actionWithTitle:[NSString stringWithFormat:@"%@",self.names[i]]
                                      style:UIAlertActionStyleDefault
                                      handler:^(UIAlertAction *action) {
                                          [self editNameAtIndex:i];
                                      }];
            [editName addAction:addName];
        }
        
        [self.vcTracker.currentViewController presentViewController:editName animated:YES completion:nil];
    }
}

/******************************************************************************
 * displayNames
 *
 * This method loads all names listed in the namesArray and displays them
 * using the namesLabel on screen. If there are no names then text = @""
 *****************************************************************************/
- (void)displayNames {
    if (self.names.count == 0) {
        self.namesLabel.text = @"";
    }
    else{
        for (int i = 0; i < [self.names count]; i++) {
            NSString *name = self.names[i];
            if (i > 0) {
                self.namesLabel.text = [self.namesLabel.text stringByAppendingString:
                                        [NSString stringWithFormat:@", %@",name]];
            }
            else
                self.namesLabel.text = [NSString stringWithFormat:@"%@",name];
        }
    }
    UserInfo *user = [UserInfo sharedUserInfo];
    [user updateResearchers:self.namesLabel.text];
}

/***************************************************************************************************
 * doneEditingTextField
 *
 * This method is called when the textField is finished editing and will check what field was edited,
 * and then take action to update userInfo. It will also make any necessary changes to the database.
 **************************************************************************************************/
- (void)doneEditingTextField:(NSNotification *)notification {
    UserInfo *userInfo = [UserInfo sharedUserInfo];
    UITextField *textField = notification.object;
    
    RLMResults *results = [UserProvenance objectsWhere:
                           [NSString stringWithFormat:@"username = '%@'",userInfo.username]];
    UserProvenance *userProv;
    RLMRealm *realm = [RLMRealm defaultRealm];
    [realm beginWriteTransaction];
    if ([results firstObject]) {
        userProv = [results firstObject];
    }
    
    if ([textField isEqual:self.projectNameTF]) {
        userInfo.project = self.projectNameTF.text;
        [self showOrHideButtons];
        if (userProv) {
            if (![userProv.project isEqualToString:self.projectNameTF.text]) {
                userProv.project = self.projectNameTF.text;
            }
        }
    }
    else if ([textField isEqual:self.instructorMentorTF]) {
        userInfo.instructor = self.instructorMentorTF.text;
        if (userProv) {
            if (![userProv.instructor isEqualToString:self.instructorMentorTF.text]) {
                userProv.instructor = self.instructorMentorTF.text;
            }
        }
    }
    else if ([textField isEqual:self.currentTempTF]) {
        userInfo.currentTemp = self.currentTempTF.text;
    }
    else if ([textField isEqual:self.classNumberTF]) {
        userInfo.classNumber = self.classNumberTF.text;
        if (userProv) {
            if (![userProv.classNumber isEqualToString:self.classNumberTF.text]) {
                userProv.classNumber = self.classNumberTF.text;
            }
        }
    }
    if (userProv) {
        [realm addObject:userProv];
    }
    [realm commitWriteTransaction];
}

/******************************************************************************
 * textFieldShouldReturn
 *
 * This method tells the UITextField to relinquish status as first responder.
 ******************************************************************************/
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

/******************************************************************************
 * reverseVisible
 *
 * This method reverses the hidden attribute of the userInfo viewBox.
 *****************************************************************************/
- (void)reverseVisible {
    if (self.viewBox.hidden == NO) {
        self.viewBox.hidden = YES;
    }
    else
        self.viewBox.hidden = NO;
}

/******************************************************************************
 * getNewName
 *
 * This method is called when the addNameButton is tapped and it prompts the
 * user to enter information for new name.
 *****************************************************************************/
- (void)getNewName:(id)sender {
    [SystemSounds playTockSound];
    UIAlertController *getName = [UIAlertController alertControllerWithTitle:@"Enter First and Last Name"
                                                                     message:nil preferredStyle:UIAlertControllerStyleAlert];
    [getName addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"First name";
        textField.autocapitalizationType = UITextAutocapitalizationTypeSentences;
    }];
    [getName addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Last name";
        textField.autocapitalizationType = UITextAutocapitalizationTypeSentences;
    }];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel
                                                   handler:^(UIAlertAction *action) {}];
    [getName addAction:cancel];
    UIAlertAction *doneAction = [UIAlertAction actionWithTitle:@"Done" style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction *action)
    {
           UITextField *firstName, *lastName;
           firstName = getName.textFields[0];
           lastName = getName.textFields[1];
           if ([firstName.text isEqualToString:@""] || [lastName.text isEqualToString:@""]) {
               [self getNewName:sender];
           }
           else{
               NSString *fullName = [firstName.text stringByAppendingString:
                                     [NSString stringWithFormat:@" %@",lastName.text]];
               [self addName:fullName];
           }
       }];
    [getName addAction:doneAction];
    [self.vcTracker.currentViewController presentViewController:getName animated:YES completion:nil];
}

/******************************************************************************
 * addName
 
 * This method adds the name entered into the array, and then displays it on
 * the screen.
 *****************************************************************************/
- (void)addName:(NSString*)nameEntered {
    // create array to hold one name if it isn't created yet
    if (self.names == nil) {
        self.names = [[NSMutableArray alloc]initWithCapacity:1];
        [self.names insertObject:nameEntered atIndex:0];
    }
    else{
        [self.names addObject:nameEntered];
    }
    [self displayNames];
    if (self.names.count > 0 && ![self.projectNameTF.text isEqualToString:@""]) {
        [self.communityButtons moveDownIntoView];
    }
    if (self.names.count == 1) {
        [self addNameToDatabase:[self.names firstObject]];
    }
}

- (void)addNameToDatabase:(NSString*)name {
    UserInfo *userInfo = [UserInfo sharedUserInfo];
    RLMResults *results = [UserProvenance objectsWhere:
                           [NSString stringWithFormat:@"username = '%@'",userInfo.username]];
    UserProvenance *userProv;
    
    if ([results firstObject]) {
        userProv = [results firstObject];
    }
    else{
        userProv = [[UserProvenance alloc]init];
    }
    RLMRealm *realm = [RLMRealm defaultRealm];
    [realm beginWriteTransaction];
    userProv.researchers = name;
    [realm addObject:userProv];
    [realm commitWriteTransaction];
}

- (void)addNameToArray:(NSString*)name {
    if (!self.names) {
        self.names = [NSMutableArray new];
    }
    if ([name containsString:@","]) { // multiple names to insert
        NSArray *temp = [name componentsSeparatedByString:@","];
        for (id object in temp) {
            NSString *str = object;
            if ([str characterAtIndex:0] == ' ') {
                [str stringByReplacingCharactersInRange:NSMakeRange(0, 1) withString:@""];
            }
            [self.names addObject:str];
        }
    }
    else {
        [self.names addObject:name];
    }
}

/******************************************************************************
 * subtractName
 *
 * This method subtracts the name the user selected to remove, and then
 * displays what is left in the array, if any.
 *****************************************************************************/
- (void)subtractName:(NSUInteger)index {
    [self.names removeObjectAtIndex:index];
    [self displayNames];
    if (self.names.count == 0) {
        [self.communityButtons moveUpOutOfView];
    }
}

/******************************************************************************
 * subtractAllNames
 *
 * This method subtracts all names and then calls to make the view disappear.
 *****************************************************************************/
- (void)subtractAllNames {
    [self.names removeAllObjects];
    [self displayNames];
    [self.communityButtons moveUpOutOfView];
}

/******************************************************************************
 * editNameAtIndex
 *
 * This method gives the user a popup to enter a new name to in place of the
 * name the user selected to edit.
 *****************************************************************************/
- (void)editNameAtIndex:(int)index {
    UIAlertController *editName = [UIAlertController alertControllerWithTitle:@"Enter First and Last Name"
                                                                      message:nil preferredStyle:UIAlertControllerStyleAlert];
    [editName addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"First name";
        [textField setAutocapitalizationType:UITextAutocapitalizationTypeSentences];
    }];
    [editName addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Last name";
        [textField setAutocapitalizationType:UITextAutocapitalizationTypeSentences];
    }];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel
                                                   handler:^(UIAlertAction *action) {}];
    [editName addAction:cancel];
    UIAlertAction *doneAction = [UIAlertAction actionWithTitle:@"Done"
                                                         style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction *action)
    {
           UITextField *firstName, *lastName;
           firstName = editName.textFields[0];
           lastName = editName.textFields[1];
           if ([firstName.text isEqualToString:@""] || [lastName.text isEqualToString:@""]) {
               [self editNameAtIndex:index];
           }
           else{
               NSString *fullName = [firstName.text stringByAppendingString:
                                     [NSString stringWithFormat:@" %@",lastName.text]];
               [self.names replaceObjectAtIndex:index withObject:fullName];
               [self displayNames];
           }
       }];
    [editName addAction:doneAction];
    [self.vcTracker.currentViewController presentViewController:editName animated:YES completion:nil];
}

- (void)showOrHideButtons {
    UserInfo *userInfo = [UserInfo sharedUserInfo];
    if (![self.projectNameTF.text isEqualToString:@""]
        && ![userInfo.researchers isEqualToString:@""]
        && self.communityButtons.buttonsInView == NO) {
        [self.communityButtons moveDownIntoView];
    }
    else{
        if (self.communityButtons.buttonsInView == YES) {
            if ([self.projectNameTF.text isEqualToString:@""]){
                [self.communityButtons moveUpOutOfView];
            }
        }
    }
}

// resets properties when user logs out
- (void)reset {
    self.namesLabel.text = @"";
    [self.names removeAllObjects];
    self.projectNameTF.text = @"";
    self.instructorMentorTF.text = @"";
    self.currentTempTF.text = @"";
    self.classNumberTF.text = @"";
    [self.classSwitch setOn:NO];
    [self.classNumberTF setHidden:YES];
    [self.independentResearchLabel setHidden:NO];
}

// sets switch properties when user logs in (called from HomeScreen class)
- (void)setClassSwitch {
    if ([self.classNumberTF.text isEqualToString:@""]) {
        [self.classSwitch setOn:NO animated:YES];
        [self.classNumberTF setHidden:YES];
        [self.independentResearchLabel setHidden:NO];
    }
    else{
        [self.classSwitch setOn:YES animated:YES];
        [self.classNumberTF setHidden:NO];
        [self.independentResearchLabel setHidden:YES];
    }
}

@end
