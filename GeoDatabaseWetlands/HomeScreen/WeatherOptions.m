//
//  WeatherOptions.m
//  GeoDatabaseWetlands
//
//  Created by Jonathan Mason on 4/30/15.
//  Copyright (c) 2015 Jonathan Mason. All rights reserved.
//

#import "WeatherOptions.h"
#define sunnyKey                        @"Sunny"
#define partlyCloudyKey                 @"Partly Cloudy"
#define cloudyKey                       @"Cloudy"
#define overcastKey                     @"Overcast"
#define fogMistKey                      @"Fog/Mist"
#define lightRainKey                    @"Light Rain"
#define rainKey                         @"Rain"
#define thunderstormKey                 @"Thunderstorm"
#define lightWindKey                    @"Light Wind"
#define windyKey                        @"Windy"
#define heavyWindsKey                   @"Heavy Winds"
#define rainSnowMixKey                  @"Rain/Snow Mix"
#define lightSnowKey                    @"Light Snow"
#define snowKey                         @"Snow"
#define heavySnowKey                    @"Heavy Snow"
#define hazeSmokeKey                    @"Haze/Smoke"
#define hailKey                         @"Hail"
#define humidKey                        @"Humid"
#define selectedWeatherConditionsKey    @"selectedWeatherConditions"

@implementation WeatherOptions

+ (id)sharedWeatherOptions {
    static dispatch_once_t onceToken;
    __strong static id sharedObject = nil;
    
    dispatch_once(&onceToken, ^{
        sharedObject = [[self alloc]init];
    });
    
    return sharedObject;
}

- (id)init {
    if (self = [super init]) {
        _selectedWeatherConditions = [NSMutableDictionary new];
        [self setupInstructions];
        [self setupBackground];
        [self setupLabels];
        [self populateLabelsIntoView];
    }
    return self;
}

- (void)reset {
    UILabel *label;
    for (id key in self.selectedWeatherConditions) {
        label = self.selectedWeatherConditions[key];
        label.textColor = [UIColor grayColor];
    }
}

/******************************************************************************
 * setupInstructions
 
 * This method creates a label used to show instructions above the weather
 * selection table.
 *****************************************************************************/
- (void)setupInstructions {
    self.instructions = [[UILabel alloc]initWithFrame:CGRectMake(199, 325, 370, 40)];
    self.instructions.textColor = [UIColor whiteColor];
    self.instructions.text = @"Tap to Highlight Your Selection(s)";
    self.instructions.textAlignment = NSTextAlignmentCenter;
    [self.instructions setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:25]];
    [self.instructions setHidden:YES];
}

/******************************************************************************
 * setupBackground
 
 * This method creates the background behind where the labels are shown.
 *****************************************************************************/
- (void)setupBackground {
    self.backgroundView = [[UIImageView alloc]initWithFrame:CGRectMake(199, 365, 370, 370)];
    [self.backgroundView setImage:[UIImage imageNamed:@"weatherBackgroundView"]];
    [self.backgroundView setUserInteractionEnabled:YES];
    [self.backgroundView setHidden:YES];
}

/******************************************************************************
 * setupLabels
 
 * This method creates the labels that are used to select each weather
 * condition.
 *****************************************************************************/
- (void)setupLabels {

    self.sunny = [[UILabel alloc]initWithFrame:CGRectMake(10, 5, 175, 40)];
    [self.sunny setText:@"Sunny"];
    [self.sunny setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:25]];
    self.sunny.textAlignment = NSTextAlignmentCenter;
    self.sunny.textColor = [UIColor grayColor];
    [self.sunny addGestureRecognizer:[self assignTapGestureRecognizer]];
    [self.selectedWeatherConditions setObject:self.sunny forKey:sunnyKey];
    
    self.partlyCloudy = [[UILabel alloc]initWithFrame:CGRectMake(10, 45, 175, 40)];
    [self.partlyCloudy setText:@"Partly Cloudy"];
    [self.partlyCloudy setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:25]];
    self.partlyCloudy.textAlignment = NSTextAlignmentCenter;
    self.partlyCloudy.textColor = [UIColor grayColor];
    [self.partlyCloudy addGestureRecognizer:[self assignTapGestureRecognizer]];
    [self.selectedWeatherConditions setObject:self.partlyCloudy forKey:partlyCloudyKey];
    
    self.cloudy = [[UILabel alloc]initWithFrame:CGRectMake(10, 85, 175, 40)];
    [self.cloudy setText:@"Cloudy"];
    [self.cloudy setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:25]];
    self.cloudy.textAlignment = NSTextAlignmentCenter;
    self.cloudy.textColor = [UIColor grayColor];
    [self.cloudy addGestureRecognizer:[self assignTapGestureRecognizer]];
    [self.selectedWeatherConditions setObject:self.cloudy forKey:cloudyKey];
    
    self.overcast = [[UILabel alloc]initWithFrame:CGRectMake(10, 125, 175, 40)];
    [self.overcast setText:@"Overcast"];
    [self.overcast setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:25]];
    self.overcast.textAlignment = NSTextAlignmentCenter;
    self.overcast.textColor = [UIColor grayColor];
    [self.overcast addGestureRecognizer:[self assignTapGestureRecognizer]];
    [self.selectedWeatherConditions setObject:self.overcast forKey:overcastKey];
    
    self.fogMist = [[UILabel alloc]initWithFrame:CGRectMake(10, 165, 175, 40)];
    [self.fogMist setText:@"Fog/Mist"];
    [self.fogMist setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:25]];
    self.fogMist.textAlignment = NSTextAlignmentCenter;
    self.fogMist.textColor = [UIColor grayColor];
    [self.fogMist addGestureRecognizer:[self assignTapGestureRecognizer]];
    [self.selectedWeatherConditions setObject:self.fogMist forKey:fogMistKey];
    
    self.lightRain = [[UILabel alloc]initWithFrame:CGRectMake(10, 205, 175, 40)];
    [self.lightRain setText:@"Light Rain"];
    [self.lightRain setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:25]];
    self.lightRain.textAlignment = NSTextAlignmentCenter;
    self.lightRain.textColor = [UIColor grayColor];
    [self.lightRain addGestureRecognizer:[self assignTapGestureRecognizer]];
    [self.selectedWeatherConditions setObject:self.lightRain forKey:lightRainKey];
    
    self.rain = [[UILabel alloc]initWithFrame:CGRectMake(10, 245, 175, 40)];
    [self.rain setText:@"Rain"];
    [self.rain setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:25]];
    self.rain.textAlignment = NSTextAlignmentCenter;
    self.rain.textColor = [UIColor grayColor];
    [self.rain addGestureRecognizer:[self assignTapGestureRecognizer]];
    [self.selectedWeatherConditions setObject:self.rain forKey:rainKey];
    
    self.thunderstorm = [[UILabel alloc]initWithFrame:CGRectMake(10, 285, 175, 40)];
    [self.thunderstorm setText:@"Thunderstorm"];
    [self.thunderstorm setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:25]];
    self.thunderstorm.textAlignment = NSTextAlignmentCenter;
    self.thunderstorm.textColor = [UIColor grayColor];
    [self.thunderstorm addGestureRecognizer:[self assignTapGestureRecognizer]];
    [self.selectedWeatherConditions setObject:self.thunderstorm forKey:thunderstormKey];
    
    self.lightWind = [[UILabel alloc]initWithFrame:CGRectMake(10, 325, 175, 40)];
    [self.lightWind setText:@"Light Wind"];
    [self.lightWind setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:25]];
    self.lightWind.textAlignment = NSTextAlignmentCenter;
    self.lightWind.textColor = [UIColor grayColor];
    [self.lightWind addGestureRecognizer:[self assignTapGestureRecognizer]];
    [self.selectedWeatherConditions setObject:self.lightWind forKey:lightWindKey];
    
    self.windy = [[UILabel alloc]initWithFrame:CGRectMake(185, 5, 175, 40)];
    [self.windy setText:@"Windy"];
    [self.windy setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:25]];
    self.windy.textAlignment = NSTextAlignmentCenter;
    self.windy.textColor = [UIColor grayColor];
    [self.windy addGestureRecognizer:[self assignTapGestureRecognizer]];
    [self.selectedWeatherConditions setObject:self.windy forKey:windyKey];
    
    self.heavyWinds = [[UILabel alloc]initWithFrame:CGRectMake(185, 45, 175, 40)];
    [self.heavyWinds setText:@"Heavy Winds"];
    [self.heavyWinds setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:25]];
    self.heavyWinds.textAlignment = NSTextAlignmentCenter;
    self.heavyWinds.textColor = [UIColor grayColor];
    [self.heavyWinds addGestureRecognizer:[self assignTapGestureRecognizer]];
    [self.selectedWeatherConditions setObject:self.heavyWinds forKey:heavyWindsKey];
    
    self.rainSnowMix = [[UILabel alloc]initWithFrame:CGRectMake(185, 85, 175, 40)];
    [self.rainSnowMix setText:@"Rain/Snow Mix"];
    [self.rainSnowMix setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:25]];
    self.rainSnowMix.textAlignment = NSTextAlignmentCenter;
    self.rainSnowMix.textColor = [UIColor grayColor];
    [self.rainSnowMix addGestureRecognizer:[self assignTapGestureRecognizer]];
    [self.selectedWeatherConditions setObject:self.rainSnowMix forKey:rainSnowMixKey];
    
    self.lightSnow = [[UILabel alloc]initWithFrame:CGRectMake(185, 125, 175, 40)];
    [self.lightSnow setText:@"Light Snow"];
    [self.lightSnow setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:25]];
    self.lightSnow.textAlignment = NSTextAlignmentCenter;
    self.lightSnow.textColor = [UIColor grayColor];
    [self.lightSnow addGestureRecognizer:[self assignTapGestureRecognizer]];
    [self.selectedWeatherConditions setObject:self.lightSnow forKey:lightSnowKey];
    
    self.snow = [[UILabel alloc]initWithFrame:CGRectMake(185, 165, 175, 40)];
    [self.snow setText:@"Snow"];
    [self.snow setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:25]];
    self.snow.textAlignment = NSTextAlignmentCenter;
    self.snow.textColor = [UIColor grayColor];
    [self.snow addGestureRecognizer:[self assignTapGestureRecognizer]];
    [self.selectedWeatherConditions setObject:self.snow forKey:snowKey];
    
    self.heavySnow = [[UILabel alloc]initWithFrame:CGRectMake(185, 205, 175, 40)];
    [self.heavySnow setText:@"Heavy Snow"];
    [self.heavySnow setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:25]];
    self.heavySnow.textAlignment = NSTextAlignmentCenter;
    self.heavySnow.textColor = [UIColor grayColor];
    [self.heavySnow addGestureRecognizer:[self assignTapGestureRecognizer]];
    [self.selectedWeatherConditions setObject:self.heavySnow forKey:heavySnowKey];
    
    self.hazeSmoke = [[UILabel alloc]initWithFrame:CGRectMake(185, 245, 175, 40)];
    [self.hazeSmoke setText:@"Haze/Smoke"];
    [self.hazeSmoke setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:25]];
    self.hazeSmoke.textAlignment = NSTextAlignmentCenter;
    self.hazeSmoke.textColor = [UIColor grayColor];
    [self.hazeSmoke addGestureRecognizer:[self assignTapGestureRecognizer]];
    [self.selectedWeatherConditions setObject:self.hazeSmoke forKey:hazeSmokeKey];
    
    self.hail = [[UILabel alloc]initWithFrame:CGRectMake(185, 285, 175, 40)];
    [self.hail setText:@"Hail"];
    [self.hail setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:25]];
    self.hail.textAlignment = NSTextAlignmentCenter;
    self.hail.textColor = [UIColor grayColor];
    [self.hail addGestureRecognizer:[self assignTapGestureRecognizer]];
    [self.selectedWeatherConditions setObject:self.hail forKey:hailKey];
    
    self.humid = [[UILabel alloc]initWithFrame:CGRectMake(185, 325, 175, 40)];
    [self.humid setText:@"Humid"];
    [self.humid setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:25]];
    self.humid.textAlignment = NSTextAlignmentCenter;
    self.humid.textColor = [UIColor grayColor];
    [self.humid addGestureRecognizer:[self assignTapGestureRecognizer]];
    [self.selectedWeatherConditions setObject:self.humid forKey:humidKey];
}

/******************************************************************************
 * populateLabelsIntoView
 
 * This method adds all labels to the view and sets userInteraction to YES.
 *****************************************************************************/
- (void)populateLabelsIntoView {
    UILabel *label;
    for (id key in self.selectedWeatherConditions) {
        label = self.selectedWeatherConditions[key];
        label.userInteractionEnabled = YES;
        [self.backgroundView addSubview:self.selectedWeatherConditions[key]];
    }
}

/******************************************************************************
 * tappedLabel
 
 * This method handles when the user taps a weather condition label by changing
 * the color of the label (gray NOT selected, white selected) and adds it to
 * the dictionary.
 *****************************************************************************/
- (IBAction)tappedLabel:(id)sender {
    UITapGestureRecognizer *gesture = sender;
    UILabel *labelTapped = (UILabel*)[gesture view];
    
    if (labelTapped.textColor == [UIColor grayColor]) {
        labelTapped.textColor = [UIColor whiteColor];
    }
    else{
        labelTapped.textColor = [UIColor grayColor];
    }
}

/******************************************************************************
 * assignTapGestureRecognizer
 
 * This method returns a UITapGesureRecognizer which is used to call the
 * tappedLabel method.
 *****************************************************************************/
- (UITapGestureRecognizer*)assignTapGestureRecognizer {
    UITapGestureRecognizer *tapGesture =
    [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tappedLabel:)];
    return tapGesture;
}

- (void)refreshWeatherSelection {
    for (id key in self.selectedWeatherConditions) {
        [self.backgroundView addSubview:self.selectedWeatherConditions[key]];
    }
}

// updates weatherConditions loaded from local database
// must go through all selectedWeatherConditions to set labels gray or white
- (void)copyWeatherOptionsFromDatabase:(NSArray*)conditions {
    for (id key in self.selectedWeatherConditions) {
        UILabel *label = self.selectedWeatherConditions[key];
        if ([conditions containsObject:key]) {
            label.textColor = [UIColor whiteColor];
        }
        else{
            label.textColor = [UIColor grayColor];
        }
    }
}

// takes in a loaded WeatherOptions and copies weather options dictionary,
// then adds the gesture recognizer to each label
- (void)copyLoadedWeatherOptions:(WeatherOptions*)copy {
    self.selectedWeatherConditions = copy.selectedWeatherConditions;
    for (id key in self.selectedWeatherConditions) {
        UILabel *label = self.selectedWeatherConditions[key];
        [label addGestureRecognizer:[self assignTapGestureRecognizer]];
    }
}

- (void)toggleVisibility {
    if (self.backgroundView.hidden == NO) {
        [self.backgroundView setHidden:YES];
        [self.instructions setHidden:YES];
    }
    else{
        [self.backgroundView setHidden:NO];
        [self.instructions setHidden:NO];
    }
    
}

@end
