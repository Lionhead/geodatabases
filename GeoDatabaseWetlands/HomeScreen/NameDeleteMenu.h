//
//  NameDeleteMenu.h
//  GeoDatabaseWetlands
//
//  Created by Jonathan Mason on 12/5/14.
//  Copyright (c) 2014 Jonathan Mason. All rights reserved.
//
//  This software is intended soley for the University of Washington Bothell.

#import <UIKit/UIKit.h>
#import "UserInfo.h"
#import "HomeScreen.h"

// Class creates a menu for the user to choose a name to delete from the home screen
@interface NameDeleteMenu : UIViewController

- (id)initWithNames:(NSMutableArray*)namesSaved viewController:(UIViewController*)vc;
- (void)loadView:(NSMutableArray*)arrayOfNames;

@end