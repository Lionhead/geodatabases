//
//  NameDeleteMenu.m
//  GeoDatabaseWetlands
//
//  Created by Jonathan Mason on 12/5/14.
//  Copyright (c) 2014 Jonathan Mason. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NameDeleteMenu.h"
#import "HomeDataForm.h"

@interface NameDeleteMenu()  <UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) UIViewController *controller;
@property (strong, nonatomic) NSMutableArray *arrayOfNames;

@end

@implementation NameDeleteMenu

UITableView *tableView;

int namesEntered;

- (id)init{
    self = [super init];
    
    return self;
}

// takes the userInfo and Home UIViewController
- (id)initWithNames:(NSMutableArray*)namesSaved viewController:(UIViewController*)vc{
    if(self = [super init]){
        // get reference to UserBlock for array information

        self.arrayOfNames = [[NSMutableArray alloc]initWithArray:namesSaved copyItems:YES];
        namesEntered = (int)[self.arrayOfNames count];
        
        NSString *deleteAll = @"Delete All";
        NSString *cancel = @"Cancel";
        [self.arrayOfNames addObject:deleteAll];
        [self.arrayOfNames addObject:cancel];
        self.controller = vc;
        [self.controller addChildViewController:self];
    }
    return self;
}

/******************************************************************************
 * loadView
 * This method creates initializes the tableView, sets its appearance, and
 * then loads it into view on the superview controller in homeObject.
 *****************************************************************************/
- (void)loadView:(NSMutableArray*)arrayOfNames
{
    int rowHeight = 50;
    int height = (rowHeight * namesEntered) + (rowHeight * 3);
    tableView = [[UITableView alloc] initWithFrame:CGRectMake(234, 200, 300, height) style:UITableViewStylePlain];
    //tableView.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
    tableView.delegate = self;
    tableView.dataSource = self;
    tableView.rowHeight = 50;
    tableView.backgroundColor = [UIColor colorWithWhite:0.90 alpha:1.0];
    
    [self.view addSubview:tableView];
    [self.controller.view addSubview:self.view];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

// rows needed = header, delete all, cancel, and amount of objects in array
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.arrayOfNames count];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    // The header for the section is the region name -- get this from the region at the section index.
    NSString *header = @"Choose a Name to Delete";
    return header;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 50;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *MyIdentifier = @"MyReuseIdentifier";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault  reuseIdentifier:MyIdentifier];
        cell.backgroundColor = [UIColor colorWithWhite:0.95 alpha:1.0];
    }

    cell.textLabel.text = self.arrayOfNames[indexPath.row];
    return cell;
}

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    return indexPath;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    HomeDataForm *homeForm = [HomeDataForm sharedHomeDataForm];
    
    NSString *userChoice = self.arrayOfNames[indexPath.row];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if ([userChoice isEqualToString:@"Cancel"]) {
        [self.view removeFromSuperview];
    }
    else if ([userChoice isEqualToString:@"Delete All"]){
        [homeForm subtractAllNames];
        [self.view removeFromSuperview];
    }
    else{
        [homeForm subtractName:indexPath.row];
        [self.view removeFromSuperview];
    }
}

- (void)viewDidLoad{
    [super viewDidLoad];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [self removeFromParentViewController];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end