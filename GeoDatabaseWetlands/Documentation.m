//
//  Documentation.m
//  GeoDatabaseWetlands
//
//  Created by Jonathan Mason on 5/28/14.
//  Copyright (c) 2014 Jonathan Mason. All rights reserved.
//

#import "Documentation.h"
#import "HomeScreen.h"
#import "SystemSounds.h"

@interface Documentation ()

@property (strong, nonatomic) IBOutlet UIScrollView *theScrollView;
@property (strong, nonatomic) IBOutlet UIImageView *displayImageView;
@property (strong, nonatomic) IBOutlet UIButton *homeButton;
@property (strong, nonatomic) IBOutlet UIButton *plantButton;
@property (strong, nonatomic) IBOutlet UIButton *waterButton;
@property (strong, nonatomic) IBOutlet UIButton *birdButton;
@property (strong, nonatomic) IBOutlet UIButton *photoButton;


@end

@implementation Documentation

// segue back to Home Page when hitting cancel button
- (IBAction)hitBackButton:(id)sender {
    [SystemSounds playTockSound];
    [self.navigationController popViewControllerAnimated:YES];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    UIViewControllerTracker *vcTracker = [UIViewControllerTracker sharedVC];
    vcTracker.currentViewController = self;
    [self prepNavigationControllerOnLoad];
    [self setNotes];
    // Do any additional setup after loading the view.
}

- (void)configureButtons {
    
}

- (IBAction)buttonClicked:(UIButton*)button {
    [self deselectAllButtons];
    [button setBackgroundImage:[UIImage imageNamed:@"docButtonBackgroundsSelected.png"]
                      forState:UIControlStateNormal];
    if ([button isEqual:self.homeButton]) {
        [self.homeButton setBackgroundImage:[UIImage imageNamed:@"docButtonBackgroundsSelected.png"]
                                   forState:UIControlStateSelected];
        [self.displayImageView setImage:[UIImage imageNamed:@"homeScreenDoc.png"]];
    }
    else if ([button isEqual:self.plantButton]) {
        [self.plantButton setBackgroundImage:[UIImage imageNamed:@"docButtonBackgroundsSelected.png"]
                                   forState:UIControlStateSelected];
        [self.displayImageView setImage:[UIImage imageNamed:@"plantScreenDoc.png"]];
    }
    else if ([button isEqual:self.waterButton]) {
        [self.waterButton setBackgroundImage:[UIImage imageNamed:@"docButtonBackgroundsSelected.png"]
                                    forState:UIControlStateSelected];
        [self.displayImageView setImage:[UIImage imageNamed:@"waterScreenDoc.png"]];
    }
    else if ([button isEqual:self.birdButton]) {
        [self.birdButton setBackgroundImage:[UIImage imageNamed:@"docButtonBackgroundsSelected.png"]
                                   forState:UIControlStateSelected];
        [self.displayImageView setImage:[UIImage imageNamed:@"birdScreenDoc.png"]];
    }
    else if ([button isEqual:self.photoButton]) {
        [self.photoButton setBackgroundImage:[UIImage imageNamed:@"docButtonBackgroundsSelected.png"]
                                    forState:UIControlStateSelected];
        [self.displayImageView setImage:[UIImage imageNamed:@"photoScreenDoc.png"]];
    }
}

- (void)deselectAllButtons {
    [self.homeButton setBackgroundImage:[UIImage imageNamed:@"docButtonBackgrounds.png"]
                               forState:UIControlStateNormal];
    [self.plantButton setBackgroundImage:[UIImage imageNamed:@"docButtonBackgrounds.png"]
                                forState:UIControlStateNormal];
    [self.waterButton setBackgroundImage:[UIImage imageNamed:@"docButtonBackgrounds.png"]
                                forState:UIControlStateNormal];
    [self.birdButton setBackgroundImage:[UIImage imageNamed:@"docButtonBackgrounds.png"]
                               forState:UIControlStateNormal];
    [self.photoButton setBackgroundImage:[UIImage imageNamed:@"docButtonBackgrounds.png"]
                                forState:UIControlStateNormal];
}

/***************************************************************************************************
 * prepNavigationControllerOnLoad
 *
 * This method sets the navigation controller properties for this class.
 **************************************************************************************************/
-(void)prepNavigationControllerOnLoad {
    self.navigationController.navigationBarHidden = NO;
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.view.backgroundColor = [UIColor clearColor];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// prep all data that needs to be sent over to the PhotoViewer during segue
- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"DocToHomeSegue"]) {
        HomeScreen *controller = (HomeScreen *)segue.destinationViewController;
        controller.login = self.login; //pass user login information
    }
}

-(void)setNotes{
    self.documentationNotes = @"";
}

- (void)viewWillDisappear:(BOOL)animated{
    self.navigationController.navigationBarHidden = NO;
}

@end
