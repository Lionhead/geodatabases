//
//  UIViewControllerTracker.m
//  GeoDatabaseWetlands
//
//  Created by Jonathan Mason on 7/3/15.
//  Copyright (c) 2015 Jonathan Mason. All rights reserved.
//

#import "UIViewControllerTracker.h"

@implementation UIViewControllerTracker

+ (id)sharedVC {
    static dispatch_once_t onceToken;
    __strong static id sharedObject = nil;
    
    dispatch_once(&onceToken, ^{
        sharedObject = [[self alloc]init];
    });
    return sharedObject;
}

@end
