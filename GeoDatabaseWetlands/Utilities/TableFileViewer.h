//
//  TableFileViewer.h
//  GeoDatabaseWetlands
//
//  Created by Jonathan Mason on 7/17/14.
//  Copyright (c) 2014 Jonathan Mason. All rights reserved.
//
/*******************************************************************************************
 *******************************************************************************************
 *
 * This class is used to display all the files saved locally so they can be either deleted,
 * or be loaded.
 *
 *******************************************************************************************
 *******************************************************************************************/
#import <UIKit/UIKit.h>
#import "UserLogin.h"
#import "UserInfo.h"
#import "LocalFileManager.h"
@class UserLogin;


@interface TableFileViewer : UITableViewController

@property (weak, nonatomic) UserInfo *userInfo;
@property (strong, nonatomic) UserLogin *login;
@property int saveNumber;
@property (strong, nonatomic) NSString *communitySource;
@property (strong, nonatomic) NSString *workingFilename;
@property (strong, nonatomic) NSString *localFilePath;

@property BOOL clickedCompareDataFromWaterCommunity;
@property BOOL clickedLoadDataFromPlantCommunity;
@property BOOL clickedDeleteDataFromPlantCommunity;
@property BOOL clickedLoadDataFromWaterCommunity;
@property BOOL clickedDeleteDataFromWaterCommunity;
@property BOOL clickedLoadDataFromBirdCommunity;

@end


