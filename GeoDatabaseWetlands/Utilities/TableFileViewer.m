//
//  TableFileViewer.m
//  GeoDatabaseWetlands
//
//  Created by Jonathan Mason on 7/17/14.
//  Copyright (c) 2014 Jonathan Mason. All rights reserved.
//
/***************************************************************************************************
 * TableFileViewer class inherits from UITableViewController, and brings up
 * a table a files that can opened or deleted, depending on the users selection.
 *****************************************************************************/

#import "TableFileViewer.h"
#import "PlantCommunity.h"
#import "FileTableCell.h"
#import "TimeUnits.h"
#import "PlantCommunityProvenance.h"
#import "WaterCommunityProvenance.h"
#import <Realm/Realm.h>

#define PlantCommunityFolder @"PlantCommunity"
#define WaterCommunityFolder @"WaterCommunity"
#define BirdCommunityFolder @"BirdCommunity"
#define messageViewFont @"Avenir-Roman"

@interface TableFileViewer ()

@end

@implementation TableFileViewer

int csvFileCount; // the current total number of csv files in the sandbox
int amountOfDataColumns;
BOOL loadingData;
BOOL loadUserInfo;
NSString *folderPath;
NSString *rowValue; // used to hold the name of the file in a row
NSMutableArray *savedCSVFiles; // container for csv files
NSMutableArray *dataLoaded; // container for csv files
NSString *openedCSVFile; // contains information loaded from a saved csv file
NSMutableArray *csvFileNames; // contains the file names of each csv file in sandbox
NSMutableArray *csvFileDates; // date of last time file was modified or created
RLMResults *data;
// --------------------------------/

- (void)viewDidLoad
{
    [super viewDidLoad];
    loadingData = NO;
    loadUserInfo = NO;
    [self prepNavigationControllerOnLoad];
    
    
    // load plant data
    if ([self.communitySource isEqualToString:PlantCommunityFolder]) {
        data = [[PlantCommunityProvenance objectsWhere:[NSString stringWithFormat:@"username = '%@'",self.userInfo.username]]
                                       sortedResultsUsingProperty:@"dateStarted" ascending:NO];
        
        NSLog(@"Data loaded for user: %@ == \n%@",self.userInfo.username,data);

    }
    else if ([self.communitySource isEqualToString:WaterCommunityFolder]){
        NSString *folderPath = [NSString stringWithFormat:@"/%@/%@",
                                WaterCommunityFolder,self.userInfo.username];
        [self buildCSVArrayWithFilesFromFolder:folderPath];
    }
    else if ([self.communitySource isEqualToString:BirdCommunityFolder]){
        NSString *folderPath = [NSString stringWithFormat:@"/%@/%@",
                                BirdCommunityFolder,self.userInfo.username];
        [self buildCSVArrayWithFilesFromFolder:folderPath];
    }

    // Uncomment the following line to preserve selection between presentations.
    self.clearsSelectionOnViewWillAppear = NO;
}

/***************************************************************************************************
 * prepNavigationControllerOnLoad
 *
 * This method sets the navigation controller properties for this class.
 **************************************************************************************************/
-(void)prepNavigationControllerOnLoad {
    self.navigationController.navigationBarHidden = NO;
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.view.backgroundColor = [UIColor clearColor];
}

/***************************************************************************************************
 * countCSVFiles
 *
 * This method counts the number of currently saved csv files in the sandbox.
 **************************************************************************************************/
- (int)countCSVFilesInFolder:(NSString*)folder {
        NSString *csvFile = @"csv";
        NSArray *paths = NSSearchPathForDirectoriesInDomains
        (NSDocumentDirectory, NSUserDomainMask, YES);
        NSError *error = nil;
        NSString *documentsDirectory = [[paths objectAtIndex:0] stringByAppendingString:folder];
        NSArray *contents = [[NSFileManager defaultManager]
                             contentsOfDirectoryAtPath: documentsDirectory error:&error];
        NSEnumerator *enumerator = [contents objectEnumerator];
        NSString *filename;
        
        csvFileCount = 0;
        while ((filename = [enumerator nextObject])) {
            if ([[filename pathExtension] isEqualToString:csvFile]) {
                csvFileCount++;
            }
        }
        return csvFileCount;
}

/***************************************************************************************************
 * buildCSVArray
 *
 * This method builds an array of file names and data objects of csv files.
 **************************************************************************************************/
- (void)buildCSVArrayWithFilesFromFolder:(NSString*)folder {
    NSString *csvFile = @"csv";
    NSError *error;
    NSString *filePath = [self checkForDirectory:folder];
    
    if (filePath) {
        NSArray *contents = [[NSFileManager defaultManager]
                             contentsOfDirectoryAtPath:filePath error:&error];
        if (error != nil) {
            NSLog(@"Error [buildCSVArrayWithFilesFromFolder]: %@ - %@",
                  error.description,error.domain);
        }
        csvFileCount = [self countCSVFilesInFolder:folder]; //
        csvFileNames = [NSMutableArray arrayWithCapacity:csvFileCount];
        csvFileDates = [NSMutableArray arrayWithCapacity:csvFileCount];
        
        NSEnumerator *enumerator = [contents objectEnumerator];
        NSString *filename;
        NSString *name;
        NSError *pathError;
        int index = 0;
        while ((filename = [enumerator nextObject])) {
            if ([[filename pathExtension] isEqualToString:csvFile]) {
                NSDictionary *fileAttributes = [[NSFileManager defaultManager]attributesOfItemAtPath:
                            [NSString stringWithFormat:@"%@/%@",filePath,filename] error:&pathError];
                if (pathError) {
                    NSLog(@"Error [buildCSVArrayWithFilesFromFolder]: %@ - %@",
                          pathError.description,pathError.domain);
                    return;
                }
                NSDate *date = [fileAttributes objectForKey:NSFileModificationDate];
                NSString *formattedDate = [NSString stringWithFormat:@"Last Modified on: %@ at %@",
                                          [TimeUnits getDateFromDate:date],
                                          [TimeUnits getTimeFromDate:date]];
                csvFileDates[index] = formattedDate;
                name = [filename stringByDeletingPathExtension];
                csvFileNames[index] = name;
                index++;
            }
        }
    }
}

/***************************************************************************************************
 * checkForDirectoryForUser
 *
 * This method checks to see if there is a directory for this user's files.
 * If there isn't then it creates one. If it is unable to create the path then
 * it returns nil and logs error.
 **************************************************************************************************/
- (NSString*)checkForDirectory:(NSString*)folder {
    NSString *filePath;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    filePath = [[paths objectAtIndex:0] stringByAppendingPathComponent:
                [NSString stringWithFormat:@"%@/",folder]];
    NSError *pathError;
    if (![[NSFileManager defaultManager] fileExistsAtPath:filePath]) //Does directory already exist?
    {   // if not then create directory
        if (![[NSFileManager defaultManager] createDirectoryAtPath:filePath
                                       withIntermediateDirectories:YES
                                                        attributes:nil
                                                             error:&pathError])
        {
            NSLog(@"Error: Create directory error: %@", pathError);
            return nil; // could not create so do not save
        }
    }
    
    return filePath;
}

- (NSIndexPath *)tableView:(UITableView *)tableView
  willSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    return indexPath;
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    rowValue = csvFileNames[indexPath.row];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    rowValue = csvFileNames[indexPath.row];

    if (self.userInfo.clickedLoadData == YES) {
        [self confirmLoadFile:rowValue];
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
    }
    else if(self.userInfo.clickedDeleteFile == YES){
        [self confirmDeleteFile:rowValue];
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
    }
}

- (void)confirmLoadFile:(NSString*)file {
    NSString *alertMessage = [[NSString alloc]
                    initWithFormat:@"Open %@?",file];
    UIAlertController *warning = [UIAlertController alertControllerWithTitle:@"Confirm"
                                                                     message:alertMessage
                                                              preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel"
                                                     style:UIAlertActionStyleDefault
                                                   handler:nil];
    [warning addAction:cancel];
    UIAlertAction *yes = [UIAlertAction actionWithTitle:@"Yes"
                                                  style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction *action){
                                                    loadingData = YES;
                                                    [self popupReplaceUserInfo];
                                                }];
    [warning addAction:yes];
    [self presentViewController:warning animated:YES completion:nil];
}

- (void)confirmDeleteFile:(NSString*)file {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:
                                [NSString stringWithFormat:@"Delete %@?",file]
                                                message:nil
                                        preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel"
                                                           style:UIAlertActionStyleCancel
                                                         handler:nil];
    [alert addAction:cancelAction];
    UIAlertAction *yesAction = [UIAlertAction actionWithTitle:@"Yes"
                                                        style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction *action)
                                {
                                    if ([self.communitySource isEqualToString:PlantCommunityFolder]) {
                                        [LocalFileManager deleteFile:file
                                                            inFolder:PlantCommunityFolder
                                                            fromUser:self.userInfo.username];
                                        [LocalFileManager deleteCSVFile:file
                                                               inFolder:PlantCommunityFolder
                                                               fromUser:self.userInfo.username];
                                        [self reloadFilesAfterDeletingfromFolder:PlantCommunityFolder];
                                    }
                                    else if ([self.communitySource isEqualToString:WaterCommunityFolder]){
                                        [LocalFileManager deleteFile:file
                                                            inFolder:WaterCommunityFolder
                                                            fromUser:self.userInfo.username];
                                        [LocalFileManager deleteCSVFile:file
                                                               inFolder:WaterCommunityFolder
                                                               fromUser:self.userInfo.username];
                                        [self reloadFilesAfterDeletingfromFolder:WaterCommunityFolder];
                                    }
                                }];
    [alert addAction:yesAction];
    [self presentViewController:alert animated:YES completion:nil];
}

/***************************************************************************************************
 * popupReplaceUserInfo
 *
 * This method gives the user a popup asking if they want to also replace all
 * the current user information entered on the home screen with the user
 * information that was saved when the file being opened was saved.
 **************************************************************************************************/
- (void)popupReplaceUserInfo {
    UIAlertController *warning = [UIAlertController
                        alertControllerWithTitle:@"Replace User Information on Home Screen?"
                        message:@"Select Yes, if you also want to replace all the user information"
                                  " on the Home Screen with what was entered when this file was saved."
                        preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *no = [UIAlertAction actionWithTitle:@"No"
                        style:UIAlertActionStyleDefault
                        handler:^(UIAlertAction *action){
                            self.workingFilename = rowValue;
                            [self.navigationController popViewControllerAnimated:YES];
                        }];
    [warning addAction:no];
    UIAlertAction *yes = [UIAlertAction actionWithTitle:@"Yes"
                        style:UIAlertActionStyleDefault
                        handler:^(UIAlertAction *action){
                            self.workingFilename = rowValue;
                            loadUserInfo = YES;
                            [self.navigationController popViewControllerAnimated:YES];
                        }];
    [warning addAction:yes];
    [self presentViewController:warning animated:YES completion:nil];
}

/***************************************************************************************************
 * resetPlantCommFilesAfterDelete
 *
 * This method resets the table of plant community files after a CSV file has
 * been deleted.
 **************************************************************************************************/
- (void)reloadFilesAfterDeletingfromFolder:(NSString*)folder {
    NSString *folderPath = [NSString stringWithFormat:@"/%@/%@",
                            folder,self.userInfo.username];
    [self buildCSVArrayWithFilesFromFolder:folderPath];
    [self.tableView reloadData];
}

/***************************************************************************************************
 * grabFile
 *
 * This method opens the selected file in the table view, and then presents
 * the data from that file in the Plant Community fields.
 **************************************************************************************************/
- (NSString*)grabCSVFileFromFolder:(NSString*)folder {

    NSArray *paths = NSSearchPathForDirectoriesInDomains
    (NSDocumentDirectory, NSUserDomainMask, YES);
    
    NSString *documentsDirectory = [[paths objectAtIndex:0] stringByAppendingString:folder];
    NSString *filePath;

    filePath = [documentsDirectory stringByAppendingPathComponent:rowValue];
    NSString *file = [NSString stringWithContentsOfFile:filePath
                                encoding:NSUTF8StringEncoding error:nil];

    return file;
}

/***************************************************************************************************
 * returnSavedNumber
 *
 * This method checks to see if a filename has an additional number on the end
 * to represent what saved number it is, and if it does it returns that number.
 **************************************************************************************************/
- (int)returnSavedNumber:(NSString*)stringToParse {
    int result = 0;

    NSRange range = NSMakeRange(1, 1);
    NSString *secondChar  = [stringToParse substringWithRange:range];

    // see if we get double digits
    @try {
        result = [stringToParse intValue];
    }
    @catch (NSException *exception) {
        NSLog(@"%@",exception);
    }
    
    // see if we get one digit
    @try {
        result = [secondChar intValue];
    }
    @catch (NSException *exception) {
        NSLog(@"%@",exception);
    }
    
    return result;
}

/***************************************************************************************************
 * splitHeadersFromData
 *
 * This method splits all the header files from the data using the '\n' and
 * then puts all of them into 2 separate array indexes.
 * It then returns the result in the array.
 **************************************************************************************************/
- (NSArray*)splitHeadersFromData:(NSString*)stringToSplit {
    NSArray * splitDataArray = [stringToSplit componentsSeparatedByString:@"\n"];
    return splitDataArray;
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [data count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    FileTableCell *cell = (FileTableCell*)[tableView dequeueReusableCellWithIdentifier:@"FileTableCell"];
    
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"FileTableCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
   
    for (int i = 0; i < csvFileCount; i++) {
        if (indexPath.row == i) {
            cell.filename.text = csvFileNames[i];
            cell.dateSaved.text = csvFileDates[i];
        }
    }
    
    return cell;
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

- (void)viewWillDisappear:(BOOL)animated {
    self.navigationController.navigationBarHidden = YES;
    if ([self.communitySource isEqualToString:PlantCommunityFolder]) {
        PlantCommunity *controller = (PlantCommunity*)[self.navigationController topViewController];
        self.userInfo.plantWorkingFilename = [self.workingFilename
                                              stringByDeletingPathExtension];
        controller.displayedFilename.text = self.userInfo.plantWorkingFilename;
        controller.userInfo.clickedLoadData = loadingData;
        controller.userInfo.clickedLoadDataWithUserInfo = loadUserInfo;
        controller.login = self.login;
        controller.userInfo = self.userInfo;
    }
    else if ([self.communitySource isEqualToString:WaterCommunityFolder]){
        WaterCommunity *controller = (WaterCommunity*)[self.navigationController topViewController];
        self.userInfo.waterWorkingFilename = [self.workingFilename
                                              stringByDeletingPathExtension];
        controller.displayedFilename.text = self.userInfo.waterWorkingFilename;
        controller.userInfo.clickedLoadData = loadingData;
        controller.userInfo.clickedLoadDataWithUserInfo = loadUserInfo;
        controller.login = self.login;
        controller.userInfo = self.userInfo;
    }
    else if ([self.communitySource isEqualToString:BirdCommunityFolder]){
        
    }
}

@end
