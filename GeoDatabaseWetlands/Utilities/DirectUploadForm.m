//
//  DirectUploadForm.m
//  GeoDatabaseWetlands
//
//  Created by Jonathan Mason on 11/15/14.
//  Copyright (c) 2014 Jonathan Mason. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DirectUploadForm.h"
#import "UIViewControllerTracker.h"
#import "ViewManager.h"
#import "PlantCommunity.h"
#import "WaterCommunity.h"
#import "PlantCommunityProvenance.h"
#import "WaterCommunityProvenance.h"
#import <Realm/Realm.h>

#define plantType @"plant"
#define waterType @"water"


@implementation DirectUploadForm

AFHTTPRequestOperationManager *manager;
UIViewControllerTracker *vcTracker;
UserInfo *userInfo;
float bytesProcessed;
float bytesToProcess;
float percentComplete;
UIProgressView *progressView;
UIImageView *viewForProgress;
UILabel *progressLabel;
NSString *progressStr;
UIColor *viewBackgroundColor;
NSMutableArray *bytesToWrite;


// ensures only one instance is made
+ (id)sharedUploadFormWithUserInfo:(UserInfo*)info {
    static dispatch_once_t predicate = 0;
    __strong static id sharedObject = nil;
    
    dispatch_once(&predicate, ^{
        sharedObject = [[self alloc]initWithUserInfo:(UserInfo*)info];
    });
    return sharedObject;
}

+ (id)sharedUploadForm {
    static dispatch_once_t predicate = 0;
    __strong static id sharedObject = nil;
    
    dispatch_once(&predicate, ^{
        sharedObject = [[self alloc]init];
    });
    return sharedObject;
}

// set the properties on initialization
- (id)initWithUserInfo:(UserInfo*)info {
    if (self = [super init]) {
        userInfo = info;
        _postParameters = [NSDictionary new];
        manager = [AFHTTPRequestOperationManager manager];
        vcTracker = [UIViewControllerTracker sharedVC];
        [self setupProgressView];
    }
    return self;
}

// set the properties on initialization
- (id)init {
    if (self = [super init]) {
        _postParameters = [NSDictionary new];
        manager = [AFHTTPRequestOperationManager manager];
        vcTracker = [UIViewControllerTracker sharedVC];
        [self setupProgressView];
    }
    return self;
}

- (void)setupProgressView {
    viewBackgroundColor = [UIColor colorWithRed:220.0/255.0
                                          green:220.0/255.0
                                           blue:220.0/255.0
                                          alpha:1.0];
    
    viewForProgress = [[UIImageView alloc]initWithFrame:CGRectMake(297, 440, 180, 100)];
    [viewForProgress setBackgroundColor:viewBackgroundColor];
    [viewForProgress setImage:[UIImage imageNamed:@"directUploadFormProgressViewBackground"]];
    
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:viewForProgress.bounds
                                                        cornerRadius:10];
    CAShapeLayer *maskLayer = [CAShapeLayer layer];
    maskLayer.frame = viewForProgress.bounds;
    maskLayer.path = maskPath.CGPath;
    [viewForProgress.layer setMask:maskLayer];

    percentComplete = 0.0f;
    progressStr = [NSString stringWithFormat:@"Uploading\n%.02f%%",(percentComplete*100)];
    
    progressLabel = [[UILabel alloc]initWithFrame:CGRectMake(30, 7, 120, 70)];
    [progressLabel setTextColor:[UIColor blackColor]];
    [progressLabel setFont:[UIFont fontWithName:@"HelveticaNeue" size:22.0]];
    [progressLabel setText:progressStr];
    [progressLabel setTextAlignment:NSTextAlignmentCenter];
    [progressLabel setNumberOfLines:2];
    [viewForProgress addSubview:progressLabel];
    
    progressView = [[UIProgressView alloc]initWithProgressViewStyle:UIProgressViewStyleDefault];
    [progressView setFrame:CGRectMake(10, 77, 160, 5)];
    progressView.trackTintColor = [UIColor whiteColor];
    progressView.progress = 0.0f;
    [progressView setProgress:progressView.progress];
    
    bytesProcessed = 0.0f;
    [viewForProgress addSubview:progressView];
    [viewForProgress setHidden:YES];
    [vcTracker.currentViewController.view addSubview:viewForProgress];
    [self performSelectorOnMainThread:@selector(showProgress)
                           withObject:nil
                        waitUntilDone:NO];
}

/******************************************************************************
 * postFormDataToServer
 *
 * This method will post the data to the GeoDatabase server. It returns success
 * if data could be posted, else NO success.
 ******************************************************************************/
- (void)postFormDataToServer:(NSDictionary*)parameters ofType:(NSString*)type {
    NSString *URLString;
    BOOL urlSet = NO;
    if ([type isEqualToString:plantType]) { // upload plant forms
        URLString = @"http://69.91.198.44:8080/GeodatabaseServer1/Form";
        urlSet = YES;
    }
    else if ([type isEqualToString:waterType]) { // upload water forms
        URLString = @"http://69.91.198.44:8080/GeodatabaseServer1/WaterQualityForm";
        urlSet = YES;
    }
    
    if (urlSet) {
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        manager.requestSerializer = [AFHTTPRequestSerializer serializer];
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/x-www-form-urlencoded"];
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/plain"];
        
        [manager POST:URLString
           parameters:parameters
              success:^(AFHTTPRequestOperation *operation, id responseObject)
         {
             NSLog(@"success");
             self.uploadSuccess = YES;
             NSString *title = @"Success";
             NSString *text = @"Data has been uploaded to the campus server.";
             [self popupUploadResult:title andText:text];
         }
              failure:
         ^(AFHTTPRequestOperation *operation, NSError *error) {
             NSLog(@"Error [postFormDataToServer]: %@", error);
             NSLog(@"Error Response --> %@",operation.responseString);
             self.uploadSuccess = NO;
             NSString *title = @"Error";
             NSString *text = @"There could be a problem connecting to the campus server. "
             "Data will be saved locally.";
             [self popupUploadResult:title andText:text];
         }];
    }
}

/******************************************************************************
 * getDataFromServer
 *
 * This method uses the GET method with AFNetworking to download information
 * from the GeoDatabase server for the given user logged in.
 ******************************************************************************/
- (void)getDataFromServer:(UserLogin*)login {
    NSString *URLString = @"69.91.198.44:8080/GeodatabaseServer/Form";
    NSDictionary *parameters = @{@"username" : login.username, @"password" : login.password};
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    [manager GET:URLString parameters:parameters
         success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSLog(@"responseObject: %@", responseObject);
         NSLog(@"operation.responseString: %@",operation.responseString);
         NSLog(@"operation.response: %@",operation.response);
         self.downloadSuccess = YES;
     }
         failure:^(AFHTTPRequestOperation *operation, NSError *error)
    {
         NSLog(@"Error [getDataFromServer]: %@", error);
         NSLog(@"Error Response --> %@",operation.responseString);
         self.downloadSuccess = NO;
     }];
}

/******************************************************************************
 * postImageToServerFromPath
 *
 * This method uploads an image file to the campus server.
 ******************************************************************************/
- (void)postImageToServer:(ImageNode*)node numberPosting:(int)total {
    
    bytesToWrite = [NSMutableArray new];
    [viewForProgress setHidden:NO];
    [progressView setHidden:NO];
    
    NSData *fileToUpload = UIImageJPEGRepresentation(node.image, 1.0);
    NSString *filenameNoExt = [node.filename stringByDeletingPathExtension];
    
    NSString *metadata = [NSString stringWithFormat:@"{\"Filename\":\"%@\",\"Username\":\"%@\",\"Project\":\"%@\",\"Date\":\"%@\",\"Caption\":\"%@\",\"Description\":\"%@\",\"Latitude\":\"%@\",\"Longitude\":\"%@\",\"Notes\":\"%@\"}",node.filename,node.username,node.project,node.date,node.caption,node.title,node.latitude,node.longitude,node.notes];
    
    NSDictionary *parameters = @{ @"username" : node.username,
                                  @"metadata" : metadata};
    
    if ([NSJSONSerialization isValidJSONObject:parameters]) {
        NSLog(@"YES! JSON Serialized!");
    }
    
    // 1. Create `AFHTTPRequestSerializer` which will create your request.
    AFHTTPRequestSerializer *serializer = [AFHTTPRequestSerializer serializer];

    // 2. Create an `NSMutableURLRequest`.
    NSMutableURLRequest *request =
    [serializer multipartFormRequestWithMethod:@"POST" URLString:@"http://69.91.198.44:8080/GeodatabaseServer/FileUpload"
                                    parameters:parameters
                     constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
                         [formData appendPartWithFileData:fileToUpload
                                                     name:filenameNoExt
                                                 fileName:node.filename
                                                 mimeType:@"image/jpeg"];
                     }error:nil];
    
    // 3. Create and use `AFHTTPRequestOperationManager` to create an `AFHTTPRequestOperation` from the `NSMutableURLRequest` that we just created.
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    // setting options to allow reading fragments (this will NOT work without it set)
    manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    manager.responseSerializer.acceptableContentTypes = [[NSSet alloc] initWithObjects:@"image/jpeg",@"text/plain", nil];
    
    AFHTTPRequestOperation *operation =
    [manager HTTPRequestOperationWithRequest:request
                                     success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                         NSLog(@"upload image Success!");
                                         //sets the boolean flag to signal upload success
                                         self.uploadPictureSuccess = YES;

                                     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         NSLog(@"image upload Failure %@", error.description);
                                         //sets the boolean flag to signal upload failure
                                         self.uploadPictureSuccess = NO;
                                         [self removeProgressView];
                                     }];
    
    // 4. Set the progress block of the operation.
    [operation setUploadProgressBlock:^(NSUInteger __unused bytesWritten,
                                                long long totalBytesWritten,
                                                long long totalBytesExpectedToWrite) {
        
        if (bytesToWrite.count == total) {
            NSNumber *num = [bytesToWrite objectAtIndex:0];
            if ([num longLongValue] == totalBytesExpectedToWrite) {
                bytesToProcess = (float)totalBytesExpectedToWrite;
                bytesProcessed = (float)totalBytesWritten;
                percentComplete = bytesProcessed/bytesToProcess;
                [self showProgress];
                 if (totalBytesWritten == totalBytesExpectedToWrite) {
                     progressStr = @"Upload\nComplete";
                     [progressLabel setText:progressStr];
                     [self removeProgressView];
                 }
            }
        }
        else{
            NSNumber *num = [NSNumber numberWithLongLong:totalBytesExpectedToWrite];
            if (![bytesToWrite containsObject:num]) {
                [bytesToWrite addObject:num];
            }
        }
        NSLog(@"Wrote %lld/%lld", totalBytesWritten, totalBytesExpectedToWrite);
    }];
    [progressView setHidden:NO];
    [operation start];
    
}

// presents the updated progress amount
- (void)showProgress {
    if (percentComplete < 1) {
        progressStr = [NSString stringWithFormat:@"Uploading\n%.01f%%",(percentComplete*100)];
        [progressLabel setText:progressStr];
        progressView.progress = percentComplete;
        [progressView setProgress:progressView.progress];
    }
}

- (void)removeProgressView {
    [UIView animateWithDuration:1.0 delay:1.0 options:UIViewAnimationOptionCurveLinear animations:^{
        viewForProgress.layer.opacity = 0.0;
    } completion:^(BOOL finished) {
        [self reset];
    }];
}

- (void)reset {
    [viewForProgress setHidden:YES];
    [progressView setHidden:YES];
    viewForProgress.layer.opacity = 1.0;
    [bytesToWrite removeAllObjects];
    bytesToProcess = 0;
    bytesProcessed = 0;
    percentComplete = 0;
    progressStr = [NSString stringWithFormat:@"Uploading\n%.01f%%",(percentComplete*100)];
    [progressLabel setText:progressStr];
}

- (void)popupUploadResult:(NSString*)title andText:(NSString*)text {
    UIViewControllerTracker *vcTracker = [UIViewControllerTracker sharedVC];
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title
                                                                   message:text
                                                            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:
    ^(UIAlertAction *action){
        if ([title isEqualToString:@"Error"] && [vcTracker.currentViewController isKindOfClass:[PlantCommunity class]]) {
            PlantCommunity *plant = (PlantCommunity*)vcTracker.currentViewController;
            [plant saveButton:nil];
        }
        // TO BE COMPLETED
        /*
        if ([title isEqualToString:@"Error"] && [vcTracker.currentViewController isKindOfClass:[WaterCommunity class]]) {
            WaterCommunity *water = (WaterCommunity*)vcTracker.currentViewController;
            //[water saveButton:nil];
        }
         */
    }];
    
    [alert addAction:ok];
    [vcTracker.currentViewController presentViewController:alert animated:YES completion:nil];
}
@end