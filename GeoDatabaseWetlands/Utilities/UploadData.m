//
//  UploadData.m
//  GeoDatabaseWetlands
//
//  Created by Jonathan Mason on 9/15/15.
//  Copyright © 2015 Jonathan Mason. All rights reserved.
//

#import "UploadData.h"

@implementation UploadData

- (id)initWithParameters:(NSDictionary*)dict {
    if (self = [super init]) {
        _data = [[NSDictionary alloc]initWithDictionary:dict];
        dict = nil;
    }
    return self;
}

@end
