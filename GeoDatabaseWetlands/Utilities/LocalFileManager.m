//
//  FileManager.m
//  GeoDatabaseWetlands
//
//  Created by Jonathan Mason on 6/17/15.
//  Copyright (c) 2015 Jonathan Mason. All rights reserved.
//

#import "LocalFileManager.h"

@implementation LocalFileManager

- (id)init {
    if (self = [super init]) {
        
    }
    return self;
}

/******************************************************************************
 * deletePlantCommFiles
 *
 * This method deletes the CSV files selected in the tableView.
 ******************************************************************************/
+ (void)deleteFile:(NSString*)filename inFolder:(NSString*)folder fromUser:(NSString*)user {
    
    NSError *error = nil;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains
    (NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *filePath = [[paths objectAtIndex:0] stringByAppendingPathComponent:
                          [NSString stringWithFormat:@"/%@/%@",folder,user]];
    
    // if files exist then delete
    NSArray *contents = [[NSFileManager defaultManager]
                         contentsOfDirectoryAtPath: filePath error:&error];
    NSEnumerator *e = [contents objectEnumerator];
    NSString *filenameToDelete;
    
    while ((filenameToDelete = [e nextObject])) {
        if ([filenameToDelete isEqualToString:filename]) {
            [fileManager removeItemAtPath:
             [filePath stringByAppendingPathComponent:filenameToDelete] error:&error];
        }
    }
    if (error) {
        NSLog(@"ERROR [deletePlantCommFiles]: Desc:%@",error.description);
    }
}

/******************************************************************************
 * deleteCSVFile
 *
 * This method deletes the CSV files selected in the tableView.
 ******************************************************************************/
+ (void)deleteCSVFile:(NSString*)filename inFolder:(NSString*)folder fromUser:(NSString*)user {
    
    NSError *error = nil;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains
    (NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *filePath = [[paths objectAtIndex:0] stringByAppendingPathComponent:
                          [NSString stringWithFormat:@"/%@/%@",folder,user]];
    
    // if files exist then delete
    NSArray *contents = [[NSFileManager defaultManager]
                         contentsOfDirectoryAtPath: filePath error:&error];
    NSEnumerator *e = [contents objectEnumerator];
    NSString *fileToDelete;
    
    while ((fileToDelete = [e nextObject])) {
        if([fileToDelete isEqualToString:[NSString stringWithFormat:@"%@.csv",filename]]){
            [fileManager removeItemAtPath:[filePath
                                           stringByAppendingPathComponent:fileToDelete] error:&error];
        }
    }
}

+ (void)deleteImageFile:(NSString*)fileName fromDir:(NSString*)dir {
    NSString *path;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    path = [[paths objectAtIndex:0] stringByAppendingPathComponent:dir];
    path = [path stringByAppendingPathComponent:fileName];
    
    NSError *error = nil;
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    [fileManager removeItemAtPath:path error:&error];
    if (error) {
        NSLog(@"ERROR [deleteImageFileFromPath]: Desc:%@  Domain:%@",error.description,error.domain);
    }
}

+ (void)deleteImageNode:(NSString*)nodeName fromDir:(NSString*)dir {
    NSString *path;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    path = [[paths objectAtIndex:0] stringByAppendingPathComponent:dir];
    path = [path stringByAppendingPathComponent:nodeName];
    
    NSError *error = nil;
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    [fileManager removeItemAtPath:path error:&error];
    if (error) {
        NSLog(@"ERROR [deleteImageNode]: Desc:%@  Domain:%@",error.description,error.domain);
    }
}

/******************************************************************************
 * checkForDirectoryForUser
 *
 * This method checks to see if there is a directory for this user's files.
 * If there isn't then it creates one. If it is unable to create the path then
 * it returns nil and logs error.
 ******************************************************************************/
+ (NSString*)checkForDirectoryForUser:(NSString*)user inSubdirectory:(NSString *)subdirectory {
    NSString *path;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    path = [[paths objectAtIndex:0] stringByAppendingPathComponent:
            [NSString stringWithFormat:@"/%@/%@",subdirectory,user]];
    NSError *pathError;
    if (![[NSFileManager defaultManager] fileExistsAtPath:path])	//Does directory already exist?
    {   // if not then create directory
        if (![[NSFileManager defaultManager] createDirectoryAtPath:path
                                       withIntermediateDirectories:YES
                                                        attributes:nil
                                                             error:&pathError])
        {
            NSLog(@"Create directory error: %@", pathError);
            return nil; // could not create so do not save
        }
    }
    return path;
}

/***************************************************************************************************
 * checkForDirectoryForUserPhotos
 *
 * This method checks to see if there is a directory for this user's photos.
 * If there isn't then it creates one. If it is unable to create the path then
 * it returns nil and logs error.
 **************************************************************************************************/
+ (NSString*)checkForDirectoryForUserPhotos:(NSString*)user inCommunity:(NSString*)community {
    NSString *filePath;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    filePath = [[paths objectAtIndex:0] stringByAppendingPathComponent:
                [NSString stringWithFormat:@"%@/%@/%@/",@"Photos",user,community]];
    NSError *pathError;
    if (![[NSFileManager defaultManager] fileExistsAtPath:filePath]) //Does directory already exist?
    {   // if not then create directory
        if (![[NSFileManager defaultManager] createDirectoryAtPath:filePath
                                       withIntermediateDirectories:YES
                                                        attributes:nil
                                                             error:&pathError])
        {
            NSLog(@"Create directory error: %@", pathError);
            return nil; // could not create so do not save
        }
    }
    return filePath;
}

/******************************************************************************
 * checkFileName
 *
 * This method checks to see if there is a file in the sandbox already named
 * what the current file name to be saved is. If yes returns true.
 ******************************************************************************/
+ (BOOL)checkFileName:(NSString*)fileName atPath:(NSString*)path {
    NSError *error;
    NSArray *contents = [[NSFileManager defaultManager]contentsOfDirectoryAtPath:path error:&error];
    if (error) {
        NSLog(@"Error [checkFileName]: Desc:%@  Domain:%@",error.description,error.domain);
    }
    NSString *csvFile = [fileName stringByAppendingString:@".csv"];
    NSEnumerator *e = [contents objectEnumerator];
    NSString *currentFilename;
    while ((currentFilename = [e nextObject])) {
        if ([currentFilename isEqualToString:csvFile]) {
            return YES;
        }
    }
    return NO;
}


/******************************************************************************
 * createDirectoryUsingThisPath
 *
 * This method takes a string of a path to create. This method assumes their
 * isn't already a path there by that name.
 ******************************************************************************/
+ (void)createDirectoryUsingThisPath:(NSString*)path {
    NSError *error;
    [[NSFileManager defaultManager] createDirectoryAtPath:path
                              withIntermediateDirectories:YES
                                               attributes:nil
                                                    error:&error];
    if (error) {
        NSLog(@"Error [createDirectoryUsingThisPath]:%@ Domain:%@",
              error.description,error.domain);
    }
}

+ (BOOL)saveAsCSVFile:(NSString*)data withPathAndName:(NSString*)csvFilePath {
    NSError *saveError;
    [data writeToFile:csvFilePath
           atomically:NO
             encoding:NSUTF8StringEncoding
                error:&saveError];
    if (saveError) {
        NSLog(@"Error [saveAsCSVFile]:%@ Domain:%@",saveError.description,saveError.domain);
        return NO;
    }
    return YES;
}

@end
