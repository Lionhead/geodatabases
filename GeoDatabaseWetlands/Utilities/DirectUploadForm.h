
/***********************************************************************
 * DirectUploadForm.h
 * 
 * This class is called from the PlantCommunity and PhotoViewer classes
 * to upload data to the UW GeoDatabase server. It utilizes the
 * interfaces from AFNetworking to communicate with a RESTful API at UW.
 ***********************************************************************/

#import <UIKit/UIKit.h>
#import "AFNetworking.h"
#import "UserLogin.h"
#import "UserInfo.h"
#import "ImageNode.h"


// See comment at top of file for a complete description
@interface DirectUploadForm : NSObject

@property NSString *postResult; // the reply message from server when posting
@property NSDictionary *postParameters; // the parameters used to post form data
@property NSString *parameterTest;

@property BOOL uploadSuccess;
@property BOOL downloadSuccess;
@property BOOL uploadPictureSuccess;

- (id)initWithUserInfo:(UserInfo*)info;
+ (id)sharedUploadFormWithUserInfo:(UserInfo*)info;
+ (id)sharedUploadForm;
- (void)postFormDataToServer:(NSDictionary*)parameters ofType:(NSString*)type;
- (void)getDataFromServer:(UserLogin*)login;
- (void)postImageToServer:(ImageNode*)node numberPosting:(int)total;


@end