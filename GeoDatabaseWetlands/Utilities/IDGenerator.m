//
//  IDGenerator.m
//  GeoDatabaseWetlands
//
//  Created by Jonathan Mason on 12/20/15.
//  Copyright © 2015 Jonathan Mason. All rights reserved.
//

#import "IDGenerator.h"
#import "TimeUnits.h"

@implementation IDGenerator

/******************************************************************************
 * getDeviceId
 *
 * This method returns a string with the deviceUUID.
 ******************************************************************************/
+ (NSString*)getDeviceId {
    NSString* uniqueIdentifier = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    return uniqueIdentifier;
}

// formatted by date/time/username; used to distinguish all uploads to database
+ (NSString*)getFormID {
    NSString *formID;
    char a = returnGlyphValue();
    char b = returnGlyphValue();
    char c = returnGlyphValue();
    
    NSString *chars = [NSString stringWithFormat:@"%c%c%c",a,b,c];
    NSString *str = [NSString stringWithFormat:@"%ld%@",random()%999,chars];
    
    formID = [NSString stringWithFormat:@"%@%@%@",
              [TimeUnits presentDate],
              [TimeUnits presentTime],
              str];
    formID = [formID stringByReplacingOccurrencesOfString:@"/" withString:@""];
    formID = [formID stringByReplacingOccurrencesOfString:@":" withString:@""];
    
    return formID;
}

int returnGlyphValue() {
    int n = random()%90;
    while (n < 65) {
        n += 26;
    }
    return n;
}

// unique identifier
+ (NSString*)getUUID {
    NSString *uuid;
    
    char a = returnGlyphValue();
    char b = returnGlyphValue();
    char c = returnGlyphValue();
    NSString *chars = [NSString stringWithFormat:@"%c%c%c",a,b,c];
    NSString *str = [NSString stringWithFormat:@"%ld%@%ld",random()%99,chars,random()%99];
    NSString *deviceID = [self getDeviceId];
    if (deviceID.length > 5) {
        deviceID = [deviceID stringByPaddingToLength:5 withString:deviceID startingAtIndex:0];
    }
    uuid = [NSString stringWithFormat:@"%@-%@-%@",
            [self getFormID],
            deviceID,
            str];
    
    return uuid;
}

@end
