//
//  SystemSounds.m
//  GeoDatabaseWetlands
//
//  Created by Jonathan Mason on 6/2/15.
//  Copyright (c) 2015 Jonathan Mason. All rights reserved.
//

#import "SystemSounds.h"

@implementation SystemSounds


- (id)init {
    if (self = [super init]) {
        
    }
    return self;
}

/******************************************************************************
 * playSystemSound
 *
 * This method plays the called system sound identified by filename and ext.
 ******************************************************************************/
+ (void)playSystemSound:(NSString *)filename fileExt:(NSString *) ext {
    
    NSString *filePath = [[NSBundle mainBundle] pathForResource:filename ofType:ext];
    if ([[NSFileManager defaultManager] fileExistsAtPath : filePath]) {
        
        NSURL *fileURL = [NSURL URLWithString:filePath];
        
        SystemSoundID soundID;
        AudioServicesCreateSystemSoundID((__bridge_retained CFURLRef)fileURL,&soundID);
        AudioServicesPlaySystemSound(soundID);
        AudioServicesAddSystemSoundCompletion(soundID, NULL, NULL,
            (AudioServicesSystemSoundCompletionProc)AudioServicesDisposeSystemSoundID, NULL);
    }
    else {
        NSLog(@"error, file not found: %@", filePath);
    }
}

+ (void)playTockSound {
    AudioServicesPlaySystemSound(1104);
}

/******************************************************************************
 * playErrorSound
 *
 * This method plays the error system sound.
 ******************************************************************************/
+ (void)playErrorSound {
    [self playSystemSound:@"error" fileExt:@"wav"];
}

/******************************************************************************
 * playLogoutSound
 *
 * This method plays the logout system sound.
 ******************************************************************************/
+ (void)playLogoutSound {
    [self playSystemSound:@"Purr" fileExt:@"aiff"];
}

+ (void)playClickSound {
    [self playSystemSound:@"click" fileExt:@"aif"];
}

@end
