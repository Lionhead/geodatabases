//
//  UploadQueue.h
//  GeoDatabaseWetlands
//
//  Created by Jonathan Mason on 9/14/15.
//  Copyright © 2015 Jonathan Mason. All rights reserved.
//
/*******************************************************************************************
 *******************************************************************************************
 *
 * This class is responsible for maintaining a queue of data to be uploaded to the server
 * while the user is unable to connect to it.
 *
 *******************************************************************************************
 *******************************************************************************************/

#import <Foundation/Foundation.h>

@interface UploadQueue : NSObject

@property (strong, nonatomic) NSMutableArray *queue;
@property (strong, nonatomic) NSMutableArray *filenames; // 

+ (id)sharedUploadQueue;

- (void)queueObject:(id)object;
- (void)emptyQueue;
- (void)runQueue;

@end
