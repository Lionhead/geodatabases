//
//  TimeUnits.m
//  GeoDatabaseWetlands
//
//  Created by Jonathan Mason on 7/22/15.
//  Copyright (c) 2015 Jonathan Mason. All rights reserved.
//

#import "TimeUnits.h"

@implementation TimeUnits

/******************************************************************************
 * getPresentTime
 *
 * Return's the present time in format:HH:mm:ss
 ******************************************************************************/
+ (NSString*)presentTime {
    NSDate *date = [NSDate date];
    NSDateFormatter *timeFormatter = [[NSDateFormatter alloc] init];
    [timeFormatter setDateFormat:@"HH:mm:ss"];
    NSString *theTime = [timeFormatter stringFromDate:date];
    return theTime;
}

/******************************************************************************
 * getPresentDate
 *
 * Return's the present date in format:yyyy-MM-dd
 ******************************************************************************/
+ (NSString*)presentDate {
    NSDate *date = [NSDate date];
    NSDateFormatter *timeFormatter = [[NSDateFormatter alloc] init];
    [timeFormatter setDateFormat:@"yyyy/MM/dd"];
    NSString *theDate = [timeFormatter stringFromDate:date];
    return theDate;
}

/******************************************************************************
 * getTimeFromDate
 *
 * Return's the present time in format:HH:mm:ss
 ******************************************************************************/
+ (NSString*)getTimeFromDate:(NSDate*)date {
    NSDateFormatter *timeFormatter = [[NSDateFormatter alloc] init];
    [timeFormatter setDateFormat:@"HH:mm:ss"];
    NSString *theTime = [timeFormatter stringFromDate:date];
    return theTime;
}

/******************************************************************************
 * getDateFromDate
 *
 * Return's the present date in format:yyyy-MM-dd
 ******************************************************************************/
+ (NSString*)getDateFromDate:(NSDate*)date {
    NSDateFormatter *timeFormatter = [[NSDateFormatter alloc] init];
    [timeFormatter setDateFormat:@"yyyy/MM/dd"];
    NSString *theDate = [timeFormatter stringFromDate:date];
    return theDate;
}

@end
