//
//  ConnectionWatcher.h
//  GeoDatabaseWetlands
//
//  Created by Jonathan Mason on 7/3/15.
//  Copyright (c) 2015 Jonathan Mason. All rights reserved.
//
//  This software is intended soley for the University of Washington Bothell.
//
/*******************************************************************************************
 *******************************************************************************************
 *
 * This class is used to watch for when there is a connection to the server if the user
 * attempts to save data and can't. If a connection to the server becomes present, it will
 * prompt the user to confirm uploading the data.
 *
 *******************************************************************************************
 *******************************************************************************************/

#import <Foundation/Foundation.h>
#import "Reachability.h"

// See comment at top of file for a complete description
@interface ConnectionWatcher : NSObject

@property (strong, nonatomic)UIViewController *vcPtr;
+(ConnectionWatcher*)startConnectionWatcher;
+ (BOOL)isWatching;
+ (void)stopWatching;

@end
