//
//  UploadQueue.m
//  GeoDatabaseWetlands
//
//  Created by Jonathan Mason on 9/14/15.
//  Copyright © 2015 Jonathan Mason. All rights reserved.
//

#import "UploadQueue.h"
#import "UploadData.h"
#import "DirectUploadForm.h"

#define queueKey @"queue"

@implementation UploadQueue

+ (id)sharedUploadQueue {
    static dispatch_once_t onceToken;
    __strong static id sharedObject = nil;
    
    dispatch_once(&onceToken, ^{
        sharedObject = [[self alloc]init];
    });
    
    return sharedObject;
}

- (id)init {
    if (self = [super init]) {
        _queue = [NSMutableArray new];
    }
    return self;
}

- (void)queueObject:(id)object {
    [self.queue insertObject:object atIndex:self.queue.count];
}

- (void)emptyQueue {
    [self.queue removeAllObjects];
}

- (void)runQueue {
    DirectUploadForm *form = [DirectUploadForm sharedUploadForm];
    UploadData *uploadData;
    for (id object in self.queue) {
        uploadData = object;
        [form postFormDataToServer:uploadData.data ofType:uploadData.dataType];
    }

}

@end
