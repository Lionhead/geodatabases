//
//  UploadData.h
//  GeoDatabaseWetlands
//
//  Created by Jonathan Mason on 9/15/15.
//  Copyright © 2015 Jonathan Mason. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UploadData : NSObject

@property (strong, nonatomic) NSDictionary *data;
@property (strong, nonatomic) NSString *dataID;
@property (strong, nonatomic) NSString *dataType;

- (id)initWithParameters:(NSDictionary*)dict;

@end
