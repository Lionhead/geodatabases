 //
//  MailAttachment.m
//  GeoDatabaseWetlands
//
//  Created by Jonathan Mason on 11/16/15.
//  Copyright © 2015 Jonathan Mason. All rights reserved.
//

#import "MailAttachment.h"
#import "TimeUnits.h"
#import <MessageUI/MessageUI.h>

@interface MailAttachment () <MFMailComposeViewControllerDelegate>

@property (strong, nonatomic) UIViewController *viewController;

@end

@implementation MailAttachment

- (id)initWithViewController:(UIViewController*)vc {
    if (self = [super init]) {
        _viewController = vc;
    }
    return self;
}

- (void)popupAskToEmailSpreadsheetForFile:(NSData*)fileData withName:(NSString*)filename {
    UIAlertController *alert = [UIAlertController
                                alertControllerWithTitle:@"Would you like the spreadsheet emailed?"
                                                 message:@"Note: You must have a mail account setup on this ipad to use this feature."
                                          preferredStyle:UIAlertControllerStyleAlert];
    
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"address@domain.com";
    }];
    
    UIAlertAction *yesAction = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction * _Nonnull action)
    {
        UITextField *textField = [alert.textFields firstObject];
        [self configureEmail:textField.text withAttachment:fileData withName:filename];
    }];
    [alert addAction:yesAction];
    
    UIAlertAction *noAction = [UIAlertAction actionWithTitle:@"No"
                                                       style:UIAlertActionStyleDefault
                                                      handler:nil];
    [alert addAction:noAction];
    
    [self.viewController presentViewController:alert animated:YES completion:nil];
}

- (void)configureEmail:(NSString*)address withAttachment:(NSData*)fileData withName:(NSString*)filename {
    NSString *emailTitle = [NSString stringWithFormat:@"Water Quality Spreadsheet %@ - %@",
                            [TimeUnits presentDate],
                            [TimeUnits presentTime]];
    
    NSArray *recipent = [NSArray arrayWithObject:address];
    
    if ([MFMailComposeViewController canSendMail]){
        MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
        mc.mailComposeDelegate = self;
        [mc setSubject:emailTitle];
        [mc setToRecipients:recipent];
        
        NSString *mimeType = @"text/csv";
        
        [mc addAttachmentData:fileData mimeType:mimeType fileName:filename];
        [self.viewController presentViewController:mc animated:YES completion:nil];
    }
    else{
        NSLog(@"MFMailComposeViewController cannot send mail");
        [self popupCannotSendMail];
    }
}


- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }

    [self.viewController dismissViewControllerAnimated:YES completion:NULL];
}

- (void)popupCannotSendMail {
    
}

@end
