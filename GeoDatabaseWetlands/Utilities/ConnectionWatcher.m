//
//  ConnectionWatcher.m
//  GeoDatabaseWetlands
//
//  Created by Jonathan Mason on 7/3/15.
//  Copyright (c) 2015 Jonathan Mason. All rights reserved.
//

#import "ConnectionWatcher.h"
#import "UIViewControllerTracker.h"
#import "UploadQueue.h"
#import "UploadData.h"

@implementation ConnectionWatcher {
    Reachability* internetReachable;
}

BOOL watching;
BOOL cancelled;
NSTimeInterval TwentySecs = 20.0;
NSTimeInterval OneMin = 60.0;
NSTimeInterval FiveMins = 300.0;

// ensures only one instance is made
+ (id)startConnectionWatcher {
    static dispatch_once_t onceToken;
    __strong static id sharedObject = nil;
    
    dispatch_once(&onceToken, ^{
        sharedObject = [[self alloc]init];
    });
    
    return sharedObject;
}

- (id)init {
    if(self = [super init]){
        watching = YES;
        cancelled = NO;
        [self performSelector:@selector(watchForConnection) withObject:self afterDelay:FiveMins];
    }
    return self;
}

+ (BOOL)isWatching {
    return watching;
}

+ (void)stopWatching {
    [NSObject cancelPreviousPerformRequestsWithTarget:self
                                             selector:@selector(watchForConnection)
                                               object:self];
    watching = NO;
    cancelled = YES;
}

- (void)watchForConnection {
    // connection established
    if ([self testWifiConnection] && watching) {
        watching = NO;
        UploadQueue *uploadQueue = [UploadQueue sharedUploadQueue];
        NSString *files = [self returnListOfFilesInQueue:uploadQueue];
        
        if (![files isEqualToString:@"error"]){
            UIAlertController *internetReady = [UIAlertController
                    alertControllerWithTitle:@"Attention"
                        message:[NSString stringWithFormat:@"You now have an internet connection available."
                                 " Would you like to upload the following files: %@"
                                 " ?",files]
                    preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel"
                                                         style:UIAlertActionStyleCancel
                                                       handler:nil];
            [internetReady addAction:cancel];
            
            UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Ok"
                                                         style:UIAlertActionStyleCancel
                                                       handler:^(UIAlertAction * _Nonnull action)
            {
                [uploadQueue runQueue];
               }];
            [internetReady addAction:ok];
            
            UIViewControllerTracker *tracker = [UIViewControllerTracker sharedVC];
            UIViewController *vc = [tracker currentViewController];
            [vc presentViewController:internetReady animated:YES completion:nil];
        }
    }
    else{ // no connection, check again after time delay
        if (!cancelled) {
            [self performSelector:@selector(watchForConnection) withObject:self afterDelay:FiveMins];
        }
    }
}

// Checks if we have an internet connection or not
- (BOOL)testWifiConnection {
    internetReachable = [Reachability reachabilityForLocalWiFi];
    [internetReachable startNotifier];
    
    return internetReachable.currentReachabilityStatus;
}

- (NSString*)returnListOfFilesInQueue:(UploadQueue*)uploadQueue {
    NSString *list = @"";
    UploadData *uploadData;
    for (id object in uploadQueue.queue) {
        uploadData = object;
        list = [list stringByAppendingString:[NSString stringWithFormat:@"%@, ",uploadData.dataID]];
    }
    if (list.length > 2) {
        list = [list substringToIndex:list.length-2];
    }
    else 
        return @"error";
    
    return list;
}



@end
