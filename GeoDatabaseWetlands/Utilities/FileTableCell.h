//
//  FileTableCell.h
//  GeoDatabaseWetlands
//
//  Created by Jonathan Mason on 8/11/15.
//  Copyright (c) 2015 Jonathan Mason. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FileTableCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *filename;
@property (weak, nonatomic) IBOutlet UILabel *dateSaved;

@end
