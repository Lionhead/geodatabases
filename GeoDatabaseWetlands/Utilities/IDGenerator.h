//
//  IDGenerator.h
//  GeoDatabaseWetlands
//
//  Created by Jonathan Mason on 12/20/15.
//  Copyright © 2015 Jonathan Mason. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IDGenerator : NSObject

+ (NSString*)getDeviceId;
+ (NSString*)getFormID;
+ (NSString*)getUUID;

@end
