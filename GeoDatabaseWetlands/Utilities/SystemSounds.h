//
//  SystemSounds.h
//  GeoDatabaseWetlands
//
//  Created by Jonathan Mason on 6/2/15.
//  Copyright (c) 2015 Jonathan Mason. All rights reserved.
//

#import <Foundation/Foundation.h>
@import AudioToolbox;

// Plays system type sounds
@interface SystemSounds : NSObject

@property SystemSoundID loginSound;

+ (void)playErrorSound;
+ (void)playLogoutSound;
+ (void)playClickSound;
+ (void)playTockSound;

@end
