//
//  TimeUnits.h
//  GeoDatabaseWetlands
//
//  Created by Jonathan Mason on 7/22/15.
//  Copyright (c) 2015 Jonathan Mason. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TimeUnits : NSObject

+ (NSString*)presentTime;
+ (NSString*)presentDate;
+ (NSString*)getTimeFromDate:(NSDate*)date;
+ (NSString*)getDateFromDate:(NSDate*)date;

@end
