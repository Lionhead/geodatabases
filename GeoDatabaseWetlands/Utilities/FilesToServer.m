//
//  FilesToServer.m
//  GeoDatabaseWetlands
//
//  Created by Jonathan Mason on 8/24/14.
//  Copyright (c) 2014 Jonathan Mason. All rights reserved.
//

/*
 
 THIS CLASS IS DEPRECATED. IT HAS BEEN REPLACED BY DirectUploadForm. YOU CAN HOWEVER TAKE CODE
 FROM THIS TO USE IN DirectUploadForm.
 
 */

#import "FilesToServer.h"
#import "AFNetworking.h"

@interface FilesToServer ()

@property int fileCount;
@property (strong, nonatomic) NSString *rowValue;

@end

@implementation FilesToServer

NSString *newFileName; // used when saving a new file

/******************************************************************************
 * countFiles
 *
 * This method counts the number of currently saved csv files in the sandbox.
 ******************************************************************************/
- (int)countFiles {
    NSString *csvFile = @"csv";
    NSString *jpgFile = @"jpg";
    // NSString *txtFile = @"txt"; // FOR TESTING ONLY
    NSArray *paths = NSSearchPathForDirectoriesInDomains
    (NSDocumentDirectory, NSUserDomainMask, YES);
    NSError *error = nil;
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSArray *contents = [[NSFileManager defaultManager]
                         contentsOfDirectoryAtPath: documentsDirectory error:&error];
    NSEnumerator *enumerator = [contents objectEnumerator];
    NSString *filename;
    
    _fileCount = 0;
    while ((filename = [enumerator nextObject])) {
        if ([[filename pathExtension] isEqualToString:csvFile] ||
            [[filename pathExtension] isEqualToString:jpgFile]){
            //[[filename pathExtension] isEqualToString:txtFile]) { // FOR TESTING ONLY
            _fileCount++;
        }
    }
    return self.fileCount;
}

/******************************************************************************
 * buildArrayOfFiles
 *
 * This method builds an array of file names to list in the table.
 ******************************************************************************/
- (void)buildArrayOfLocalFiles {

    NSString *csvFile = @"csv";
    NSString *jpgFile = @"jpg";
    //NSString *txtFile = @"txt"; //for testing only
    NSArray *paths = NSSearchPathForDirectoriesInDomains
    (NSDocumentDirectory, NSUserDomainMask, YES);
    NSError *error = nil;
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSArray *contents = [[NSFileManager defaultManager]
                         contentsOfDirectoryAtPath: documentsDirectory error:&error];
    NSEnumerator *enumerator = [contents objectEnumerator];
    NSString *filename;
    
    _localFileNames = [NSMutableArray arrayWithCapacity:self.fileCount];
    _savedFiles = [NSMutableArray arrayWithCapacity:self.fileCount];
    int arrayPosition = 0;
    while ((filename = [enumerator nextObject])) {
        // NSLog(@"file = %@",filename); // FOR TESTING ONLY
        if ([[filename pathExtension] isEqualToString:csvFile]) {
            _localFileNames[arrayPosition] = filename;
            
            NSData *csvFile = [NSData dataWithContentsOfFile:[documentsDirectory stringByAppendingPathComponent:filename]];
            _savedFiles[arrayPosition] = csvFile;
            
            arrayPosition++;
        }
        if ([[filename pathExtension] isEqualToString:jpgFile]) {
            _localFileNames[arrayPosition] = filename;
            
            UIImage *imageFile = [UIImage imageWithContentsOfFile:
                                  [documentsDirectory stringByAppendingPathComponent:filename]];
            [_savedFiles insertObject:imageFile atIndex:arrayPosition];
            
            arrayPosition++;
        }
        /* // FOR TESTING ONLY
        if ([[filename pathExtension] isEqualToString:txtFile]) {
            _localFileNames[arrayPosition] = filename;
            
            NSData *txtFile = [NSData dataWithContentsOfFile:[documentsDirectory stringByAppendingPathComponent:filename]];
            _savedFiles[arrayPosition] = txtFile;
            
            arrayPosition++;
        }
         */
    }
}

/******************************************************************************
 * uploadFile
 *
 * This method uploads the file selected by the user to the server database
 * by using AFNetworking to connect to the Java API.
 ******************************************************************************/
- (void)uploadFile {
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:_filename];

    // Choose if it is jpg or csv
    if ([[_filename pathExtension] isEqualToString:@"jpg"]){
        
        _fileToUpload = [NSData dataWithContentsOfFile:path];
        _filenameNoExt = [_filename stringByDeletingPathExtension];
        
        NSString *metadataPath = [_filenameNoExt stringByAppendingString:@"_meta.txt"];
        metadataPath = [documentsDirectory stringByAppendingPathComponent:metadataPath];
        
        _metadata = [NSString stringWithContentsOfFile:metadataPath encoding:NSUTF8StringEncoding error:nil];

        NSArray *JSONarray = [_metadata componentsSeparatedByString:@"\n"];
        
        NSString *Filename = [JSONarray[0] stringByReplacingOccurrencesOfString:@"Metadata for: " withString:@""];
        NSString *Username = [JSONarray[1] stringByReplacingOccurrencesOfString:@"Username: " withString:@""];
        NSString *Project = [JSONarray[2] stringByReplacingOccurrencesOfString:@"Project: " withString:@""];
        NSString *Date = [JSONarray[3] stringByReplacingOccurrencesOfString:@"Date: " withString:@""];
        NSString *Caption = [JSONarray[4] stringByReplacingOccurrencesOfString:@"Caption: " withString:@""];
        NSString *Description = [JSONarray[5] stringByReplacingOccurrencesOfString:@"Description: " withString:@""];
        NSString *Latitude = [JSONarray[6] stringByReplacingOccurrencesOfString:@"Latitude: " withString:@""];
        NSString *Longitude = [JSONarray[7] stringByReplacingOccurrencesOfString:@"Longitude: " withString:@""];
        NSString *Address = [JSONarray[8] stringByReplacingOccurrencesOfString:@"Address: " withString:@""];
        NSString *Notes = [JSONarray[9] stringByReplacingOccurrencesOfString:@"Notes: " withString:@""];
        
        _metadata = [NSString stringWithFormat:@"{\"Filename\":\"%@\",\"Username\":\"%@\",\"Project\":\"%@\",\"Date\":\"%@\",\"Caption\":\"%@\",\"Description\":\"%@\",\"Latitude\":\"%@\",\"Longitude\":\"%@\",\"Notes\":\"%@\"}",Filename,Username,Project,Date,Caption,Description,Latitude,Longitude,Notes];
        
        
        NSDictionary *parameters = @{ @"username" : _username,
                                      @"metadata" : _metadata};
        

        if ([NSJSONSerialization isValidJSONObject:parameters]) {
            NSLog(@"YES!");
        }

        // 1. Create `AFHTTPRequestSerializer` which will create your request.
        AFHTTPRequestSerializer *serializer = [AFHTTPRequestSerializer serializer];

        // 2. Create an `NSMutableURLRequest`.
        NSMutableURLRequest *request =
        [serializer multipartFormRequestWithMethod:@"POST" URLString:@"http://69.91.198.44:8080/GeodatabaseServer/FileUpload"
                                        parameters:parameters
                         constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
                             [formData appendPartWithFileData:_fileToUpload
                                                         name:_filenameNoExt
                                                     fileName:_filename
                                                     mimeType:@"image/jpeg,text/rtf"];
                         }error:nil];
        
        // 3. Create and use `AFHTTPRequestOperationManager` to create an `AFHTTPRequestOperation` from the `NSMutableURLRequest` that we just created.
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        
        // setting options to allow reading fragments (this will NOT work without it set)
        manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
        
        

        AFHTTPRequestOperation *operation =
        [manager HTTPRequestOperationWithRequest:request
                                         success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                             
                                             UIAlertController *success = [UIAlertController alertControllerWithTitle:@"Success"
                                                            message:nil
                                                            preferredStyle:UIAlertControllerStyleAlert];
                                             UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Ok"
                                                            style:UIAlertActionStyleCancel handler:nil];
                                             [success addAction:ok];
                                             [self presentViewController:success animated:YES completion:nil];
                                         } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                             NSLog(@"Failure %@", error.description);
                                             UIAlertController *fail = [UIAlertController alertControllerWithTitle:@"Upload Failed"
                                                            message:@"Please check your internet connection, or the status of the server."
                                                            preferredStyle:UIAlertControllerStyleAlert];
                                             UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Ok"
                                                                        style:UIAlertActionStyleCancel handler:nil];
                                             [fail addAction:ok];
                                             [self presentViewController:fail animated:YES completion:nil];
                                         }];
        
        // 4. Set the progress block of the operation.
        [operation setUploadProgressBlock:^(NSUInteger __unused bytesWritten,
                                            long long totalBytesWritten,
                                            long long totalBytesExpectedToWrite) {
            NSLog(@"Wrote %lld/%lld", totalBytesWritten, totalBytesExpectedToWrite);
        }];
        
        // 5. Begin!
        [operation start];
    }
    else if([[_filename pathExtension] isEqualToString:@"csv"]){
        
        _metadata = [NSString stringWithFormat:@"Data taken by: %@",_username];
        _fileToUpload = [NSData dataWithContentsOfFile:path];
        _filenameNoExt = [_filename stringByDeletingPathExtension];
        
        NSDictionary *parameters = @{ @"username" : _username,
                                      @"metadata" : _metadata};

        
        // 1. Create `AFHTTPRequestSerializer` which will create your request.
        AFHTTPRequestSerializer *serializer = [AFHTTPRequestSerializer serializer];
        
        //_fileToUpload = [NSData dataWithContentsOfFile:_savedFiles[_selectedRowNumber]];
        
        // 2. Create an `NSMutableURLRequest`.
        NSMutableURLRequest *request =
        [serializer multipartFormRequestWithMethod:@"POST" URLString:@"http://69.91.198.44:8080/GeodatabaseServer/FileUpload"
                                        parameters:parameters
                         constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
                             [formData appendPartWithFileData:_fileToUpload
                                                         name:_filenameNoExt
                                                     fileName:_filename
                                                     mimeType:@"text/csv"];
                         }error:nil];
        
        // 3. Create and use `AFHTTPRequestOperationManager` to create an `AFHTTPRequestOperation` from the `NSMutableURLRequest` that we just created.
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        
        // setting options to allow reading fragments (this will NOT work without it set)
        manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
        
        AFHTTPRequestOperation *operation =
        [manager HTTPRequestOperationWithRequest:request
                                         success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                             UIAlertController *success = [UIAlertController alertControllerWithTitle:@"Success"
                                                                message:nil
                                                                preferredStyle:UIAlertControllerStyleAlert];
                                             UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Ok"
                                                                                          style:UIAlertActionStyleCancel handler:nil];
                                             [success addAction:ok];
                                             [self presentViewController:success animated:YES completion:nil];
                                         } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                             NSLog(@"Failure %@", error.description);
                                             UIAlertController *fail = [UIAlertController alertControllerWithTitle:@"Upload Failed"
                                                                message:@"Please check your internet connection, or the status of the server."
                                                                preferredStyle:UIAlertControllerStyleAlert];
                                             UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Ok"
                                                                                          style:UIAlertActionStyleCancel handler:nil];
                                             [fail addAction:ok];
                                             [self presentViewController:fail animated:YES completion:nil];
                                         }];
        
        // 4. Set the progress block of the operation.
        [operation setUploadProgressBlock:^(NSUInteger __unused bytesWritten,
                                            long long totalBytesWritten,
                                            long long totalBytesExpectedToWrite) {
            NSLog(@"Wrote %lld/%lld", totalBytesWritten, totalBytesExpectedToWrite);
        }];
        
        // 5. Begin!
        [operation start];
    }
}

- (void)downloadFileNames {

    NSDictionary *parameters = @{ @"username" : _username};
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"image/jpeg"];
    // manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/csv"];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/plain"];

    [manager GET:@"http://69.91.198.44:8080/GeodatabaseServer/File"
      parameters:parameters
         success:^(AFHTTPRequestOperation *operation, id responseObject) {

             if (responseObject) {
                 NSString *temp = [operation.responseString stringByReplacingOccurrencesOfString:@" " withString:@""];
                 _downloadedFileNames = [temp componentsSeparatedByString:@","];

                 for (int i = 0; i < _downloadedFileNames.count; i++) {
                     //NSLog(@"The downloaded file name is: -->%@<--",_downloadedFileNames[i]);
                 }
                 [self.tableView reloadData]; // this populates the table
             }
         } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             NSLog(@"Error: %@",  operation.responseString);
             NSLog(@"Error: %@",  operation.response);
             NSLog(@"Error: %@", error);
         }];
}

/******************************************************************************
 * downloadFile
 *
 * This method
 ******************************************************************************/
- (void)downloadFile {
    NSDictionary *parameters = @{ @"filename" : _filename};
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];

    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"image/jpeg"];
    //manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/csv"];
    //manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/plain"];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString  *filePath;
    if (_savingWithNewName == NO) {
        filePath = [NSString stringWithFormat:@"%@/%@", documentsDirectory,_filename];
    }
    else{
        filePath = [NSString stringWithFormat:@"%@/%@", documentsDirectory,newFileName];
    }
    
    AFHTTPRequestOperation *operation = [manager GET:@"http://69.91.198.44:8080/GeodatabaseServer/FileDownload"
      parameters:parameters
         success:^(AFHTTPRequestOperation *operation, id responseObject) {
             //NSLog(@"operation: %@",  operation.responseString);
             //NSLog(@"operation: %@",  operation.response);
             //NSLog(@"operation: %@",  operation.responseObject);
         } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             NSLog(@"ERROR response: %@",  operation.response);
             NSLog(@"ERROR error: %@", error);
         }];
    [operation setDownloadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite){
        int progress = (((int)totalBytesWritten) / ((int)totalBytesExpectedToWrite));
        NSLog(@"Progress = %d",progress);
        //self.progressView.progress = progress;
    }];
    operation.outputStream = [NSOutputStream outputStreamToFileAtPath:filePath append:NO];
    [operation start];
}

/******************************************************************************
 * checkFileName
 *
 * This method checks to see if there is a file in the sandbox already named
 * what the downloading file name is. If yes returns YES.
 ******************************************************************************/
- (BOOL)checkFileName:(NSString*)fileBeingSaved {
    
    NSError *error = nil;
    NSArray *paths = NSSearchPathForDirectoriesInDomains
    (NSDocumentDirectory, NSUserDomainMask, YES);
    
    NSString *documentsDirectory = [paths objectAtIndex:0];
    // if files exist then delete
    NSArray *contents = [[NSFileManager defaultManager]
                         contentsOfDirectoryAtPath: documentsDirectory error:&error];
    NSEnumerator *e = [contents objectEnumerator];
    NSString *filename;
    while ((filename = [e nextObject])) {
        //NSLog(@"File Name = %@",filename);
        if ([filename isEqualToString:fileBeingSaved]) {
            return YES;
        }
    }
    return NO;
}

- (NSString*)getNewFileName:(NSString *)nameToUpdate {
    
    NSString *pathExt = nameToUpdate.pathExtension;
    
    NSString *newName = [NSString stringWithFormat:@"%@_%@_%@.%@",
                 [nameToUpdate substringToIndex:10],_projectName,_username,pathExt];

    int saveNumber = 1;
    while ([self checkFileName:newName]) { // loop until a name is available
        newName = [NSString stringWithFormat:@"%@_%@_%@_%i.%@",
                             [nameToUpdate substringToIndex:10],_projectName,_username,saveNumber,pathExt];
        saveNumber++;
        //NSLog(@"CHECKING: newName is --->%@",newName);
    }
    return newName;
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationController.navigationBarHidden = YES;

    _savingWithNewName = NO;
    if (_clickedUploadFile) {
        NSLog(@"clickedUploadFile");
        _fileCount = [self countFiles];
        [self buildArrayOfLocalFiles];
    }
    else if (_clickedGetFile){
        NSLog(@"clickedGetFile");
        [self downloadFileNames];
    }
    
    //[self prepareForUpload];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    return indexPath;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    _selectedRowNumber = (int)indexPath.row;
    _filenameNoExt = [_filename stringByDeletingPathExtension];
    
    if (_clickedUploadFile == YES) {
        _filename = _localFileNames[indexPath.row];
        NSString *alertMessage = [[NSString alloc]
                        initWithFormat:@"Upload %@?",_filename];
        
        UIAlertController *uploadAlert = [UIAlertController alertControllerWithTitle:@"Confirm Upload"
                                message:alertMessage
                                preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel"
                                style:UIAlertActionStyleCancel handler:nil];
        [uploadAlert addAction:cancel];
        UIAlertAction *yes = [UIAlertAction actionWithTitle:@"Cancel"
                                style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
            [self uploadFile];
        }];
        [uploadAlert addAction:yes];
        [self presentViewController:uploadAlert animated:YES completion:nil];
    }
    else if (_clickedGetFile == YES){
        _filename = _downloadedFileNames[indexPath.row];
        
        if ([self checkFileName:_filename]) {
            // ask to overwrite or save with new name
            NSString *alertMessage = [[NSString alloc]
                                      initWithFormat:@"A file by the name %@ already exists.",_filename];
            
            UIAlertController *downloadAlert = [UIAlertController alertControllerWithTitle:@"Confirm Download"
                                            message:alertMessage
                                            preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel"
                                            style:UIAlertActionStyleCancel handler:nil];
            [downloadAlert addAction:cancel];
            UIAlertAction *yes = [UIAlertAction actionWithTitle:@"Save With New Name"
                                            style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
                _savingWithNewName = YES;
                newFileName = [self getNewFileName:_filename];
                [self downloadFile];
                _savingWithNewName = NO; // reset
            }];
            [downloadAlert addAction:yes];
            [self presentViewController:downloadAlert animated:YES completion:nil];
        }
        else{
            // download the file
            NSString *alertMessage = [[NSString alloc]
                                      initWithFormat:@"Download %@?",_filename];
            
            UIAlertController *downloadAlert = [UIAlertController alertControllerWithTitle:@"Confirm Download"
                                            message:alertMessage
                                            preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
            [downloadAlert addAction:cancel];
            UIAlertAction *yes = [UIAlertAction actionWithTitle:@"Yes"
                                    style:UIAlertActionStyleCancel
                                    handler:^(UIAlertAction *action) {
                [self downloadFile];
            }];
            [downloadAlert addAction:yes];
            [self presentViewController:downloadAlert animated:YES completion:nil];
        }
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if (_clickedUploadFile) {
        return self.fileCount;
    }
    else
        return _downloadedFileNames.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MainCell"];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"MainCell"];
    }
    // Configure the cells...
    if (_clickedUploadFile) {
        if (_savedFiles[0] != nil) {
            for (int i = 0; i < self.fileCount; i++) {
                if (indexPath.row == i) {
                    cell.textLabel.text = _localFileNames[i];
                }
            }
        }
        return cell;
    }
    else{ // clickedGetFile
        if (_downloadedFileNames[0] != NULL) {
            for (int i = 0; i < _downloadedFileNames.count; i++) {
                if (indexPath.row == i) {
                    cell.textLabel.text = _downloadedFileNames[i];
                }
            }
        }
    }
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
