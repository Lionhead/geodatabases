//
//  FilesToServer.h
//  GeoDatabaseWetlands
//
//  Created by Jonathan Mason on 8/24/14.
//  Copyright (c) 2014 Jonathan Mason. All rights reserved.
//

#import <UIKit/UIKit.h>

// See comment at top of file for a complete description
@interface FilesToServer : UITableViewController

@property NSString *username;
@property NSString *projectName;
@property NSString *filename; // used to hold the name of the file in a row
@property NSString *filenameNoExt;
@property NSString *csvBeingDownloaded;
@property UIImage *imageBeingDownloaded;
@property NSData *testDownload;
@property NSString *metadata;
@property BOOL clickedUploadFile; // value used if user clicked open file
@property BOOL clickedGetFile;
@property BOOL savingWithNewName;
@property NSData *fileToUpload;
@property NSData *downloadedFile;
@property int selectedRowNumber;  //
@property NSMutableArray *localFileNames; // container for file names
@property NSArray *downloadedFileNames; // this takes the string of file names downloaded from the server
@property NSMutableArray *savedFiles; // container for files


////////////*********************** METHODS *************************/////////////////

- (BOOL)checkFileName:(NSString*)fileSave; // this method checks to see if a file downloaded has the same name as existing
- (NSString*)getNewFileName:(NSString *)nameToUpdate;
- (int)countFiles; // counts the amount of CSV files in sandbox
- (void)buildArrayOfLocalFiles; // adds file names and data files to an array
- (void)downloadFile;
- (void)uploadFile;
- (void)downloadFileNames;  // downloads file names from Apache Server

@end
