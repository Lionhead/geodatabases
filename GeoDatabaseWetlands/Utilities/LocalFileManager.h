//
//  FileManager.h
//  GeoDatabaseWetlands
//
//  Created by Jonathan Mason on 6/17/15.
//  Copyright (c) 2015 Jonathan Mason. All rights reserved.
//

#import <Foundation/Foundation.h>

// See comment at top of file for a complete description
@interface LocalFileManager : NSObject

+ (void)deleteFile:(NSString*)filename inFolder:(NSString*)folder fromUser:(NSString*)user;
+ (void)deleteCSVFile:(NSString*)filename inFolder:(NSString*)folder fromUser:(NSString*)user;
+ (void)deleteImageFile:(NSString*)fileName fromDir:(NSString*)dir;
+ (void)deleteImageNode:(NSString*)nodeName fromDir:(NSString*)dir;
+ (NSString*)checkForDirectoryForUser:(NSString*)username
                       inSubdirectory:(NSString*)subdirectory;
+ (NSString*)checkForDirectoryForUserPhotos:(NSString*)user inCommunity:(NSString*)community;
+ (BOOL)checkFileName:(NSString*)fileName atPath:(NSString*)path;
+ (void)createDirectoryUsingThisPath:(NSString*)path;
+ (BOOL)saveAsCSVFile:(NSString*)data withPathAndName:(NSString*)csvFilePath;

@end
