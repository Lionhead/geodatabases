//
//  UIViewControllerTracker.h
//  GeoDatabaseWetlands
//
//  Created by Jonathan Mason on 7/3/15.
//  Copyright (c) 2015 Jonathan Mason. All rights reserved.
//
//
/*******************************************************************************************
 *******************************************************************************************
 *
 * This class is used to keep track of the current view controller so that you can call on it
 * return the view controller at the top of the stack. You must always assign the new
 * view controller
 *
 *******************************************************************************************
 *******************************************************************************************/


#import <Foundation/Foundation.h>

@interface UIViewControllerTracker : NSObject

@property (strong, nonatomic) UIViewController *currentViewController;

+ (id)sharedVC;

@end
