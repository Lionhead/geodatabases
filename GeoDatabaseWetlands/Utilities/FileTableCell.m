//
//  FileTableCell.m
//  GeoDatabaseWetlands
//
//  Created by Jonathan Mason on 8/11/15.
//  Copyright (c) 2015 Jonathan Mason. All rights reserved.
//

#import "FileTableCell.h"

@implementation FileTableCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
