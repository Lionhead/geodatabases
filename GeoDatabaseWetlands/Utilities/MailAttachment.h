//
//  MailAttachment.h
//  GeoDatabaseWetlands
//
//  Created by Jonathan Mason on 11/16/15.
//  Copyright © 2015 Jonathan Mason. All rights reserved.
//
/*******************************************************************************************
 *******************************************************************************************
 *
 * This class is used to send emails with attachments from the app. It can only be used if
 * the user has setup an email account on the ipad.
 *
 *******************************************************************************************
 *******************************************************************************************/

#import <Foundation/Foundation.h>

@interface MailAttachment : NSObject

- (id)initWithViewController:(UIViewController*)vc;
- (void)popupAskToEmailSpreadsheetForFile:(NSData*)fileData withName:(NSString*)filename;

@end
