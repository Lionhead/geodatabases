//
//  FormatForCSV.h
//  GeoDatabaseWetlands
//
//  Created by Jonathan Mason on 12/2/14.
//  Copyright (c) 2014 Jonathan Mason. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "PlantCommunity.h"
#import "PlantFormManager.h"
#import "UserInfo.h"
#import "WaterCommunity.h"
@class PlantFormManager;
@class WaterFormManager;

// See comment at top of file for a complete description
@interface CSVformatting : NSObject

+ (CSVformatting*)sharedCSVformatting;

+ (NSString*)formatPlantFormsIntoCSV:(PlantFormManager*)form
                        withUserInfo:(UserInfo*)info
                        andPlantCommunity:(PlantCommunity*)plantCommunity;
+ (NSString*)forwardDashToUnderscore:(NSString*)string;
+ (NSString*)removeCommas:(NSString *)myString; //removes all commas when saving

+ (NSString*)formatWaterFormsIntoCSV:(WaterFormManager*)formManager
                        withUserInfo:(UserInfo*)userInfo;
+ (NSString*)handleBlankStringsOnUpload:(NSString*)str;

@end
