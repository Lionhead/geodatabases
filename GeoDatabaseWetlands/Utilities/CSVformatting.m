//
//  FormatForCSV.m
//  GeoDatabaseWetlands
//
//  Created by Jonathan Mason on 12/2/14.
//  Copyright (c) 2014 Jonathan Mason. All rights reserved.
//

#import "CSVformatting.h"
#import "PlantCommunityForm.h"

@implementation CSVformatting

// ensures only one instance is made
+ (id)sharedCSVformatting {
    static dispatch_once_t predicate = 0;
    __strong static id sharedObject = nil;
    
    dispatch_once(&predicate, ^{
        sharedObject = [[self alloc]init];
    });
    
    return sharedObject;
}

// initialization
-(id)init {
    self = [super init];
    
    return self;
}

/***************************************************************************************************
 * removeCommas
 *
 * Removes all commas found in a string and returns the resulting string
 **************************************************************************************************/
+ (NSString *)removeCommas:(NSString *)myString {
    
    NSString *result = [myString
                        stringByReplacingOccurrencesOfString:@"," withString:@""];
    return result;
}

/***************************************************************************************************
 * removeSpaces
 *
 * Sets the delegates for the objects which need them on viewDidLoad.
 **************************************************************************************************/
- (NSString *)replaceSpaceWithUnderScoreIn:(NSString *)string {
    NSString *result = [string
                        stringByReplacingOccurrencesOfString:@" " withString:@"_"];
    
    return result;
}

/***************************************************************************************************
 * formatPlantFormsIntoCSV
 *
 * This method is in charge of calling all other methods to properly format
 * a plant community data save into a csv file.
 **************************************************************************************************/
+ (NSString*)formatPlantFormsIntoCSV:(PlantFormManager*)plantFormManager
                        withUserInfo:(UserInfo*)info
                        andPlantCommunity:(PlantCommunity *)plantCommunity {
    // gather names, date, weather, project name, instructor/mentor, class number, air temp, weather
    NSString *result = @"";
    if (![info.classNumber isEqualToString:@""]) { // with a class number
        result = [result stringByAppendingString:[self plantCommunityHeadersWithClass]];
        result = [result stringByAppendingString:[self giveMeThisManyCarriageReturns:1]];
        result = [result stringByAppendingString:
                  [self dataWithClassUsingPlantCommunity:plantCommunity andUserInfo:info]];
    }
    else{ // no class number
        result = [result stringByAppendingString:[self stringHeadersWithNoClass]];
        result = [result stringByAppendingString:[self giveMeThisManyCarriageReturns:1]];
        result = [result stringByAppendingString:
                  [self dataWithNoClassUsingPlantCommunity:plantCommunity andUserInfo:info]];
    }
    
    result = [result stringByAppendingString:[self giveMeThisManyCarriageReturns:3]];
    
    PlantCommunityForm *form;
    // get form titles
    for (int i = 0; i < plantFormManager.arrayOfPlantForms.count; i++) {
        form = plantFormManager.arrayOfPlantForms[i];
        result = [result stringByAppendingString:
                  [NSString stringWithFormat:@"%@,",form.communityLabel.text]];
    }
    // get form text entries
    result = [result stringByAppendingString:@"\r\n"];
    for (int i = 0; i < plantFormManager.arrayOfPlantForms.count; i++) {
        form = plantFormManager.arrayOfPlantForms[i];
        result = [result stringByAppendingString:[NSString stringWithFormat:@"%@,",
            [[self encapsulateTextForCSV:form.communityText.text]
             stringByReplacingOccurrencesOfString:@"\n" withString:@"\r\n"]]];
    }
    return result;
}

/***************************************************************************************************
 * stringHeadersWithNoClass
 *
 * This method returns a string with all the headers used in the Plant Community
 * with a header for class number.
 **************************************************************************************************/
+ (NSString*)stringHeadersWithNoClass {
    NSString *result = @"Date,Time Started,Username,Researchers,Project,"
                        "Instructor/Mentor,Air Temperature,Weather Conditions";
    return result;
}

/***************************************************************************************************
 * plantCommunityHeadersWithClass
 *
 * This method returns a string with all the headers used in the Plant Community
 * with a header for class number.
 **************************************************************************************************/
+ (NSString*)plantCommunityHeadersWithClass {
    NSString *result = @"Date,Time Started,Username,Researchers,Project,Instructor/Mentor,"
                        "Class Number,Air Temperature,Weather Conditions";
    return result;
}

/***************************************************************************************************
 * dataWithNoClassUsingPlantCommunity :andUserInfo
 *
 * This method returns a string with all the user data Plant community data
 * collected, but without a class number.
 **************************************************************************************************/
+ (NSString*)dataWithNoClassUsingPlantCommunity:(PlantCommunity*)plant andUserInfo:(UserInfo*)info {
    NSString *result = [NSString stringWithFormat:@"%@,%@,%@,%@,%@,%@,%@,%@",
        info.plantDateStarted,info.plantTimeStarted,info.username,
        [NSString stringWithFormat:@"\"%@\"",info.researchers ],info.project,
        [self giveBlankStringIfNull:info.instructor],
        [self giveBlankStringIfNull:info.currentTemp],
        [self returnStringFromDictionaryEntries:info.weatherOptionsPtr.selectedWeatherConditions]];
    return result;
}

/***************************************************************************************************
 * dataWithClassUsingPlantCommunity :andUserInfo
 *
 * This method returns a string with all the user data Plant community data
 * collected, including a class number.
 **************************************************************************************************/
+ (NSString*)dataWithClassUsingPlantCommunity:(PlantCommunity*)plant andUserInfo:(UserInfo*)info {
    NSString *result = [NSString stringWithFormat:@"%@,%@,%@,%@,%@,%@,%@,%@,%@",
        info.plantDateStarted,info.plantTimeStarted,info.username,
        [NSString stringWithFormat:@"\"%@\"",info.researchers ],info.project,
        [self giveBlankStringIfNull:info.instructor],info.classNumber,
        [self giveBlankStringIfNull:info.currentTemp],
        [self returnStringFromDictionaryEntries:info.weatherOptionsPtr.selectedWeatherConditions]];
    [result stringByReplacingOccurrencesOfString:@"(null)" withString:@""];
    return result;
}

/***************************************************************************************************
 * giveBlankStringIfNull
 *
 * This method returns a a blank string if a UITextField or NSString is (null).
 * This is used so that there aren't and null statements in csv files.
 **************************************************************************************************/
+ (NSString*)giveBlankStringIfNull:(id)object {
    if ([object isKindOfClass:[UITextField class]] ){
        UITextField *textField = object;
        if(textField != nil && ![textField.text isEqualToString:@""]){
            return textField.text;
        }
    }
    else if ([object isKindOfClass:[NSString class]]){
        NSString *str = object;
        if (str != nil) {
            return str;
        }
    }
    return @"";
}

+ (NSString*)forwardDashToUnderscore:(NSString*)string {
    return [string stringByReplacingOccurrencesOfString:@"/" withString:@"_"];
}

/***************************************************************************************************
 * formatNamesFromArray
 *
 * This method returns a string of names separated
 **************************************************************************************************/
- (NSString*)formatNamesFromArray:(NSArray*)names {
    NSString *result;
    for (int i = 0; i < names.count; i++) {
        result = [result stringByAppendingString:names[i]];
    }
    
    return result;
}

/***************************************************************************************************
 * giveMeThisManyCommas
 *
 * This method returns a string of N amount of commas.
 **************************************************************************************************/
+ (NSString*)giveMeThisManyCommas:(int)amount {
    NSString *result = @"";
    for (int i = 0; i < amount; i++) {
        result = [result stringByAppendingPathComponent:@","];
    }
    return result;
}

+ (NSString*)encapsulateTextForCSV:(NSString*)text {
    text = [NSString stringWithFormat:@"\"%@\"",text];
    return text;
}

/***************************************************************************************************
 * giveMeThisManyCarriageReturns
 *
 * This method returns a string of N amount of carriage returns.
 **************************************************************************************************/
+ (NSString*)giveMeThisManyCarriageReturns:(int)amount {
    NSString *result = @"";
    for (int i = 0; i < amount; i++) {
        result = [result stringByAppendingPathComponent:@"\r\n"];
    }
    result = [result stringByReplacingOccurrencesOfString:@"/" withString:@""];
    return result;
}

/***************************************************************************************************
 * returnStringFromDictionaryEntries
 *
 * This method creates a string from all the values in a dictionary. A dictionary is used to hold
 * the weather conditions. Each condition is separated by a comma.
 **************************************************************************************************/
+ (NSString*)returnStringFromDictionaryEntries:(NSMutableDictionary*)dict {
    int counter = 0;
    UILabel *label;
    NSString *result =@"";
    for (id key in dict) {
        label = dict[key];
        result = [result stringByAppendingString:[NSString stringWithFormat:@"%@",label.text]];
        counter++;
        if (counter < dict.count) {
            result = [result stringByAppendingString:@", "];
        }
    }
    result = [NSString stringWithFormat:@"\"%@\"",result];
    
    
    return result;
}

/***************************************************************************************************
 * formatWaterFormsIntoCSV
 *
 * This method creates a string for a csv spreadsheet for the water community.
 **************************************************************************************************/
+ (NSString*)formatWaterFormsIntoCSV:(WaterFormManager*)formManager withUserInfo:(UserInfo*)userInfo {
    NSString *result = [CSVformatting waterCommHeader];
    
    // loop through form by form
    WaterCommunityForm *form;
    for (int i = 0; i < formManager.arrayOfForms.count; i++) {
        form = formManager.arrayOfForms[i];
        NSString *info = [CSVformatting addUserInfo:userInfo];
        NSString *weatherInfo = [CSVformatting addWeatherInfo:userInfo];
        NSString *top = [NSString stringWithFormat:@"%@,latitude,longitude\n,%@,%@\n",
                  form.siteID,
                  form.latitude.titleLabel.text,
                  form.longitude.titleLabel.text];
        NSString *columns = @"Date,Temp(c),pH,Depth,Conductivity,Chlorophyll,Mean Turbidity (NTU),"
                "Dissolved Oxygen (mg/l),Dissolved Oxygen (% Sat),Suna Nitrate (mg/l),"
                "Salinity (ppt),Blue Green Algae (ug/l),Water Flow Velocity (cm/s),Tide Stage,Water Body Type,"
                "Secchi Disk Extinction Depth (m),Additional Notes\n";
        NSString *data = [NSString stringWithFormat:@"%@,%@,%@,%@,%@,%@,%@,%@,%@"
                          ",%@,%@,%@,%@,%@,%@,%@,%@",
                          [CSVformatting handleBlankStrings:[CSVformatting encapsulateTextForCSV:form.displayedTime.text]],
                          [CSVformatting handleBlankStrings:[CSVformatting encapsulateTextForCSV:form.temp.text]],
                          [CSVformatting handleBlankStrings:[CSVformatting encapsulateTextForCSV:form.pH.text]],
                          [CSVformatting handleBlankStrings:[CSVformatting encapsulateTextForCSV:form.depth.text]],
                          [CSVformatting handleBlankStrings:[CSVformatting encapsulateTextForCSV:form.conductivity.text]],
                          [CSVformatting handleBlankStrings:[CSVformatting encapsulateTextForCSV:form.chlorophyll.text]],
                          [CSVformatting handleBlankStrings:[CSVformatting encapsulateTextForCSV:form.meanTurbidity.text]],
                          [CSVformatting handleBlankStrings:[CSVformatting encapsulateTextForCSV:form.dissolvedOxygenMg.text]],
                          [CSVformatting handleBlankStrings:[CSVformatting encapsulateTextForCSV:form.dissolvedOxygenSat.text]],
                          [CSVformatting handleBlankStrings:[CSVformatting encapsulateTextForCSV:form.suna.text]],
                          [CSVformatting handleBlankStrings:[CSVformatting encapsulateTextForCSV:form.salinity.text]],
                          [CSVformatting handleBlankStrings:[CSVformatting encapsulateTextForCSV:form.blueGreenAlgae.text]],
                          [CSVformatting handleBlankStrings:[CSVformatting encapsulateTextForCSV:form.waterFlowVelocity.text]],
                          [CSVformatting handleBlankStrings:[CSVformatting encapsulateTextForCSV:form.tideStage.text]],
                          [CSVformatting handleBlankStrings:[CSVformatting encapsulateTextForCSV:form.waterBodyType.text]],
                          [CSVformatting handleBlankStrings:[CSVformatting encapsulateTextForCSV:form.secchiDiskExtDepth.text]],
                          [CSVformatting handleBlankStrings:[CSVformatting encapsulateTextForCSV:form.notes.text]]];
        result = [result stringByAppendingString:[NSString stringWithFormat:@"%@%@%@%@%@\n\n\n",
                                                info,weatherInfo,top,columns,data]];
    }
    result = [result stringByReplacingOccurrencesOfString:@"/" withString:@"_"];
    result = [result stringByReplacingOccurrencesOfString:@"(" withString:@""];
    result = [result stringByReplacingOccurrencesOfString:@")" withString:@""];
    
    return result;
}

// specifically used for weather community csv files
+ (NSString*)waterCommHeader {
    NSString *header = @"UWB/CC Campus, Wetland and North Creek Monitoring Database\n"
                        "UWB Wetland Sampling Sites\n\n";
    return header;
}

// specifically used for weather community csv files
+ (NSString*)addUserInfo:(UserInfo*)userInfo {
    NSString *classResearch, *answer;
    if (![userInfo.classNumber isEqualToString:@""]) {
        classResearch = @"Class Number";
        answer = userInfo.classNumber;
    }
    else{
        classResearch = @"Independent Research";
        answer = @"Yes";
    }
    NSString *info = [NSString stringWithFormat:@"Username,Researchers,Project Name,"
            "Instructor/Mentor,%@\n%@,%@,%@,%@\n",
            classResearch,
                      userInfo.username,
                      [CSVformatting encapsulateTextForCSV:userInfo.researchers],
                      [CSVformatting encapsulateTextForCSV:userInfo.project],
                      [CSVformatting encapsulateTextForCSV:answer]];
    return info;
}

+ (NSString*)addWeatherInfo:(UserInfo*)userInfo {
    NSString *weatherInfo = [NSString stringWithFormat:@"Air Temp,Weather\n%@,%@\n",
                      [CSVformatting encapsulateTextForCSV:userInfo.currentTemp],
                      [CSVformatting encapsulateTextForCSV:userInfo.weatherConditions]];
    
    return weatherInfo;
}

// if string is blank return 'NDC' for csv formatting
+ (NSString*)handleBlankStrings:(NSString*)str {
    if ([str isEqualToString:@"\"\""]) {
        return @"\"NDC\"";
    }
    return str;
}

+ (NSString*)handleBlankStringsOnUpload:(NSString*)str {
    if ([str isEqualToString:@""]) {
        return @"NDC";
    }
    return str;
}

@end