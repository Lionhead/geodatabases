//
//  SearchView.h
//  GeoDatabaseWetlands
//
//  Created by Jonathan Mason on 9/17/14.
//  Copyright (c) 2014 Jonathan Mason. All rights reserved.
//
//  This software is intended soley for the University of Washington Bothell.

#import <UIKit/UIKit.h>

// See comment at top of file for a complete description
@interface SearchView : UIViewController

@property (strong, nonatomic) IBOutlet UIWebView *viewWeb;

@property (strong) NSString *targetSearch;

- (NSString*)formatSearchString: (NSString*)topic;

@end
