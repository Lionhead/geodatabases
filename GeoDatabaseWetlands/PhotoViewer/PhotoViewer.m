//
//  PhotoViewer.m
//  GeoDatabaseWetlands
//
//  Created by Jonathan Mason on 5/12/14.
//  Copyright (c) 2014 Jonathan Mason. All rights reserved.
//
/***************************************************************************************************
 * PhotoViewer class inherits from UIViewController, and uses the following
 * protocols: <UIImagePickerControllerDelegate, UINavigationControllerDelegate,
 * CLLocationManagerDelegate>
 *
 * From this class the user can call on the camera to take photos, delete,
 * browse through, add captions, add notes, add labels, perform a search on
 * the label, save metadata, and upload to the database server.
 *
 * Saving Format: /UserFolder/CommunityFolder
 *
 **************************************************************************************************/

#import "PhotoViewer.h"
#import "PlantCommunity.h"
#import "ImageNode.h"
#import "SearchView.h"
#import "AFNetworking.h"
#import "PhotoViewerErrors.h"
#import "PhotoMetadataView.h"
#import <Realm/Realm.h>
#import "PhotoViewerProvenance.h"
#import "SystemSounds.h"

#define fullScreenWidth 768
#define fullScreenHeight 1024
#define defaultPhotoWidth 768
#define defaultPhotoHeight 1024
#define imageDefaultXcoord 0
#define imageDefaultYcoord 0
#define centerScreenX 384
#define photoLabelY 40
#define photoLabelHeight 30
#define PlantCommunityFolder @"PlantCommunity"
#define WaterCommunityFolder @"WaterCommunity"
#define BirdCommunityFolder @"BirdCommunity"
#define CommunityCount 3


@interface PhotoViewer (CameraDelegateMethods)

@property (weak,atomic) UIImage *captionedImage; // used for adding caption to image
@property (weak,nonatomic) NSNumber *valueForOrientation; // used for adding caption to image

@end

@implementation PhotoViewer {
    IBOutlet UIImageView *backgroundImageView;
    IBOutlet UIView *backgroundView;
    CLLocationManager *locationManager;
    CLGeocoder *geocoder;
    CLPlacemark *placemark;
    UIButton *backButton;
    UIImage *savingImage; // used for saving the image taken
    BOOL fullScreenMode;
    PhotoMetadataView *metadataView;
    DirectUploadForm *uploadForm;
    ViewManager *viewManager;
    NSString *latitude;
    NSString *longitude;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.userInfo = [UserInfo sharedUserInfo];
    self.vcTracker = [UIViewControllerTracker sharedVC];
    self.vcTracker.currentViewController = self;
    self.login = [UserLogin sharedUserLogin];
    metadataView = [[PhotoMetadataView alloc]initWithFrame:CGRectMake(184, 180, 400, 535)]; // bug
    [self.view addSubview:metadataView];
    
    self.loggedInAs.text = [NSString stringWithFormat:@"Logged in as: %@",self.userInfo.username];
    self.loggedInAs.textColor = [UIColor lightGrayColor];
    
    [self prepNavigationControllerOnLoad];

    viewManager = [[ViewManager alloc]initWithUser:self.userInfo];
    [self.view addSubview:viewManager.scrollView];
    [self.view addSubview:viewManager.fullView];
    viewManager.metadataView = metadataView;
    
    [self configureBackButton];
    [self configureImageTitle];
    [self updateImageTitleSize];
    [self addGestures];
    [self configureMenuButton];
    [self.view bringSubviewToFront:self.cameraButton];
    [self setButtonVisibityOnReload]; //
    [self prepDropDownMenu];
    [self.view bringSubviewToFront:self.loggedInAs];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:YES];
    if (self.vcTracker.currentViewController != self) {
        self.vcTracker.currentViewController = self;
    }
}

- (void)setButtonVisibityOnReload {
    if (viewManager.imageContainer.count == 0) {
        [self.menuButton setHidden:NO];
        [self.view bringSubviewToFront:self.menuButton];
        [self.cameraButton setHidden:NO];
        [self.photoTitle setHidden:YES];
        [backButton setHidden:NO];
    }
    else{
        [self.menuButton setHidden:YES];
        [self.cameraButton setHidden:YES];
        [self.photoTitle setHidden:YES];
        [backButton setHidden:YES];
    }
}

- (void)configureMenuButton {
    [self.view bringSubviewToFront:self.menuButton];
    self.menuButton.layer.shadowColor = [UIColor blackColor].CGColor;
    self.menuButton.layer.shadowOffset = CGSizeMake(2,5);
    self.menuButton.layer.shadowOpacity = .75f;
    
    self.cameraButton.layer.shadowColor = [UIColor blackColor].CGColor;
    self.cameraButton.layer.shadowOffset = CGSizeMake(2,5);
    self.cameraButton.layer.shadowOpacity = .75f;
}

/***************************************************************************************************
 * prepNavigationControllerOnLoad
 *
 * This method sets the navigation controller properties for this class.
 **************************************************************************************************/
- (void)prepNavigationControllerOnLoad {
    self.navigationController.navigationBarHidden = YES;
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.view.backgroundColor = [UIColor clearColor];
}

// initializes and sets properties to label above photo used as a title for image
- (void)configureImageTitle {
    self.photoTitle = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 0, 0)];
    [self.photoTitle setText:[self addGenericImageDescription]];
    [self.photoTitle setTextColor:[UIColor blackColor]];
    [self.photoTitle setTextAlignment:NSTextAlignmentCenter];
    [self.photoTitle setUserInteractionEnabled:YES];
    [self.photoTitle setFont:[UIFont fontWithName:@"HelveticaNeue" size:20]];
    
    [self.photoTitle setPreferredMaxLayoutWidth:500];
    self.photoTitle.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.photoTitle];
}

// this method automatically adjusts the size of the label based on the length of text
// it also adds a mask to round the corners
- (void)updateImageTitleSize {
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:
                            [UIFont fontWithName:@"HelveticaNeue" size:20],
                            NSFontAttributeName,
                            [UIColor whiteColor],
                            NSForegroundColorAttributeName, nil];
    CGSize textSize = [self.photoTitle.text sizeWithAttributes:attributes];
   
    CGFloat edgeSize = 35; // extra space made for rounded edge
    CGRect newLabelRect = CGRectMake(centerScreenX - ((textSize.width + edgeSize)/2), photoLabelY,
                                     textSize.width+edgeSize, photoLabelHeight);
    [self.photoTitle setFrame:newLabelRect];
    self.photoTitle.layer.shadowColor = [UIColor blackColor].CGColor;
    self.photoTitle.layer.shadowOffset = CGSizeMake(2,5);
    self.photoTitle.layer.shadowOpacity = .75f;

    UIBezierPath *maskPath= [UIBezierPath bezierPathWithRoundedRect:self.photoTitle.bounds
                                                       cornerRadius:10];
    CAShapeLayer *maskLayer = [CAShapeLayer layer];
    maskLayer.frame = self.photoTitle.bounds;
    maskLayer.path = maskPath.CGPath;
    [self.photoTitle.layer setMask:maskLayer];
}

- (BOOL)isMetadataHidden {
    return metadataView.hidden;
}

- (BOOL)isDropDownMenuHidden {
    return self.dropDownMenu.viewBox.hidden;
}

/***************************************************************************************************
 * returnImageNodeOfDisplayedImage
 *
 * This method returns the index of where the image passed in sits in the
 * imageContainer.
 **************************************************************************************************/
- (ImageNode*)returnImageNodeOfDisplayedImage {
    if (viewManager.imageContainer.count > 0) {
        id object = [viewManager.imageContainer current];
        ImageNode *node;
        if ([object isKindOfClass:[ImageNode class]]) {
            node = object;
            return node;
        }
        else{
            NSLog(@"Error: [PhotoViewer returnImageNodeOfDisplayedImage]");
            return nil;
        }
    }
    return nil;
}

/***************************************************************************************************
 * removeImageFromDatabase
 *
 * This method removes the image that user is deleting from the database.
 **************************************************************************************************/
- (void)removeImageFromDatabase:(ImageNode*)node {
    RLMResults *results = [PhotoViewerProvenance objectsWhere:
                            [NSString stringWithFormat:@"username = '%@' AND filename = '%@'",
                             node.username,node.filename]];
    
    if (results.count > 0) {
        PhotoViewerProvenance *pvp = [results firstObject];
        RLMRealm *realm = [RLMRealm defaultRealm];
        [realm beginWriteTransaction];
        [realm deleteObject:pvp];
        [realm commitWriteTransaction];
    }
    else{
        NSLog(@"Error: [PhotoViewer removeImageFromDatabase]: Could not find image to delete");
    }
}

/***************************************************************************************************
 * deleteImage
 *
 * This method creates a popup to confirm the user wants to delete the image
 * If yes then it calls the deleteConfirmed method.
 **************************************************************************************************/
- (void)deleteImage:(id)sender {
    if (metadataView.hidden == YES && viewManager.imageContainer.count != 0
                                                    && self.dropDownMenu.viewBox.hidden == YES) {
        UIAlertController *alert = [UIAlertController
                        alertControllerWithTitle:@"Delete this image?"
                        message:nil preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel"
                        style:UIAlertActionStyleCancel handler:nil];
        [alert addAction:cancelAction];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Yes"
                        style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [self deleteConfirmed];
        }];
        [alert addAction:okAction];
        [self presentViewController:alert animated:YES completion:nil];
    }
}

- (void)updatePhotoTitleWithString:(NSString*)string {
    if ([string isEqualToString:@""]) {
        self.photoTitle.text = [self addGenericImageDescription];
    }
    else
        self.photoTitle.text = string;
}

/***************************************************************************************************
 * deleteConfirmed
 *
 * This method calls on the LocalFileManager to delete an image from the local
 * drive by referencing data from the ImageNode.
 **************************************************************************************************/
- (void)deleteConfirmed {
    if (metadataView.isHidden == NO) {
        [metadataView setHidden:YES];
    }
    NSUInteger idx = [viewManager.imageContainer returnCurrentIndex];
    ImageNode *nodeToDelete = viewManager.imageContainer[idx];
    ImageNode *node;
    
    if (viewManager.imageContainer.count > 1) {
        if (!nodeToDelete) {
            [self popupIndexOutOfBoundsErrorFrom:@"[PhotoViewer: deleteConfirmed]"];
        }
        else{
            if (idx == 0){ // first image in container
                node = viewManager.imageContainer[idx+1];
            }
            else{ // else just move focus to the node before in container
                [viewManager.imageContainer setCurrentIndex:idx-1];
                node = viewManager.imageContainer[idx-1];
            }
        }
        [viewManager.fullView setImage:node.image];
        [self updatePhotoTitleWithString:node.title];
    }
    else{ //only 1 image to delete
        [viewManager.fullView setImage:nil];
    }
    [viewManager.imageContainer removeObjectAtIndex:idx];
    [self removeImageFromDatabase:nodeToDelete];
    [viewManager updateScrollContentSize];
    [self setButtonVisibityOnReload];
    [self updateImageTitleSize];
    NSLog(@"%lu",(unsigned long)[viewManager.imageContainer returnCurrentIndex]);
}

- (void)hideButtons {
    [self.menuButton setHidden:YES];
    [self.dropDownMenu.viewBox setHidden:YES];
    [self.photoTitle setHidden:YES];
    [self.cameraButton setHidden:YES];
    [backButton setHidden:YES];
}

- (void)showButtons {
    [self.menuButton setHidden:NO];
    [self.cameraButton setHidden:NO];
    [backButton setHidden:NO];
    // if there is an image in fullscreen mode
    if (viewManager.fullView.hidden == NO && viewManager.imageContainer.count > 0) {
        [self.photoTitle setHidden:NO];
    }
}


/***************************************************************************************************
 * toggleButtons
 *
 * This method toggles the visibility of the buttons shown on screen.
 **************************************************************************************************/
- (void)toggleButtons {
    if (self.menuButton.hidden == YES) {
        [self showButtons];
    }
    else
        [self hideButtons];
}

/***************************************************************************************************
 * hitDropDownMenuButton
 *
 * This method swaps the drop down menu between visible or not when the user
 * hits the button for it.
 **************************************************************************************************/
- (IBAction)hitDropDownMenuButton:(id)sender {
    [SystemSounds playTockSound];
    if (metadataView.hidden == YES && [self.dropDownMenu returnStartButtonIsHidden]) {
        if (self.dropDownMenu.viewBox.hidden == YES) {
            
            self.dropDownMenu.viewBox.hidden = NO;
            if (viewManager.fullView.hidden == NO) {
                if ([self.photoTitle.text isEqualToString:[self addGenericImageDescription]]) {
                    [self.dropDownMenu showFullScreenButtonsWithInfoButton:NO];
                }
                else
                    [self.dropDownMenu showFullScreenButtonsWithInfoButton:YES];
            }
            else
                [self.dropDownMenu showMultipleImageButtons];
            [self.view bringSubviewToFront:self.dropDownMenu.viewBox];
        }
        else
            self.dropDownMenu.viewBox.hidden = YES;
    }
}

/***************************************************************************************************
 * prepDropDownMenu
 *
 * This method instantiates the dropDownMenu, sets it to hidden, and adds it
 * to the superview (this viewController).
 **************************************************************************************************/
- (void)prepDropDownMenu {
    self.dropDownMenu = [[PhotoViewerMenu alloc]initWithPhotoViewer:self];
    [self.dropDownMenu.viewBox setHidden:YES];
    self.dropDownMenu.viewManager = viewManager;
    [self.view addSubview:self.dropDownMenu.viewBox];
}

/***************************************************************************************************
 * imageMetadataButton
 *
 * This method brings the notesView and the save button for it into view, and
 * loads whatever previous notes where saved about this photo.
 **************************************************************************************************/
- (void)imageMetadataButton:(id)sender {
    if (metadataView.hidden == YES) {
        [metadataView showMetadata];
        [self.view bringSubviewToFront:metadataView];
    }
    else{
        [metadataView setHidden:YES];
    }
}

- (IBAction)openCamera:(id)sender {
    [SystemSounds playTockSound];
    if (metadataView.hidden == YES) {
        [self launchCameraControllerFromViewController:self usingDelegate:self];
    }
}

/***************************************************************************************************
 * launchCameraControllerFromViewController
 *
 * This method launches the camera by using UIImagePickerController class,
 * and setting sourceType to default camera w/ no user editing, and self delegate.
 **************************************************************************************************/
-(BOOL) launchCameraControllerFromViewController:(UIViewController*) controller usingDelegate:
(id <UIImagePickerControllerDelegate, UINavigationControllerDelegate>) delegate {
    
    //variable to check whether there is a camera available
    BOOL truefalse = [UIImagePickerController isSourceTypeAvailable:
                      UIImagePickerControllerSourceTypeCamera];
    
    //if there is a camera, the delegate passed exists, and the controller
    //passed exists, proceed on, otherwise don't go any further
    if (!truefalse || (delegate == nil) || (controller == nil)) {
        NSLog(@"no can do, delegate/camera/view controller doesn't exist!");
        return NO;
    }
    // set cameraController properties
    UIImagePickerController *cameraController = [[UIImagePickerController alloc] init];
    cameraController.sourceType = UIImagePickerControllerSourceTypeCamera;
    cameraController.allowsEditing = NO;
    cameraController.delegate = delegate;
    [controller presentViewController:cameraController animated:YES completion:nil];
    
    return false;
}

// Handles when user hits the cancel button
- (void) imagePickerControllerDidCancel:(UIImagePickerController *) picker {
    [picker dismissViewControllerAnimated:YES completion:nil]; //dismisses the camera controller
}

/***************************************************************************************************
 * imagePickerController (saving a photo taken)
 *
 * This method grabs the media from the camera and calls to create a filename
 * and path for the image to be saved. It then shows the image on the imageView
 * for the user to see and calls a method to ask user for a caption. Finally it
 * checks to see what buttons need shown by calling the method
 * [setButtonVisibityOnReload].
 **************************************************************************************************/
- (void)imagePickerController:(UIImagePickerController*) picker
                didFinishPickingMediaWithInfo:(NSDictionary *)info {
    // get GPS location while loading the camera
    [self startGPSLocationServices];
    
    NSString *mediaType = [info objectForKey: UIImagePickerControllerMediaType];
    UIImage *originalImage, *editedImage, *imageToSave;
    NSString *filename;
    
    // Process for saving an image
    if (CFStringCompare ((CFStringRef) mediaType, kUTTypeImage, 0)== kCFCompareEqualTo) {
        //if it's an image
        //Assign the edited image to editedImage
        editedImage = (UIImage *) [info objectForKey:
                                   UIImagePickerControllerEditedImage];
        //Assign the original image to originalImage
        originalImage = (UIImage *) [info objectForKey:
                                     UIImagePickerControllerOriginalImage];
        
        //Check to see if there was indeed an edited image,
        // if so use that, otherwise use the original
        if (editedImage) {
            imageToSave = editedImage;
        } else {
            imageToSave = originalImage;
        }
        // get file name and folder to save images to
        filename = [NSString stringWithFormat:@"%@_%@.jpg",[self getDateAndTimeForFileName],
                    self.userInfo.username];
    }
    [picker dismissViewControllerAnimated: YES completion:nil]; //Dismiss the controller
    [self.photoTitle setHidden:YES];
    NSNumber *orientationValue = [self getOrientationFromDict:info];
    UIImage *temp = [self rotateImage:imageToSave basedOnOrientationValue:orientationValue];
    [viewManager switchToFullScreenWithImage:temp];
    
    [self askUserForCaptionOnImage:imageToSave withFilename:filename
                           withOrientation:orientationValue];
} // end imagePickerController

/***************************************************************************************************
 * askUserForCaptionOnImage
 *
 * This method will pop up an alert view to retrieve the information from the
 * user to use as a caption.
 **************************************************************************************************/
- (void)askUserForCaptionOnImage:(UIImage*)image withFilename:(NSString*)filename
                         withOrientation:(NSNumber*)orientation {
    
    UIAlertController *askForCaption = [UIAlertController
                alertControllerWithTitle:@"Great shot! Would you like to enter a caption?"
                message:@"WARNING: captions are permanent and can only be done once per photo."
                preferredStyle:UIAlertControllerStyleAlert];
    
    [askForCaption addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Enter caption here";
        textField.keyboardType = UIKeyboardAppearanceDark;
    }];
    
    // cancelAction will save the original image to drive and reference to ImageNode
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel"
               style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
                   
                   [self hideButtons];
                   [self createImageNodeForImage:image withFilename:filename andCaption:@""];
                   
               }];
    [askForCaption addAction:cancelAction];
    
    // add caption to image and then save it to drive and reference to ImageNode
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Ok"
               style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                   
       UITextField *temp = askForCaption.textFields.firstObject;
       if ([temp.text isEqualToString:@""]) {
           [self askUserForCaptionOnImage:image
                             withFilename:filename
                             withOrientation:orientation];
       }
       else{
           UIImage *finalImage = [self addCaptionToImage:image capturedString:temp.text];
           finalImage = [self rotateImage:finalImage basedOnOrientationValue:orientation];
           // set image
           [viewManager switchToFullScreenWithImage:finalImage];
           [self hideButtons];
           [self createImageNodeForImage:finalImage withFilename:filename andCaption:temp.text];
           
       }
               }];
    [askForCaption addAction:okAction];
    [self presentViewController:askForCaption animated:YES completion:nil];
}

- (void)createImageNodeForImage:(UIImage*)image withFilename:(NSString*)filename andCaption:(NSString*)caption {
    ImageNode *node = [ImageNode new];
    node.image = image;
    node.imageView = [[UIImageView alloc]initWithImage:image];
    node.username = self.userInfo.username;
    node.filename = filename;
    node.community = self.community;
    node.caption = caption;
    node.project = self.userInfo.project;
    node.date = [self getPresentDate];
    node.time = [self getPresentTime];
    if (latitude && ![latitude isEqualToString:@""]) {
        node.longitude = longitude;
        node.latitude = latitude;
    }
    
    [self saveToDatabase:node];
    [viewManager updateArrayByAddingImageNode:node];
    [self updatePhotoTitleWithString:node.title];
    [self updateImageTitleSize];
}

- (void)saveToDatabase:(ImageNode*)node {
    PhotoViewerProvenance *pvp = [PhotoViewerProvenance new];
    pvp.image = UIImageJPEGRepresentation(node.image, 1.0);
    pvp.username = node.username;
    pvp.filename = node.filename;
    pvp.community = node.community;
    pvp.caption = node.caption;
    pvp.project = node.project;
    pvp.date = node.date;
    pvp.time = node.time;
    pvp.title = node.title;
    if (node.longitude) {
        pvp.longitude = node.longitude;
        pvp.latitude = node.latitude;
    }
    if (node.address) {
        pvp.address = node.address;
    }
    pvp.notes = node.notes;
    
    RLMRealm *realm = [RLMRealm defaultRealm];
    [realm transactionWithBlock:^{
        [realm addObject:pvp];
    }];
}

- (BOOL)updateDatabase:(ImageNode*)node withValue:(NSString*)value forProperty:(NSString*)property{
    RLMResults *results = [PhotoViewerProvenance objectsWhere:
                           [NSString stringWithFormat:@"username = '%@' AND filename = '%@'",
                            node.username,node.filename]];
    if (results.count != 1) {
        return NO;
    }
    PhotoViewerProvenance *pvp = results.firstObject;
    
    RLMRealm *realm = [RLMRealm defaultRealm];
    [realm beginWriteTransaction];
    [pvp setValue:value forKey:property];
    [realm commitWriteTransaction];
    
    return YES;
}

// checks the photo's metaData to get orientation value
/**************************************************************************
 *   6    (6 is top of ipad)
 * 1   3
 *   8
 **************************************************************************/
- (NSNumber*)getOrientationFromDict:(NSDictionary*)info {
    NSMutableDictionary *dict = [info valueForKey:@"UIImagePickerControllerMediaMetadata"];
    NSNumber *orientation = [dict valueForKey:@"Orientation"];
    return orientation;
}

// if orientation value == 1 || 3, image is landscape, rotate 90 then place
- (UIImage*)rotateImage:(UIImage*)image basedOnOrientationValue:(NSNumber*)value {
    UIImage *imageToSave;
    NSNumber *three = [NSNumber numberWithInt:3];
    NSNumber *one = [NSNumber numberWithInt:1];
    
    if ([value isEqualToNumber:three]) {
        imageToSave = [self rotateUIImage:image clockwise:NO];
    }
    else if ([value isEqualToNumber:one]){
        imageToSave = [self rotateUIImage:image clockwise:YES];
    }
    if (imageToSave) {
        return imageToSave;
    }
    return image; // not landscape
}

/***************************************************************************************************
 * rotateUIImage
 *
 * code source: http://stackoverflow.com/questions/14857728/how-to-rotate-uiimage
 **************************************************************************************************/
- (UIImage*)rotateUIImage:(UIImage*)sourceImage clockwise:(BOOL)clockwise {
    CGSize size = sourceImage.size;
    UIGraphicsBeginImageContext(CGSizeMake(size.height, size.width));
    [[UIImage imageWithCGImage:[sourceImage CGImage] scale:1.0
            orientation:clockwise ? UIImageOrientationRight : UIImageOrientationLeft]
            drawInRect:CGRectMake(0,0,size.height ,size.width)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}

/***************************************************************************************************
 * getDateAndTimeForFileName
 *
 * Returns a string with a date and time formatted for use in a file name;
 * the format is:
 **************************************************************************************************/
- (NSString*)getDateAndTimeForFileName {
    NSString *theDateAndTime = [NSString stringWithFormat:@"%@_%@",
                                [self getPresentDate],[self getPresentTime]];
    theDateAndTime = [theDateAndTime stringByReplacingOccurrencesOfString:@":" withString:@""];
    return theDateAndTime;
}

/***************************************************************************************************
 * getPresentTime
 *
 * Return's the present time in format:HH:mm:ss
 **************************************************************************************************/
- (NSString*)getPresentTime {
    NSDate *date = [NSDate date];
    NSDateFormatter *timeFormatter = [[NSDateFormatter alloc] init];
    [timeFormatter setDateFormat:@"HH:mm:ss"];
    NSString *theTime = [timeFormatter stringFromDate:date];
    return theTime;
}

/***************************************************************************************************
 * getPresentDate
 *
 * Return's the present date in format:yyyy-MM-dd
 **************************************************************************************************/
- (NSString*)getPresentDate {
    NSDate *date = [NSDate date];
    NSDateFormatter *timeFormatter = [[NSDateFormatter alloc] init];
    [timeFormatter setDateFormat:@"yyyy-MM-dd"];
    NSString *theDate = [timeFormatter stringFromDate:date];
    return theDate;
}



-(void)keyboardWillHide:(NSNotification *)notification{
    
}

/***************************************************************************************************
 * addCaptionToImage
 *
 * This method adds a caption to the photo saved. The caption is captured by
 * the popup after the photo is taken. The caption is added to the bottom right
 * portion of the image. Behind the text is a black bar so that the white type
 * can be visible regardless of the background in the photo.
 **************************************************************************************************/
- (UIImage*)addCaptionToImage:(UIImage*)imageToSave capturedString:(NSString*)caption {
    
    UIColor *textColor = [UIColor colorWithWhite:1.0 alpha:1.0];
    UIFont *font = [UIFont systemFontOfSize:50];
    CGFloat paddingX = 20.f;
    CGFloat paddingY = 20.f;
    
    // Compute rect to draw the text inside
    CGSize imageSize = imageToSave.size;
    
    NSDictionary *attr = @{NSForegroundColorAttributeName: textColor, NSFontAttributeName: font};
    CGSize textSize = [caption sizeWithAttributes:attr];
    CGRect textRect = CGRectMake(imageSize.width - textSize.width - paddingX,
                                 imageSize.height - textSize.height - paddingY,
                                 textSize.width, textSize.height);
    
    // Create the image
    UIGraphicsBeginImageContext(imageSize);
    [imageToSave drawInRect:CGRectMake(0, 0, imageSize.width, imageSize.height)];
    
    // Create the background for caption
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetRGBFillColor(context, 0.0, 0.0, 0.0, 0.6);
    // Add Filled Rectangle,
    CGContextFillRect(context, CGRectMake(imageSize.width - textSize.width - paddingX-10,
                                          imageSize.height - textSize.height - paddingY-10,
                                          (textSize.width+20),
                                          (textSize.height+20)));
    
    [caption drawInRect:CGRectIntegral(textRect) withAttributes:attr];
    
    UIImage *resultImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return resultImage;
}

/***************************************************************************************************
 * popupToAddLabelTitle
 *
 * This method will pop up an alert view to retrieve the information from the
 * user to use as a label for this photo that can be searched online.
 **************************************************************************************************/
- (void)popupToAddLabelTitle {
    if (metadataView.hidden == YES) {
        UIAlertController *getLabel = [UIAlertController
                           alertControllerWithTitle:@"Give this image a title to search for."
                                            message:nil
                                     preferredStyle:UIAlertControllerStyleAlert];
        
        [getLabel addTextFieldWithConfigurationHandler:^(UITextField *textField) {
            textField.placeholder = @"Add title here";
        }];
        
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel"
                                                               style:UIAlertActionStyleCancel
                                                             handler:nil];
        [getLabel addAction:cancelAction];
        
        // ADD LABEL
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Ok"
                                                           style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction *action)
           {
               UITextField *temp = getLabel.textFields.firstObject;
               ImageNode *node = [self returnImageNodeOfDisplayedImage];
               
               if (!node) {
                   NSLog(@"[PhotoViewer: popupToAddLabelTitle]: could not find node");
               }
               else{
                   node.title = temp.text;
                   //[self saveToDatabase:node];
                   [self updateDatabase:node withValue:node.title forProperty:@"title"];
                   [self updatePhotoTitleWithString:node.title];
                   [self updateImageTitleSize];
                   [metadataView reloadMetadata];
                   [self.dropDownMenu infoButtonVisibility];
               }
           }];
        [getLabel addAction:okAction];
        [self presentViewController:getLabel animated:YES completion:nil];
    }
}

// logs error and gives user a popup of error so they know action couldn't take place
- (void)popupIndexOutOfBoundsErrorFrom:(NSString*)method {
    NSLog(@"Error %@: Index out of bounds]",method);
    UIAlertController *error = [UIAlertController alertControllerWithTitle:@"Error"
            message:[NSString stringWithFormat:@"Index out of bounds for: %@",method]
            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Ok"
                                                 style:UIAlertActionStyleCancel
                                               handler:nil];
    [error addAction:ok];
    [self presentViewController:error animated:YES completion:nil];
}

/***************************************************************************************************
 * addGestures
 *
 * This method enables user interaction and tap gestures to the photo label and background view.
 **************************************************************************************************/
- (void)addGestures {
    UITapGestureRecognizer *tappedLabelGesture = [[UITapGestureRecognizer alloc]
                        initWithTarget:self action:@selector(popupToAddLabelTitle)];
    tappedLabelGesture.numberOfTapsRequired = 1;
    [self.photoTitle addGestureRecognizer:tappedLabelGesture];
    
    [backgroundView setUserInteractionEnabled:YES];
    
    UITapGestureRecognizer *tappedBackground = [[UITapGestureRecognizer alloc]
                                                  initWithTarget:self
                                                  action:@selector(toggleButtons)];
    tappedBackground.numberOfTouchesRequired = 1;
    [backgroundView addGestureRecognizer:tappedBackground];
}

// segue back to Plant Community when hitting cancel button
- (void)hitBackButton:(id)sender {
    [SystemSounds playTockSound];
    if (metadataView.hidden == YES) {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

/***************************************************************************************************
 * configureBackButton
 *
 * This method creates a button on top of where the navigation bar would be
 * so that the user can cancel and segue back to the Plant Community.
 **************************************************************************************************/
-(void)configureBackButton {
    backButton = [UIButton buttonWithType:UIButtonTypeSystem];
    backButton.titleLabel.text = @"BackButton";
    [backButton setFrame:CGRectMake(25, 41, 35, 35)];
    [backButton setBackgroundImage:[UIImage imageNamed:@"leftArrowPhotoViewer"] forState:0];
    backButton.titleLabel.font = [UIFont systemFontOfSize:18];
    backButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [backButton setShowsTouchWhenHighlighted:YES];
    backButton.layer.shadowColor = [UIColor blackColor].CGColor;
    backButton.layer.shadowOffset = CGSizeMake(2, 5);
    backButton.layer.shadowOpacity = 0.75;
    [backButton addTarget:self
                   action:@selector(hitBackButton:)
         forControlEvents:UIControlEventTouchUpInside];
    [backButton setHidden:NO];
    [self.view addSubview:backButton];
}

/******************************************************************************
 * addGenericImageDescription
 *
 * This method creates a generic description saved as a string and returns it.
 ******************************************************************************/
- (NSString*)addGenericImageDescription {
    NSString *genericLabel = @"Click Title To Add";
    return genericLabel;
}

- (void)searchByImageTitle:(id)sender {
    if (metadataView.hidden == YES) {
        [self.navigationController.navigationBar setHidden:NO];
        [self performSegueWithIdentifier:@"photoToSearchSegue" sender:self];
    }
}

- (int)returnImageCount {
    return (int)viewManager.imageContainer.count;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:YES];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
    if ([self isMovingFromParentViewController]) {
        // We were removed from the navigation controller's view controller stack
        // thus, we can infer that the back button was pressed
        [metadataView setHidden:YES];
    }
    if (locationManager != nil) {
        [locationManager stopMonitoringSignificantLocationChanges];
    }
}

/*
 * CSS490: Code added to display confirmation when picture is uploaded.
 */
- (void)uploadTheseImages:(ImageNode*)node imageCount:(int)count {
    if (self.userInfo.approvedUser) {
        uploadForm = [DirectUploadForm sharedUploadFormWithUserInfo:self.userInfo];
        [uploadForm postImageToServer:node numberPosting:count];
        
        /*
         * Checks to see if the images were uploaded to the server properly
         */
        if (uploadForm.uploadPictureSuccess)
        {
            //Creates the dialogue
            UIAlertController *alert = [UIAlertController
                                        alertControllerWithTitle:@"Success!"
                                        message:@"The pictures were uploaded to the server!"
                                        preferredStyle:UIAlertControllerStyleAlert];
            //Creates the ok action
            UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
            //Adds the ok action
            [alert addAction:ok];
            //displays the dialoge
            [self presentViewController:alert animated:YES completion:nil];
        }
        else // images failed to load to the server
        {
            //create dialoge
            UIAlertController *alert = [UIAlertController
                                        alertControllerWithTitle:@"FAILURE"
                                        message:@"Pictures failed to upload to the server"
                                        preferredStyle:UIAlertControllerStyleAlert];
            //Create the ok action
            UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
            //Add the ok action
            [alert addAction:ok];
            //display dialoge
            [self presentViewController:alert animated:YES completion:nil];
        }
    }
    else
        [self popupNoUploadApproval];
}

- (void)uploadImage:(id)sender {
    if (metadataView.hidden == YES) {
        if (self.userInfo.approvedUser) {
            [self popupConfirmUpload];
        }
        else{
            [self popupNoUploadApproval];
        }
    }
}

- (void)popupConfirmUpload {
    UIAlertController *upload = [UIAlertController
                                    alertControllerWithTitle:@"Upload this image?"
                                    message:nil
                                    preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel"
                                                       style:UIAlertActionStyleCancel
                                                     handler:nil];
    [upload addAction:cancel];
    UIAlertAction *confirm = [UIAlertAction actionWithTitle:@"Yes"
                                                      style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction *action) {
                uploadForm = [[DirectUploadForm alloc]initWithUserInfo:self.userInfo];
                ImageNode *node = [self returnImageNodeOfDisplayedImage];
                [uploadForm postImageToServer:node numberPosting:1];
            }];
    [upload addAction:confirm];
    [self presentViewController:upload animated:YES completion:nil];
}

- (void)popupNoUploadApproval {
    UIAlertController *noUploads = [UIAlertController
                                    alertControllerWithTitle:@"You must login with a valid username"
                                    " and password to upload files."
                                    message:nil
                                    preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Ok"
                                                       style:UIAlertActionStyleCancel
                                                     handler:nil];
    [noUploads addAction:okAction];
    [self presentViewController:noUploads animated:YES completion:nil];
}

/***************************************************************************************************
 * startGPSLocationServices (saving a photo taken)
 *
 * Instantiate locationManager if needed, set properties, and start updating
 * location. Also instantiate geocoder if needed.
 **************************************************************************************************/
- (void)startGPSLocationServices {
    if (locationManager == nil) {
        locationManager = [[CLLocationManager alloc] init];
        locationManager.delegate = self;
        locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        locationManager.distanceFilter = kCLDistanceFilterNone;
        if ([locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
            [locationManager requestWhenInUseAuthorization];
        }
        [locationManager startUpdatingLocation];
    }
    if (geocoder == nil) {
        geocoder = [[CLGeocoder alloc] init];
    }
    [locationManager startMonitoringSignificantLocationChanges];
}

-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    NSLog(@"Error [locationManager]: %@",error);
    NSLog(@"Failed to get GPS location.");
    
    UIAlertController *locationError = [UIAlertController alertControllerWithTitle:@"Error"
                                    message:@"Failed to Get Your Location"
                                    preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Ok"
                                                 style:UIAlertActionStyleCancel
                                               handler:nil];
    [locationError addAction:ok];
    [self presentViewController:locationError animated:YES completion:nil];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {

    CLLocation *currentLocation = [locations lastObject];
    if (currentLocation != nil) {

        latitude = [NSString stringWithFormat:@"%.8f",
                              currentLocation.coordinate.latitude];
        longitude = [NSString stringWithFormat:@"%.8f",
                               currentLocation.coordinate.longitude];
        NSLog(@"latitude: %.8f",currentLocation.coordinate.latitude);
        NSLog(@"longitude: %.8f",currentLocation.coordinate.longitude);
        [locationManager stopUpdatingLocation];
    }
    
    // used to get an address (deprectated)
    /*
    [geocoder reverseGeocodeLocation:currentLocation
                   completionHandler:^(NSArray *placemarks, NSError *error) {
        if (error == nil && [placemarks count] > 0) {
            placemark = [placemarks lastObject];
            
            self.address = [NSString stringWithFormat:@"%@ %@, %@ %@, %@",
                                 placemark.subThoroughfare, placemark.thoroughfare,
                                 placemark.postalCode, placemark.locality,
                                 placemark.administrativeArea];
        }
        else{
            NSLog(@"%@",error.debugDescription);
        }
    }];
     */
}

- (void)showInProgressAlert {
    UIAlertController *alert = [UIAlertController
                                alertControllerWithTitle:@"Build in Progress"
                                message:nil
                                preferredStyle:UIAlertControllerStyleAlert];
    [self presentViewController:alert animated:YES completion:nil];
    
    double delayInSeconds = 1.0;
    dispatch_time_t endTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(endTime, dispatch_get_main_queue(), ^{
        [alert dismissViewControllerAnimated:YES completion:nil];
    });
}


// Standard Bundle Methods:
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation

// segues to a web view which loads search info on the photo's label
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"photoToSearchSegue"]) {
        SearchView *controller = (SearchView *)segue.destinationViewController;
        controller.targetSearch = self.photoTitle.text;
    }
}

@end
