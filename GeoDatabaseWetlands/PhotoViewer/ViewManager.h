//
//  ViewManager.h
//  GeoDatabaseWetlands
//
//  Created by Jonathan Mason on 8/12/15.
//  Copyright (c) 2015 Jonathan Mason. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ImageNode.h"
#import "PhotoViewer.h"
#import "UIViewControllerTracker.h"
#import "UserInfo.h"
#import "ImageArray.h"
@class PhotoMetadataView;

@interface ViewManager : NSObject

@property (strong, nonatomic) PhotoMetadataView *metadataView;
@property (strong, nonatomic) UIImageView *fullView;
@property (strong, nonatomic) UIScrollView *scrollView;
@property (strong, nonatomic) ImageArray *imageContainer;
@property (strong, nonatomic) NSMutableArray *imagesSelected;

- (id)initWithUser:(UserInfo*)user;
- (void)applyImageGestures:(UIImageView*)image;
- (void)highlightAllImages;
- (void)resetImageBorders;
- (void)updateScrollContentSize;
- (void)refreshScreenAfterSort;
- (void)updateArrayByAddingImageNode:(ImageNode*)node;
- (void)loadAllImages;
- (void)loadSpecificCommunityImages:(NSString*)community;
- (void)switchToFullScreenWithImage:(UIImage*)image;
- (void)uploadMultipleImages;
- (void)deleteSelectedImages;
- (void)emptyUploadImagesArray;
- (void)buildFourUpViewWithImages:(NSMutableArray*)imagesShown;

@end
