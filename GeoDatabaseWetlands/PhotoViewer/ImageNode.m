//
//  ImageNode.m
//  GeoDatabaseWetlands
//
//  Created by Jonathan Mason on 8/19/14.
//  Copyright (c) 2014 Jonathan Mason. All rights reserved.
//
/******************************************************************************
 * ImageNode class inherits from NSObject, and is used to hold the reference
 * to all photos taken, and to their metadata.
 *****************************************************************************/

#import "ImageNode.h"
#define imageKey @"image"
#define usernameKey @"username"
#define fileDirKey @"fileDir"
#define filenameKey @"filename"
#define nodeNameKey @"nodeName"
#define communityKey @"community"
#define captionKey @"caption"
#define projectKey @"project"
#define dateKey @"date"
#define timeKey @"time"
#define titleKey @"title"
#define latitudeKey @"latitude"
#define longitudeKey @"longitude"
#define addressKey @"address"
#define notesKey @"notes"
#define imageMetaDictKey @"imageMetaDict"

@implementation ImageNode

- (id)init
{
    self = [super init];
    if (self) {
        _filename = @"";
        _username = @"";
        _community = @"";
        _caption = @"";
        _project = @"";
        _time = [self getPresentTime];
        _date = [self getPresentDate];
        _title = @"";
        _latitude = @"";
        _longitude = @"";
        _address = @"";
        _notes = @"";
    }
    return self;
}

- (NSString*)metaDataName:(NSString*)filename {
    NSString *metaFilename = [filename stringByDeletingPathExtension];
    return [metaFilename stringByAppendingString:@"Metadata"];
}

/******************************************************************************
 * getPresentTime
 *
 * Return's the present time in format:HH:mm:ss
 ******************************************************************************/
- (NSString*)getPresentTime {
    NSDate *date = [NSDate date];
    NSDateFormatter *timeFormatter = [[NSDateFormatter alloc] init];
    [timeFormatter setDateFormat:@"HH:mm:ss"];
    NSString *theTime = [timeFormatter stringFromDate:date];
    return theTime;
}

/******************************************************************************
 * getPresentDate
 *
 * Return's the present date in format:yyyy-MM-dd
 ******************************************************************************/
- (NSString*)getPresentDate {
    NSDate *date = [NSDate date];
    NSDateFormatter *timeFormatter = [[NSDateFormatter alloc] init];
    [timeFormatter setDateFormat:@"yyyy-MM-dd"];
    NSString *theDate = [timeFormatter stringFromDate:date];
    return theDate;
}

/******************************************************************************
 * addGenericImageTitle
 *
 * This method creates a generic Title saved as a string and returns it.
 ******************************************************************************/
- (NSString*)addGenericImageTitle {
    NSString *genericLabel = @"Click Title To Add";
    return genericLabel;
}

@end
