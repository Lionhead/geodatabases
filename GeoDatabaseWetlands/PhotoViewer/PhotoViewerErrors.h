//
//  PhotoViewerErrors.h
//  GeoDatabaseWetlands
//
//  Created by Jonathan Mason on 3/9/15.
//  Copyright (c) 2015 Jonathan Mason. All rights reserved.
//
//  This software is intended soley for the University of Washington Bothell.

@interface PhotoViewerErrors : NSObject



@end

NSString *PhotoViewerErrorDomain = @"com.UWB.PhotoViewer.ErrorDomain";


enum {
    PhotoViewerEmptyError,
    CouldNotDeleteFileError
};