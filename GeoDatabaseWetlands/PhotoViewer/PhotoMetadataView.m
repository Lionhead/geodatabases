//
//  PhotoMetadataView.m
//  GeoDatabaseWetlands
//
//  Created by Jonathan Mason on 7/30/15.
//  Copyright (c) 2015 Jonathan Mason. All rights reserved.
//

#import "PhotoMetadataView.h"
#import "ImageNode.h"
#import "UIViewControllerTracker.h"

// keys for ImageNode
#define imagePtrKey @"imagePtr"
#define userPtrKey @"userPtr"
#define fileDirPtrKey @"fileDirPtr"
#define filenamePtrKey @"filenamePtr"
#define nodeNamePtrKey @"nodeNamePtr"
#define usernamePtrKey @"usernamePtr"
#define projectPtrKey @"projectPtr"
#define datePtrKey @"datePtr"
#define timePtrKey @"timePtr"
#define descriptionPtrKey @"descriptionPtr"
#define latitudePtrKey @"latitudePtr"
#define longitudePtrKey @"longitudePtr"
#define addressPtrKey @"addressPtr"
#define notesPtrKey @"notesPtr"
#define genericDescription @"Click Title To Add"
#define latitudeKey @"Latitude"
#define longitudeKey @"Longitude"

@implementation PhotoMetadataView

UIViewControllerTracker *vcTracker;
PhotoViewer *photoViewerPtr;

// metadata menu features
UILabel *metadata;
UILabel *takenByLabel;
UILabel *projectLabel;
UILabel *titledLabel;
UILabel *dateLabel;
UILabel *timeLabel;
UILabel *latitudeLabel;
UILabel *longitudeLabel;
UILabel *notesLabel;
UITextView *notes;
UIButton *saveButton;
UIButton *addGPSButton;
NSString *latitudeDecDegrees;
NSString *latitudeDecDegreesMin;
NSString *latitudeDecDegreesMinSec;
NSString *longitudeDecDegrees;
NSString *longitudeDecDegreesMin;
NSString *longitudeDecDegreesMinSec;

// converts decimal degrees to decimal mins/secs
NSString* (^convertDecToDecMin)(NSString* decimal);
NSString* (^convertDecToDecMinSec)(NSString* decimal);
NSString* (^convertDecMinSecToDec)(NSString* decimalMinSec);

- (id)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        vcTracker = [UIViewControllerTracker sharedVC];
        [self setBackgroundColor:[UIColor blackColor]];
        UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.bounds
                                                            cornerRadius:8];
        CAShapeLayer *maskLayer = [CAShapeLayer layer];
        maskLayer.frame = self.bounds;
        maskLayer.path = maskPath.CGPath;
        self.layer.opacity = .8f;
        [self.layer setMask:maskLayer];
        
        [self drawRect:self.frame];
        [self createUnitConvertingBlocks];
        [self configureMetadataLabels];
        [self reloadMetadata];
        [self setHidden:YES];
    }
    return self;
}

- (void)drawRect:(CGRect)rect {
    // Drawing code
    
    notes = [[UITextView alloc]initWithFrame:CGRectMake(15, 335, 370, 185)];
    notes.restorationIdentifier = notesPtrKey;
    [notes setTextColor:[UIColor whiteColor]];
    [notes setFont:[UIFont fontWithName:@"HelveticaNeue" size:22]];
    notes.backgroundColor = [UIColor darkGrayColor];
    notes.layer.opacity = .8f;
    
    UIBezierPath *notesMaskPath = [UIBezierPath bezierPathWithRoundedRect:notes.bounds
                                                             cornerRadius:8];
    CAShapeLayer *notesMaskLayer = [CAShapeLayer layer];
    notesMaskLayer.frame = notes.bounds;
    notesMaskLayer.path = notesMaskPath.CGPath;
    [notes.layer setMask:notesMaskLayer];
    [self addSubview:notes];
}

- (void)configureMetadataLabels {
    metadata = [[UILabel alloc]initWithFrame:CGRectMake(15, 5, 100, 30)];
    [metadata setText:@"Metadata"];
    [metadata setTextColor:[UIColor lightGrayColor]];
    [metadata setFont:[UIFont fontWithName:@"HelveticaNeue" size:22]];
    [self addSubview:metadata];
    
    takenByLabel = [[UILabel alloc]initWithFrame:CGRectMake(15, 65, 370, 30)];
    takenByLabel.restorationIdentifier = usernamePtrKey;
    [takenByLabel setTextColor:[UIColor lightGrayColor]];
    [takenByLabel setFont:[UIFont fontWithName:@"HelveticaNeue" size:22]];
    [self addSubview:takenByLabel];
    
    projectLabel = [[UILabel alloc]initWithFrame:CGRectMake(15, 95, 370, 30)];
    projectLabel.restorationIdentifier = projectPtrKey;
    [projectLabel setTextColor:[UIColor lightGrayColor]];
    [projectLabel setFont:[UIFont fontWithName:@"HelveticaNeue" size:22]];
    [self addSubview:projectLabel];
    
    titledLabel = [[UILabel alloc]initWithFrame:CGRectMake(15, 125, 370, 30)];
    titledLabel.restorationIdentifier = descriptionPtrKey;
    [titledLabel setTextColor:[UIColor lightGrayColor]];
    [titledLabel setFont:[UIFont fontWithName:@"HelveticaNeue" size:22]];
    [self addSubview:titledLabel];
    
    dateLabel = [[UILabel alloc]initWithFrame:CGRectMake(15, 155, 370, 30)];
    dateLabel.restorationIdentifier = datePtrKey;
    [dateLabel setTextColor:[UIColor lightGrayColor]];
    [dateLabel setFont:[UIFont fontWithName:@"HelveticaNeue" size:22]];
    [self addSubview:dateLabel];
    
    timeLabel = [[UILabel alloc]initWithFrame:CGRectMake(15, 185, 370, 30)];
    timeLabel.restorationIdentifier = timePtrKey;
    [timeLabel setTextColor:[UIColor lightGrayColor]];
    [timeLabel setFont:[UIFont fontWithName:@"HelveticaNeue" size:22]];
    [self addSubview:timeLabel];
    
    latitudeLabel = [[UILabel alloc]initWithFrame:CGRectMake(15, 215, 370, 30)];
    latitudeLabel.restorationIdentifier = latitudePtrKey;
    [latitudeLabel setTextColor:[UIColor lightGrayColor]];
    [latitudeLabel setFont:[UIFont fontWithName:@"HelveticaNeue" size:22]];
    [self addSubview:latitudeLabel];
    
    longitudeLabel = [[UILabel alloc]initWithFrame:CGRectMake(15, 245, 370, 30)];
    longitudeLabel.restorationIdentifier = longitudePtrKey;
    [longitudeLabel setTextColor:[UIColor lightGrayColor]];
    [longitudeLabel setFont:[UIFont fontWithName:@"HelveticaNeue" size:22]];
    [self addSubview:longitudeLabel];
    
    notesLabel = [[UILabel alloc]initWithFrame:CGRectMake(15, 305, 370, 30)];
    [notesLabel setText:@"additional notes:"];
    [notesLabel setTextColor:[UIColor lightGrayColor]];
    [notesLabel setFont:[UIFont fontWithName:@"HelveticaNeue" size:22]];
    [self addSubview:notesLabel];
    
    saveButton = [[UIButton alloc]initWithFrame:CGRectMake(335, 5, 50, 30)];
    [saveButton setTitle:@"Save" forState:UIControlStateNormal];
    [saveButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [saveButton.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue" size:22]];
    [saveButton.titleLabel setTextAlignment:NSTextAlignmentRight];
    [saveButton setShowsTouchWhenHighlighted:YES];
    [saveButton addTarget:self action:@selector(hitSaveButton:)
         forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:saveButton];
    
    addGPSButton = [[UIButton alloc]initWithFrame:CGRectMake(75, 275, 250, 30)];
    [addGPSButton setTitle:@"Manually Enter GPS" forState:UIControlStateNormal];
    [addGPSButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [addGPSButton.titleLabel setFont:[UIFont fontWithName:@"HelveticeNeue" size:22]];
    [addGPSButton.titleLabel setTextAlignment:NSTextAlignmentCenter];
    [addGPSButton setShowsTouchWhenHighlighted:YES];
    [addGPSButton addTarget:self
                     action:@selector(manuallyAddGPS:)
           forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:addGPSButton];
}

- (void)hitSaveButton:(id)sender {
    ImageNode *node = [photoViewerPtr returnImageNodeOfDisplayedImage];
    if (notes.text) {
        node.notes = notes.text;
    }
    if (latitudeDecDegrees) {
        node.latitude = latitudeDecDegrees;
        node.longitude = longitudeDecDegrees;
    }
    [photoViewerPtr updateDatabase:node withValue:node.notes forProperty:@"notes"];
    [photoViewerPtr updateDatabase:node withValue:node.latitude forProperty:@"latitude"];
    [photoViewerPtr updateDatabase:node withValue:node.longitude forProperty:@"longitude"];
    
    [self setHidden:YES];
}

- (void)showMetadata {
    [self reloadMetadata];
    [self setHidden:NO];
}

- (void)reloadMetadata {
    
    ImageNode *node = [photoViewerPtr returnImageNodeOfDisplayedImage];
    if (node) {
        [takenByLabel setText:[NSString stringWithFormat:@"taken by: %@",node.username]];
        [projectLabel setText:[NSString stringWithFormat:@"project: %@",node.project]];
        NSString *title = node.title;
        if ([title isEqualToString:genericDescription]) {
            title = @"";
        }
        [titledLabel setText:[NSString stringWithFormat:@"titled: %@",title]];
        [dateLabel setText:[NSString stringWithFormat:@"date: %@",node.date]];
        [timeLabel setText:[NSString stringWithFormat:@"time: %@",node.time]];
        NSString *latitude = node.latitude;
        if (!latitude) {
            latitude = @"";
        }
        [latitudeLabel setText:[NSString stringWithFormat:@"latitude: %@",latitude]];
        NSString *longitude = node.longitude;
        if (!longitude) {
            longitude = @"";
        }
        [longitudeLabel setText:[NSString stringWithFormat:@"longitude: %@",longitude]];
        if (node.notes) {
            [notes setText:[NSString stringWithFormat:@"%@",node.notes]];
        }
    }
}

- (void)manuallyAddGPS:(id)sender {
    [self getGPSInfo:latitudeKey];
}

- (void)getGPSInfo:(NSString*)unit {

    UIAlertController *getGPS = [UIAlertController
                   alertControllerWithTitle:[NSString stringWithFormat:@"Enter %@",unit]
                                    message:nil
                             preferredStyle:UIAlertControllerStyleAlert];
    [getGPS addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Enter Degrees";
        textField.keyboardType = UIKeyboardTypeDecimalPad;
    }];
    [getGPS addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Enter Minutes";
        textField.keyboardType = UIKeyboardTypeDecimalPad;
    }];
    [getGPS addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Enter Seconds";
        textField.keyboardType = UIKeyboardTypeDecimalPad;
    }];
    if ([unit isEqualToString:latitudeKey]) {
        UIAlertAction *switchToDec = [UIAlertAction actionWithTitle:@"Switch to Decimal Degree"
                                                              style:UIAlertActionStyleDefault
                                                            handler:^(UIAlertAction *action)
                                      {[self getDecimalDegree:latitudeKey];}];
        [getGPS addAction:switchToDec];
    }
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel"
                                                     style:UIAlertActionStyleCancel
                                                   handler:nil];
    [getGPS addAction:cancel];
    UIAlertAction *done = [UIAlertAction actionWithTitle:@"Done"
                                                   style:UIAlertActionStyleDefault
                                                 handler:^(UIAlertAction *action) {
     UITextField *textField0 = getGPS.textFields[0];
     UITextField *textField1 = getGPS.textFields[1];
     UITextField *textField2 = getGPS.textFields[2];
     NSString *deg = textField0.text;
     NSString *min = textField1.text;
     NSString *sec = textField2.text;
     NSString *strToConvert = [NSString stringWithFormat:@"%@ %@ %@",deg,min,sec];
        if ([unit isEqualToString:latitudeKey]) {
         latitudeDecDegrees = convertDecMinSecToDec(strToConvert);
         latitudeDecDegreesMin = convertDecToDecMin(latitudeDecDegrees);
         latitudeDecDegreesMinSec = [NSString stringWithFormat:@"%@ %@' %@\"",deg,min,sec];
         [latitudeLabel setText:[NSString stringWithFormat:@"latitude: %@",latitudeDecDegrees]];
            [self getGPSInfo:longitudeKey];
        }
        else{
            longitudeDecDegrees = convertDecMinSecToDec(strToConvert);
            longitudeDecDegreesMin = convertDecToDecMin(latitudeDecDegrees);
            longitudeDecDegreesMinSec = [NSString stringWithFormat:@"%@ %@' %@\"",deg,min,sec];
            [longitudeLabel setText:[NSString stringWithFormat:@"latitude: %@",longitudeDecDegrees]];
        }
 }];
    [getGPS addAction:done];
    [vcTracker.currentViewController presentViewController:getGPS animated:YES completion:nil];
}

- (void)getDecimalDegree:(NSString*)unit {

    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil
                        message:[NSString stringWithFormat:@"Enter %@ in decimal format",unit]
                preferredStyle:UIAlertControllerStyleAlert];
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Decimal numbers only.";
        textField.keyboardType = UIKeyboardTypeDecimalPad;
    }];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel"
                                                     style:UIAlertActionStyleCancel
                                                   handler:nil];
    [alert addAction:cancel];
    UIAlertAction *done = [UIAlertAction actionWithTitle:@"Done"
                                                   style:UIAlertActionStyleDefault
                                                 handler:^(UIAlertAction *action) {
    UITextField *textField = alert.textFields.firstObject;

     if ([unit isEqualToString:latitudeKey]) {
         latitudeDecDegrees = textField.text;
         latitudeDecDegreesMin = convertDecToDecMin(latitudeDecDegrees);
         latitudeDecDegreesMinSec = convertDecToDecMinSec(latitudeDecDegrees);
         [latitudeLabel setText:[NSString stringWithFormat:@"latitude: %@",latitudeDecDegrees]];
         [self getDecimalDegree:longitudeKey];
     }
     else{
         longitudeDecDegrees = textField.text;
         longitudeDecDegreesMin = convertDecToDecMin(longitudeDecDegrees);
         longitudeDecDegreesMinSec = convertDecToDecMinSec(longitudeDecDegrees);
         [longitudeLabel setText:[NSString stringWithFormat:@"longitude: %@",longitudeDecDegrees]];
     }
 }];
    [alert addAction:done];
    [vcTracker.currentViewController presentViewController:alert animated:YES completion:nil];
}

// converts decimal degrees to decimal mins/secs and back
- (void)createUnitConvertingBlocks {
    convertDecToDecMin = ^NSString*(NSString* decimal){
        double deg = (int)[decimal doubleValue];
        double min = (int)(([decimal doubleValue] - deg) * 60);
        NSString *result = [NSString stringWithFormat:@"%.0f %.0f'",deg,min];
        return result;
    };
    convertDecToDecMinSec = ^NSString*(NSString* decimal){
        double deg = (int)[decimal doubleValue];
        double min = (int)(([decimal doubleValue] - deg) * 60);
        double sec = ([decimal doubleValue] - deg - min/60)*3600;
        NSString *result = [NSString stringWithFormat:@"%.0f %.0f' %.6f\"",deg,min,sec];
        return result;
    };
    convertDecMinSecToDec = ^NSString*(NSString* decimalMinSec){
        NSArray *set = [decimalMinSec componentsSeparatedByString:@" "];
        double dec = [set[0] doubleValue];
        double min = [set[1] doubleValue];
        double sec = [set[2] doubleValue];
        double decDegrees = (dec + min/60 + sec/3600);
        NSString *result = [NSString stringWithFormat:@"%.6f",decDegrees];
        return result;
    };
}

@end
