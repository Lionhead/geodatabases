//
//  PhotoViewerMenu.h
//  GeoDatabaseWetlands
//
//  Created by Jonathan Mason on 6/15/15.
//  Copyright (c) 2015 Jonathan Mason. All rights reserved.
//
@class PhotoViewer;
@class ViewManager;
#import <Foundation/Foundation.h>

@interface PhotoViewerMenu : NSObject

// See comment at top of file for a complete description
@property (strong, nonatomic) UIView *viewBox;
@property (strong, nonatomic) ViewManager *viewManager;

+(id)sharedPhotoViewerMenu:(PhotoViewer*)photoViewer;
-(id)initWithPhotoViewer:(PhotoViewer*)photoViewer;
- (void)closeMenu;
- (void)infoButtonVisibility;
- (void)showFullScreenButtonsWithInfoButton:(BOOL)show;
- (void)showMultipleImageButtons;
- (BOOL)returnStartButtonIsHidden;

@end
