//
//  PhotoViewer.h
//  GeoDatabaseWetlands
//
//  Created by Jonathan Mason on 5/12/14.
//  Copyright (c) 2014 Jonathan Mason. All rights reserved.
//
//  This software is intended soley for the University of Washington Bothell.

#import "ImageNode.h"
#import "UserLogin.h"
#import "UserInfo.h"
#import "PlantCommunity.h"
#import "PlantFormManager.h"
#import "PhotoViewerMenu.h"
#import "UIViewControllerTracker.h"
#import "ViewManager.h"
#import <UIKit/UIKit.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import <CoreLocation/CoreLocation.h>

// See comment at top of file for a complete description
@interface PhotoViewer : UIViewController <UIImagePickerControllerDelegate,
                                            UINavigationControllerDelegate,
                                            CLLocationManagerDelegate>

@property UserLogin *login;
@property (strong, nonatomic) UIStoryboardSegue *sourceSegue; // how user got here
@property (strong, nonatomic) UserInfo *userInfo;
@property (strong, nonatomic) PhotoViewerMenu *dropDownMenu;
@property (strong, nonatomic) UIViewControllerTracker *vcTracker;
@property (strong, nonatomic) NSString *community;
@property (strong, nonatomic) UILabel *photoTitle;  // label above photo
@property (strong, nonatomic) IBOutlet UILabel *loggedInAs; // outlet to label showing who loggedin
@property (strong, nonatomic) IBOutlet UIButton *menuButton;
@property (strong, nonatomic) IBOutlet UIButton *cameraButton;
@property BOOL switchVariable;

- (IBAction)openCamera:(id)sender;
- (IBAction)hitDropDownMenuButton:(id)sender;

- (BOOL)isMetadataHidden;
- (BOOL)updateDatabase:(ImageNode*)node withValue:(NSString*)value forProperty:(NSString*)property;
- (void)searchByImageTitle:(id)sender;
- (void)deleteImage:(id)sender;
- (void)hitBackButton:(id)sender;
- (void)uploadTheseImages:(ImageNode*)node imageCount:(int)count;
- (void)uploadImage:(id)sender;
- (void)imageMetadataButton:(id)sender;
- (ImageNode*)returnImageNodeOfDisplayedImage;
- (void)popupToAddLabelTitle;
- (int)returnImageCount;
- (void)updatePhotoTitleWithString:(NSString*)string;
- (void)toggleButtons;
- (void)updateImageTitleSize;
- (void)setButtonVisibityOnReload;
- (void)popupNoUploadApproval;
- (void)hideButtons;
- (void)removeImageFromDatabase:(ImageNode*)node;


@end
