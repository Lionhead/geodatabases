//
//  SearchView.m
//  GeoDatabaseWetlands
//
//  Created by Jonathan Mason on 9/17/14.
//  Copyright (c) 2014 Jonathan Mason. All rights reserved.
//
/******************************************************************************
 * SearchView class inherits from UIViewController, and brings up a view
 * of the web using wikipedia. The first page brought up is automatically
 * the wikipedia page of what the user added as a description of the photo
 * that they took.
 *****************************************************************************/

#import "SearchView.h"
#import "SystemSounds.h"

@interface SearchView()

@end

@implementation SearchView

UIButton *backButton;

- (NSString*)formatSearchString: (NSString*)topic {
    
    NSString *result = [topic stringByReplacingOccurrencesOfString:@" " withString:@"_"];

    return result;
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setupBackButton];
    //[self prepNavigationControllerOnLoad];
    NSLog(@"%@",self.targetSearch);
    NSString *halfURL = @"http://en.wikipedia.org/wiki/";
    NSString *fullURL = [halfURL stringByAppendingString:self.targetSearch];
    NSString *formattedURL = [self formatSearchString:fullURL];
    NSLog(@"%@",formattedURL);
    NSURL *url = [NSURL URLWithString:formattedURL];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    if (requestObj != nil) {
        [self.viewWeb loadRequest:requestObj];
    }
    else{
        NSLog(@"Not found in wiki");
        fullURL = self.targetSearch;
        NSURL *url = [NSURL URLWithString:fullURL];
        NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
        [self.viewWeb loadRequest:requestObj];
    }
}

/***************************************************************************************************
 * prepNavigationControllerOnLoad
 *
 * This method sets the navigation controller properties for this class.
 **************************************************************************************************/
-(void)prepNavigationControllerOnLoad {
    [self.navigationController.navigationBar setHidden:NO];
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.view.backgroundColor = [UIColor blackColor];
}

/******************************************************************************
 * setupBackButton
 *
 * This method creates a button on top of where the navigation bar would be
 * so that the user can cancel and segue back to the PhotoViewer.
 ******************************************************************************/
-(void)setupBackButton {
    backButton = [UIButton buttonWithType:UIButtonTypeSystem];
    [backButton setTitle:@"Go Back" forState:UIControlStateNormal];
    [backButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [backButton.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue" size:20]];
    [backButton setFrame:CGRectMake(30, 30, 80, 35)];
    backButton.titleLabel.font = [UIFont systemFontOfSize:18];
    backButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [backButton setShowsTouchWhenHighlighted:YES];
    [backButton addTarget:self
                   action:@selector(hitBackButton:)
         forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backButton];
}

// segue back to PhotoViewer when hitting cancel button
- (IBAction)hitBackButton:(id)sender {
    [SystemSounds playTockSound];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillDisappear:(BOOL)animated {
    [self.navigationController.navigationBar setHidden:YES];
}

@end