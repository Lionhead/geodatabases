//
//  ImageNode.h
//  GeoDatabaseWetlands
//
//  Created by Jonathan Mason on 8/19/14.
//  Copyright (c) 2014 Jonathan Mason. All rights reserved.
//
//  This software is intended soley for the University of Washington Bothell.

#import <Foundation/Foundation.h>
#import "UserInfo.h"

// See comment at top of file for a complete description
@interface ImageNode : NSObject

// pointers to the image and its metadata
@property (strong, nonatomic) UIImage  *image;
@property (strong, nonatomic) UIImageView *imageView;
@property (strong, nonatomic) NSString *username;
@property (strong, nonatomic) NSString *filename;
@property (strong, nonatomic) NSString *community;
@property (strong, nonatomic) NSString *caption;
@property (strong, nonatomic) NSString *project;
@property (strong, nonatomic) NSString *date;
@property (strong, nonatomic) NSString *time;
@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) NSString *latitude;
@property (strong, nonatomic) NSString *longitude;
@property (strong, nonatomic) NSString *address;
@property (strong, nonatomic) NSString *notes;


@end



