//
//  ImageArray.m
//  GeoDatabaseWetlands
//
//  Created by Jonathan Mason on 10/13/15.
//  Copyright © 2015 Jonathan Mason. All rights reserved.
//

#import "ImageArray.h"

@interface ImageArray()

@property (strong, nonatomic) NSMutableArray *classArray;
//@property NSUInteger current;

@end

NSUInteger current;

@implementation ImageArray

- (id)init {
    if (self = [super init]) {
        current = 0;
        _classArray = [NSMutableArray new];
    }
    return self;
}

- (NSUInteger)returnCurrentIndex {
    return current;
}

- (void)setCurrentIndex:(NSUInteger)index {
    current = index;
}

#pragma mark NSArray

-(NSUInteger)count
{
    return [self.classArray count];
}

-(id)objectAtIndex:(NSUInteger)index
{
    return [self.classArray objectAtIndex:index];
}

#pragma mark NSMutableArray

- (void)insertObject:(id)anObject atIndex:(NSUInteger)index
{
    [self.classArray insertObject:anObject atIndex:index];
}

- (void)removeObjectAtIndex:(NSUInteger)index
{
    [self.classArray removeObjectAtIndex:index];
    
}

- (void)addObject:(id)anObject
{
    [self.classArray addObject:anObject];
    [self setCurrentIndex:self.count-1];
}

- (void)removeLastObject
{
    [self.classArray removeLastObject];
}

- (void)replaceObjectAtIndex:(NSUInteger)index withObject:(id)anObject
{
    [self.classArray replaceObjectAtIndex:index withObject:anObject];
}

- (void)removeAllObjects {
    [self.classArray removeAllObjects];
    [self setCurrentIndex:0];
}

- (id)current {
    return [self.classArray objectAtIndex:[self returnCurrentIndex]];
}

- (id)next {
    if (self.count == 0) {
        return nil;
    }
    else if (self.count == 1 || current == self.count-1) { // is or move to first object
        current = 0;
        return self[current];
    }
    
    return self.classArray[++current]; // increment to next object
}

- (id)previous {
    if (self.count == 0) {
        return nil;
    }
    else if (self.count == 1) {
        return self[current];
    }
    else if (current == 0 && self.count > 1){
        current = self.count-1;
        return self[current];
    }
    
    return self.classArray[--current];
}

@end
