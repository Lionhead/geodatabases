//
//  PhotoMetadataView.h
//  GeoDatabaseWetlands
//
//  Created by Jonathan Mason on 7/30/15.
//  Copyright (c) 2015 Jonathan Mason. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PhotoViewer.h"

@interface PhotoMetadataView : UIView

- (id)initWithFrame:(CGRect)frame;
- (void)showMetadata;
- (void)reloadMetadata;

@end
