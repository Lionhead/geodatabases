//
//  ImageArray.h
//  GeoDatabaseWetlands
//
//  Created by Jonathan Mason on 10/13/15.
//  Copyright © 2015 Jonathan Mason. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ImageArray : NSMutableArray

- (NSUInteger)returnCurrentIndex;
- (void)setCurrentIndex:(NSUInteger)current;
- (id)current;
- (id)next;
- (id)previous;

@end
