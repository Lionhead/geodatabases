//
//  ViewManager.m
//  GeoDatabaseWetlands
//
//  Created by Jonathan Mason on 8/12/15.
//  Copyright (c) 2015 Jonathan Mason. All rights reserved.
//


#import "ViewManager.h"
#import "PhotoMetadataView.h"
#import "DirectUploadForm.h"
#import "LocalFileManager.h"
#import "PhotoViewerProvenance.h"
#import "WaterCommunityProvenance.h"

#import <Realm/Realm.h>

#define viewBoxX 20
#define viewBoxY 37
#define fullScreenWidth 768
#define fullScreenHeight 1024
#define fourUpViewWidth 728
#define fourUpViewHeight 964
#define fourUpImageWidth 354
#define fourUpImageHeight 472
#define imageGutter 20
#define PlantCommunityFolder @"PlantCommunity"
#define WaterCommunityFolder @"WaterCommunity"
#define BirdCommunityFolder @"BirdCommunity"
#define CommunityCount 3

@interface ViewManager()

@property (strong, nonatomic) UserInfo *userInfo;

@end

@implementation ViewManager

PhotoViewer *photoViewerPtr;
UIViewControllerTracker *vcTracker;

- (id)initWithUser:(UserInfo*)user {
    if (self = [super init]) {
        self.userInfo = user;
        vcTracker = [UIViewControllerTracker sharedVC];
        photoViewerPtr = (PhotoViewer*)vcTracker.currentViewController;

        _fullView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, fullScreenWidth, fullScreenHeight)];
        [self.fullView setUserInteractionEnabled:YES];
        [self applyImageGestures:self.fullView];
        
        _scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(viewBoxX, viewBoxY, fourUpViewWidth, fourUpViewHeight)];
        [self.scrollView setUserInteractionEnabled:YES];
        [self.scrollView setScrollEnabled:YES];
        [self.scrollView setBounces:YES];
        [self.scrollView setBouncesZoom:YES];
        [self.scrollView setMultipleTouchEnabled:YES];
        [self.scrollView setShowsVerticalScrollIndicator:YES];
        [self.scrollView setDirectionalLockEnabled:YES];
        [self.scrollView setAutoresizesSubviews:YES];

        [self loadAllImages];
        
        if (self.imageContainer.count > 1) {
            // show up to 4 in one screen
            [self buildFourUpViewWithImages:self.imageContainer];
            [self.fullView setHidden:YES];
        }
        else if (self.imageContainer.count == 1){
            // show just 1 at a time
            ImageNode *node = self.imageContainer.firstObject;
            [self.fullView setImage:node.image];
        }
    }
    return self;
}


// called after taking a new photo
- (void)switchToFullScreenWithImage:(UIImage*)image {
    [self.fullView setHidden:NO];
    self.fullView.alpha = 0.0;
    [self.fullView setImage:image];
    [photoViewerPtr hideButtons];
    
    [UIView animateWithDuration:.375 animations:^{
        self.fullView.alpha = 1.0;
    } completion:^(BOOL finished) {
        [[self.scrollView subviews]makeObjectsPerformSelector:@selector(removeFromSuperview)];
    }];
}

- (void)buildFourUpViewWithImages:(NSMutableArray*)imagesShown {
    [[self.scrollView subviews]makeObjectsPerformSelector:@selector(removeFromSuperview)];
    [self resetScrollSize];
    double step = 0;
    
    for (int i = 0; i < imagesShown.count; i++) {
        int x = (((i % 2)*imageGutter)+((i % 2)*fourUpImageWidth));
        int y = (floor(step) * fourUpImageHeight) + (floor(step) * imageGutter);
        // increase size of scroller to fit if needed (
        if (step >= 2) {
            [self setScrollContentSize:step];
        }

        ImageNode *node = imagesShown[i];
        node.imageView = [[UIImageView alloc]
                            initWithFrame:CGRectMake(x, y, fourUpImageWidth, fourUpImageHeight)];
        [node.imageView setImage:node.image];
        [node.imageView setUserInteractionEnabled:YES];
        [self applyImageGestures:node.imageView];
        [self.scrollView addSubview:node.imageView];
        step += .5;
    }
    [photoViewerPtr hideButtons];
    [self.imageContainer setCurrentIndex:0];
}

- (void)updateScrollContentSize {
    [self setScrollContentSize:self.imageContainer.count / 2];
}

- (void)resetScrollSize {
    [self.scrollView setFrame:CGRectMake(viewBoxX, viewBoxY, fourUpViewWidth, fourUpViewHeight)];
    [self.scrollView setContentSize:CGSizeMake(fourUpViewWidth, fourUpViewHeight)];
}

- (void)setScrollContentSize:(double)num {
    if (num >= 2) {
        int height = ((floor(num)+1) * fourUpImageHeight) + (floor(num) * imageGutter);
        [self.scrollView setContentSize:CGSizeMake(fourUpViewWidth, height)];
    }
}

// called when using a longTapGesture on the image
// scaleImage will toggle between full screen and 4up view
- (void)scaleImage:(id)sender {
    UIGestureRecognizer *gesture = sender;
    UIImageView *image = (UIImageView*)gesture.view;
    
    if (gesture.state == UIGestureRecognizerStateBegan) {
        // make view 4up
        if (image.bounds.size.width == [UIScreen mainScreen].bounds.size.width){
            [UIView animateWithDuration:.1875 animations:^{
                [self.fullView setAlpha:0.0];
            } completion:^(BOOL finished) {
                [self.fullView setAlpha:1.0];
                [self.fullView setImage:nil];
                [self.fullView setHidden:YES];
                [self buildFourUpViewWithImages:self.imageContainer];
                [photoViewerPtr.photoTitle setHidden:YES];
                [self.scrollView setAlpha:0.0];
                [UIView animateWithDuration:0.25 animations:^{
                    [self.scrollView setAlpha:1.0];
                }];
            }];
        }
        // make view full screen
        else if (image.bounds.size.width == fourUpImageWidth){
            [self.fullView setFrame:CGRectMake((image.frame.origin.x + viewBoxX),
                                               (image.frame.origin.y + viewBoxY),
                                               image.frame.size.width,
                                               image.frame.size.height)];
            
            [self.fullView setImage:image.image];
            [self.fullView setHidden:NO];
            int idx = [self indexOfImage:image.image];
            if (idx >= 0) {
                [self.imageContainer setCurrentIndex:idx];
            }
            // update image title
            ImageNode *node = [self.imageContainer current];
            [photoViewerPtr updatePhotoTitleWithString:node.title];
            [photoViewerPtr updateImageTitleSize];
            [self.metadataView reloadMetadata];
            
            [UIView animateWithDuration:0.5 animations:^{
                [self.fullView setFrame:CGRectMake(0, 0, fullScreenWidth, fullScreenHeight)];
            } completion:^(BOOL finished) {
                [[self.scrollView subviews]makeObjectsPerformSelector:@selector(removeFromSuperview)];
            }];
        }
    }
}

- (int)indexOfImage:(UIImage*)image {
    ImageNode *node;
    int notFound = -1;
    for (int i = 0; i < self.imageContainer.count; i++) {
        node = self.imageContainer[i];
        if ([node.image isEqual:image]) {
            return i;
        }
    }
    return notFound;
}

- (void)highlightImageBorder:(UIImageView*)image {
    if (image.layer.borderColor == [UIColor blueColor].CGColor) {
        image.layer.borderColor = [UIColor greenColor].CGColor;
    }
    else
        image.layer.borderColor = [UIColor blueColor].CGColor;
}

- (void)oneTouchTap:(id)sender {
    // brings controls in or out of view
    UIGestureRecognizer *gesture = sender;
    UIImageView *image = (UIImageView*)gesture.view;
    
    if (image.layer.borderWidth > 0) {
        [self highlightImageBorder:image];
    }
    else if (photoViewerPtr.dropDownMenu.viewBox.hidden == YES) {
        [photoViewerPtr toggleButtons];
    }
}

- (void)showNextImage:(id)sender {
    UIGestureRecognizer *gesture = sender;
    UIImageView *image = (UIImageView*)gesture.view;
    if (self.imageContainer.count > 1) {
        if (image.frame.size.width == fullScreenWidth
            && photoViewerPtr.dropDownMenu.viewBox.hidden == YES){

            ImageNode *node = [self.imageContainer next];
            if (node) {
                [self animateSlideImageLeft:node];
            }
        }
    }
}

- (void)animateSlideImageLeft:(ImageNode*)node {
    [UIView animateWithDuration:0.125 animations:^{
        self.fullView.frame = CGRectOffset(self.fullView.frame, -fullScreenWidth, 0);
    } completion:^(BOOL finished) {
        self.fullView.frame = CGRectMake(fullScreenWidth, 0, fullScreenWidth, fullScreenHeight);
        [self.fullView setImage:node.image];
        [UIView animateWithDuration:0.125 animations:^{
            self.fullView.frame = CGRectOffset(self.fullView.frame, -fullScreenWidth, 0);
        } completion:^(BOOL finished) {
            [photoViewerPtr updatePhotoTitleWithString:node.title];
            [photoViewerPtr updateImageTitleSize];
            [self.metadataView reloadMetadata];
        }];
    }];
}

- (NSString*)returnImageTitle {
    PhotoViewerProvenance *pvp = [self.imageContainer current];
    if (pvp.title && ![pvp.title isEqualToString:@""]) {
        return pvp.title;
    }
    return [self addGenericImageDescription];
}

/******************************************************************************
 * addGenericImageDescription
 *
 * This method creates a generic description saved as a string and returns it.
 ******************************************************************************/
- (NSString*)addGenericImageDescription {
    NSString *genericLabel = @"Click Title To Add";
    return genericLabel;
}

- (void)showPreviousImage:(id)sender {
    UIGestureRecognizer *gesture = sender;
    UIImageView *image = (UIImageView*)gesture.view;
    if (self.imageContainer.count > 1) {
        if (image.frame.size.width == fullScreenWidth && photoViewerPtr.dropDownMenu.viewBox.hidden == YES){
            
            ImageNode *node = [self.imageContainer previous];
            if (node) {
                [self animateSlideImageRight:node];
            }
        }
    }
}

- (void)animateSlideImageRight:(ImageNode*)node {
    [UIView animateWithDuration:0.125 animations:^{
        self.fullView.frame = CGRectOffset(self.fullView.frame, fullScreenWidth, 0);
    } completion:^(BOOL finished) {
        self.fullView.frame = CGRectMake(-fullScreenWidth, 0, fullScreenWidth, fullScreenHeight);
        [self.fullView setImage:node.image];
        [UIView animateWithDuration:0.125 animations:^{
            self.fullView.frame = CGRectOffset(self.fullView.frame, fullScreenWidth, 0);
        } completion:^(BOOL finished) {
            [photoViewerPtr updatePhotoTitleWithString:node.title];
            [photoViewerPtr updateImageTitleSize];
            [self.metadataView reloadMetadata];
        }];
    }];
}

- (void)uploadMultipleImages {
    ImageNode *node;
    for (id object in self.imagesSelected) {
        node = object;
        [photoViewerPtr uploadTheseImages:node imageCount:(int)self.imagesSelected.count];
    }
}

/***************************************************************************************************
 * deleteSelectedImages
 *
 * This method calls to remove all imagesSelected from the database, and then reloads all images
 * that weren't deleted into view.
 **************************************************************************************************/
- (void)deleteSelectedImages {
    [[self.scrollView subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
    ImageNode *node;
    for (id object in self.imagesSelected) {
        node = object;
        [photoViewerPtr removeImageFromDatabase:node];
    }
    [self.imagesSelected removeAllObjects];
    [self loadAllImages];
}



- (void)emptyUploadImagesArray {
    [self.imagesSelected removeAllObjects];
}

- (void)hitUploadImageButton:(id)sender {
    if (photoViewerPtr.dropDownMenu.viewBox.hidden == YES) {
        UIGestureRecognizer *gesture = sender;
        UIImageView *image = (UIImageView*)gesture.view;
        if (image.frame.size.width == fullScreenWidth){
            
            [photoViewerPtr uploadImage:sender];
        }
    }
}

- (void)deleteImage:(id)sender {
    if (photoViewerPtr.dropDownMenu.viewBox.hidden == YES) {
        UIGestureRecognizer *gesture = sender;
        UIImageView *image = (UIImageView*)gesture.view;
        if (image.frame.size.width == fullScreenWidth){
            [photoViewerPtr deleteImage:sender];
        }
    }
}

/***************************************************************************************************
 * returnIndexOfImage
 *
 * This method returns the index of where the image passed in sits in the
 * imageContainer.
 **************************************************************************************************/
- (NSUInteger)returnIndexOfImage:(UIImage*)image inArray:(NSMutableArray*)array {
    NSUInteger index = [array indexOfObjectPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
        ImageNode *node = array[idx];
        if (image == node.image) {
            *stop = YES;
            return YES;
        }
        return NO;
    }];
    return index;
}

- (void)applyImageGestures:(UIImageView*)image {
    
    UILongPressGestureRecognizer *longPressPhotoGesture = [[UILongPressGestureRecognizer alloc]
                                                           initWithTarget:self
                                                           action:@selector(scaleImage:)];
    
    [image addGestureRecognizer:longPressPhotoGesture];
    
    
    UITapGestureRecognizer *oneTouchTapGesture = [[UITapGestureRecognizer alloc]
                                                  initWithTarget:self
                                                  action:@selector(oneTouchTap:)];
    oneTouchTapGesture.numberOfTouchesRequired = 1;
    [image addGestureRecognizer:oneTouchTapGesture];
    
    UISwipeGestureRecognizer *swipeRightGesture = [[UISwipeGestureRecognizer alloc]
                                                   initWithTarget:self
                                                   action:@selector(showPreviousImage:)];
    swipeRightGesture.direction = UISwipeGestureRecognizerDirectionRight;
    [image addGestureRecognizer:swipeRightGesture];
    
    UISwipeGestureRecognizer *swipeLeftGesture = [[UISwipeGestureRecognizer alloc]
                                                  initWithTarget:self
                                                  action:@selector(showNextImage:)];
    swipeLeftGesture.direction = UISwipeGestureRecognizerDirectionLeft;
    [image addGestureRecognizer:swipeLeftGesture];
    
    UISwipeGestureRecognizer *swipeUpGesture = [[UISwipeGestureRecognizer alloc]
                                                initWithTarget:self
                                                action:@selector(hitUploadImageButton:)];
    swipeUpGesture.direction = UISwipeGestureRecognizerDirectionUp;
    [image addGestureRecognizer:swipeUpGesture];
    
    UISwipeGestureRecognizer *swipeDownGesture = [[UISwipeGestureRecognizer alloc]
                                                  initWithTarget:self
                                                  action:@selector(deleteImage:)];
    swipeDownGesture.direction = UISwipeGestureRecognizerDirectionDown;
    [image addGestureRecognizer:swipeDownGesture];
}

- (void)highlightAllImages {
    if (self.imageContainer.count > 0) {
        for (id object in self.imageContainer) {
            ImageNode *node = object;
            [node.imageView.layer setBorderColor:[UIColor blueColor].CGColor];
            [node.imageView.layer setBorderWidth:2.0];
        }
    }
}

- (void)resetImageBorders {
    if (self.imageContainer.count > 0) {
        for (id object in self.imageContainer) {
            ImageNode *node = object;
            [node.imageView.layer setBorderColor:[UIColor clearColor].CGColor];
            [node.imageView.layer setBorderWidth:0.0];
        }
    }
}

- (void)refreshScreenAfterSort {
    if (self.imageContainer.count > 0) {
        if (self.fullView.hidden == YES) {
            [self buildFourUpViewWithImages:self.imageContainer];
        }
        else{
            ImageNode *node = self.imageContainer[0];
            [photoViewerPtr updatePhotoTitleWithString:node.title];
            [photoViewerPtr updateImageTitleSize];
            [self.fullView setImage:node.image];
        }
    }
    else{
        for (id object in self.scrollView.subviews) {
            [object removeFromSuperview];
        }
        [self.fullView setImage:nil];
        [self.fullView setHighlighted:YES];
    }
}

/***************************************************************************************************
 * loadAllImages
 *
 * This method grabs all images for the user from the database and loads them into the imageContainer.
 **************************************************************************************************/
- (void)loadAllImages {
    self.imageContainer = [ImageArray new];
    // query by username only for all images
    RLMResults *images = [PhotoViewerProvenance objectsWhere:
                          [NSString stringWithFormat:@"username = '%@'",self.userInfo.username]];
    // create imageNodes for every object returned from query
    for (int i = 0; i < images.count; i++) {
        
        PhotoViewerProvenance *pvp = images[i];
        ImageNode *node = [ImageNode new];
        node.image = [UIImage imageWithData:pvp.image];
        node.imageView = [[UIImageView alloc]initWithImage:node.image];
        node.username = pvp.username;
        node.filename = pvp.filename;
        node.community = pvp.community;
        node.caption = pvp.caption;
        node.project = pvp.project;
        node.date = pvp.date;
        node.time = pvp.time;
        node.title = pvp.title;
        node.latitude = pvp.latitude;
        node.longitude = pvp.longitude;
        node.address = pvp.address;
        node.notes = pvp.notes;
        
        [self.imageContainer addObject:node];
    }

    [self refreshScreenAfterSort];
    [photoViewerPtr setButtonVisibityOnReload];
}

/***************************************************************************************************
 * updateArrayByAddingImageNode
 *
 * This method adds a new ImageNode to the imageContainer:
 **************************************************************************************************/
- (void)updateArrayByAddingImageNode:(ImageNode*)node {
    if (self.imageContainer == nil) {
        self.imageContainer = [ImageArray new];
        [self.imageContainer addObject:node];
    }
    else
        [self.imageContainer addObject:node];
}

/***************************************************************************************************
 * loadSpecificCommunityImages
 *
 * Loads all images from the community chosen under the current user.
 **************************************************************************************************/
- (void)loadSpecificCommunityImages:(NSString*)community {

    if (self.imageContainer) {
        [self.imageContainer removeAllObjects];
    }
    else
        self.imageContainer = [ImageArray new];
    

    RLMResults *images = [PhotoViewerProvenance objectsWhere:
        [NSString stringWithFormat:@"username = '%@' AND community = '%@'",
                                                self.userInfo.username,community]];
    
    // create imageNodes for every object returned from query
    for (int i = 0; i < images.count; i++) {
        
        PhotoViewerProvenance *pvp = images[i];
        ImageNode *node = [ImageNode new];
        node.image = [UIImage imageWithData:pvp.image];
        node.imageView = [[UIImageView alloc]initWithImage:node.image];
        node.username = pvp.username;
        node.filename = pvp.filename;
        node.community = pvp.community;
        node.caption = pvp.caption;
        node.project = pvp.project;
        node.date = pvp.date;
        node.time = pvp.time;
        node.title = pvp.title;
        node.latitude = pvp.latitude;
        node.longitude = pvp.longitude;
        node.address = pvp.address;
        node.notes = pvp.notes;
        
        [self.imageContainer addObject:node];
    }

    [self refreshScreenAfterSort];
    [photoViewerPtr setButtonVisibityOnReload];
}

- (void)showError:(NSString*)errorMessage {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error"
                                                                   message:errorMessage
                                                            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Ok"
                                                     style:UIAlertActionStyleCancel
                                                   handler:nil];
    [alert addAction:cancel];
    [photoViewerPtr presentViewController:alert animated:YES completion:nil];
}

@end
