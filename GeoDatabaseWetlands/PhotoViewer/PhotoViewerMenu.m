//
//  PhotoViewerMenu.m
//  GeoDatabaseWetlands
//
//  Created by Jonathan Mason on 6/15/15.
//  Copyright (c) 2015 Jonathan Mason. All rights reserved.
//

#import "PhotoViewerMenu.h"
#import "PhotoViewer.h"
#import "ViewManager.h"
#import "UserInfo.h"
#import "SystemSounds.h"
#define buttonHeight 40
#define buttonWidth 200
#define fontSize 22
#define PlantCommunityFolder @"PlantCommunity"
#define WaterCommunityFolder @"WaterCommunity"
#define BirdCommunityFolder @"BirdCommunity"
PhotoViewer *photoViewerPtr;
UIButton *infoButton;
UIButton *uploadImageButton;
UIButton *imageMetadataButton;
UIButton *deleteImagesButton;
UIButton *allPhotosButton;
UIButton *plantPhotosButton;
UIButton *waterPhotosButton;
UIButton *birdPhotosButton;
UIButton *startButton;
UIButton *cancelButton;
UIColor *highlight;

@implementation PhotoViewerMenu

+(id)sharedPhotoViewerMenu:(PhotoViewer*)photoViewer {
    static dispatch_once_t predicate = 0;
    __strong static id sharedObject = nil;
    
    dispatch_once(&predicate, ^{
        sharedObject = [[self alloc]initWithPhotoViewer:photoViewer];
    });
    return sharedObject;
}

// set the properties on initialization
-(id)initWithPhotoViewer:(PhotoViewer*)photoViewer {
    self = [super init];
    if (self) {
        highlight = [UIColor colorWithRed: 200.0/255.0f
                                    green:175.0/255.0f
                                     blue:255.0/255.0f
                                    alpha:1.0];
        [self generateMenuView];
        [self generateButtons];
        photoViewerPtr = photoViewer;
    }
    return self;
}

- (BOOL)returnStartButtonIsHidden {
    return startButton.hidden;
}

- (void)generateMenuView {
    self.viewBox = [[UIView alloc]initWithFrame:CGRectMake(545, 95, 180, 405)];
}

// sets properties for all buttons and displays them on the UI
- (void)generateButtons {
    
    uploadImageButton = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, buttonWidth, buttonHeight)];
    [uploadImageButton setTitle:@"Upload Images" forState:UIControlStateNormal];
    [uploadImageButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    uploadImageButton.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:fontSize];
    [uploadImageButton addTarget:self action:@selector(hitUploadImagesButton:)
         forControlEvents:UIControlEventTouchUpInside];
    uploadImageButton.backgroundColor = [UIColor colorWithWhite:1.0 alpha:1.0];
    [uploadImageButton setShowsTouchWhenHighlighted:YES];
    [uploadImageButton.layer setMask:[self addMaskToButton:uploadImageButton]];
    [self.viewBox addSubview:uploadImageButton];
    
    deleteImagesButton = [[UIButton alloc]initWithFrame:CGRectMake(0, 45, buttonWidth, buttonHeight)];
    [deleteImagesButton setTitle:@"Delete Images" forState:UIControlStateNormal];
    [deleteImagesButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    deleteImagesButton.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:fontSize];
    [deleteImagesButton addTarget:self action:@selector(hitdeleteImagesButton:)
               forControlEvents:UIControlEventTouchUpInside];
    deleteImagesButton.backgroundColor = [UIColor colorWithWhite:1.0 alpha:1.0];
    [deleteImagesButton setShowsTouchWhenHighlighted:YES];
    [deleteImagesButton.layer setMask:[self addMaskToButton:deleteImagesButton]];
    [self.viewBox addSubview:deleteImagesButton];
    
    imageMetadataButton = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, buttonWidth, buttonHeight)];
    [imageMetadataButton setTitle:@"Show Metadata" forState:UIControlStateNormal];
    [imageMetadataButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    imageMetadataButton.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:fontSize];
    [imageMetadataButton addTarget:self action:@selector(hitImageMetadataButton:)
                forControlEvents:UIControlEventTouchUpInside];
    imageMetadataButton.backgroundColor = [UIColor colorWithWhite:1.0 alpha:1.0];
    [imageMetadataButton setShowsTouchWhenHighlighted:YES];
    [imageMetadataButton.layer setMask:[self addMaskToButton:imageMetadataButton]];
    [self.viewBox addSubview:imageMetadataButton];
    
    infoButton = [[UIButton alloc]initWithFrame:CGRectMake(0, 45, buttonWidth, buttonHeight)];
    [infoButton setTitle:@"Info on Image" forState:UIControlStateNormal];
    [infoButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    infoButton.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:fontSize];
    [infoButton addTarget:self action:@selector(hitInfoButton:)
         forControlEvents:UIControlEventTouchUpInside];
    infoButton.backgroundColor = [UIColor colorWithWhite:1.0 alpha:1.0];
    [infoButton setShowsTouchWhenHighlighted:YES];
    [infoButton.layer setMask:[self addMaskToButton:infoButton]];
    [self.viewBox addSubview:infoButton];
    
    allPhotosButton = [[UIButton alloc]initWithFrame:CGRectMake(10, 225, buttonWidth-10, buttonHeight)];
    [allPhotosButton setTitle:@"All Photos" forState:UIControlStateNormal];
    [allPhotosButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    allPhotosButton.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:fontSize];
    allPhotosButton.backgroundColor = highlight;
    [allPhotosButton addTarget:self action:@selector(hitAllPhotosButton:)
              forControlEvents:UIControlEventTouchUpInside];
    [allPhotosButton.layer setMask:[self addMaskToButton:allPhotosButton]];
    [self.viewBox addSubview:allPhotosButton];
    
    plantPhotosButton = [[UIButton alloc]initWithFrame:CGRectMake(10, 270, buttonWidth-10, buttonHeight)];
    [plantPhotosButton setTitle:@"Plant Community" forState:UIControlStateNormal];
    [plantPhotosButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    plantPhotosButton.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:fontSize];
    plantPhotosButton.backgroundColor = [UIColor colorWithWhite:1.0 alpha:1.0];
    [plantPhotosButton addTarget:self action:@selector(hitPlantPhotosButton:)
                forControlEvents:UIControlEventTouchUpInside];
    [plantPhotosButton.layer setMask:[self addMaskToButton:plantPhotosButton]];
    [self.viewBox addSubview:plantPhotosButton];
    
    waterPhotosButton = [[UIButton alloc]initWithFrame:CGRectMake(10, 315, buttonWidth-10, buttonHeight)];
    [waterPhotosButton setTitle:@"Water Community" forState:UIControlStateNormal];
    [waterPhotosButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    waterPhotosButton.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:fontSize];
    [waterPhotosButton addTarget:self action:@selector(hitWaterPhotosButton:)
                forControlEvents:UIControlEventTouchUpInside];
    waterPhotosButton.backgroundColor = [UIColor colorWithWhite:1.0 alpha:1.0];
    [waterPhotosButton.layer setMask:[self addMaskToButton:waterPhotosButton]];
    [self.viewBox addSubview:waterPhotosButton];
    
    birdPhotosButton = [[UIButton alloc]initWithFrame:CGRectMake(10, 360, buttonWidth-10, buttonHeight)];
    [birdPhotosButton setTitle:@"Bird Community" forState:UIControlStateNormal];
    [birdPhotosButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    birdPhotosButton.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:fontSize];
    [birdPhotosButton addTarget:self action:@selector(hitBirdPhotosButton:)
               forControlEvents:UIControlEventTouchUpInside];
    birdPhotosButton.backgroundColor = [UIColor colorWithWhite:1.0 alpha:1.0];
    [birdPhotosButton.layer setMask:[self addMaskToButton:birdPhotosButton]];
    [self.viewBox addSubview:birdPhotosButton];
    
    startButton = [[UIButton alloc]initWithFrame:CGRectMake(164, 60, buttonWidth, buttonHeight)];
    [startButton setTitle:@"Start Upload" forState:UIControlStateNormal];
    [startButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    startButton.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:fontSize];
    [startButton addTarget:self action:@selector(hitStartButton:)
               forControlEvents:UIControlEventTouchUpInside];
    startButton.backgroundColor = [UIColor blackColor];
    [startButton setAlpha:0.8];
    [startButton.layer setMask:[self addMaskToButton:startButton]];
    [startButton setHidden:YES];
    [photoViewerPtr.view addSubview:startButton];
    
    cancelButton = [[UIButton alloc]initWithFrame:CGRectMake(406, 60, buttonWidth, buttonHeight)];
    [cancelButton setTitle:@"Cancel" forState:UIControlStateNormal];
    [cancelButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    cancelButton.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:fontSize];
    [cancelButton addTarget:self action:@selector(hitCancelButton:)
          forControlEvents:UIControlEventTouchUpInside];
    cancelButton.backgroundColor = [UIColor blackColor];
    [cancelButton setAlpha:0.8];
    [cancelButton.layer setMask:[self addMaskToButton:cancelButton]];
    [cancelButton setHidden:YES];
    [photoViewerPtr.view addSubview:cancelButton];
}

- (void)hitCancelButton:(id)sender {
    [SystemSounds playTockSound];
    [self.viewManager resetImageBorders];
    [startButton setHidden:YES];
    [cancelButton setHidden:YES];
}

/******************************************************************************
 * animateButtonSelection
 *
 * This method gives a flashing effect to the button on touch then closes menu.
 ******************************************************************************/
- (void)animateButtonSelection:(UIButton*)button {
    NSLog(@"%@",button.titleLabel.text);
    [UIView animateWithDuration:.05 animations:^{
        button.backgroundColor = highlight;
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:.05 animations:^{
            button.backgroundColor = [UIColor grayColor];
        } completion:^(BOOL finished) {
            [UIView animateWithDuration:.05 animations:^{
                button.backgroundColor = highlight;
            } completion:^(BOOL finished) {
                [UIView animateWithDuration:.05 animations:^{
                    button.backgroundColor = [UIColor grayColor];
                } completion:^(BOOL finished) {
                    [UIView animateWithDuration:.05 animations:^{
                        button.backgroundColor = highlight;
                    } completion:^(BOOL finished) {
                        [UIView animateWithDuration:.05 animations:^{
                            button.backgroundColor = [UIColor grayColor];
                        } completion:^(BOOL finished) {
                            [UIView animateWithDuration:.05 animations:^{
                                button.backgroundColor = highlight;
                            } completion:^(BOOL finished) {
                                [self closeMenu];
                            }];
                        }];
                    }];
                }];
            }];
        }];
    }];
}

- (void)popupNoImages {
    UIAlertController *alert = [UIAlertController
                                alertControllerWithTitle:@"No images present"
                                                 message:nil
                                          preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Ok"
                                                       style:UIAlertActionStyleCancel
                                                     handler:nil];
    [alert addAction:okAction];
    [photoViewerPtr presentViewController:alert animated:YES completion:nil];
}

- (void)hitInfoButton:(id)sender {
    [SystemSounds playTockSound];
    if ([photoViewerPtr returnImageCount] != 0) {
        [photoViewerPtr searchByImageTitle:sender];
        [self.viewBox setHidden:YES];
    }
    else{
        [self popupNoImages];
        [self.viewBox setHidden:YES];
    }
}

- (void)hitUploadImagesButton:(id)sender {
    [SystemSounds playTockSound];
    if (self.viewManager.imageContainer.count > 0) {
        if (photoViewerPtr.userInfo.approvedUser == YES) {
            if (self.viewManager.fullView.hidden == NO) {
                [self.viewManager buildFourUpViewWithImages:self.viewManager.imageContainer];
            }
            if (self.viewManager.imagesSelected.count > 0) {
                [self.viewManager.imagesSelected removeAllObjects];
            }
            [startButton setTitle:@"Upload Selected" forState:UIControlStateNormal];
            [startButton setHidden:NO];
            [cancelButton setHidden:NO];
            // put border on all images that can be highlighted
            [self.viewManager highlightAllImages];
            [self.viewBox setHidden:YES];
        }
        else{
            [photoViewerPtr popupNoUploadApproval];
        }
    }
    else
        [self popupNoImages];
}

- (void)hitdeleteImagesButton:(id)sender {
    [SystemSounds playTockSound];
    if (self.viewManager.imageContainer.count > 0) {
        [startButton setTitle:@"Delete Selected" forState:UIControlStateNormal];
        [startButton setHidden:NO];
        [cancelButton setHidden:NO];
        [self.viewManager highlightAllImages];
        [self.viewBox setHidden:YES];
    }
    else
        [self popupNoImages];
}

- (void)hitStartButton:(id)sender {
    [SystemSounds playTockSound];
    if ([self selectedAtLeastOneImage]){
        if ([startButton.titleLabel.text isEqualToString:@"Upload Selected"]) {
            [self.viewManager uploadMultipleImages];
        }
        else if ([startButton.titleLabel.text isEqualToString:@"Delete Selected"]){ // delete files
            [self.viewManager deleteSelectedImages];
        }
    }
    else{
        [self popupNoSelectionWarning];
    }
    [self.viewManager resetImageBorders];
    [startButton setHidden:YES];
    [cancelButton setHidden:YES];
}

- (void)popupNoSelectionWarning {
    UIAlertController *alert = [UIAlertController
                                alertControllerWithTitle:@"No selection was made."
                                message:nil
                                preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Ok"
                                                 style:UIAlertActionStyleCancel
                                               handler:nil];
    [alert addAction:ok];
    [photoViewerPtr presentViewController:alert animated:YES completion:nil];
}

- (BOOL)selectedAtLeastOneImage {
    if (!self.viewManager.imagesSelected) {
        self.viewManager.imagesSelected = [NSMutableArray new];
    }
    BOOL selected = NO;
    for (id object in self.viewManager.imageContainer) {
        ImageNode *node = object;
        if (node.imageView.layer.borderColor == [UIColor greenColor].CGColor) {
            selected = YES;
            [self.viewManager.imagesSelected addObject:node];
        }
    }
    return selected;
}

- (void)hitImageMetadataButton:(id)sender {
    [SystemSounds playTockSound];
    if ([photoViewerPtr returnImageCount] != 0) {
        [photoViewerPtr imageMetadataButton:sender];
        [self.viewBox setHidden:YES];
    }
    else{
        [self popupNoImages];
        [self.viewBox setHidden:YES];
    }
}

/******************************************************************************
 * hitAllPhotosButton
 *
 * This method handles when the user hits the button to view all photos.
 ******************************************************************************/
- (void)hitAllPhotosButton:(id)sender {
    [SystemSounds playTockSound];
    allPhotosButton.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:fontSize];
    plantPhotosButton.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:fontSize];
    waterPhotosButton.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:fontSize];
    birdPhotosButton.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:fontSize];
    
    plantPhotosButton.backgroundColor = [UIColor whiteColor];
    waterPhotosButton.backgroundColor = [UIColor whiteColor];
    birdPhotosButton.backgroundColor = [UIColor whiteColor];
    
    double delayInSeconds = 0.5;
    dispatch_time_t runTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_async(dispatch_get_main_queue(), ^{
        [self animateButtonSelection:allPhotosButton];
        dispatch_after(runTime, dispatch_get_main_queue(), ^{
            [self.viewManager loadAllImages];
        });
    });
}

/******************************************************************************
 * hitPlantPhotosButton
 *
 * This method handles when the user hits the button to view all photos.
 ******************************************************************************/
- (void)hitPlantPhotosButton:(id)sender {
    [SystemSounds playTockSound];
    allPhotosButton.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:fontSize];
    plantPhotosButton.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:fontSize];
    waterPhotosButton.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:fontSize];
    birdPhotosButton.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:fontSize];
    
    allPhotosButton.backgroundColor = [UIColor whiteColor];
    waterPhotosButton.backgroundColor = [UIColor whiteColor];
    birdPhotosButton.backgroundColor = [UIColor whiteColor];
    
    double delayInSeconds = 0.5;
    dispatch_time_t runTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_async(dispatch_get_main_queue(), ^{
        [self animateButtonSelection:plantPhotosButton];
        dispatch_after(runTime, dispatch_get_main_queue(), ^{
            [self.viewManager loadSpecificCommunityImages:PlantCommunityFolder];
        });
    });
}

/******************************************************************************
 * hitWaterPhotosButton
 *
 * This method handles when the user hits the button to view all photos.
 ******************************************************************************/
- (void)hitWaterPhotosButton:(id)sender {
    [SystemSounds playTockSound];
    allPhotosButton.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:fontSize];
    plantPhotosButton.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:fontSize];
    waterPhotosButton.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:fontSize];
    birdPhotosButton.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:fontSize];
    
    allPhotosButton.backgroundColor = [UIColor whiteColor];
    plantPhotosButton.backgroundColor = [UIColor whiteColor];
    birdPhotosButton.backgroundColor = [UIColor whiteColor];
    
    double delayInSeconds = 0.5;
    dispatch_time_t runTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_async(dispatch_get_main_queue(), ^{
        [self animateButtonSelection:waterPhotosButton];
        dispatch_after(runTime, dispatch_get_main_queue(), ^{
            [self.viewManager loadSpecificCommunityImages:WaterCommunityFolder];
        });
    });
}

/******************************************************************************
 * hitBirdPhotosButton
 *
 * This method handles when the user hits the button to view all photos.
 ******************************************************************************/
- (void)hitBirdPhotosButton:(id)sender {
    [SystemSounds playTockSound];
    allPhotosButton.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:fontSize];
    plantPhotosButton.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:fontSize];
    waterPhotosButton.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:fontSize];
    birdPhotosButton.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:fontSize];
    
    allPhotosButton.backgroundColor = [UIColor whiteColor];
    plantPhotosButton.backgroundColor = [UIColor whiteColor];
    waterPhotosButton.backgroundColor = [UIColor whiteColor];
    
    double delayInSeconds = 0.5;
    dispatch_time_t runTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_async(dispatch_get_main_queue(), ^{
        [self animateButtonSelection:birdPhotosButton];
        dispatch_after(runTime, dispatch_get_main_queue(), ^{
            [self.viewManager loadSpecificCommunityImages:BirdCommunityFolder];
        });
    });
}

- (void)closeMenu{
    self.viewBox.hidden = YES;
}

- (void)infoButtonVisibility {
    if ([photoViewerPtr.photoTitle.text isEqualToString:@"Click Title To Add"]
        || [photoViewerPtr.photoTitle.text isEqualToString:@""]
        || photoViewerPtr.photoTitle.hidden == YES) {
        [infoButton setHidden:YES];
    }
    else
        [infoButton setHidden:NO];
}

- (CALayer*)addMaskToButton:(UIButton*)button {
    UIBezierPath *maskPath= [UIBezierPath bezierPathWithRoundedRect:button.bounds
                                                       cornerRadius:8];
    CAShapeLayer *maskLayer = [CAShapeLayer layer];
    maskLayer.frame = button.bounds;
    maskLayer.path = maskPath.CGPath;
    return maskLayer;
}

- (void)showFullScreenButtonsWithInfoButton:(BOOL)show {
    [uploadImageButton setHidden:YES];
    [deleteImagesButton setHidden:YES];
    [imageMetadataButton setHidden:NO];
    if (show) {
        [infoButton setHidden:NO];
    }
    else
        [infoButton setHidden:YES];
}

- (void)showMultipleImageButtons {
    [imageMetadataButton setHidden:YES];
    [infoButton setHidden:YES];
    [uploadImageButton setHidden:NO];
    [deleteImagesButton setHidden:NO];
}

@end
