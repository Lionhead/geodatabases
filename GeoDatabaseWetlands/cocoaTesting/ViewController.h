//
//  ViewController.h
//  cocoaTesting
//
//  Created by Jonathan Mason on 1/4/15.
//  Copyright (c) 2015 Jonathan Mason. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SampleProtocol.h"

@interface ViewController : UIViewController

@property (strong, nonatomic) IBOutlet UITextField *valueInput;

- (IBAction)goButton:(id)sender;

@property (strong, nonatomic) IBOutlet UIProgressView *progressBar;

@property (strong, nonatomic) IBOutlet UILabel *resultLabel;

@end

