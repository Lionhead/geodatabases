//
//  SampleProtocol.h
//  cocoaTesting
//
//  Created by Jonathan Mason on 1/6/15.
//  Copyright (c) 2015 Jonathan Mason. All rights reserved.
//

@protocol SampleProtocolDelegate <NSObject>

@required // keywords signaling anything required to implement under this protocol
- (void)processCompleted;

@optional // optional implementation

@end

@interface SampleProtocol : NSObject

{
    // delegate to respond back
    id <SampleProtocolDelegate> _delegate;
}
@property (nonatomic,strong) id delegate;

- (void)startSampleProcess; // instance method

@end
