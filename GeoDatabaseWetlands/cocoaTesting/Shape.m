//
//  Shape.m
//  cocoaTesting
//
//  Created by Jonathan Mason on 3/26/15.
//  Copyright (c) 2015 Jonathan Mason. All rights reserved.
//

#import "Shape.h"

@implementation Shape


- (id)initWithShape:(int)x :(int)y :(int)width :(int)height{
    self = [super init];
    if (self) {
        self.myView = [[UIView alloc]initWithFrame:CGRectMake(x, y, width, height)];
        self.myView.backgroundColor = [UIColor blackColor];
    }
    return self;
}

-(void)changeBounds:(int)xPos :(int)yPos{
    CGRect myBounds = self.myView.bounds;
    myBounds.origin.x += xPos;
    myBounds.origin.y += yPos;
    self.myView.bounds = myBounds;
}

@end
