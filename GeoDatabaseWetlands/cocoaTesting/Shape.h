//
//  Shape.h
//  cocoaTesting
//
//  Created by Jonathan Mason on 3/26/15.
//  Copyright (c) 2015 Jonathan Mason. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface Shape : NSObject

@property (strong,nonatomic) UIView *myView;

- (id)initWithShape:(int)x :(int)y :(int)width :(int)height;
-(void)changeBounds:(int)xPos :(int)yPos;

@end
