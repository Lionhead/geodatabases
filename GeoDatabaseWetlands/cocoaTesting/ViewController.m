//
//  ViewController.m
//  cocoaTesting
//
//  Created by Jonathan Mason on 1/4/15.
//  Copyright (c) 2015 Jonathan Mason. All rights reserved.
//

#import "ViewController.h"
#import "Shape.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    SampleProtocol *sampleProtocol = [[SampleProtocol alloc]init];
    sampleProtocol.delegate = self;
    
    [_resultLabel setText:@"Processing..."];
    [sampleProtocol startSampleProcess];
    
    Shape *myShape = [[Shape alloc]initWithShape:50 :450 :50 :100];
    [self.view addSubview:myShape.myView];
    [myShape changeBounds:100 :-50];
    [UIView animateWithDuration:5.0 animations:^{
        
        [self.view addSubview:myShape.myView];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)goButton:(id)sender {
    
}

- (void)processCompleted{
    [_resultLabel setText:@"Process Completed"];
}

@end
