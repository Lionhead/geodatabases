//
//  SampleProtocol.m
//  cocoaTesting
//
//  Created by Jonathan Mason on 1/6/15.
//  Copyright (c) 2015 Jonathan Mason. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SampleProtocol.h"

@implementation SampleProtocol

- (void)startSampleProcess{
    
    [NSTimer scheduledTimerWithTimeInterval:3.0 target:self.delegate selector:@selector(processCompleted) userInfo:nil repeats:NO];
}

@end