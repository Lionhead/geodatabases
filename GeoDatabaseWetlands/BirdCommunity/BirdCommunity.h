//
//  BirdCommunity.h
//  GeoDatabaseWetlands
//
//  Created by Jonathan Mason on 7/7/15.
//  Copyright (c) 2015 Jonathan Mason. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserLogin.h"
#import "UserInfo.h"
#import "DirectUploadForm.h"
#import "CSVformatting.h"
#import "Reachability.h"
#import "LocalFileManager.h"
#import "UIViewControllerTracker.h"

// See comment at top of file for a complete description
@interface BirdCommunity : UIViewController

@property (strong, nonatomic) UserLogin *login;
@property (strong, nonatomic) UserInfo *userInfo;
@property (strong,atomic) DirectUploadForm *uploadForm;
@property (strong, nonatomic) Reachability *internetReachable;
@property (strong, nonatomic) UIViewControllerTracker *vcTracker;



@end
