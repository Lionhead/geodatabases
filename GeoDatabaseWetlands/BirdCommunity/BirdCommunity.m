//
//  BirdCommunity.m
//  GeoDatabaseWetlands
//
//  Created by Jonathan Mason on 7/7/15.
//  Copyright (c) 2015 Jonathan Mason. All rights reserved.
//

#import "BirdCommunity.h"

@interface BirdCommunity ()

@end

@implementation BirdCommunity

- (void)viewDidLoad {
    [super viewDidLoad];
    _vcTracker = [UIViewControllerTracker sharedVC];
    self.vcTracker.currentViewController = self;
    // Do any additional setup after loading the view.
    /*    testing prototype size
    UIImageView *background = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 768, 1024)];
    [background setContentMode:UIViewContentModeScaleAspectFit];
    [background setImage:[UIImage imageNamed:@"birdCommunity"]];
    [self.view addSubview:background];
     */
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    if ([segue.identifier isEqualToString:@"birdToPhotoViewerSegue"]) {
        PhotoViewer *controller = (PhotoViewer*)segue.destinationViewController;
        controller.sourceSegue = segue;
        controller.userInfo = self.userInfo;
        controller.community = @"BirdCommunity";
    }
}

@end
