
/*******************************************************************************************
 *******************************************************************************************
 *
 *  GeoDatabase Wetlands iPad Application
 *
 *  Created for the University of Washington, Bothell, Office of Research
 *
 *  The is application is intended only for students at the University of WA, Bothell.
 *
 *
 *
 *  Free sound effects taken from http://www.grsites.com/
 *  Free image icons taken from http://www.flaticon.com/
 *
 *******************************************************************************************
 *******************************************************************************************/
//
//  Documentation.h
//  GeoDatabaseWetlands
//
//  Created by Jonathan Mason on 5/28/14.
//  Copyright (c) 2014 Jonathan Mason. All rights reserved.
//
//  This software is intended soley for the University of Washington Bothell.

#import <UIKit/UIKit.h>
#import "UserLogin.h"
#import "UIViewControllerTracker.h"

@interface Documentation : UIViewController

@property (weak,nonatomic)NSString *documentationNotes;
@property (strong, nonatomic)UserLogin *login;
@property (weak, nonatomic) IBOutlet UITextView *textView;


@end
