//
//  UserLogin.h
//  GeoDatabaseWetlands
//
//  Created by Jonathan Mason.
//  Copyright (c) 2014 Jonathan Mason. All rights reserved.
//
/***********************************************************************
 * UserLogin uses the singleton pattern to create one login instance
 * while the app is running. This instance is used to determine whether
 * the user logs in as a guest or is a valid member who can connect
 * to the database server.
***********************************************************************/

#import <UIKit/UIKit.h>
// See comment at top of file for a complete description
@interface UserLogin : NSObject

@property(strong, nonatomic) NSString *username;
@property(strong, nonatomic) NSString *password;
@property(strong, nonatomic) NSString *changedPassword;

@property BOOL guestLogin;
@property BOOL authenticationCheckComplete;
@property(strong, nonatomic) NSString *passwordChangeResult;
@property(strong, nonatomic) NSString *serverAuthenticationResponseString;


+ (UserLogin*)sharedUserLogin; // ensures singleton pattern
- (BOOL)getNetworkApproval; // approval to upload or download files
- (void)authenticateUser; // authenticate username and password
- (void)changePassword; // changes the user's password on the server

@end



