//
//  UserInfo.h
//  GeoDatabaseWetlands
//
//  Created by Jonathan Mason on 4/30/15.
//  Copyright (c) 2015 Jonathan Mason. All rights reserved.
//
//  This software is intended soley for the University of Washington Bothell.
/*******************************************************************************************
 *******************************************************************************************
 *
 * This class is responsible for creating the elements used to retrieve user information
 * that is needed for all communities. This included researcher names, project, air temp,
 * instructor, class number, and weather conditions.
 *
 *******************************************************************************************
 *******************************************************************************************/

#import <Foundation/Foundation.h>
#import "CommunityButtons.h"
#import "UserLogin.h"
#import "WeatherOptions.h"
#import "UploadQueue.h"

@class HomeScreen;
// See comment at top of file for a complete description
@interface UserInfo : NSObject <UITextFieldDelegate>

@property (strong, nonatomic) CommunityButtons *communityButtons;
@property (strong, nonatomic) WeatherOptions *weatherOptionsPtr;
@property (strong, nonatomic) UploadQueue *uploadQueue;
@property BOOL approvedUser;
@property BOOL clickedLoadData;
@property BOOL clickedLoadDataWithUserInfo;
@property BOOL clickedDeleteFile;
@property BOOL updateHomeScreen;
@property (strong, nonatomic) NSString *username;
@property (strong, nonatomic) NSString *researchers;
@property (strong, nonatomic) NSString *project;
@property (strong, nonatomic) NSString *classNumber;
@property (strong, nonatomic) NSString *instructor;
@property (strong, nonatomic) NSString *weatherConditions;
@property (strong, nonatomic) NSString *currentTemp;
@property (strong, nonatomic) NSString *plantWorkingFilename;
@property (strong, nonatomic) NSString *waterWorkingFilename;
@property (strong, nonatomic) NSString *birdWorkingFilename;
@property (strong, nonatomic) NSString *plantTimeStarted;
@property (strong, nonatomic) NSString *plantDateStarted;
@property (strong, nonatomic) NSString *waterTimeStarted;
@property (strong, nonatomic) NSString *waterDateStarted;
@property (strong, nonatomic) NSString *plantFormID;
@property (strong, nonatomic) NSString *waterFormID;
@property (strong, nonatomic) NSString *birdFormID;

+ (id)sharedUserInfo;

- (void)updateUserInfoWithFileLoaded:(UserInfo*)info;
- (void)resetFields;
- (void)resetFormIDs;
- (void)updateResearchers:(NSString*)researchers;

@end


