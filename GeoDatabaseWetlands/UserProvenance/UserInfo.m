//
//  UserInfo.m
//  GeoDatabaseWetlands
//
//  Created by Jonathan Mason on 4/30/15.
//  Copyright (c) 2015 Jonathan Mason. All rights reserved.
//

#import "UserInfo.h"
#import "NameDeleteMenu.h"

#define usernameKey                 @"username"
#define plantTimeStartedKey         @"plantTimeStarted"
#define plantDateStartedKey         @"plantDateStarted"
#define waterTimeStartedKey         @"waterTimeStarted"
#define waterDateStartedKey         @"waterDateStarted"
#define viewBoxKey                  @"viewBox"
#define namesKey                    @"names"
#define namesLabelKey               @"namesLabel"
#define addNameButtonKey            @"addNameButton"
#define subtractNameButtonKey       @"subtractNameButton"
#define weatherOptionsPtrKey        @"weatherOptionsPtr"
#define weatherButtonKey            @"weatherButton"
#define weatherConditionsKey        @"weatherConditions"
#define projectNameTFKey            @"projectNameTF"
#define instructorMentorTFKey       @"instructorMentorTF"
#define currentTempTFKey            @"currentTempTF"
#define classNumberTFKey            @"classNumberTF"
#define independentResearchLabelKey @"independentResearchLabel"
#define classSwitchKey              @"classSwitch"


@interface UserInfo() {

}

@property (strong, nonatomic) HomeScreen *home;

@end

@implementation UserInfo

+ (id)sharedUserInfo {
    static dispatch_once_t onceToken;
    __strong static id sharedObject = nil;
    
    dispatch_once(&onceToken, ^{
        sharedObject = [[self alloc]init];
    });
    
    return sharedObject;
}


- (id)init {
    if (self = [super init]) {
        [self resetFields];
        _uploadQueue = [UploadQueue new];
    }
    return self;
}

- (void)updateWeatherConditions {
    self.weatherConditions = @"";
    for (id key in self.weatherOptionsPtr.selectedWeatherConditions) {
        UILabel *label = self.weatherOptionsPtr.selectedWeatherConditions[key];
        if (label.textColor == [UIColor whiteColor]) {
            label = [self.weatherOptionsPtr.selectedWeatherConditions valueForKey:key];
            self.weatherConditions = [self.weatherConditions stringByAppendingString:
                                      [NSString stringWithFormat:@"%@, ",label.text]];
        }
    }
    if (self.weatherConditions) {
        if (self.weatherConditions.length > 2) {
            self.weatherConditions = [self.weatherConditions
                                      substringToIndex:self.weatherConditions.length-2];
        }
    }
}


/******************************************************************************
 * updateUserInfoWithFileLoaded
 *
 * This method updates the properties of UserInfo when the user loads UserInfo
 * data that was locally stored.
 ******************************************************************************/
- (void)updateUserInfoWithFileLoaded:(UserInfo*)info {
    self.plantTimeStarted = info.plantTimeStarted;
    self.plantDateStarted = info.plantDateStarted;
}

/******************************************************************************
 * updateWeatherInfoWith
 *
 * This method takes the properties of WeatherOptions what where passed in by
 * being loaded from local file storage. It then calls to update the properties.
 ******************************************************************************/
- (void)updateWeatherInfoWith:(WeatherOptions*)weatherOptions {
    self.weatherOptionsPtr = weatherOptions;
    [self.weatherOptionsPtr refreshWeatherSelection];
}

- (void)updateResearchers:(NSString*)researchers {
    self.researchers = researchers;
}

- (void)resetFields {
    self.username = @"";
    self.researchers = @"";
    self.project = @"";
    self.classNumber = @"";
    self.instructor = @"";
    self.weatherConditions = @"";
    self.currentTemp = @"";
    self.plantWorkingFilename = @"";
    self.waterWorkingFilename = @"";
    self.birdWorkingFilename = @"";
    self.plantTimeStarted = @"";
    self.plantDateStarted = @"";
    self.waterTimeStarted = @"";
    self.waterDateStarted = @"";
    [self resetFormIDs];
}

- (void)resetFormIDs {
    self.plantFormID = @"";
    self.waterFormID = @"";
    self.birdFormID = @"";
}

@end
