//
//  UserLogin.m
//  GeoDatabaseWetlands
//
//  Created by Jonathan Mason
//
/***********************************************************************
 * UserLogin uses the singleton pattern to create one login instance
 * while the app is running. This instance is used to determine whether
 * the user logs in as a guest or is a valid member who can connect
 * to the database server.
 ***********************************************************************/

#import <Foundation/Foundation.h>
#import "UserLogin.h"
#import "AFNetworking.h"
#import "HomeScreen.h"

@interface UserLogin()

@end

@implementation UserLogin

HomeScreen *home;

// ensures only one instance is made
+ (id)sharedUserLogin {
    static dispatch_once_t onceToken;
    __strong static id sharedObject = nil;
    
    dispatch_once(&onceToken, ^{
        sharedObject = [[self alloc]init];
    });

    return sharedObject;
}

// set the properties on initialization
- (id)init {
    if(self = [super init]){
        _username = @"";
        _password = @"";
        _changedPassword = @"";
        _guestLogin = NO;
        _authenticationCheckComplete = NO;
    }
    return self;
}

// return YES if approved to connect to server
- (BOOL)getNetworkApproval {
    if (self.guestLogin == YES) {
        return NO;
    }
    else
        return YES;
}

/******************************************************************************
 * authenticateUser
 *
 * This method connects to the UW server to authenticate the username and
 * password for login. An observer has been added in the HomeScreen class to
 * check to see if the authenticationCheckComplete property is modified. It
 * will then check to see if the user was approved or not and follow the
 * correct set of actions.
 ******************************************************************************/
- (void)authenticateUser {
    NSDictionary *parameters = @{ @"username" : self.username,
                                  @"password" : self.password};
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];

    AFHTTPRequestOperation *operation = [manager GET:@"http://69.91.198.44:8080/GeodatabaseServer1/Login"
                                          parameters:parameters
                                             success:^(AFHTTPRequestOperation *operation, id responseObject)
    {
         [self setGuestLogin:NO];
         self.serverAuthenticationResponseString = operation.responseString;
         self.authenticationCheckComplete = YES;
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         
         self.serverAuthenticationResponseString = operation.responseString;
     }];

    [operation start];
}

/******************************************************************************
 * changePassword
 *
 * This method connects to the UW server to change the users old password into
 * a new password.
 ******************************************************************************/
- (void)changePassword {
    NSDictionary *parameters = @{ @"username" : self.username,
                                  @"oldPassword" : self.password,
                                  @"newPassword" : self.changedPassword};
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    AFHTTPRequestOperation *operation = [manager GET:@"http://69.91.198.44:8080/GeodatabaseServer/ChangePassword"
                                          parameters:parameters
                                             success:^(AFHTTPRequestOperation *operation, id responseObject)
    {
        NSLog(@"Changed password");
         self.passwordChangeResult = operation.responseString;
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"changePassword: FAIL!");
         NSLog(@"error -> %@",operation.response);
         NSLog(@"error -> %@",operation.responseString);
         self.passwordChangeResult = operation.responseString;
     }];
    
    [operation start];
}


@end