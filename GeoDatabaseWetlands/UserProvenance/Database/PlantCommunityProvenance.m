//
//  PlantCommunityProvenance.m
//  GeoDatabaseWetlands
//
//  Created by Jonathan Mason on 9/23/15.
//  Copyright © 2015 Jonathan Mason. All rights reserved.
//

#import "PlantCommunityProvenance.h"
#import <objc/runtime.h>

@implementation PlantCommunityProvenance

// Specify default values for properties

+ (NSDictionary *)defaultPropertyValues
{
    NSMutableDictionary *defaultValues = [NSMutableDictionary dictionary];
    unsigned int numberOfProperties = 0;
    objc_property_t *propertyArray = class_copyPropertyList([self class], &numberOfProperties);
    for (NSUInteger i = 0; i < numberOfProperties; i++) {
        objc_property_t property = propertyArray[i];
        //aquire name
        NSString *name = [[NSString alloc] initWithUTF8String:property_getName(property)];
        //find the type attribute
        unsigned int numOfAttributes;
        objc_property_attribute_t *propertyAttributes = property_copyAttributeList(property, &numOfAttributes);
        for ( unsigned int ai = 0; ai < numOfAttributes; ai++ ) {
            switch (propertyAttributes[ai].name[0]) {
                    //type of the property
                case 'T':
                {
                    //add the default value
                    NSString *propertyType = [[NSString alloc] initWithUTF8String:propertyAttributes[ai].value];
                    if([propertyType isEqualToString:@"@\"NSString\""]){
                        [defaultValues setObject:[NSString new] forKey:name];
                    }else if([propertyType isEqualToString:@"@\"NSDate\""]){
                        [defaultValues setObject:[NSDate dateWithTimeIntervalSince1970:0] forKey:name];
                    }else if([propertyType isEqualToString:@"@\"NSData\""]){
                        [defaultValues setObject:[NSData new] forKey:name];
                    }
                    break;
                }
                default:
                    break;
            }
        }
    }
    return defaultValues;
}

// Specify properties to ignore (Realm won't persist these)

//+ (NSArray *)ignoredProperties
//{
//    return @[];
//}

@end
