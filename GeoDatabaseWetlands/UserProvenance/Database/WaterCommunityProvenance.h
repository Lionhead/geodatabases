//
//  WaterCommunityProvenance.h
//  GeoDatabaseWetlands
//
//  Created by Jonathan Mason on 9/23/15.
//  Copyright © 2015 Jonathan Mason. All rights reserved.
//

#import <Realm/Realm.h>

@interface WaterCommunityProvenance : RLMObject

@property (strong, nonatomic) NSString *uuid; //Added property uuid
@property (strong, nonatomic) NSString *formID;
@property (strong, nonatomic) NSString *siteID;
@property (strong, nonatomic) NSString *deviceID;
@property (strong, nonatomic) NSString *username;
@property (strong, nonatomic) NSString *filename;
@property (strong, nonatomic) NSString *researchers;
@property (strong, nonatomic) NSString *temperature;
@property (strong, nonatomic) NSString *weather;
@property (strong, nonatomic) NSString *project;
@property (strong, nonatomic) NSString *classNumber;
@property (strong, nonatomic) NSString *instructor;
@property (strong, nonatomic) NSString *dateStarted;
@property (strong, nonatomic) NSString *timeStarted;
@property (strong, nonatomic) NSString *latitudeDecDegrees;
@property (strong, nonatomic) NSString *latitudeDecDegreesMin;
@property (strong, nonatomic) NSString *latitudeDecDegreesMinSec;
@property (strong, nonatomic) NSString *longitudeDecDegrees;
@property (strong, nonatomic) NSString *longitudeDecDegreesMin;
@property (strong, nonatomic) NSString *longitudeDecDegreesMinSec;
@property (strong, nonatomic) NSString *waterTemp;
@property (strong, nonatomic) NSString *suna;
@property (strong, nonatomic) NSString *depth;
@property (strong, nonatomic) NSString *conductivity;
@property (strong, nonatomic) NSString *chlorophyll;
@property (strong, nonatomic) NSString *pH;
@property (strong, nonatomic) NSString *waterFlowVelocity;
@property (strong, nonatomic) NSString *tideStage;
@property (strong, nonatomic) NSString *salinity;
@property (strong, nonatomic) NSString *dissolvedOxygenSat;
@property (strong, nonatomic) NSString *dissolvedOxygenMg;
@property (strong, nonatomic) NSString *secchiDiskExtDepth;
@property (strong, nonatomic) NSString *meanTurbidity;
@property (strong, nonatomic) NSString *blueGreenAlgae;
@property (strong, nonatomic) NSString *waterBodyType;
@property (strong, nonatomic) NSString *notes;

@end

// This protocol enables typed collections. i.e.:
// RLMArray<WaterCommunityProvenance>
RLM_ARRAY_TYPE(WaterCommunityProvenance)
