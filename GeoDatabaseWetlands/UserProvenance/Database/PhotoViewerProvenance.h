//
//  PhotoViewerProvenance.h
//  GeoDatabaseWetlands
//
//  Created by Jonathan Mason on 10/13/15.
//  Copyright © 2015 Jonathan Mason. All rights reserved.
//

#import <Realm/Realm.h>

@interface PhotoViewerProvenance : RLMObject

@property (strong, nonatomic) NSData *image;
@property (strong, nonatomic) NSString *username;
@property (strong, nonatomic) NSString *filename;
@property (strong, nonatomic) NSString *community;
@property (strong, nonatomic) NSString *caption;
@property (strong, nonatomic) NSString *project;
@property (strong, nonatomic) NSString *date;
@property (strong, nonatomic) NSString *time;
@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) NSString *latitude;
@property (strong, nonatomic) NSString *longitude;
@property (strong, nonatomic) NSString *address;
@property (strong, nonatomic) NSString *notes;

@end

// This protocol enables typed collections. i.e.:
// RLMArray<PhotoViewerProvenance>
RLM_ARRAY_TYPE(PhotoViewerProvenance)
