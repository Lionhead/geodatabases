//
//  UserProvenance.h
//  GeoDatabaseWetlands
//
//  Created by Jonathan Mason on 9/23/15.
//  Copyright © 2015 Jonathan Mason. All rights reserved.
//

#import <Realm/Realm.h>

@interface UserProvenance : RLMObject

@property (strong, nonatomic) NSString *username;
@property (strong, nonatomic) NSString *researchers;
@property (strong, nonatomic) NSString *project;
@property (strong, nonatomic) NSString *classNumber;
@property (strong, nonatomic) NSString *instructor;

@end

// This protocol enables typed collections. i.e.:
// RLMArray<UserProvenance>
RLM_ARRAY_TYPE(UserProvenance)
