//
//  PhotoViewerProvenance.m
//  GeoDatabaseWetlands
//
//  Created by Jonathan Mason on 10/13/15.
//  Copyright © 2015 Jonathan Mason. All rights reserved.
//

#import "PhotoViewerProvenance.h"

@implementation PhotoViewerProvenance

// Specify default values for properties

+ (NSDictionary *)defaultPropertyValues
{
    return @{@"username" : @"",
             @"filename" : @"",
             @"community" : @"",
             @"caption" : @"",
             @"project" : @"",
             @"date" : @"",
             @"time" : @"",
             @"title" : @"",
             @"latitude" : @"",
             @"longitude" : @"",
             @"address" : @"",
             @"notes" : @""};
}

// Specify properties to ignore (Realm won't persist these)

//+ (NSArray *)ignoredProperties
//{
//    return @[];
//}

@end
