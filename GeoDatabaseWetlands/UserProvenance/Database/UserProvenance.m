//
//  UserProvenance.m
//  GeoDatabaseWetlands
//
//  Created by Jonathan Mason on 9/23/15.
//  Copyright © 2015 Jonathan Mason. All rights reserved.
//

#import "UserProvenance.h"

@implementation UserProvenance

+ (NSString*)primaryKey{
    return @"username";
}

// Specify default values for properties

+ (NSDictionary *)defaultPropertyValues
{
    return @{@"username" : @"Guest",
             @"researchers" : @"",
             @"project" : @"",
             @"classNumber" : @"",
             @"instructor" : @""};
}

// Specify properties to ignore (Realm won't persist these)

//+ (NSArray *)ignoredProperties
//{
//    return @[];
//}

@end
