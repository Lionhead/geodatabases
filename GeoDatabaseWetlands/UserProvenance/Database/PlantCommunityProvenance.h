//
//  PlantCommunityProvenance.h
//  GeoDatabaseWetlands
//
//  Created by Jonathan Mason on 9/23/15.
//  Copyright © 2015 Jonathan Mason. All rights reserved.
//

#import <Realm/Realm.h>

@interface PlantCommunityProvenance : RLMObject

@property (strong, nonatomic) NSString *formId;
@property (strong, nonatomic) NSString *deviceId;
@property (strong, nonatomic) NSString *username;
@property (strong, nonatomic) NSString *filename;
@property (strong, nonatomic) NSString *researchers;
@property (strong, nonatomic) NSString *temperature;
@property (strong, nonatomic) NSString *weather;
@property (strong, nonatomic) NSString *project;
@property (strong, nonatomic) NSString *classNumber;
@property (strong, nonatomic) NSString *instructor;
@property (strong, nonatomic) NSString *dateStarted;
@property (strong, nonatomic) NSString *timeStarted;
@property (strong, nonatomic) NSString *alderHairGrass;
@property (strong, nonatomic) NSString *alderShrub;
@property (strong, nonatomic) NSString *alderSloughSedge;
@property (strong, nonatomic) NSString *conifer;
@property (strong, nonatomic) NSString *creekBuffer;
@property (strong, nonatomic) NSString *edgeGrassBuffer;
@property (strong, nonatomic) NSString *edgeTreeBuffer;
@property (strong, nonatomic) NSString *logJam;
@property (strong, nonatomic) NSString *meadow;
@property (strong, nonatomic) NSString *pointBar;
@property (strong, nonatomic) NSString *shadyDepression;
@property (strong, nonatomic) NSString *shrubThicket;
@property (strong, nonatomic) NSString *sunnyDepression;

@end

// This protocol enables typed collections. i.e.:
// RLMArray<PlantCommunityProvenance>
RLM_ARRAY_TYPE(PlantCommunityProvenance)
