//
//  AppDelegate.h
//  GeoDatabaseWetlands
//
//  Created by Jonathan Mason on 5/11/14.
//  Copyright (c) 2014 Jonathan Mason. All rights reserved.
//
//  This software is intended soley for the University of Washington Bothell.

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
